﻿namespace FacilShoppingReports
{
    partial class frmConfSeparacao
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.txtEAN = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.txtCEP = new System.Windows.Forms.TextBox();
            this.txtRastreio = new System.Windows.Forms.TextBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.lstItensPedido = new System.Windows.Forms.ListView();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.chkIgnorarData = new System.Windows.Forms.CheckBox();
            this.chkReenvio = new System.Windows.Forms.CheckBox();
            this.lblPlataforma = new System.Windows.Forms.Label();
            this.lblCliente = new System.Windows.Forms.Label();
            this.lblPedido = new System.Windows.Forms.Label();
            this.lblLoja = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.lblPedidoPlataforma = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.lblTotal = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.timer2 = new System.Windows.Forms.Timer(this.components);
            this.chkDigitar = new System.Windows.Forms.CheckBox();
            this.label6 = new System.Windows.Forms.Label();
            this.lblLidos = new System.Windows.Forms.Label();
            this.chkProblema = new System.Windows.Forms.CheckBox();
            this.txtProblema = new System.Windows.Forms.TextBox();
            this.groupBox3.SuspendLayout();
            this.SuspendLayout();
            // 
            // txtEAN
            // 
            this.txtEAN.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtEAN.Location = new System.Drawing.Point(24, 143);
            this.txtEAN.Name = "txtEAN";
            this.txtEAN.Size = new System.Drawing.Size(430, 29);
            this.txtEAN.TabIndex = 2;
            this.txtEAN.TextChanged += new System.EventHandler(this.txtEAN_TextChanged);
            this.txtEAN.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtEAN_KeyPress);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(21, 127);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(29, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "EAN";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(469, 9);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(28, 13);
            this.label3.TabIndex = 7;
            this.label3.Text = "CEP";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(21, 9);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(46, 13);
            this.label4.TabIndex = 6;
            this.label4.Text = "Rastreio";
            // 
            // txtCEP
            // 
            this.txtCEP.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCEP.Location = new System.Drawing.Point(472, 25);
            this.txtCEP.Name = "txtCEP";
            this.txtCEP.Size = new System.Drawing.Size(290, 29);
            this.txtCEP.TabIndex = 1;
            this.txtCEP.TextChanged += new System.EventHandler(this.txtCEP_TextChanged);
            this.txtCEP.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtCEP_KeyPress);
            // 
            // txtRastreio
            // 
            this.txtRastreio.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtRastreio.Location = new System.Drawing.Point(24, 25);
            this.txtRastreio.Name = "txtRastreio";
            this.txtRastreio.Size = new System.Drawing.Size(430, 29);
            this.txtRastreio.TabIndex = 0;
            this.txtRastreio.TextChanged += new System.EventHandler(this.txtRastreio_TextChanged);
            this.txtRastreio.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtRastreio_KeyPress);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.lstItensPedido);
            this.groupBox3.Location = new System.Drawing.Point(24, 178);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(871, 305);
            this.groupBox3.TabIndex = 25;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Itens do Pedido";
            // 
            // lstItensPedido
            // 
            this.lstItensPedido.Location = new System.Drawing.Point(7, 16);
            this.lstItensPedido.Name = "lstItensPedido";
            this.lstItensPedido.Size = new System.Drawing.Size(858, 283);
            this.lstItensPedido.TabIndex = 21;
            this.lstItensPedido.UseCompatibleStateImageBehavior = false;
            this.lstItensPedido.View = System.Windows.Forms.View.Details;
            // 
            // timer1
            // 
            this.timer1.Interval = 500;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // chkIgnorarData
            // 
            this.chkIgnorarData.AutoSize = true;
            this.chkIgnorarData.Location = new System.Drawing.Point(94, 60);
            this.chkIgnorarData.Name = "chkIgnorarData";
            this.chkIgnorarData.Size = new System.Drawing.Size(85, 17);
            this.chkIgnorarData.TabIndex = 39;
            this.chkIgnorarData.Text = "Ignorar Data";
            this.chkIgnorarData.UseVisualStyleBackColor = true;
            // 
            // chkReenvio
            // 
            this.chkReenvio.AutoSize = true;
            this.chkReenvio.Location = new System.Drawing.Point(22, 60);
            this.chkReenvio.Name = "chkReenvio";
            this.chkReenvio.Size = new System.Drawing.Size(66, 17);
            this.chkReenvio.TabIndex = 38;
            this.chkReenvio.Text = "Reenvio";
            this.chkReenvio.UseVisualStyleBackColor = true;
            // 
            // lblPlataforma
            // 
            this.lblPlataforma.AutoSize = true;
            this.lblPlataforma.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPlataforma.Location = new System.Drawing.Point(333, 61);
            this.lblPlataforma.Name = "lblPlataforma";
            this.lblPlataforma.Size = new System.Drawing.Size(51, 16);
            this.lblPlataforma.TabIndex = 42;
            this.lblPlataforma.Text = "label3";
            // 
            // lblCliente
            // 
            this.lblCliente.AutoSize = true;
            this.lblCliente.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCliente.Location = new System.Drawing.Point(18, 91);
            this.lblCliente.Name = "lblCliente";
            this.lblCliente.Size = new System.Drawing.Size(51, 16);
            this.lblCliente.TabIndex = 41;
            this.lblCliente.Text = "label3";
            // 
            // lblPedido
            // 
            this.lblPedido.AutoSize = true;
            this.lblPedido.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPedido.Location = new System.Drawing.Point(212, 61);
            this.lblPedido.Name = "lblPedido";
            this.lblPedido.Size = new System.Drawing.Size(51, 16);
            this.lblPedido.TabIndex = 40;
            this.lblPedido.Text = "label3";
            // 
            // lblLoja
            // 
            this.lblLoja.AutoSize = true;
            this.lblLoja.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblLoja.Location = new System.Drawing.Point(550, 61);
            this.lblLoja.Name = "lblLoja";
            this.lblLoja.Size = new System.Drawing.Size(51, 16);
            this.lblLoja.TabIndex = 43;
            this.lblLoja.Text = "label3";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(490, 91);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(102, 16);
            this.label2.TabIndex = 44;
            this.label2.Text = "N.Plataforma:";
            // 
            // lblPedidoPlataforma
            // 
            this.lblPedidoPlataforma.AutoSize = true;
            this.lblPedidoPlataforma.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPedidoPlataforma.Location = new System.Drawing.Point(594, 91);
            this.lblPedidoPlataforma.Name = "lblPedidoPlataforma";
            this.lblPedidoPlataforma.Size = new System.Drawing.Size(51, 16);
            this.lblPedidoPlataforma.TabIndex = 45;
            this.lblPedidoPlataforma.Text = "label3";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(490, 61);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(49, 16);
            this.label5.TabIndex = 46;
            this.label5.Text = "LOJA:";
            this.label5.Click += new System.EventHandler(this.label5_Click);
            // 
            // lblTotal
            // 
            this.lblTotal.AutoSize = true;
            this.lblTotal.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTotal.Location = new System.Drawing.Point(567, 152);
            this.lblTotal.Name = "lblTotal";
            this.lblTotal.Size = new System.Drawing.Size(16, 16);
            this.lblTotal.TabIndex = 48;
            this.lblTotal.Text = "0";
            this.lblTotal.Click += new System.EventHandler(this.label6_Click);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(460, 152);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(107, 16);
            this.label7.TabIndex = 47;
            this.label7.Text = "Total de Itens:";
            this.label7.Click += new System.EventHandler(this.label7_Click);
            // 
            // timer2
            // 
            this.timer2.Interval = 500;
            this.timer2.Tick += new System.EventHandler(this.timer2_Tick);
            // 
            // chkDigitar
            // 
            this.chkDigitar.AutoSize = true;
            this.chkDigitar.Location = new System.Drawing.Point(768, 35);
            this.chkDigitar.Name = "chkDigitar";
            this.chkDigitar.Size = new System.Drawing.Size(71, 17);
            this.chkDigitar.TabIndex = 49;
            this.chkDigitar.Text = "Digitação";
            this.chkDigitar.UseVisualStyleBackColor = true;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(684, 61);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(105, 17);
            this.label6.TabIndex = 50;
            this.label6.Text = "LIDOS HOJE:";
            // 
            // lblLidos
            // 
            this.lblLidos.AutoSize = true;
            this.lblLidos.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblLidos.Location = new System.Drawing.Point(784, 61);
            this.lblLidos.Name = "lblLidos";
            this.lblLidos.Size = new System.Drawing.Size(17, 17);
            this.lblLidos.TabIndex = 51;
            this.lblLidos.Text = "0";
            // 
            // chkProblema
            // 
            this.chkProblema.AutoSize = true;
            this.chkProblema.Location = new System.Drawing.Point(463, 116);
            this.chkProblema.Name = "chkProblema";
            this.chkProblema.Size = new System.Drawing.Size(70, 17);
            this.chkProblema.TabIndex = 52;
            this.chkProblema.Text = "Problema";
            this.chkProblema.UseVisualStyleBackColor = true;
            this.chkProblema.CheckedChanged += new System.EventHandler(this.chkProblema_CheckedChanged);
            // 
            // txtProblema
            // 
            this.txtProblema.Enabled = false;
            this.txtProblema.Location = new System.Drawing.Point(531, 114);
            this.txtProblema.Name = "txtProblema";
            this.txtProblema.Size = new System.Drawing.Size(359, 20);
            this.txtProblema.TabIndex = 53;
            // 
            // frmConfSeparacao
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(907, 487);
            this.Controls.Add(this.txtProblema);
            this.Controls.Add(this.chkProblema);
            this.Controls.Add(this.lblLidos);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.chkDigitar);
            this.Controls.Add(this.lblTotal);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.lblPedidoPlataforma);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.lblLoja);
            this.Controls.Add(this.lblPlataforma);
            this.Controls.Add(this.lblCliente);
            this.Controls.Add(this.lblPedido);
            this.Controls.Add(this.chkIgnorarData);
            this.Controls.Add(this.chkReenvio);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.txtCEP);
            this.Controls.Add(this.txtRastreio);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtEAN);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmConfSeparacao";
            this.Text = "Conferência de Separação";
            this.Load += new System.EventHandler(this.frmConfSeparacao_Load);
            this.groupBox3.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txtEAN;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtCEP;
        private System.Windows.Forms.TextBox txtRastreio;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.ListView lstItensPedido;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.CheckBox chkIgnorarData;
        private System.Windows.Forms.CheckBox chkReenvio;
        private System.Windows.Forms.Label lblPlataforma;
        private System.Windows.Forms.Label lblCliente;
        private System.Windows.Forms.Label lblPedido;
        private System.Windows.Forms.Label lblLoja;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label lblPedidoPlataforma;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label lblTotal;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Timer timer2;
        private System.Windows.Forms.CheckBox chkDigitar;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label lblLidos;
        private System.Windows.Forms.CheckBox chkProblema;
        private System.Windows.Forms.TextBox txtProblema;
    }
}