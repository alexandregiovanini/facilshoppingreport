﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using MySql.Data.MySqlClient;

namespace FacilShoppingReports
{
    public partial class LOGIN : Form
    {
        public bool blAprovado;
        public int intPermissao;
        public string strOperador;

        public LOGIN()
        {
            InitializeComponent();
        }

        private void Login_Load(object sender, EventArgs e)
        {

        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            blAprovado = false;

            if (txtLogin.Text.Length > 0 && txtSenha.Text.Length > 0)
            {
                
                if (Aprova_Login(txtLogin.Text, global.AcertaSenha(txtLogin.Text, txtSenha.Text)) == true)
                {
                    blAprovado = true;
                    strOperador = txtLogin.Text;

                    
                    //MessageBox.Show("Nome do computador: " + Environment.MachineName);
                    //MessageBox.Show("Nome do Usuario: " + Environment.UserName);
                    string strIp = new WebClient().DownloadString("http://ipinfo.io/ip");

                    GravaAcesso(Environment.MachineName, Environment.UserName, strIp, txtLogin.Text);

                    this.Close();
                }
                else
                {
                    MessageBox.Show("Login Inválido!", "PontoZero Informa");
                }

            }
            else
            {
                MessageBox.Show("Preencher todos os dados!", "PontoZero Informa");
            }           

        }

        private void GravaAcesso(string strNomeMaquinda, string strUsuarioMaquina, string strIp, string strUsuario)
        {
            String query;
            MySqlCommand cmd1;
            MySqlDataReader dataReader;



            query = "SELECT * FROM LOGACESSOS WHERE ";
            query = query + " NomeMaquinda = '" + strNomeMaquinda + "'";
            query = query + " AND UsuarioMaquina = '" + strUsuarioMaquina + "' ";
            query = query + " AND Ip = '" + strIp + "' ";
            query = query + " AND Usuario = '" + strUsuario + "' ";

            global.myDB.CloseConnection();
            if (global.myDB.connection.State == ConnectionState.Closed)
            {
                global.myDB.OpenConnection();
            }

            cmd1 = new MySqlCommand(query, global.myDB.connection);

            dataReader = cmd1.ExecuteReader();

            if (dataReader.Read())
            {
                query = "UPDATE LOGACESSOS SET Data_Acesso = now()  WHERE ";
                query = query + " NomeMaquinda = '" + strNomeMaquinda + "'";
                query = query + " AND UsuarioMaquina = '" + strUsuarioMaquina + "' ";
                query = query + " AND Ip = '" + strIp + "' ";
                query = query + " AND Usuario = '" + strUsuario + "' ";

                global.myDB.CloseConnection();

                if (global.myDB.connection.State == ConnectionState.Closed)
                {
                    global.myDB.OpenConnection();
                }

                cmd1 = new MySqlCommand(query, global.myDB.connection);

                try
                {
                    cmd1.ExecuteNonQuery();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
            else
            {
            
                query = "INSERT INTO LOGACESSOS (NomeMaquinda, UsuarioMaquina, Ip, Usuario, Data_Acesso)";
                query = query + " VALUES ( '";
                query = query + strNomeMaquinda + "', '" + strUsuarioMaquina + "', '" + strIp + "', '" + strUsuario + "', NOW());";


                global.myDB.CloseConnection();

                if (global.myDB.connection.State == ConnectionState.Closed)
                {
                    global.myDB.OpenConnection();
                }

                cmd1 = new MySqlCommand(query, global.myDB.connection);

                try
                {
                    cmd1.ExecuteNonQuery();
                    //MessageBox.Show("Registro gravado com sucesso!", "PontoZero Informa");
                }
                catch (Exception ex)
                {
                    //MessageBox.Show(ex.Message);
                }
                
            }

            dataReader.Close();

        }


        private bool Aprova_Login(string strLogin, string strSenha)
        {
            MySqlCommand cmd1;
            MySqlDataReader dataReaderRastreio;
            bool blAchou;
            string strComplemento = "";

            switch (intPermissao)
            {
                case 0:
                    strComplemento = "AND LIBERAR_LUDOPEDIA = 1;";
                    break;
                case 1:
                    strComplemento = "AND ACESSO = 1;";
                    break;
                default:
                    MessageBox.Show("PERMISSÃO NÃO CONCEDIDA!!", "PontoZero Informa");
                    blAchou = false;
                    return blAchou;
                    break;
            }


            string query = "SELECT * FROM USUARIOS WHERE USUARIO = '" + strLogin + "' AND PASSWORD = '" + strSenha + "' " + strComplemento;

            global.myDB.CloseConnection();
            if (global.myDB.connection.State == ConnectionState.Closed)
            {
                global.myDB.OpenConnection();
            }

            cmd1 = new MySqlCommand(query, global.myDB.connection);

            dataReaderRastreio = cmd1.ExecuteReader();

            if (dataReaderRastreio.Read())
            {
                blAchou = true;
            }
            else
            {
                blAchou = false;
            }

            dataReaderRastreio.Close();

            return blAchou;
        }

        private void cmdNovo_Click(object sender, EventArgs e)
        {
            blAprovado = false;

            if (txtLogin.Text.Length > 0 && txtSenha.Text.Length > 0)
            {
                GravaUsuario(txtLogin.Text, global.AcertaSenha(txtLogin.Text, txtSenha.Text));
            }
            else
            {
                MessageBox.Show("Preencher todos os dados!", "PontoZero Informa");
            }     
        }

        private static void GravaUsuario(string strUsuario, string strSenha)
        {

            String query;

            query = "INSERT INTO USUARIOS (USUARIO, PASSWORD)";
            query = query + " VALUES ( '";
            query = query + strUsuario + "', '" + strSenha + "');";
            

            global.myDB.CloseConnection();

            if (global.myDB.connection.State == ConnectionState.Closed)
            {
                global.myDB.OpenConnection();
            }

            MySqlCommand cmd1 = new MySqlCommand(query, global.myDB.connection);

            try
            {
                cmd1.ExecuteNonQuery();
                MessageBox.Show("Registro gravado com sucesso!", "PontoZero Informa");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void btnSair_Click(object sender, EventArgs e)
        {
            blAprovado = false;
            this.Close();
        }
    }
}
