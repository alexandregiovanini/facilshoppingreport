CREATE TABLE agentes (id INT NOT NULL PRIMARY KEY AUTO_INCREMENT, 
						 data date,
						 fantasia CHAR(75), 
                         razao CHAR(75), 
                         endereco CHAR(75), 
                         numero CHAR(6), 
                         complemento CHAR(30),
                         bairro CHAR(75), 
                         cidade CHAR(75), 
                         estado CHAR(2), 
                         cep CHAR(9), 
						 fornecedor BIT,
                         fabricante BIT,
                         cnpj CHAR(18), 
                         telefone CHAR(15),
                         contato1 CHAR(75), 
                         telefone1 CHAR(15), 
                         contato2 CHAR(75), 
                         telefone2 CHAR(15),
                         observacao CHAR(100));
                         
                         
                        select * from agentes;
                        drop table agentes;
                         
                         
