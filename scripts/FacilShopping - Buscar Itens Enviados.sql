select IC.rastreio, IC.plataforma, VENDA.increment_id, VENDA_ITEM.sku, VENDA_ITEM.NAME 
FROM sales_flat_order VENDA
INNER JOIN sales_flat_order_item VENDA_ITEM
ON VENDA_ITEM.order_id = VENDA.entity_id
INNER JOIN ITENS_ENVIO_CORREIOS IC
ON IC.increment_id = VENDA.increment_id
and IC.data > '2019-03-18'
ORDER BY VENDA.increment_id;