﻿using Microsoft.Reporting.WinForms;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FacilShoppingReports
{
    public partial class frmRel_CorreiosMLPlace : Form
    {
        public int intNumeroEnvioMLPlace;
        public int intQuantidadeMLPlace;

        public frmRel_CorreiosMLPlace()
        {
            InitializeComponent();
        }

        private void frmRel_CorreiosMLPlace_Load(object sender, EventArgs e)
        {
            System.Drawing.Printing.PageSettings ps = new System.Drawing.Printing.PageSettings();
            ps.Landscape = false;
            ps.PaperSize = new System.Drawing.Printing.PaperSize("A4", 827, 1170);
            ps.PaperSize.RawKind = (int)System.Drawing.Printing.PaperKind.A4;

            ps.Margins.Top = 5;
            ps.Margins.Bottom = 5;
            ps.Margins.Left = 40;
            ps.Margins.Right = 5;

            reportViewer1.SetPageSettings(ps);

            reportViewer1.ProcessingMode = Microsoft.Reporting.WinForms.ProcessingMode.Local;

            LocalReport localReport = reportViewer1.LocalReport;

            localReport.ReportPath = "mercadoplace.rdlc";


            ReportParameter[] parametros = { new ReportParameter("teste1", intNumeroEnvioMLPlace.ToString()), new ReportParameter("quantidade", intQuantidadeMLPlace.ToString()), new ReportParameter("data", "DATA: " + DateTime.Today.ToShortDateString()), new ReportParameter("hora", "HORA:" + DateTime.Now.ToShortTimeString()) };

            reportViewer1.LocalReport.SetParameters(parametros);

            reportViewer1.RefreshReport();
        }
    }
}
