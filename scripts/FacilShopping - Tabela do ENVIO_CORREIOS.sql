CREATE TABLE ENVIO_CORREIOS (id INT NOT NULL PRIMARY KEY AUTO_INCREMENT, 
							data_criacao datetime NOT NULL,
                            data_impressao datetime NULL,
							usuario_criacao CHAR(30) NULL,
                            mercadolivre bit default 0,
                            magalu bit default 0,
                            b2w bit default 0,
                            mlplace bit default 0,
                            shopee bit default 0,
                            amazon bit default 0,
							usuario_impressao CHAR(30) NULL,
                            enviadoemail bit NOT NULL DEFAULT 0);
                         
CREATE TABLE ITENS_ENVIO_CORREIOS (id INT NOT NULL PRIMARY KEY AUTO_INCREMENT, 
							data datetime NOT NULL,
                            id_ENVIO_CORREIOS INT NOT NULL,
							usuario CHAR(30) NULL,
                            rastreio CHAR(100) NULL,
                            sedex CHAR(100) NULL,
                            increment_id CHAR(30) NULL,
                            plataforma CHAR(60) NULL,
                            enviadoemail bit NOT NULL DEFAULT 0);
                            
alter table ENVIO_CORREIOS add COLUMN amazon bit not NULL default 0;
                            
SET SQL_SAFE_UPDATES=1;                  
select * from ENVIO_CORREIOS where sequoia = true order by data_criacao desc;

select * from ITENS_ENVIO_CORREIOS where id_ENVIO_CORREIOS = 4027;
update ENVIO_CORREIOS set data_impressao = "" where id = 4027;
alter table ENVIO_CORREIOS add column mlplace bit default 0;
alter table ENVIO_CORREIOS add column b2w bit default 0;

drop table ENVIO_CORREIOS2;
                         