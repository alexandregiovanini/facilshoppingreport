﻿namespace FacilShoppingReports
{
    partial class frmRelatorioCorreio
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lstCorreios = new System.Windows.Forms.ListView();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.btnSair = new System.Windows.Forms.Button();
            this.btnLimpar = new System.Windows.Forms.Button();
            this.btPesquisar = new System.Windows.Forms.Button();
            this.lblRegistros = new System.Windows.Forms.Label();
            this.frameVendas = new System.Windows.Forms.GroupBox();
            this.lblBusca = new System.Windows.Forms.Label();
            this.txtProduto = new System.Windows.Forms.TextBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.optCliente = new System.Windows.Forms.RadioButton();
            this.optRastreio = new System.Windows.Forms.RadioButton();
            this.optPedido = new System.Windows.Forms.RadioButton();
            this.mskDtFinal = new System.Windows.Forms.MaskedTextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.mskDtInicial = new System.Windows.Forms.MaskedTextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.optPlataforma = new System.Windows.Forms.RadioButton();
            this.groupBox2.SuspendLayout();
            this.frameVendas.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // lstCorreios
            // 
            this.lstCorreios.HideSelection = false;
            this.lstCorreios.Location = new System.Drawing.Point(9, 81);
            this.lstCorreios.Name = "lstCorreios";
            this.lstCorreios.Size = new System.Drawing.Size(1016, 428);
            this.lstCorreios.TabIndex = 18;
            this.lstCorreios.UseCompatibleStateImageBehavior = false;
            this.lstCorreios.View = System.Windows.Forms.View.Details;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.btnSair);
            this.groupBox2.Controls.Add(this.btnLimpar);
            this.groupBox2.Controls.Add(this.btPesquisar);
            this.groupBox2.Location = new System.Drawing.Point(1031, 44);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(92, 148);
            this.groupBox2.TabIndex = 19;
            this.groupBox2.TabStop = false;
            // 
            // btnSair
            // 
            this.btnSair.Image = global::FacilShoppingReports.Properties.Resources.exit_icon;
            this.btnSair.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnSair.Location = new System.Drawing.Point(2, 104);
            this.btnSair.Name = "btnSair";
            this.btnSair.Size = new System.Drawing.Size(85, 40);
            this.btnSair.TabIndex = 12;
            this.btnSair.Text = "Sair";
            this.btnSair.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnSair.UseVisualStyleBackColor = true;
            this.btnSair.Click += new System.EventHandler(this.btnSair_Click);
            // 
            // btnLimpar
            // 
            this.btnLimpar.Image = global::FacilShoppingReports.Properties.Resources.delete_icon;
            this.btnLimpar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnLimpar.Location = new System.Drawing.Point(2, 57);
            this.btnLimpar.Name = "btnLimpar";
            this.btnLimpar.Size = new System.Drawing.Size(85, 40);
            this.btnLimpar.TabIndex = 11;
            this.btnLimpar.Text = "Limpar";
            this.btnLimpar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnLimpar.UseVisualStyleBackColor = true;
            this.btnLimpar.Click += new System.EventHandler(this.btnLimpar_Click);
            // 
            // btPesquisar
            // 
            this.btPesquisar.Image = global::FacilShoppingReports.Properties.Resources.search_icon1;
            this.btPesquisar.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
            this.btPesquisar.Location = new System.Drawing.Point(2, 11);
            this.btPesquisar.Name = "btPesquisar";
            this.btPesquisar.Size = new System.Drawing.Size(85, 40);
            this.btPesquisar.TabIndex = 10;
            this.btPesquisar.Text = "Pesquisar";
            this.btPesquisar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btPesquisar.UseVisualStyleBackColor = true;
            this.btPesquisar.Click += new System.EventHandler(this.btPesquisar_Click);
            // 
            // lblRegistros
            // 
            this.lblRegistros.AutoSize = true;
            this.lblRegistros.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRegistros.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.lblRegistros.Location = new System.Drawing.Point(748, 25);
            this.lblRegistros.Name = "lblRegistros";
            this.lblRegistros.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.lblRegistros.Size = new System.Drawing.Size(236, 24);
            this.lblRegistros.TabIndex = 20;
            this.lblRegistros.Text = "TOTAL DE REGISTROS: 0";
            // 
            // frameVendas
            // 
            this.frameVendas.Controls.Add(this.lblBusca);
            this.frameVendas.Controls.Add(this.lblRegistros);
            this.frameVendas.Controls.Add(this.txtProduto);
            this.frameVendas.Controls.Add(this.groupBox1);
            this.frameVendas.Controls.Add(this.mskDtFinal);
            this.frameVendas.Controls.Add(this.label7);
            this.frameVendas.Controls.Add(this.mskDtInicial);
            this.frameVendas.Controls.Add(this.label4);
            this.frameVendas.Location = new System.Drawing.Point(7, 1);
            this.frameVendas.Name = "frameVendas";
            this.frameVendas.Size = new System.Drawing.Size(1020, 73);
            this.frameVendas.TabIndex = 21;
            this.frameVendas.TabStop = false;
            this.frameVendas.Text = "Vendas";
            // 
            // lblBusca
            // 
            this.lblBusca.AutoSize = true;
            this.lblBusca.Location = new System.Drawing.Point(464, 33);
            this.lblBusca.Name = "lblBusca";
            this.lblBusca.Size = new System.Drawing.Size(50, 13);
            this.lblBusca.TabIndex = 46;
            this.lblBusca.Text = "Pesquisa";
            // 
            // txtProduto
            // 
            this.txtProduto.Location = new System.Drawing.Point(517, 28);
            this.txtProduto.Name = "txtProduto";
            this.txtProduto.Size = new System.Drawing.Size(232, 20);
            this.txtProduto.TabIndex = 45;
            this.txtProduto.TextChanged += new System.EventHandler(this.txtProduto_TextChanged);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.optPlataforma);
            this.groupBox1.Controls.Add(this.optCliente);
            this.groupBox1.Controls.Add(this.optRastreio);
            this.groupBox1.Controls.Add(this.optPedido);
            this.groupBox1.Location = new System.Drawing.Point(172, 15);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(272, 39);
            this.groupBox1.TabIndex = 40;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Pesquisar por";
            // 
            // optCliente
            // 
            this.optCliente.AutoSize = true;
            this.optCliente.Location = new System.Drawing.Point(140, 15);
            this.optCliente.Name = "optCliente";
            this.optCliente.Size = new System.Drawing.Size(57, 17);
            this.optCliente.TabIndex = 16;
            this.optCliente.Text = "Cliente";
            this.optCliente.UseVisualStyleBackColor = true;
            // 
            // optRastreio
            // 
            this.optRastreio.AutoSize = true;
            this.optRastreio.Location = new System.Drawing.Point(75, 15);
            this.optRastreio.Name = "optRastreio";
            this.optRastreio.Size = new System.Drawing.Size(64, 17);
            this.optRastreio.TabIndex = 15;
            this.optRastreio.Text = "Rastreio";
            this.optRastreio.UseVisualStyleBackColor = true;
            // 
            // optPedido
            // 
            this.optPedido.AutoSize = true;
            this.optPedido.Checked = true;
            this.optPedido.Location = new System.Drawing.Point(9, 15);
            this.optPedido.Name = "optPedido";
            this.optPedido.Size = new System.Drawing.Size(58, 17);
            this.optPedido.TabIndex = 14;
            this.optPedido.TabStop = true;
            this.optPedido.Text = "Pedido";
            this.optPedido.UseVisualStyleBackColor = true;
            // 
            // mskDtFinal
            // 
            this.mskDtFinal.Location = new System.Drawing.Point(75, 38);
            this.mskDtFinal.Mask = "00/00/0000";
            this.mskDtFinal.Name = "mskDtFinal";
            this.mskDtFinal.Size = new System.Drawing.Size(91, 20);
            this.mskDtFinal.TabIndex = 39;
            this.mskDtFinal.ValidatingType = typeof(System.DateTime);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(16, 43);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(55, 13);
            this.label7.TabIndex = 38;
            this.label7.Text = "Data Final";
            // 
            // mskDtInicial
            // 
            this.mskDtInicial.Location = new System.Drawing.Point(75, 14);
            this.mskDtInicial.Mask = "00/00/0000";
            this.mskDtInicial.Name = "mskDtInicial";
            this.mskDtInicial.Size = new System.Drawing.Size(91, 20);
            this.mskDtInicial.TabIndex = 37;
            this.mskDtInicial.ValidatingType = typeof(System.DateTime);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(11, 18);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(60, 13);
            this.label4.TabIndex = 36;
            this.label4.Text = "Data Inicial";
            // 
            // optPlataforma
            // 
            this.optPlataforma.AutoSize = true;
            this.optPlataforma.Location = new System.Drawing.Point(201, 15);
            this.optPlataforma.Name = "optPlataforma";
            this.optPlataforma.Size = new System.Drawing.Size(75, 17);
            this.optPlataforma.TabIndex = 17;
            this.optPlataforma.Text = "Plataforma";
            this.optPlataforma.UseVisualStyleBackColor = true;
            // 
            // frmRelatorioCorreio
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1139, 517);
            this.ControlBox = false;
            this.Controls.Add(this.frameVendas);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.lstCorreios);
            this.ImeMode = System.Windows.Forms.ImeMode.Close;
            this.Name = "frmRelatorioCorreio";
            this.Text = "Relatório dos Correios";
            this.Load += new System.EventHandler(this.frmRelatorioCorreio_Load);
            this.groupBox2.ResumeLayout(false);
            this.frameVendas.ResumeLayout(false);
            this.frameVendas.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ListView lstCorreios;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button btnSair;
        private System.Windows.Forms.Button btnLimpar;
        private System.Windows.Forms.Button btPesquisar;
        private System.Windows.Forms.Label lblRegistros;
        private System.Windows.Forms.GroupBox frameVendas;
        private System.Windows.Forms.Label lblBusca;
        private System.Windows.Forms.TextBox txtProduto;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.RadioButton optRastreio;
        private System.Windows.Forms.RadioButton optPedido;
        private System.Windows.Forms.MaskedTextBox mskDtFinal;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.MaskedTextBox mskDtInicial;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.RadioButton optCliente;
        private System.Windows.Forms.RadioButton optPlataforma;
    }
}