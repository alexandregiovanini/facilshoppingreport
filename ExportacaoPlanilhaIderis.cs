﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;


using System.IO;
using System.Runtime.InteropServices;
//using Excel = Microsoft.Office.Interop.Excel;       //microsoft Excel 14 object in references-> COM tab

namespace FacilShoppingReports
{


    public partial class ExportacaoPlanilhaIderis : Form
    {
        public ExportacaoPlanilhaIderis()
        {
            InitializeComponent();
        }

        private void btnSair_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void CarregaList()
        {
            

            lstEstoque.CheckBoxes = false;
            lstEstoque.Columns.Add("ID", 40);
            lstEstoque.Columns.Add("SKU", 160);
            lstEstoque.Columns.Add("DESCRICAO", 350);
            lstEstoque.Columns.Add("VARIACAO", 150);
            lstEstoque.Columns.Add("IDERIS", 60);
            lstEstoque.Columns.Add("MAGENTO", 75);
                        
        }



        private void  ExibeQuantidade()
        {
            string strListaTemp = " SKU; MAGENTO; IDERIS; DESCRICAO\r\n";


            if (lstEstoque.Items.Count > 0)
            {
                try
                {
                    foreach (ListViewItem item in lstEstoque.Items)
                    {
                        item.SubItems[5].Text = BuscaQuantidade(item.SubItems[1].Text);
                        item.BackColor = Color.White;
                        if (item.SubItems[5].Text != item.SubItems[4].Text && (Convert.ToDecimal(item.SubItems[5].Text) > 0   || Convert.ToDecimal(item.SubItems[4].Text) > 0 ))
                        {
                            item.BackColor = Color.Red;

                            strListaTemp = strListaTemp + item.SubItems[1].Text + "   ;    " + item.SubItems[5].Text + "   ;    " + item.SubItems[4].Text + "   ;    " + item.SubItems[2].Text + "\r\n"; 
                        }
                    }

                }
                catch (Exception ex)
                {
                    MessageBox.Show("Erro : " + ex.Message, "PontoZero Informa");
                }
            }

           // MessageBox.Show(strListaTemp);

            using (var writer = new StreamWriter("estoque.csv"))
            {
                    // Escreve uma string formatada no arquivo
                    writer.Write("{0:0.0} ", strListaTemp);             
            }
        }


        private String BuscaQuantidade(String strSKU)
        {
            MySqlCommand cmd;
            MySqlDataReader dataReader;
            string strRetorno;
            bool blAchou;

            blAchou = false;
            strRetorno = "0";

            string query = "select qty from cataloginventory_stock_item inner join catalog_product_entity on cataloginventory_stock_item.product_id = catalog_product_entity.entity_id where sku = '" + strSKU.Replace("'", "") + "'; ";

            try
            {
                global.myDB.CloseConnection();

                if (global.myDB.connection.State == ConnectionState.Closed)
                {
                    global.myDB.OpenConnection();
                }

                cmd = new MySqlCommand(query, global.myDB.connection);

                dataReader = cmd.ExecuteReader();

                while (dataReader.Read())
                {
                    blAchou = true;

                    if (Double.Parse(dataReader["qty"].ToString()) < 0)
                    {
                        strRetorno = "0";
                    }
                    else
                    {
                        strRetorno = Double.Parse(dataReader["qty"].ToString()).ToString();
                    }

                }

                dataReader.Close();

            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro : " + ex.Message, "PontoZero Informa");
            }

            if (blAchou == false)
            {
                strRetorno = BuscaQuantidadeCombo(strSKU);
            }

            return strRetorno;
        }

        private void CarregaExcel()
        {
            //String strColuna1, strColuna2, strColuna3, strColuna4, strColuna5, strCaminho;

            //var fileContent = string.Empty;
            //var filePath = string.Empty;

            //using (OpenFileDialog openFileDialog = new OpenFileDialog())
            //{
            //    openFileDialog.InitialDirectory = "c:\\";
            //    //openFileDialog.Filter = "txt files (*.txt)|*.txt|All files (*.*)|*.*";
            //    openFileDialog.FilterIndex = 2;
            //    openFileDialog.RestoreDirectory = true;

            //    if (openFileDialog.ShowDialog() == DialogResult.OK)
            //    {
            //        //Get the path of specified file
            //        filePath = openFileDialog.FileName;

            //        //Read the contents of the file into a stream
            //        var fileStream = openFileDialog.OpenFile();

            //        using (StreamReader reader = new StreamReader(fileStream))
            //        {
            //            fileContent = reader.ReadToEnd();
            //        }
            //    }
            //}

            ////MessageBox.Show(fileContent, "File Content at path: " + filePath, MessageBoxButtons.OK);


            //if(filePath !="")
            //{
            //    //Create COM Objects. Create a COM object for everything that is referenced
            //    Excel.Application xlApp = new Excel.Application();
            //    Excel.Workbook xlWorkbook = xlApp.Workbooks.Open(@filePath);
            //    Excel._Worksheet xlWorksheet = xlWorkbook.Sheets[1];
            //    Excel.Range xlRange = xlWorksheet.UsedRange;

            //    int rowCount = xlRange.Rows.Count;
            //    int colCount = xlRange.Columns.Count;

            //    //iterate over the rows and columns and print to the console as it appears in the file
            //    //excel is not zero based!!

            //    ListViewItem item;
            //    for (int i = 2; i <= rowCount; i++)
            //    {
            //        strColuna1 = "";
            //        strColuna2 = "";
            //        strColuna3 = "";
            //        strColuna4 = "";
            //        strColuna5 = "";

            //        strColuna1 = xlRange.Cells[i, 1].Value2.ToString();
            //        strColuna2 = xlRange.Cells[i, 2].Value2.ToString();
            //        //if(xlRange.Cells[i, 3] != null)
            //        if (xlRange.Cells[i, 3] != null && xlRange.Cells[i, 3].Value2 != null)
            //        {
            //            strColuna3 = xlRange.Cells[i, 3].Value2.ToString();
            //        }

            //        strColuna4 = xlRange.Cells[i, 4].Value2.ToString();
            //        strColuna5 = xlRange.Cells[i, 5].Value2.ToString();
            //        //xlRange.Cells[i, j].Value2.ToString() 
            //        item = new ListViewItem(new[] { strColuna1, strColuna2, strColuna3, strColuna4, strColuna5, "" });

            //        lstEstoque.Items.Add(item);
            //    }

            //    //cleanup
            //    GC.Collect();
            //    GC.WaitForPendingFinalizers();

            //    //rule of thumb for releasing com objects:
            //    //  never use two dots, all COM objects must be referenced and released individually
            //    //  ex: [somthing].[something].[something] is bad

            //    //release com objects to fully kill excel process from running in the background
            //    Marshal.ReleaseComObject(xlRange);
            //    Marshal.ReleaseComObject(xlWorksheet);

            //    //close and release
            //    xlWorkbook.Close();
            //    Marshal.ReleaseComObject(xlWorkbook);

            //    //quit and release
            //    xlApp.Quit();
            //    Marshal.ReleaseComObject(xlApp);
            //}


        }

        private String BuscaQuantidadeCombo(String strSKU)
        {
            MySqlCommand cmd;
            MySqlDataReader dataReader;
            string strRetorno;

            strRetorno = "0";

            string query = "SELECT (select qty from cataloginventory_stock_item ";
            query = query + " inner join catalog_product_entity on cataloginventory_stock_item.product_id = catalog_product_entity.entity_id ";
            query = query + " where sku = ITEM_COMBO.SKUITEM) estoque ";
            query = query + " FROM ITEM_COMBO ";
            query = query + " inner join COMBO ";
            query = query + " on ITEM_COMBO.ID_COMBO = COMBO.id ";
            query = query + " where SKUCOMBO = '" + strSKU.Replace("'", "") + "' ";
            query = query + " order by 1 limit 1";

            try
            {
                global.myDB.CloseConnection();

                if (global.myDB.connection.State == ConnectionState.Closed)
                {
                    global.myDB.OpenConnection();
                }

                cmd = new MySqlCommand(query, global.myDB.connection);

                dataReader = cmd.ExecuteReader();

                while (dataReader.Read())
                {
                    if (Double.Parse(dataReader["estoque"].ToString()) < 0)
                    {
                        strRetorno = "0";
                    }
                    else
                    {
                        strRetorno = Double.Parse(dataReader["estoque"].ToString()).ToString();
                    }
                }

                dataReader.Close();

            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro : " + ex.Message, "PontoZero Informa");
            }

            return strRetorno;
        }


        private void GerarXLSOffice()
        {

            //Microsoft.Office.Interop.Excel.Application XcelApp = new Microsoft.Office.Interop.Excel.Application();
            //int l = 1;

            //if (lstEstoque.Items.Count > 0)
            //{
            //    try
            //    {
            //        XcelApp.Application.Workbooks.Add(Type.Missing);



            //        XcelApp.Cells[1, 1] = "ID";
            //        XcelApp.Cells[1, 2] = "SKU";
            //        XcelApp.Cells[1, 3] = "DESCRICAO";
            //        XcelApp.Cells[1, 4] = "VARIACAO";
            //        XcelApp.Cells[1, 5] = "QUANTIDADE";

            //        foreach (ListViewItem item in lstEstoque.Items)
            //        {

            //            l = l + 1;


            //            for (int j = 1; j < lstEstoque.Columns.Count + 1; j++)
            //            {
            //                if (j < 5)
            //                {
            //                    XcelApp.Cells[l, j] = item.SubItems[j - 1].Text;
            //                }
            //                else if (j > 5)
            //                {
            //                    XcelApp.Cells[l, j - 1] = item.SubItems[j - 1].Text;
            //                }
            //            }

            //        }

            //        XcelApp.Columns.AutoFit();
            //        //
            //        XcelApp.Visible = true;
            //    }
            //    catch (Exception ex)
            //    {
            //        MessageBox.Show("Erro : " + ex.Message, "PontoZero Informa");
            //        XcelApp.Quit();
            //    }
            //}
        }
        

        private void ExportacaoPlanilhaIderis_Load(object sender, EventArgs e)
        {
            CarregaList();
        }

        private void btPesquisar_Click(object sender, EventArgs e)
        {
            lstEstoque.Visible = false;
            lblAviso.Text = "AGUARDE PESQUISANDO ESTOQUE NA BASE...";
            lblAviso.Visible = true;
            lblAviso.Refresh();

            ExibeQuantidade();

            lblAviso.Visible = false;
            lstEstoque.Visible = true;


        }

        private void btnExportar_Click(object sender, EventArgs e)
        {
            lstEstoque.Visible = false;
            lblAviso.Text = "AGUARDE GERANDO PLANILHA...";
            lblAviso.Visible = true;
            lblAviso.Refresh();

            GerarXLSOffice();

            lblAviso.Visible = false;
            lstEstoque.Visible = true;
        }

        private void cmdImportar_Click(object sender, EventArgs e)
        {
            LimpaList();
            CarregaExcel();
        }

        private void LimpaList()
        {
            lstEstoque.Items.Clear();
        }

        private void cmdErros_Click(object sender, EventArgs e)
        {
            System.Diagnostics.Process.Start(@"estoque.csv");
        }
    }
}
