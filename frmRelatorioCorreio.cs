﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FacilShoppingReports
{
    public partial class frmRelatorioCorreio : Form
    {
        public frmRelatorioCorreio()
        {
            InitializeComponent();
            Limpar();
        }

        private void btnSair_Click(object sender, EventArgs e)
        {
            //dataReader.Close();
            this.Close();
        }

        private void btPesquisar_Click(object sender, EventArgs e)
        {
            Pesquisa();
        }


        private void Pesquisa()
        {
            MySqlCommand cmd;
            MySqlDataReader dataReader;
            string query;
            int i;

            lstCorreios.Columns.Clear();
            lstCorreios.Items.Clear();

            //DateTime dateInicial = DateTime.Today;
            //DateTime dateFinal = DateTime.Today;

            query = "select PL.dt_pedido, PL.dt_integracao, IC.data, EC.data_impressao, IC.rastreio, IC.plataforma, PL.id_pedido PEDIDO, PL.cod_pedido PEDIDO_PLATAFORMA, PL.nome ";
            query = query + " from ENVIO_CORREIOS EC ";
            query = query + " inner JOIN ITENS_ENVIO_CORREIOS IC ";
            query = query + " ON EC.id = IC.id_ENVIO_CORREIOS ";

            query = query + " left JOIN PEDIDOS_INTEGRADOS PL ";
            query = query + " ON IC.increment_id = PL.id_pedido ";
            //query = query + " inner JOIN ITENS_PEDIDOS_INTEGRADOS IP ";
            //query = query + " ON PL.id_pedido = IP.id_pedido ";
            query = query + " and ((PL.dt_pedido between '";

            query = query + System.DateTime.Parse(mskDtInicial.Text).ToString("yyyy-MM-dd") + "' AND '" + System.DateTime.Parse(mskDtFinal.Text).ToString("yyyy-MM-dd") + " 23:59:59')  or IC.increment_id = '00001') ";



            query = query + " WHERE  1 = 1 ";

           

            if (txtProduto.Text.Length != 0)
            {
                if(optRastreio.Checked == true)
                {
                    query = query + " AND (IC.rastreio like '%" + txtProduto.Text + "%' )";
                }
                else if(optCliente.Checked == true)
                {
                    query = query + " AND (PL.nome like '%" + txtProduto.Text + "%' )";
                }
                else if(optPedido.Checked == true)
                {
                    query = query + " AND (PL.id_pedido like '%" + txtProduto.Text + "%'  OR PL.cod_pedido like '%" + txtProduto.Text + "%')";
                }
                else if (optPlataforma.Checked == true)
                {
                    query = query + " AND (IC.plataforma like '%" + txtProduto.Text + "%' )";
                }
            }

            query = query + " UNION ";

            query = query + " select PL.created_at dt_pedido, PL.updated_at dt_integracao, IC.data, EC.data_impressao, IC.rastreio, IC.plataforma, PL.increment_id PEDIDO, PL.canal_id PEDIDO_PLATAFORMA, UCASE(CONCAT(PL.customer_firstname, ' ', PL.customer_lastname)) nome ";
            query = query + " from ENVIO_CORREIOS EC ";
            query = query + " left JOIN ITENS_ENVIO_CORREIOS IC ";
            query = query + " ON EC.id = IC.id_ENVIO_CORREIOS ";

            query = query + " inner JOIN sales_flat_order PL ";
            query = query + " ON IC.increment_id = PL.increment_id ";
            //query = query + " left JOIN sales_flat_order_item IP ";
            //query = query + " ON IP.order_id = PL.entity_id ";
            
            query = query + " WHERE (PL.created_at between '";
            
            query = query + System.DateTime.Parse(mskDtInicial.Text).ToString("yyyy-MM-dd") + "' AND '" + System.DateTime.Parse(mskDtFinal.Text).ToString("yyyy-MM-dd") + " 23:59:59') ";

            if (txtProduto.Text.Length != 0)
            {
                if (optRastreio.Checked == true)
                {
                    query = query + " AND (IC.rastreio like '%" + txtProduto.Text + "%')";
                }
                else if (optCliente.Checked == true)
                {
                    query = query + " AND (PL.customer_firstname like '%" + txtProduto.Text + "%' )";
                }
                else if (optPedido.Checked == true)
                {
                    query = query + " AND (PL.increment_id like '%" + txtProduto.Text + "%')";
                }
                else if (optPlataforma.Checked == true)
                {
                    query = query + " AND (IC.plataforma like '%" + txtProduto.Text + "%' )";
                }
            }
            
            query = query + " ORDER BY 4, 7;";

            if (global.myDB.connection.State == ConnectionState.Closed)
            {
                global.myDB.OpenConnection();
            }

            cmd = new MySqlCommand(query, global.myDB.connection);

            try
            {
                dataReader = cmd.ExecuteReader();
            }
            catch (Exception)
            {
                global.myDB.OpenConnection();
                cmd = new MySqlCommand(query, global.myDB.connection);
                dataReader = cmd.ExecuteReader();
            }
            
            lstCorreios.CheckBoxes = false;

            lstCorreios.Columns.Add("DT PEDIDO", 120);
            lstCorreios.Columns.Add("DT STATUS", 120);
            lstCorreios.Columns.Add("DT LEITURA", 120);
            lstCorreios.Columns.Add("DT ENVIO", 120);
            lstCorreios.Columns.Add("RASTREIO", 100);
            lstCorreios.Columns.Add("PLATAFORMA", 100);
            lstCorreios.Columns.Add("PEDIDO", 90);
            lstCorreios.Columns.Add("PEDIDO PLATAFORMA", 90);
            //lstCorreios.Columns.Add("DESCRICAO", 190);
            //lstCorreios.Columns.Add("QUANTIDADE", 110);
            //lstCorreios.Columns.Add("SKU", 150);
            lstCorreios.Columns.Add("CLIENTE", 150);

            i = 0;

            while (dataReader.Read())
            {
                i = i + 1;


                ListViewItem item;

                lblRegistros.Text = "TOTAL DE REGISTROS: " + Convert.ToString(i);                                                                                                                                                                                                                                                   

                item = new ListViewItem(new[] { dataReader["dt_pedido"].ToString(), dataReader["dt_integracao"].ToString(), dataReader["data"].ToString(), dataReader["data_impressao"].ToString(), dataReader["rastreio"].ToString(), dataReader["plataforma"].ToString(), dataReader["PEDIDO"].ToString(), dataReader["PEDIDO_PLATAFORMA"].ToString(), dataReader["NOME"].ToString() });

                lstCorreios.Items.Add(item);
            }

            dataReader.Close();
        }

        private void Limpar()
        {
            txtProduto.Text = "";
            lblRegistros.Text = "TOTAL DE REGISTROS: 0";
            
            mskDtFinal.Text = DateTime.Now.ToString("dd-MM-yyyy");
            mskDtInicial.Text = DateTime.Now.ToString("dd-MM-yyyy");

            lstCorreios.Columns.Clear();
            lstCorreios.Items.Clear();

        }

        private void btnLimpar_Click(object sender, EventArgs e)
        {
            Limpar();
        }

        private void frmRelatorioCorreio_Load(object sender, EventArgs e)
        {

        }

        private void txtProduto_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
