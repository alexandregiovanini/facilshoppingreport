﻿
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

//using Word = Microsoft.Office.Interop.Word;
using System.Drawing.Printing;

namespace FacilShoppingReports
{
    public partial class frmEtiquetaNF : Form
    {
        MySqlDataReader dataReader;
        bool blTempoDig;
        private DSBarCode.BarCodeCtrl userControl11;
      
        public frmEtiquetaNF()
        {
            InitializeComponent();
            ApagarImagens();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            RastreioDigitado();
        }


        private void RastreioDigitado()
        {
            String strDigitado;
            bool blContinua = false;

            strDigitado = txtRastreio.Text;

            if (txtRastreio.TextLength > 3)
            {
                blTempoDig = false;
                timer1.Enabled = false;

                blContinua = false;

                if (global.BuscaCampo("ITENS_ENVIO_CORREIOS", "RASTREIO", "RASTREIO", txtRastreio.Text) != txtRastreio.Text)
                {
                    blContinua = true;
                }
                else
                {
                    if (MessageBox.Show("Esse Rastreio já foi enviado!! Pesquisa mesmo assim??", "PontoZero Informa", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1) == DialogResult.Yes)
                    {
                        //DIGITAR SENHA
                        blContinua = true;
                    }
                }

                if (blContinua == true)
                {


                    if (global.BuscaCampo("PEDIDOS_INTEGRADOS", "cd_rastreio", "cd_rastreio", txtRastreio.Text) == txtRastreio.Text)
                    {
                        if (Carrega_Pedido(txtRastreio.Text) == true)
                        {
                            if (txtRastreio.Text == "")
                            {
                                txtRastreio.Text = txtRastreio.Text;
                            }

                            txtRastreio.Text = "";
                            txtCEP.Text = "";

                            txtRastreio.Focus();
                        }
                    }
                    else
                    {
                        //MessageBox.Show("BUSCA CEP", "PontoZero Informa");
                        txtCEP.Text = "";
                        lblPedido.Text = "";
                        lblCliente.Text = "";
                        lblPlataforma.Text = "";
                        lblPedidoPlataforma.Text = "";
                        lblLoja.Text = "";
                        chkReenvio.Checked = false;
                        chkIgnorarData.Checked = false;

                        txtCEP.Focus();
                    }

                }
                else
                {
                    //MessageBox.Show("MERCADOLIVRE", "PontoZero Informa");
                }
            }





            if (global.myDB.connection.State != ConnectionState.Closed)
            {
                global.myDB.CloseConnection();
            }
        }

        private void txtRastreio_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((blTempoDig == false && chkDigitar.Checked == false) || e.KeyChar == 13)
            {
                timer1.Enabled = true;
            }
        }


        private bool Carrega_Pedido(String strRastreio)
        {
            MySqlCommand cmd;
            bool blExibe;
            string strMSG;
            string strPedido = "", strLoja = "", strCanal = "", strConta = "", strNome = "", strSobrenome = "", strPrimeiroNome = "", strNomeMeio = "", strUltimoNome = "";
            string strChave = "", strNF = "", strNFSerie = "", strDtEmissao = "", strEnderecoD1 = "", strEnderecoD2 = "", strEnderecoD3 = "";

            string strNomeBarra = "";

            blExibe = false;

            string query = " SELECT LOJA, CONTALOJA, replace(PEDIDOS_INTEGRADOS.marketplace ,'B2W - ','') CANAL, id_pedido INCREMENT_ID, cod_pedido CANAL_ID, nome CUSTOMER_FIRSTNAME, '' CUSTOMER_MIDDLENAME, '' CUSTOMER_LASTNAME, nome FIRSTNAME, ";
            query = query + " '' LASTNAME, 1 CONTADOR, ";

            query = query + " nf, serie, chave, dt_emissao, endereco, complemento, bairro, cep, cidade, uf ";

            query = query + " from PEDIDOS_INTEGRADOS ";

            query = query + " WHERE STATUS NOT IN('Pagamento cancelado')  ";

            if (chkIgnorarData.Checked == false)
            {
                query = query + " and PEDIDOS_INTEGRADOS.dt_pedido > (date_sub(now(), interval 5 day)) ";
            }

            query = query + " AND cd_rastreio = '" + strRastreio + "' ";


            if (global.myDB.connection.State == ConnectionState.Closed)
            {
                global.myDB.OpenConnection();
            }

            cmd = new MySqlCommand(query, global.myDB.connection);

            try
            {
                dataReader = cmd.ExecuteReader();
            }
            catch (Exception)
            {
                global.myDB.OpenConnection();
                cmd = new MySqlCommand(query, global.myDB.connection);
                dataReader = cmd.ExecuteReader();
            }

            while (dataReader.Read())
            {

                blExibe = false;
                if (Convert.ToInt32(dataReader["contador"]) >= 2)
                {
                    if (dataReader["CANAL"].ToString() == "SUBMARINO" || dataReader["CANAL"].ToString() == "SHOPTIME" || dataReader["CANAL"].ToString() == "LOJASAMERICANAS")
                    {
                        strMSG = "Confirma o pedido " + dataReader["canal_id"].ToString() + " para o cliente " + dataReader["firstname"].ToString() + " " + dataReader["lastname"].ToString();
                    }
                    else
                    {
                        strMSG = "Confirma o pedido " + dataReader["increment_id"].ToString() + " para o cliente " + dataReader["firstname"].ToString() + " " + dataReader["lastname"].ToString();
                    }

                    if (MessageBox.Show(strMSG, "PontoZero Informa", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1) == DialogResult.Yes)
                    {
                        blExibe = true;
                    }
                    else
                    {
                        blExibe = false;
                    }
                }
                else
                {
                    blExibe = true;
                }

                if (blExibe == true)
                {
                    lblPedido.Text = "PEDIDO: " + dataReader["increment_id"].ToString();

                    lblLoja.Text = dataReader["LOJA"].ToString().ToUpper();

                    lblPedidoPlataforma.Text = dataReader["CANAL_ID"].ToString().ToUpper();

                    if (dataReader["CANAL"].ToString().ToUpper() == "MERCADOLIVRE" || dataReader["CANAL"].ToString().ToUpper() == "MERCADO LIVRE")
                    {
                        if (dataReader["LOJA"].ToString() == "ALPHAJOGOSLTDA" || dataReader["LOJA"].ToString() == "SHOPPINGBELA")
                        {
                            lblPlataforma.Text = "MERCADO PLACE";

                            lblLoja.Text = dataReader["LOJA"].ToString();

                        }
                        else
                        {
                            lblPlataforma.Text = "MERCADOLIVRE";
                            lblLoja.Text = dataReader["LOJA"].ToString();

                        }

                    }
                    else if (dataReader["CANAL"].ToString().ToUpper() == "SUBMARINO" || dataReader["CANAL"].ToString().ToUpper() == "SHOPTIME" || dataReader["CANAL"].ToString().ToUpper() == "LOJASAMERICANAS" || dataReader["CANAL"].ToString().ToUpper() == "AMERICANAS")
                    {
                        lblPlataforma.Text = "B2W";

                        if (dataReader["CONTALOJA"].ToString() == "financeiro@facilcard.com.br")
                        {
                            lblLoja.Text = "FACILSHOPPING";
                        }
                        else if (dataReader["CONTALOJA"].ToString() == "diretoria@facilcard.com.br")
                        {
                            lblLoja.Text = "ALPHA JOGOS";
                        }
                        else if (dataReader["CONTALOJA"].ToString() == "midia@facilcard.com.br")
                        {
                            lblLoja.Text = "BELA SHOPPING";
                        }
                        else if (dataReader["CONTALOJA"].ToString() == "comercial@mundialesportes.com.br")
                        {
                            lblLoja.Text = "MUNDIAL";
                        }

                    }
                    else if (dataReader["CANAL"].ToString() == "LUDOSTORE")
                    {
                        lblPlataforma.Text = "LUDOSTORE";
                    }
                    else if (dataReader["CANAL"].ToString() == "Magazine Luiza")
                    {
                        lblPlataforma.Text = "MAGAZINE LUIZA";
                    }
                    else
                    {
                        lblPlataforma.Text = dataReader["CANAL"].ToString().ToUpper();

                    }


                    if (dataReader["firstname"].ToString().Length > 0)
                    {
                        lblCliente.Text =  dataReader["firstname"].ToString() + " " + dataReader["lastname"].ToString();
                    }
                    else
                    {
                        lblCliente.Text =  dataReader["customer_firstname"].ToString() + " " + dataReader["customer_middlename"].ToString() + " " + dataReader["customer_lastname"].ToString();
                    }
                    // break;

                    strPedido = dataReader["increment_id"].ToString();
                    //strLoja = dataReader["customer_firstname"].ToString();
                    //strCanal = dataReader["customer_firstname"].ToString();
                    //strConta = dataReader["customer_firstname"].ToString();
                    //strNome = dataReader["customer_firstname"].ToString();
                    //strSobrenome = dataReader["customer_firstname"].ToString();
                    //strPrimeiroNome = dataReader["customer_firstname"].ToString();
                    //strNomeMeio = dataReader["customer_firstname"].ToString();
                    //strUltimoNome = dataReader["customer_firstname"].ToString();
                    //lblPedidoPlataforma.Text = dataReader["customer_firstname"].ToString();
                    strChave = dataReader["chave"].ToString();
                    strNF = dataReader["nf"].ToString();
                    strNFSerie = dataReader["serie"].ToString();
                    strDtEmissao = System.DateTime.Parse(dataReader["dt_emissao"].ToString()).ToString("dd/MM/yyyy");
                    strEnderecoD1 = dataReader["endereco"].ToString();
                    strEnderecoD2 = dataReader["complemento"].ToString() + " " + dataReader["bairro"].ToString();
                    strEnderecoD3 = dataReader["cep"].ToString() + " " + dataReader["cidade"].ToString() + " " + dataReader["uf"].ToString();



                    if (PedidoValido(dataReader["increment_id"].ToString()) == true)
                    {
                        blExibe = true;
                    }
                    else
                    {
                        blExibe = true;
                        MessageBox.Show("Esse Rastreio já foi enviado!!", "PontoZero Informa");
                    }
                    break;
                }
            }
            if (blExibe == false)
            {
                MessageBox.Show("Não possivel encontrar esse pedido!!", "PontoZero Informa");
                txtCEP.Text = "";
                lblPedido.Text = "";
            }
            else
            {
                //******************************* ETIQUETA *************************************************

                
                if (strChave.Length > 10)
                {
                    Random rnd = new Random();

                    strNomeBarra = rnd.Next().ToString();

                   // strChave = strChave + GerarDigitoVerificadorNFe(strChave).ToString();

                    ImprimeCodBarra(strChave, strNomeBarra);

                    picChave.Load("c:/temp/" + strNomeBarra + ".bmp");

                    lblChaveDigitada.Text = strChave;

                    lblNFData.Text = "NF:" + strNF + " Série: " + strNFSerie + " Emissão: " + strDtEmissao.Substring(0, 10);

                    lblEnderecoD1.Text = strEnderecoD1;
                    lblEnderecoD2.Text = strEnderecoD2;
                    lblEnderecoD3.Text = strEnderecoD3;

                    lblEndereco1.Text = "Rua Ipiranga, 957";
                    lblEndereco2.Text = "Sala 1 - Bairro: Jardim Santista";
                    lblEndereco3.Text = "CEP:08.730-000 - Mogi das Cruzes - SP";
                    lblTelefone.Text = "(11) 3883-0109";

                    PesquisaItensPedido(strPedido);

                    if (strChave.Substring(6, 8) == "28880675") //FACILCARD
                    {
                        lblNomeCNPJ.Text = "FACIL SHOPPING LTDA";
                        lblCNPJ.Text = "28.880.675/0001-91";
                    }

                    if (strChave.Substring(6, 8) == "30922327") //ALPHAJOGOS
                    {
                        lblNomeCNPJ.Text = "ALPHA JOGOS LTDA";
                        lblCNPJ.Text = "30.922.327/0001-81";
                    }

                    if (strChave.Substring(6, 8) == "23527805") //BELLA SHOPPING
                    {
                        lblNomeCNPJ.Text = "BELA SHOPPING LTDA";
                        lblCNPJ.Text = "23.527.805/0001-93";
                        lblTelefone.Text = "(11) 3883-0115";
                    }

                    if (strChave.Substring(6, 8) == "34848323") //MUNDIAL
                    {
                        lblNomeCNPJ.Text = "MUNDIAL SHOPPING COMERC DE ART ESPORTIV LTDA";
                        lblCNPJ.Text = "34.848.323/0001-51";

                        lblEndereco1.Text = "Rua Aprigio de Oliveira";
                        lblEndereco2.Text = "";
                        lblEndereco3.Text = "";
                        lblTelefone.Text = "";
                    }


                     printDocument1.Print(); 

                }

                //******************************* ETIQUETA FIM ************************************************* 
            }

            dataReader.Close();

            if (global.myDB.connection.State != ConnectionState.Closed)
            {
                global.myDB.CloseConnection();
            }

            return blExibe;
        }


        private bool PedidoValido(string strPedido)
        {
            bool blValido = true;
            MySqlCommand cmd1;
            MySqlDataReader dataPedido;

            string query = "select * from ITENS_ENVIO_CORREIOS where increment_id = '" + strPedido + "';";

            global.myDB.CloseConnection();

            if (global.myDB.connection.State == ConnectionState.Closed)
            {
                global.myDB.OpenConnection();
            }

            cmd1 = new MySqlCommand(query, global.myDB.connection);

            dataPedido = cmd1.ExecuteReader();

            if (dataPedido.Read())
            {
                blValido = false;
            }

            dataPedido.Close();

            if (global.myDB.connection.State != ConnectionState.Closed)
            {
                global.myDB.CloseConnection();
            }

            return blValido;
        }

        private void txtCEP_KeyPress(object sender, KeyPressEventArgs e)
        {
           
        }


        private void Carrega_CEP(String strCEP)
        {
            MySqlCommand cmd;
            bool blExibe;

            string strPedido = "", strLoja = "", strCanal = "", strConta = "", strNome = "", strSobrenome = "", strPrimeiroNome = "", strNomeMeio = "", strUltimoNome = "";

            string strChave = "", strNF = "", strNFSerie = "", strDtEmissao = "", strEnderecoD1 = "", strEnderecoD2 = "", strEnderecoD3 = "";

            string strNomeBarra = "";

            blExibe = false;

            string query = " SELECT  LOJA, CONTALOJA, replace(PEDIDOS_INTEGRADOS.marketplace ,'B2W - ','') CANAL, id_pedido INCREMENT_ID, cod_pedido CANAL_ID, nome CUSTOMER_FIRSTNAME, '' CUSTOMER_MIDDLENAME, '' CUSTOMER_LASTNAME, nome FIRSTNAME, ";
            query = query + " '' LASTNAME,  ";

            query = query + " nf, serie, chave, dt_emissao, endereco, complemento, bairro, cep, cidade, uf, ";

            query = query + " (SELECT COUNT(*) from PEDIDOS_INTEGRADOS ";
            query = query + " WHERE STATUS NOT IN('Pagamento cancelado') ";

            if (strCEP.Length == 9)
            {
                query = query + " AND (CEP = '" + strCEP + "' OR CEP = '" + strCEP.Substring(0, 5) + "-" + strCEP.Substring(5, 3) + "' or CEP = '" + strCEP.Substring(0, 8) + "' )";
            }
            else
            {
                query = query + " AND (CEP = '" + strCEP + "' OR CEP = '" + strCEP.Substring(0, 5) + "-" + strCEP.Substring(5, 3) + "')";
            }

            query = query + " and id_pedido not in (Select increment_id from ITENS_ENVIO_CORREIOS where increment_id = id_pedido)  ";

            query = query + ") contador";


            query = query + " from PEDIDOS_INTEGRADOS ";

            query = query + " WHERE STATUS NOT IN('Pagamento cancelado')  "; // and dt_pedido > (now() - 90000000) ";
                                                                             // query = query + " AND PEDIDOS_INTEGRADOS.marketplace NOT LIKE '%MERCADO%' ";
            query = query + " AND (CEP = '" + strCEP.Substring(0, 8) + "' OR CEP = '" + strCEP + "' OR CEP = '" + strCEP.Substring(0, 5) + "-" + strCEP.Substring(5, 3) + "')";

            if (chkReenvio.Checked == false)
            {
                query = query + " and id_pedido not in (Select increment_id from ITENS_ENVIO_CORREIOS where increment_id = id_pedido)";
            }

            query = query + " order by contador desc ;";
            //*****************************************************************************************************


            if (global.myDB.connection.State == ConnectionState.Closed)
            {
                global.myDB.OpenConnection();
            }

            cmd = new MySqlCommand(query, global.myDB.connection);

            //if (dataReader.IsClosed == false)
            //{
            //    dataReader.Close();
            //}
            try
            {
                dataReader = cmd.ExecuteReader();
            }
            catch (Exception)
            {
                global.myDB.OpenConnection();
                cmd = new MySqlCommand(query, global.myDB.connection);
                dataReader = cmd.ExecuteReader();
            }

            frmCEPEncontrado frmPedEncontrados = new frmCEPEncontrado();
            frmPedEncontrados.dataReaderPedidos = dataReader;
            frmPedEncontrados.ShowDialog(this);

            blExibe = false;

            if (frmPedEncontrados.strPedido != "")
            {
                blExibe = true;
                strPedido = frmPedEncontrados.strPedido;
                strLoja = frmPedEncontrados.strLoja;
                strCanal = frmPedEncontrados.strCanal;
                strConta = frmPedEncontrados.strConta;
                strNome = frmPedEncontrados.strNome;
                strSobrenome = frmPedEncontrados.strSobrenome;
                strPrimeiroNome = frmPedEncontrados.strPrimeiroNome;
                strNomeMeio = frmPedEncontrados.strNomeMeio;
                strUltimoNome = frmPedEncontrados.strUltimoNome;
                lblPedidoPlataforma.Text = frmPedEncontrados.strPedidoPlataforma;
                strChave = frmPedEncontrados.strChave;
                strNF = frmPedEncontrados.strNF;
                strNFSerie = frmPedEncontrados.strNFSerie;
                strDtEmissao = frmPedEncontrados.strDtEmissao;
                strEnderecoD1 = frmPedEncontrados.strEndereco1;
                strEnderecoD2 = frmPedEncontrados.strEndereco2;
                strEnderecoD3 = frmPedEncontrados.strEndereco3;
                //******************************* ETIQUETA *************************************************

                if (strChave.Length > 10)
                {
                    Random rnd = new Random();

                    strNomeBarra = rnd.Next().ToString();

                    strChave = strChave + GerarDigitoVerificadorNFe(strChave).ToString();

                    ImprimeCodBarra(strChave, strNomeBarra);

                    picChave.Load("c:/temp/" + strNomeBarra + ".bmp");

                    lblChaveDigitada.Text = strChave;

                    lblNFData.Text = "NF:" + strNF + " Série: " + strNFSerie + " Emissão: " + strDtEmissao.Substring(0, 10);

                    lblEnderecoD1.Text = strEnderecoD1;
                    lblEnderecoD2.Text = strEnderecoD2;
                    lblEnderecoD3.Text = strEnderecoD3;

                    lblEndereco1.Text = "Rua Ipiranga, 957";
                    lblEndereco2.Text = "Sala 1 - Bairro: Jardim Santista";
                    lblEndereco3.Text = "CEP:08.730-000 - Mogi das Cruzes - SP";
                    lblTelefone.Text = "(11) 3883-0109";

                    PesquisaItensPedido(strPedido);

                    if (strChave.Substring(6, 8) == "28880675") //FACILCARD
                    {
                        lblNomeCNPJ.Text = "FACIL SHOPPING LTDA";
                        lblCNPJ.Text = "28.880.675/0001-91";
                    }

                    if (strChave.Substring(6, 8) == "30922327") //ALPHAJOGOS
                    {
                        lblNomeCNPJ.Text = "ALPHA JOGOS LTDA";
                        lblCNPJ.Text = "30.922.327/0001-81";
                    }

                    if (strChave.Substring(6, 8) == "23527805") //BELLA SHOPPING
                    {
                        lblNomeCNPJ.Text = "BELA SHOPPING LTDA";
                        lblCNPJ.Text = "23.527.805/0001-93";
                        lblTelefone.Text = "(11) 3883-0115";
                    }

                    if (strChave.Substring(6, 8) == "34848323") //MUNDIAL
                    {
                        lblNomeCNPJ.Text = "MUNDIAL SHOPPING COMERC DE ART ESPORTIV LTDA";
                        lblCNPJ.Text = "34.848.323/0001-51";

                        lblEndereco1.Text = "Rua Aprigio de Oliveira";
                        lblEndereco2.Text = "";
                        lblEndereco3.Text = "";
                        lblTelefone.Text = "";
                    }

                    PrinterResolution x = new PrinterResolution();
                    PrinterSettings prtSettings = new PrinterSettings();

                    x.Kind = PrinterResolutionKind.High;
                    prtSettings.DefaultPageSettings.PrinterResolution = x;
                    printDocument1.PrinterSettings = prtSettings;

                    printDocument1.Print(); 

                }

                //******************************* ETIQUETA FIM *************************************************

                lblPedido.Text = "PEDIDO: " + strPedido;

                lblLoja.Text = strLoja;

                if (strCanal == "MERCADOLIVRE")
                {
                    lblPlataforma.Text = "MERCADOLIVRE";
                }
                else if (strCanal == "SUBMARINO" || strCanal == "SHOPTIME" || strCanal == "AMERICANAS" || strCanal == "LOJASAMERICANAS")
                {
                    lblPlataforma.Text = "B2W";

                    if (strConta == "financeiro@facilcard.com.br")
                    {
                        lblLoja.Text = "FACILSHOPPING";
                    }
                    else if (strConta == "diretoria@facilcard.com.br" || strConta == "alphajogos@facilshopping.com.br")
                    {
                        lblLoja.Text = "ALPHA JOGOS";
                    }
                    else if (strConta == "midia@facilcard.com.br")
                    {
                        lblLoja.Text = "BELA SHOPPING";
                    }
                    else if (strConta == "comercial@mundialesportes.com.br")
                    {
                        lblLoja.Text = "MUNDIAL";
                    }
                }
                else if (strCanal == "LUDOSTORE")
                {
                    lblPlataforma.Text = "LUDOSTORE";
                }
                else if (strCanal == "Magazine Luiza" || strCanal == "MAGAZINE LUIZA")
                {
                    lblPlataforma.Text = "MAGAZINE LUIZA";
                    lblLoja.Text = strLoja;
                }

                else
                {
                    lblPlataforma.Text = strCanal;
                }

                if (strNome.Length > 0)
                {
                    lblCliente.Text =  strNome + " " + strSobrenome;
                }
                else
                {
                    lblCliente.Text = strPrimeiroNome + " " + strNomeMeio + " " + strUltimoNome;
                }
                // break;
                if (PedidoValido(strPedido) == true)
                {
                    blExibe = true;
                }
                else
                {
                    if (MessageBox.Show("Esse Rastreio para o pedido " + lblPedido.Text + " já foi enviado!! Pesquisa mesmo assim? ", "PontoZero Informa", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1) == DialogResult.Yes)
                    {
                        //DIGITAR SENHA
                        blExibe = true;
                    }
                    else
                    {
                        blExibe = false;
                    }

                    txtRastreio.Text = "";
                }


            }
            if (blExibe == false)
            {
                //if (Carrega_CEP_Antigo(strCEP) == false)
                //{
                //}

                MessageBox.Show("Não possivel encontrar esse pedido!!", "PontoZero Informa");
            }

            //dataReader.Close();

            if (blExibe == false)
            {
                MessageBox.Show("Não possivel encontrar esse pedido!!", "PontoZero Informa");
                txtCEP.Text = "";
                lblPedido.Text = strCEP;
            }
            else
            {
                if (txtRastreio.Text == "")
                {
                    txtRastreio.Text = txtCEP.Text;
                }                

                txtRastreio.Text = "";
                txtCEP.Text = "";
                chkReenvio.Checked = false;
                chkIgnorarData.Checked = false;
                txtRastreio.Focus();

            }

            if (global.myDB.connection.State != ConnectionState.Closed)
            {
                global.myDB.CloseConnection();
            }

        }

        private void PesquisaItensPedido(String strPedido)
        {

            MySqlCommand cmd1;
            MySqlDataReader dataPedido;

            lstItensPedido.Columns.Clear();
            lstItensPedido.Items.Clear();

            lstItensPedido.CheckBoxes = false;

            lstItensPedido.Columns.Add("Itens", 200);
            lstItensPedido.Columns.Add("QTDE", 50);

            string query = " select IP.SKU, IP.nm_produto, IP.qtde";
            query = query + " from ITENS_PEDIDOS_INTEGRADOS IP ";


            if (lblPlataforma.Text == "MERCADOLIVRE" || lblPlataforma.Text == "MERCADO PLACE")
            {
                query = query + " where id_pedido in (SELECT id_pedido FROM PEDIDOS_INTEGRADOS WHERE cd_rastreio = '" + txtRastreio.Text + "' and dt_pedido > (now() - 9000000)); ";
            }
            else
            {
                query = query + " where id_pedido = '" + strPedido + "';";
            }




            global.myDB.CloseConnection();

            if (global.myDB.connection.State == ConnectionState.Closed)
            {
                global.myDB.OpenConnection();
            }

            cmd1 = new MySqlCommand(query, global.myDB.connection);

            try
            {
                dataPedido = cmd1.ExecuteReader();
            }
            catch (Exception)
            {
                global.myDB.OpenConnection();
                cmd1 = new MySqlCommand(query, global.myDB.connection);
                dataPedido = cmd1.ExecuteReader();
            }


            while (dataPedido.Read())
            {
                ListViewItem item;

                //for (int i = 0; i < Convert.ToInt32(dataPedido["QTDE"]); i++)
                //{
                    item = new ListViewItem(new[] { dataPedido["NM_PRODUTO"].ToString(), dataPedido["QTDE"].ToString() });



                    lstItensPedido.Items.Add(item);
                //}

            }

            dataPedido.Close();

            if (global.myDB.connection.State != ConnectionState.Closed)
            {
                global.myDB.CloseConnection();
            }

            //lblTotal.Text = lstItensPedido.Items.Count.ToString();


            if (lstItensPedido.Items.Count.ToString() == "0")
            {
                //  BuscaItemIderis(strPedido);
            }


        }


        private void ImprimeCodBarra(String strCodigo, String strNome)
        {
            ApagarImagens();

            //Dim Barcode As New BarcodeLib.Barcode('1023948487474', BarcodeLib.TYPE.CODE128C)

            this.userControl11 = new DSBarCode.BarCodeCtrl();

            this.userControl11.BarCode = strCodigo;

            //this.userControl11.BarCodeHeight = 50;
            this.userControl11.BarCodeHeight = 250;
            // this.userControl11.Font = new System.Drawing.Font("Comic Sans MS", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));

            this.userControl11.LeftMargin = 0;
            this.userControl11.Location = new System.Drawing.Point(0, 0);
            this.userControl11.Name = "userControl11";
            //this.userControl11.Size = new System.Drawing.Size(224, 60);
            //this.userControl11.Size = new System.Drawing.Size(320, 60);
            this.userControl11.Size = new System.Drawing.Size(224, 60);
            this.userControl11.TabIndex = 0;
            this.userControl11.TopMargin = 0;
            this.userControl11.VertAlign = DSBarCode.BarCodeCtrl.AlignType.Center;
            this.userControl11.Weight = DSBarCode.BarCodeCtrl.BarCodeWeight.Small;

            userControl11.SaveImage("c:\\temp\\" + strNome + ".bmp");

            imgCodigoBarra.Image = Image.FromFile(@"C:\temp\" + strNome + ".bmp");

        }

        private int GerarDigitoVerificadorNFe(string chave)
        {
            int soma = 0; // Vai guardar a Soma
            int mod = -1; // Vai guardar o Resto da divisão
            int dv = -1;  // Vai guardar o DigitoVerificador
            int pesso = 2; // vai guardar o pesso de multiplicacao

            //percorrendo cada caracter da chave da direita para esquerda para fazer os calculos com o pesso
            for (int i = chave.Length - 1; i != -1; i--)
            {
                int ch = Convert.ToInt32(chave[i].ToString());
                soma += ch * pesso;
                //sempre que for 9 voltamos o pesso a 2
                if (pesso < 9)
                    pesso += 1;
                else
                    pesso = 2;
            }

            //Agora que tenho a soma vamos pegar o resto da divisão por 11
            mod = soma % 11;
            //Aqui temos uma regrinha, se o resto da divisão for 0 ou 1 então o dv vai ser 0
            if (mod == 0 || mod == 1)
                dv = 0;
            else
                dv = 11 - mod;

            return dv;
        }

        public static String geraEtiquetaComDigitoVerificador(String numeroEtiqueta)
        {
            String prefixo = numeroEtiqueta.Substring(0, 2);
            String numero = numeroEtiqueta.Substring(2, 8);
            String sufixo = numeroEtiqueta.Substring(10);
            String retorno = numero;
            String dv;
            int[] multiplicadores = { 8, 6, 4, 2, 3, 5, 9, 7 };
            int soma = 0;
            // Preenche número com 0 à esquerda  
            if (numeroEtiqueta.Length < 12)
            {
                retorno = "Error…";
            }
            else if (numero.Length < 8 && numeroEtiqueta.Length == 12)
            {
                String zeros = "";
                int diferenca = 8 - numero.Length;
                for (int i = 0; i < diferenca; i++)
                {
                    zeros += "0";
                }
                retorno = zeros + numero;
            }
            else
            {
                retorno = numero.Substring(0, 8);
            }

            for (int i = 0; i < 8; i++)
            {
                soma += Convert.ToInt32(retorno.Substring(i, 1)) * multiplicadores[i];
            }

            int resto = soma % 11;

            if (resto == 0)
            {
                dv = "5";
            }
            else if (resto == 1)
            {
                dv = "0";
            }
            else
            {
                dv = Convert.ToString(11 - resto);
            }

            retorno += dv;
            retorno = prefixo + retorno + sufixo;
            return retorno;
        }

        private bool ApagarImagens()
        {
            string[] imgList = Directory.GetFiles("C:\\temp\\", "*.bmp");

            try
            {
                foreach (string f in imgList)
                {
                    File.Delete(f);
                }
            }
            catch (System.IO.IOException e)
            {
                Console.WriteLine(e.Message);
                // return false;
            }

            return true;
        }

        private void printDocument1_PrintPage(object sender, System.Drawing.Printing.PrintPageEventArgs e)
        {

            Bitmap bm = new Bitmap(this.grupo.Width, this.grupo.Height);

            bm.SetResolution(96.0F, 96.0F); ;
            grupo.DrawToBitmap(bm, new Rectangle(0, 0, this.grupo.Width, this.grupo.Height));
            e.Graphics.DrawImage(bm, 0, 0);
            /*
            string _texto = lblChaveDigitada.Text;

            var printDocument = sender as System.Drawing.Printing.PrintDocument;

            if (printDocument != null)
            {
                using (var fonte = new Font("Times New Roman", 14))
                using (var brush = new SolidBrush(Color.Black))
                {
                    int caracteresNaPagina = 0;
                    int linhasPorPagina = 0;

                    e.Graphics.MeasureString(
                        _texto, fonte, e.MarginBounds.Size, StringFormat.GenericTypographic,
                        out caracteresNaPagina, out linhasPorPagina);

                    e.Graphics.DrawString(
                        _texto.Substring(0, caracteresNaPagina),
                        fonte,
                        brush,
                        e.MarginBounds);

                    _texto = _texto.Substring(caracteresNaPagina);

                    e.HasMorePages = _texto.Length > 0;
                }
                
                e.Graphics.DrawImage(Image.FromFile(picChave.ImageLocation), new Point(50, 0));
            }
            */

        }

        private void txtCEP_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtCEP_KeyPress_1(object sender, KeyPressEventArgs e)
        {
            lblPedido.Text = "";
            lblCliente.Text = "";
            lblPlataforma.Text = "";
            lblLoja.Text = "";
            lblPedidoPlataforma.Text = "";

            if (e.KeyChar.ToString() == "-")
            {
                e.KeyChar = '\0';
            }

            if (txtCEP.TextLength > 6)
            {
                Carrega_CEP(txtCEP.Text + e.KeyChar);
                e.KeyChar = '\0';
            }
        }

        private void txtRastreio_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtRastreio_KeyPress_1(object sender, KeyPressEventArgs e)
        {
            if ((blTempoDig == false && chkDigitar.Checked == false) || e.KeyChar == 13)
            {
                timer1.Enabled = true;
            }
        }

        private void chkReenvio_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void chkIgnorarData_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void lblCliente_Click(object sender, EventArgs e)
        {

        }
    }
}
