﻿namespace FacilShoppingReports
{
    partial class frmEtiquetaNF
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.imgCodigoBarra = new System.Windows.Forms.PictureBox();
            this.printDocument1 = new System.Drawing.Printing.PrintDocument();
            this.grupo = new System.Windows.Forms.GroupBox();
            this.lblCliente = new System.Windows.Forms.Label();
            this.lblChaveDigitada = new System.Windows.Forms.Label();
            this.lstItensPedido = new System.Windows.Forms.ListView();
            this.label1 = new System.Windows.Forms.Label();
            this.lblEnderecoD3 = new System.Windows.Forms.Label();
            this.lblEnderecoD2 = new System.Windows.Forms.Label();
            this.lblEnderecoD1 = new System.Windows.Forms.Label();
            this.lblNFData = new System.Windows.Forms.Label();
            this.picChave = new System.Windows.Forms.PictureBox();
            this.lblTelefone = new System.Windows.Forms.Label();
            this.lblCNPJ = new System.Windows.Forms.Label();
            this.lblEndereco3 = new System.Windows.Forms.Label();
            this.lblEndereco2 = new System.Windows.Forms.Label();
            this.lblEndereco1 = new System.Windows.Forms.Label();
            this.lblNomeCNPJ = new System.Windows.Forms.Label();
            this.txtCEP = new System.Windows.Forms.TextBox();
            this.chkDigitar = new System.Windows.Forms.CheckBox();
            this.label5 = new System.Windows.Forms.Label();
            this.lblPedidoPlataforma = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.lblLoja = new System.Windows.Forms.Label();
            this.lblPlataforma = new System.Windows.Forms.Label();
            this.lblPedido = new System.Windows.Forms.Label();
            this.chkIgnorarData = new System.Windows.Forms.CheckBox();
            this.chkReenvio = new System.Windows.Forms.CheckBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.txtRastreio = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.imgCodigoBarra)).BeginInit();
            this.grupo.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picChave)).BeginInit();
            this.SuspendLayout();
            // 
            // timer1
            // 
            this.timer1.Interval = 500;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // imgCodigoBarra
            // 
            this.imgCodigoBarra.ImageLocation = "c:\\temp\\test.bmp";
            this.imgCodigoBarra.Location = new System.Drawing.Point(320, 154);
            this.imgCodigoBarra.Name = "imgCodigoBarra";
            this.imgCodigoBarra.Size = new System.Drawing.Size(44, 41);
            this.imgCodigoBarra.TabIndex = 64;
            this.imgCodigoBarra.TabStop = false;
            this.imgCodigoBarra.Visible = false;
            // 
            // printDocument1
            // 
            this.printDocument1.PrintPage += new System.Drawing.Printing.PrintPageEventHandler(this.printDocument1_PrintPage);
            // 
            // grupo
            // 
            this.grupo.BackColor = System.Drawing.Color.White;
            this.grupo.Controls.Add(this.lblCliente);
            this.grupo.Controls.Add(this.lblChaveDigitada);
            this.grupo.Controls.Add(this.lstItensPedido);
            this.grupo.Controls.Add(this.label1);
            this.grupo.Controls.Add(this.lblEnderecoD3);
            this.grupo.Controls.Add(this.lblEnderecoD2);
            this.grupo.Controls.Add(this.lblEnderecoD1);
            this.grupo.Controls.Add(this.lblNFData);
            this.grupo.Controls.Add(this.picChave);
            this.grupo.Controls.Add(this.lblTelefone);
            this.grupo.Controls.Add(this.lblCNPJ);
            this.grupo.Controls.Add(this.lblEndereco3);
            this.grupo.Controls.Add(this.lblEndereco2);
            this.grupo.Controls.Add(this.lblEndereco1);
            this.grupo.Controls.Add(this.lblNomeCNPJ);
            this.grupo.Location = new System.Drawing.Point(254, 26);
            this.grupo.Name = "grupo";
            this.grupo.Size = new System.Drawing.Size(370, 483);
            this.grupo.TabIndex = 65;
            this.grupo.TabStop = false;
            // 
            // lblCliente
            // 
            this.lblCliente.AutoSize = true;
            this.lblCliente.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCliente.Location = new System.Drawing.Point(8, 421);
            this.lblCliente.Name = "lblCliente";
            this.lblCliente.Size = new System.Drawing.Size(41, 13);
            this.lblCliente.TabIndex = 93;
            this.lblCliente.Text = "label3";
            // 
            // lblChaveDigitada
            // 
            this.lblChaveDigitada.AutoSize = true;
            this.lblChaveDigitada.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblChaveDigitada.Location = new System.Drawing.Point(50, 220);
            this.lblChaveDigitada.Name = "lblChaveDigitada";
            this.lblChaveDigitada.Size = new System.Drawing.Size(41, 13);
            this.lblChaveDigitada.TabIndex = 91;
            this.lblChaveDigitada.Text = "label1";
            this.lblChaveDigitada.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lstItensPedido
            // 
            this.lstItensPedido.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.lstItensPedido.Location = new System.Drawing.Point(20, 250);
            this.lstItensPedido.Name = "lstItensPedido";
            this.lstItensPedido.Size = new System.Drawing.Size(322, 154);
            this.lstItensPedido.TabIndex = 89;
            this.lstItensPedido.UseCompatibleStateImageBehavior = false;
            this.lstItensPedido.View = System.Windows.Forms.View.Details;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(312, 463);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(41, 13);
            this.label1.TabIndex = 90;
            this.label1.Text = "label3";
            // 
            // lblEnderecoD3
            // 
            this.lblEnderecoD3.AutoSize = true;
            this.lblEnderecoD3.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblEnderecoD3.Location = new System.Drawing.Point(9, 464);
            this.lblEnderecoD3.Name = "lblEnderecoD3";
            this.lblEnderecoD3.Size = new System.Drawing.Size(64, 12);
            this.lblEnderecoD3.TabIndex = 88;
            this.lblEnderecoD3.Text = "lblEndereco";
            // 
            // lblEnderecoD2
            // 
            this.lblEnderecoD2.AutoSize = true;
            this.lblEnderecoD2.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblEnderecoD2.Location = new System.Drawing.Point(9, 450);
            this.lblEnderecoD2.Name = "lblEnderecoD2";
            this.lblEnderecoD2.Size = new System.Drawing.Size(35, 12);
            this.lblEnderecoD2.TabIndex = 87;
            this.lblEnderecoD2.Text = "label6";
            // 
            // lblEnderecoD1
            // 
            this.lblEnderecoD1.AutoSize = true;
            this.lblEnderecoD1.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblEnderecoD1.Location = new System.Drawing.Point(9, 436);
            this.lblEnderecoD1.Name = "lblEnderecoD1";
            this.lblEnderecoD1.Size = new System.Drawing.Size(35, 12);
            this.lblEnderecoD1.TabIndex = 86;
            this.lblEnderecoD1.Text = "label7";
            // 
            // lblNFData
            // 
            this.lblNFData.AutoSize = true;
            this.lblNFData.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNFData.Location = new System.Drawing.Point(9, 91);
            this.lblNFData.Name = "lblNFData";
            this.lblNFData.Size = new System.Drawing.Size(37, 13);
            this.lblNFData.TabIndex = 85;
            this.lblNFData.Text = "LOJA";
            // 
            // picChave
            // 
            this.picChave.Location = new System.Drawing.Point(17, 107);
            this.picChave.Name = "picChave";
            this.picChave.Size = new System.Drawing.Size(442, 110);
            this.picChave.TabIndex = 83;
            this.picChave.TabStop = false;
            // 
            // lblTelefone
            // 
            this.lblTelefone.AutoSize = true;
            this.lblTelefone.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTelefone.Location = new System.Drawing.Point(122, 30);
            this.lblTelefone.Name = "lblTelefone";
            this.lblTelefone.Size = new System.Drawing.Size(60, 12);
            this.lblTelefone.TabIndex = 82;
            this.lblTelefone.Text = "lblTelefone";
            // 
            // lblCNPJ
            // 
            this.lblCNPJ.AutoSize = true;
            this.lblCNPJ.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCNPJ.Location = new System.Drawing.Point(10, 30);
            this.lblCNPJ.Name = "lblCNPJ";
            this.lblCNPJ.Size = new System.Drawing.Size(34, 12);
            this.lblCNPJ.TabIndex = 81;
            this.lblCNPJ.Text = "CNPJ";
            // 
            // lblEndereco3
            // 
            this.lblEndereco3.AutoSize = true;
            this.lblEndereco3.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblEndereco3.Location = new System.Drawing.Point(10, 72);
            this.lblEndereco3.Name = "lblEndereco3";
            this.lblEndereco3.Size = new System.Drawing.Size(64, 12);
            this.lblEndereco3.TabIndex = 80;
            this.lblEndereco3.Text = "lblEndereco";
            // 
            // lblEndereco2
            // 
            this.lblEndereco2.AutoSize = true;
            this.lblEndereco2.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblEndereco2.Location = new System.Drawing.Point(10, 58);
            this.lblEndereco2.Name = "lblEndereco2";
            this.lblEndereco2.Size = new System.Drawing.Size(70, 12);
            this.lblEndereco2.TabIndex = 79;
            this.lblEndereco2.Text = "lblEndereco2";
            // 
            // lblEndereco1
            // 
            this.lblEndereco1.AutoSize = true;
            this.lblEndereco1.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblEndereco1.Location = new System.Drawing.Point(10, 44);
            this.lblEndereco1.Name = "lblEndereco1";
            this.lblEndereco1.Size = new System.Drawing.Size(70, 12);
            this.lblEndereco1.TabIndex = 78;
            this.lblEndereco1.Text = "lblEndereco1";
            // 
            // lblNomeCNPJ
            // 
            this.lblNomeCNPJ.AutoSize = true;
            this.lblNomeCNPJ.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNomeCNPJ.Location = new System.Drawing.Point(10, 16);
            this.lblNomeCNPJ.Name = "lblNomeCNPJ";
            this.lblNomeCNPJ.Size = new System.Drawing.Size(33, 12);
            this.lblNomeCNPJ.TabIndex = 77;
            this.lblNomeCNPJ.Text = "LOJA";
            // 
            // txtCEP
            // 
            this.txtCEP.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCEP.Location = new System.Drawing.Point(12, 92);
            this.txtCEP.Name = "txtCEP";
            this.txtCEP.Size = new System.Drawing.Size(106, 29);
            this.txtCEP.TabIndex = 65;
            this.txtCEP.TextChanged += new System.EventHandler(this.txtCEP_TextChanged);
            this.txtCEP.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtCEP_KeyPress_1);
            // 
            // chkDigitar
            // 
            this.chkDigitar.AutoSize = true;
            this.chkDigitar.Location = new System.Drawing.Point(177, 61);
            this.chkDigitar.Name = "chkDigitar";
            this.chkDigitar.Size = new System.Drawing.Size(71, 17);
            this.chkDigitar.TabIndex = 77;
            this.chkDigitar.Text = "Digitação";
            this.chkDigitar.UseVisualStyleBackColor = true;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(18, 177);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(41, 13);
            this.label5.TabIndex = 76;
            this.label5.Text = "LOJA:";
            // 
            // lblPedidoPlataforma
            // 
            this.lblPedidoPlataforma.AutoSize = true;
            this.lblPedidoPlataforma.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPedidoPlataforma.Location = new System.Drawing.Point(100, 206);
            this.lblPedidoPlataforma.Name = "lblPedidoPlataforma";
            this.lblPedidoPlataforma.Size = new System.Drawing.Size(41, 13);
            this.lblPedidoPlataforma.TabIndex = 75;
            this.lblPedidoPlataforma.Text = "label3";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(18, 206);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(84, 13);
            this.label2.TabIndex = 74;
            this.label2.Text = "N.Plataforma:";
            // 
            // lblLoja
            // 
            this.lblLoja.AutoSize = true;
            this.lblLoja.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblLoja.Location = new System.Drawing.Point(58, 178);
            this.lblLoja.Name = "lblLoja";
            this.lblLoja.Size = new System.Drawing.Size(41, 13);
            this.lblLoja.TabIndex = 73;
            this.lblLoja.Text = "label3";
            // 
            // lblPlataforma
            // 
            this.lblPlataforma.AutoSize = true;
            this.lblPlataforma.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPlataforma.Location = new System.Drawing.Point(18, 151);
            this.lblPlataforma.Name = "lblPlataforma";
            this.lblPlataforma.Size = new System.Drawing.Size(41, 13);
            this.lblPlataforma.TabIndex = 72;
            this.lblPlataforma.Text = "label3";
            // 
            // lblPedido
            // 
            this.lblPedido.AutoSize = true;
            this.lblPedido.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPedido.Location = new System.Drawing.Point(18, 124);
            this.lblPedido.Name = "lblPedido";
            this.lblPedido.Size = new System.Drawing.Size(41, 13);
            this.lblPedido.TabIndex = 70;
            this.lblPedido.Text = "label3";
            // 
            // chkIgnorarData
            // 
            this.chkIgnorarData.AutoSize = true;
            this.chkIgnorarData.Location = new System.Drawing.Point(86, 61);
            this.chkIgnorarData.Name = "chkIgnorarData";
            this.chkIgnorarData.Size = new System.Drawing.Size(85, 17);
            this.chkIgnorarData.TabIndex = 69;
            this.chkIgnorarData.Text = "Ignorar Data";
            this.chkIgnorarData.UseVisualStyleBackColor = true;
            this.chkIgnorarData.CheckedChanged += new System.EventHandler(this.chkIgnorarData_CheckedChanged);
            // 
            // chkReenvio
            // 
            this.chkReenvio.AutoSize = true;
            this.chkReenvio.Location = new System.Drawing.Point(14, 61);
            this.chkReenvio.Name = "chkReenvio";
            this.chkReenvio.Size = new System.Drawing.Size(66, 17);
            this.chkReenvio.TabIndex = 68;
            this.chkReenvio.Text = "Reenvio";
            this.chkReenvio.UseVisualStyleBackColor = true;
            this.chkReenvio.CheckedChanged += new System.EventHandler(this.chkReenvio_CheckedChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(10, 76);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(28, 13);
            this.label3.TabIndex = 67;
            this.label3.Text = "CEP";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(9, 10);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(46, 13);
            this.label4.TabIndex = 66;
            this.label4.Text = "Rastreio";
            // 
            // txtRastreio
            // 
            this.txtRastreio.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtRastreio.Location = new System.Drawing.Point(12, 26);
            this.txtRastreio.Name = "txtRastreio";
            this.txtRastreio.Size = new System.Drawing.Size(236, 29);
            this.txtRastreio.TabIndex = 64;
            this.txtRastreio.TextChanged += new System.EventHandler(this.txtRastreio_TextChanged);
            this.txtRastreio.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtRastreio_KeyPress_1);
            // 
            // frmEtiquetaNF
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(906, 512);
            this.Controls.Add(this.txtCEP);
            this.Controls.Add(this.imgCodigoBarra);
            this.Controls.Add(this.chkDigitar);
            this.Controls.Add(this.grupo);
            this.Controls.Add(this.txtRastreio);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.chkIgnorarData);
            this.Controls.Add(this.chkReenvio);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.lblPedidoPlataforma);
            this.Controls.Add(this.lblPedido);
            this.Controls.Add(this.lblLoja);
            this.Controls.Add(this.lblPlataforma);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmEtiquetaNF";
            this.Text = "Etiqueta de NF";
            ((System.ComponentModel.ISupportInitialize)(this.imgCodigoBarra)).EndInit();
            this.grupo.ResumeLayout(false);
            this.grupo.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picChave)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.PictureBox imgCodigoBarra;
        private System.Drawing.Printing.PrintDocument printDocument1;
        private System.Windows.Forms.GroupBox grupo;
        private System.Windows.Forms.TextBox txtCEP;
        private System.Windows.Forms.CheckBox chkDigitar;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label lblPedidoPlataforma;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label lblLoja;
        private System.Windows.Forms.Label lblPlataforma;
        private System.Windows.Forms.Label lblPedido;
        private System.Windows.Forms.CheckBox chkIgnorarData;
        private System.Windows.Forms.CheckBox chkReenvio;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtRastreio;
        private System.Windows.Forms.Label lblEndereco3;
        private System.Windows.Forms.Label lblEndereco2;
        private System.Windows.Forms.Label lblEndereco1;
        private System.Windows.Forms.Label lblNomeCNPJ;
        private System.Windows.Forms.Label lblCNPJ;
        private System.Windows.Forms.Label lblTelefone;
        private System.Windows.Forms.PictureBox picChave;
        private System.Windows.Forms.Label lblNFData;
        private System.Windows.Forms.Label lblEnderecoD3;
        private System.Windows.Forms.Label lblEnderecoD2;
        private System.Windows.Forms.Label lblEnderecoD1;
        private System.Windows.Forms.ListView lstItensPedido;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lblChaveDigitada;
        private System.Windows.Forms.Label lblCliente;
    }
}