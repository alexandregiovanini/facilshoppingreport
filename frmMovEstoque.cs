﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

namespace FacilShoppingReports
{
    public partial class frmMovEstoque : Form
    {
        public String strSKU;
        public int intQuantidade;
        public bool blEntrada;
        public int intQuantEstoque;
        public String[] strID = new String[10];

        public frmMovEstoque()
        {
            InitializeComponent();
        }

        private void btnSair_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void txtEan_TextChanged(object sender, EventArgs e)
        {

        }

        private void frmMovEstoque_Load(object sender, EventArgs e)
        {

                
        }

        private void frmMovEstoque_Activated(object sender, EventArgs e)
        {
            txtID.Text = strSKU;
        }

        private void btnGrava_Click(object sender, EventArgs e)
        {
            if (Valida() == true)
            {
                if (Gravar() == true)
                {
                    MessageBox.Show("Registro gravado com sucesso!");
                    intQuantidade = Convert.ToInt32(txtQuantidade.Text);
                    this.Close();
                }
                else
                {
                    MessageBox.Show("Ocorreu algum erro na gravação!");
                }

            }
        }

        private bool Valida()
        {

            if (txtQuantidade.TextLength <= 0)
             {
                 MessageBox.Show("Quantidade é obrigatório!!", "Validação!!");
                 return false;
             }

            if (blEntrada == false)
            {
               /* if (Valida_Solicitacao() == false)
                {
                    MessageBox.Show("Não existe um pedido de saida para esse produto ou quantidade insuficiente!!", "Validação!!");
                    return false;
                }
                else
                {
                    Grava_Separado();
                }*/
            }


            if (txtObservacao.TextLength <= 0)
            {
                MessageBox.Show("Uma observação é obrigatória!!", "Validação!!");
                return false;
            }



            return true;
        }


        private void Grava_Separado()
        {

            String query;
            int i;

            for (i = 0; i + 1 <= Convert.ToInt16(txtQuantidade.Text); i++)
            {

                query = "UPDATE sales_flat_order_item set separado = true ";
                query = query + " WHERE separar = true and separado = false and ITEM_ID  =  '" + strID[i] + "'";

                global.myDB.CloseConnection();

                if (global.myDB.connection.State == ConnectionState.Closed)
                {
                    global.myDB.OpenConnection();
                }

                MySqlCommand cmd1 = new MySqlCommand(query, global.myDB.connection);

                try
                {
                    cmd1.ExecuteNonQuery();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
        }

        public bool Cria_Array_Solicitado()
        {
            MySqlCommand cmd;
            MySqlDataReader dataListaMov;
            string query;
            int i;

            query = "SELECT  * from sales_flat_order_item where separar = true and separado = false and SKU = '" + txtID.Text + "'";

            if (global.myDB.connection.State == ConnectionState.Closed)
            {
                global.myDB.OpenConnection();
            }

            cmd = new MySqlCommand(query, global.myDB.connection);

            try
            {
                dataListaMov = cmd.ExecuteReader();
            }
            catch (Exception)
            {
                global.myDB.OpenConnection();
                cmd = new MySqlCommand(query, global.myDB.connection);

                dataListaMov = cmd.ExecuteReader();
            }

            i = 0;

            while (dataListaMov.Read())
            {
                strID[i] = dataListaMov["ITEM_ID"].ToString();

                i = i + 1;
                
            }

            dataListaMov.Close();
            return true;
        }


        public bool Valida_Solicitacao()
        {
            MySqlCommand cmd;
            MySqlDataReader dataListaMov;
            string query;
         
            query = "SELECT  SUM(COALESCE(qty_ordered, 0) )  quantidade from sales_flat_order_item where separar = true and separado = false and SKU = '" + txtID.Text + "' group by sku";


            if (global.myDB.connection.State == ConnectionState.Closed)
            {
                global.myDB.OpenConnection();
            }

            cmd = new MySqlCommand(query, global.myDB.connection);

            try
            {
                dataListaMov = cmd.ExecuteReader();
            }
            catch (Exception)
            {
                global.myDB.OpenConnection();
                cmd = new MySqlCommand(query, global.myDB.connection);

                dataListaMov = cmd.ExecuteReader();
            }


            if (dataListaMov.Read())
            {
                if (Convert.ToInt32(dataListaMov["quantidade"]) < Convert.ToUInt16( txtQuantidade.Text))
                {
                    dataListaMov.Close();
                    return false;
                }

                txtObservacao.Text = txtObservacao.Text + " - SAIDA VINCULADA AOS PEDIDOS DO DIA " + System.DateTime.Now;

                dataListaMov.Close();

                Cria_Array_Solicitado();
                return true;
            }

            dataListaMov.Close();
            return false;
        }

        public bool Gravar()
        {
            MySqlCommand cmd;
            MySqlDataReader dataListaMov;
            string query;
            bool blAtualizou = false;

            query = "SELECT * FROM itemEstoque WHERE SKU = '" + txtID.Text + "'";


            if (global.myDB.connection.State == ConnectionState.Closed)
            {
                global.myDB.OpenConnection();
            }

            cmd = new MySqlCommand(query, global.myDB.connection);

            try
            {
                dataListaMov = cmd.ExecuteReader();
            }
            catch (Exception)
            {
                global.myDB.OpenConnection();
                cmd = new MySqlCommand(query, global.myDB.connection);

                dataListaMov = cmd.ExecuteReader();
            }


            if (dataListaMov.Read())
            {
                if(blEntrada==true)
                {
                    query = "UPDATE itemEstoque " +
                        " SET QUANTIDADE = QUANTIDADE + " + txtQuantidade.Text +
                        " WHERE SKU = '" + txtID.Text + "'; ";
                }
                else
                {
                    query = "UPDATE itemEstoque " +
                            " SET QUANTIDADE = QUANTIDADE - " + txtQuantidade.Text +
                            " WHERE SKU = '" + txtID.Text + "'; ";

                }

            }
            else
            {
                if (blEntrada == true)
                {
                    query = "INSERT INTO itemEstoque (SKU, QUANTIDADE ) " +
                        " VALUES ( '" +
                        txtID.Text + "', " + txtQuantidade.Text + "); ";
                }
                else
                {
                    MessageBox.Show("Esse item ainda não foi incluido no estoque físico");
                    return false;
                }
            }

            dataListaMov.Close();

            if (global.myDB.connection.State == ConnectionState.Closed)
            {
                global.myDB.OpenConnection();
            }

            MySqlCommand cmd1 = new MySqlCommand(query, global.myDB.connection);

            try
            {
                cmd1.ExecuteNonQuery();
                blAtualizou = true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                dataListaMov.Close();
                return false;
            }

            if(blAtualizou == true)
            {

                query = "INSERT INTO MOVESTOQUE (SKU, QUANTIDADE, OBSERVACAO, DATA, ENTRADA, QUANT_ESTOQUE) ";
                query = query + " VALUES ( '" + txtID.Text + "', " + txtQuantidade.Text + ", '" + txtObservacao.Text + "', ";
                query = query + " NOW(), ";

                if(blEntrada==true)
                {
                    query = query + " TRUE, " + (intQuantEstoque + Convert.ToInt32(txtQuantidade.Text)) + ");"; 
                }
                else
                {
                    query = query + " FALSE, " + (intQuantEstoque - Convert.ToInt32(txtQuantidade.Text))  + ");";
                }
                


                if (global.myDB.connection.State == ConnectionState.Closed)
                {
                    global.myDB.OpenConnection();
                }

                cmd = new MySqlCommand(query, global.myDB.connection);

                try
                {
                    cmd.ExecuteNonQuery();
                }
                catch (Exception)
                {
                    global.myDB.OpenConnection();
                    cmd = new MySqlCommand(query, global.myDB.connection);
                    cmd.ExecuteNonQuery();
                }


            }

            dataListaMov.Close();
            return true;
        }
    }
}
