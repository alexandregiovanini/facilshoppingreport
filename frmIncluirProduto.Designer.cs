﻿namespace FacilShoppingReports
{
    partial class frmIncluirProduto
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmIncluirProduto));
            this.btnSair = new System.Windows.Forms.Button();
            this.btnAdicionar = new System.Windows.Forms.Button();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.cbProduto = new System.Windows.Forms.ComboBox();
            this.label14 = new System.Windows.Forms.Label();
            this.txtCodigo = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.txtValorLiquido = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.txtValorDesconto = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.txtValorUnitario = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.txtQuantidade = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.txtUnidade = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtCFOP = new System.Windows.Forms.TextBox();
            this.label20 = new System.Windows.Forms.Label();
            this.txtSosn = new System.Windows.Forms.TextBox();
            this.label = new System.Windows.Forms.Label();
            this.txtNCM = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.txtAliquotaIPI = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtAliquotaICMS = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txtValorIPI = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.txtValorICMS = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.groupBox4.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnSair
            // 
            this.btnSair.Image = global::FacilShoppingReports.Properties.Resources.exit_icon;
            this.btnSair.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnSair.Location = new System.Drawing.Point(796, 63);
            this.btnSair.Name = "btnSair";
            this.btnSair.Size = new System.Drawing.Size(87, 40);
            this.btnSair.TabIndex = 21;
            this.btnSair.Text = "Sair";
            this.btnSair.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnSair.UseVisualStyleBackColor = true;
            this.btnSair.Click += new System.EventHandler(this.btnSair_Click);
            // 
            // btnAdicionar
            // 
            this.btnAdicionar.Image = ((System.Drawing.Image)(resources.GetObject("btnAdicionar.Image")));
            this.btnAdicionar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnAdicionar.Location = new System.Drawing.Point(796, 19);
            this.btnAdicionar.Name = "btnAdicionar";
            this.btnAdicionar.Size = new System.Drawing.Size(87, 40);
            this.btnAdicionar.TabIndex = 22;
            this.btnAdicionar.Text = "Incluir";
            this.btnAdicionar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnAdicionar.UseVisualStyleBackColor = true;
            this.btnAdicionar.Click += new System.EventHandler(this.btnAdicionar_Click);
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.cbProduto);
            this.groupBox4.Controls.Add(this.label14);
            this.groupBox4.Controls.Add(this.txtCodigo);
            this.groupBox4.Controls.Add(this.label13);
            this.groupBox4.Controls.Add(this.txtValorLiquido);
            this.groupBox4.Controls.Add(this.label1);
            this.groupBox4.Controls.Add(this.txtValorDesconto);
            this.groupBox4.Controls.Add(this.label8);
            this.groupBox4.Controls.Add(this.txtValorUnitario);
            this.groupBox4.Controls.Add(this.label9);
            this.groupBox4.Controls.Add(this.txtQuantidade);
            this.groupBox4.Controls.Add(this.label11);
            this.groupBox4.Controls.Add(this.txtUnidade);
            this.groupBox4.Controls.Add(this.label2);
            this.groupBox4.Controls.Add(this.txtCFOP);
            this.groupBox4.Controls.Add(this.label20);
            this.groupBox4.Controls.Add(this.txtSosn);
            this.groupBox4.Controls.Add(this.label);
            this.groupBox4.Controls.Add(this.txtNCM);
            this.groupBox4.Controls.Add(this.label10);
            this.groupBox4.Location = new System.Drawing.Point(13, 12);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(774, 123);
            this.groupBox4.TabIndex = 23;
            this.groupBox4.TabStop = false;
            // 
            // cbProduto
            // 
            this.cbProduto.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Append;
            this.cbProduto.FormattingEnabled = true;
            this.cbProduto.Location = new System.Drawing.Point(200, 15);
            this.cbProduto.Name = "cbProduto";
            this.cbProduto.Size = new System.Drawing.Size(565, 21);
            this.cbProduto.TabIndex = 71;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(156, 19);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(44, 13);
            this.label14.TabIndex = 70;
            this.label14.Text = "Produto";
            // 
            // txtCodigo
            // 
            this.txtCodigo.Location = new System.Drawing.Point(57, 16);
            this.txtCodigo.Name = "txtCodigo";
            this.txtCodigo.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtCodigo.Size = new System.Drawing.Size(72, 20);
            this.txtCodigo.TabIndex = 68;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(11, 19);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(40, 13);
            this.label13.TabIndex = 67;
            this.label13.Text = "Código";
            // 
            // txtValorLiquido
            // 
            this.txtValorLiquido.Location = new System.Drawing.Point(457, 86);
            this.txtValorLiquido.Name = "txtValorLiquido";
            this.txtValorLiquido.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtValorLiquido.Size = new System.Drawing.Size(65, 20);
            this.txtValorLiquido.TabIndex = 66;
            this.txtValorLiquido.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtValorLiquido_KeyPress);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(381, 89);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(70, 13);
            this.label1.TabIndex = 65;
            this.label1.Text = "Valor Líquido";
            // 
            // txtValorDesconto
            // 
            this.txtValorDesconto.Location = new System.Drawing.Point(270, 86);
            this.txtValorDesconto.Name = "txtValorDesconto";
            this.txtValorDesconto.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtValorDesconto.Size = new System.Drawing.Size(72, 20);
            this.txtValorDesconto.TabIndex = 64;
            this.txtValorDesconto.TextChanged += new System.EventHandler(this.txtValorDesconto_TextChanged);
            this.txtValorDesconto.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtValorDesconto_KeyPress);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(184, 89);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(80, 13);
            this.label8.TabIndex = 63;
            this.label8.Text = "Valor Desconto";
            // 
            // txtValorUnitario
            // 
            this.txtValorUnitario.Location = new System.Drawing.Point(87, 86);
            this.txtValorUnitario.Name = "txtValorUnitario";
            this.txtValorUnitario.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtValorUnitario.Size = new System.Drawing.Size(65, 20);
            this.txtValorUnitario.TabIndex = 62;
            this.txtValorUnitario.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtValorUnitario_KeyPress);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(11, 89);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(70, 13);
            this.label9.TabIndex = 61;
            this.label9.Text = "Valor Unitário";
            // 
            // txtQuantidade
            // 
            this.txtQuantidade.Location = new System.Drawing.Point(692, 54);
            this.txtQuantidade.Name = "txtQuantidade";
            this.txtQuantidade.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtQuantidade.Size = new System.Drawing.Size(72, 20);
            this.txtQuantidade.TabIndex = 60;
            this.txtQuantidade.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtQuantidade_KeyPress);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(624, 57);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(62, 13);
            this.label11.TabIndex = 59;
            this.label11.Text = "Quantidade";
            // 
            // txtUnidade
            // 
            this.txtUnidade.Location = new System.Drawing.Point(528, 54);
            this.txtUnidade.Name = "txtUnidade";
            this.txtUnidade.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtUnidade.Size = new System.Drawing.Size(65, 20);
            this.txtUnidade.TabIndex = 58;
            this.txtUnidade.TextChanged += new System.EventHandler(this.txtUnidade_TextChanged);
            this.txtUnidade.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtUnidade_KeyPress);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(475, 57);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(47, 13);
            this.label2.TabIndex = 57;
            this.label2.Text = "Unidade";
            // 
            // txtCFOP
            // 
            this.txtCFOP.Location = new System.Drawing.Point(364, 54);
            this.txtCFOP.Name = "txtCFOP";
            this.txtCFOP.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtCFOP.Size = new System.Drawing.Size(72, 20);
            this.txtCFOP.TabIndex = 54;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(323, 57);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(35, 13);
            this.label20.TabIndex = 53;
            this.label20.Text = "CFOP";
            // 
            // txtSosn
            // 
            this.txtSosn.Location = new System.Drawing.Point(220, 54);
            this.txtSosn.Name = "txtSosn";
            this.txtSosn.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtSosn.Size = new System.Drawing.Size(65, 20);
            this.txtSosn.TabIndex = 39;
            // 
            // label
            // 
            this.label.AutoSize = true;
            this.label.Location = new System.Drawing.Point(179, 57);
            this.label.Name = "label";
            this.label.Size = new System.Drawing.Size(37, 13);
            this.label.TabIndex = 36;
            this.label.Text = "SOSN";
            // 
            // txtNCM
            // 
            this.txtNCM.Location = new System.Drawing.Point(57, 54);
            this.txtNCM.Name = "txtNCM";
            this.txtNCM.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtNCM.Size = new System.Drawing.Size(72, 20);
            this.txtNCM.TabIndex = 35;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(11, 57);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(31, 13);
            this.label10.TabIndex = 33;
            this.label10.Text = "NCM";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.txtAliquotaIPI);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.txtAliquotaICMS);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.txtValorIPI);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.txtValorICMS);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Location = new System.Drawing.Point(13, 141);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(775, 60);
            this.groupBox1.TabIndex = 24;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Tributação";
            // 
            // txtAliquotaIPI
            // 
            this.txtAliquotaIPI.Location = new System.Drawing.Point(678, 22);
            this.txtAliquotaIPI.Name = "txtAliquotaIPI";
            this.txtAliquotaIPI.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtAliquotaIPI.Size = new System.Drawing.Size(72, 20);
            this.txtAliquotaIPI.TabIndex = 60;
            this.txtAliquotaIPI.TextChanged += new System.EventHandler(this.txtAliquotaIPI_TextChanged);
            this.txtAliquotaIPI.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtAliquotaIPI_KeyPress);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(596, 25);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(78, 13);
            this.label3.TabIndex = 59;
            this.label3.Text = "Alíquota de IPI";
            // 
            // txtAliquotaICMS
            // 
            this.txtAliquotaICMS.Location = new System.Drawing.Point(469, 22);
            this.txtAliquotaICMS.Name = "txtAliquotaICMS";
            this.txtAliquotaICMS.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtAliquotaICMS.Size = new System.Drawing.Size(65, 20);
            this.txtAliquotaICMS.TabIndex = 58;
            this.txtAliquotaICMS.TextChanged += new System.EventHandler(this.txtAliquotaICMS_TextChanged);
            this.txtAliquotaICMS.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtAliquotaICMS_KeyPress);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(377, 25);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(91, 13);
            this.label4.TabIndex = 57;
            this.label4.Text = "Alíquota de ICMS";
            this.label4.Click += new System.EventHandler(this.label4_Click);
            // 
            // txtValorIPI
            // 
            this.txtValorIPI.Location = new System.Drawing.Point(254, 22);
            this.txtValorIPI.Name = "txtValorIPI";
            this.txtValorIPI.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtValorIPI.Size = new System.Drawing.Size(72, 20);
            this.txtValorIPI.TabIndex = 54;
            this.txtValorIPI.TextChanged += new System.EventHandler(this.txtValorIPI_TextChanged);
            this.txtValorIPI.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtValorIPI_KeyPress);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(203, 25);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(47, 13);
            this.label5.TabIndex = 53;
            this.label5.Text = "Valor IPI";
            this.label5.Click += new System.EventHandler(this.label5_Click);
            // 
            // txtValorICMS
            // 
            this.txtValorICMS.Location = new System.Drawing.Point(86, 22);
            this.txtValorICMS.Name = "txtValorICMS";
            this.txtValorICMS.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtValorICMS.Size = new System.Drawing.Size(65, 20);
            this.txtValorICMS.TabIndex = 39;
            this.txtValorICMS.TextChanged += new System.EventHandler(this.txtValorICMS_TextChanged);
            this.txtValorICMS.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtValorICMS_KeyPress);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(11, 25);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(75, 13);
            this.label6.TabIndex = 36;
            this.label6.Text = "Valor de ICMS";
            this.label6.Click += new System.EventHandler(this.label6_Click);
            // 
            // frmIncluirProduto
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(886, 212);
            this.ControlBox = false;
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.btnSair);
            this.Controls.Add(this.btnAdicionar);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmIncluirProduto";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Incluir Item";
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnSair;
        private System.Windows.Forms.Button btnAdicionar;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.TextBox txtCFOP;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.TextBox txtSosn;
        private System.Windows.Forms.Label label;
        private System.Windows.Forms.TextBox txtNCM;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox txtValorLiquido;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtValorDesconto;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txtValorUnitario;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox txtQuantidade;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox txtUnidade;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox txtAliquotaIPI;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtAliquotaICMS;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtValorIPI;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtValorICMS;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtCodigo;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.ComboBox cbProduto;
    }
}