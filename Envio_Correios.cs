﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;
using System.IO;

namespace FacilShoppingReports
{
    public partial class frmEnvio_Correios : Form
    {
        MySqlDataReader dataReader;
        bool blPressiona;
        bool blTempoDig;
        String strCertificado;
        public frmEnvio_Correios()
        {
            InitializeComponent();
            lblPedido.Text = "";
            lblCliente.Text = "";
            lblPlataforma.Text = "";
            lblLoja.Text = "";
            chkReenvio.Checked = false;
            chkPedidoAvulso.Checked = false;
            chkIgnorarData.Checked = false;
            CarregaCertificado();
            txtDados.Focus();
        }

        private void Envio_Correios_Load(object sender, EventArgs e)
        {
            Carrega_Envio("", "", true, true, true, true, false, true, true, true );
        }

        private void Carrega_Envio(String strID, String strRastreio, bool blMercadoLivre, bool blB2W, bool blMagalu, bool blMLPlace, bool blCorreios, bool blTotalExpress, bool blSequoia, bool blAmazon)
        {
            MySqlCommand cmd;
            int i = 0;
            int iML = 0;
            int iB2W = 0;
            int iMLPlace = 0;
            int iTotalExpress = 0;
            int iSequoia = 0;
            int iAmazon = 0;

            int iB2WFS = 0;
            int iB2WAJ = 0;
            int iB2WBS = 0;
            int iB2WME = 0;

            int iAmazonFS = 0;
            int iAmazonAJ = 0;
            int iAmazonBS = 0;
            int iAmazonME = 0;

            int iMagaFS = 0;
            int iMagaAJ = 0;
            int iMagaBS = 0;
            int iMagaME = 0;

            int iMagalu = 0;


            int iMPFS = 0;
            int iMPAJ = 0;
            int iMPBS = 0;
            int iMPME = 0;


            string query2;

            string query = "SELECT ENVIO.ID, ITEM.ID IDITEM, ITEM.rastreio, ITEM.data, ITEM.plataforma, ITEM.SEDEX, ENVIO.data_criacao, ENVIO.data_impressao, ENVIO.MERCADOLIVRE, ENVIO.B2W, ENVIO.AMAZON, ENVIO.MAGALU, PEDIDO.LOJA, PEDIDO.CONTALOJA, ENVIO.mlplace, ENVIO.totalexpress, ENVIO.sequoia  ";

            query = query + " FROM ENVIO_CORREIOS ENVIO ";
            query = query + " LEFT JOIN ITENS_ENVIO_CORREIOS ITEM ";
            query = query + " ON ITEM.ID_ENVIO_CORREIOS = ENVIO.ID ";


            query = query + " LEFT JOIN PEDIDOS_INTEGRADOS PEDIDO ";
            query = query + " ON PEDIDO.id_pedido = ITEM.increment_id ";

            if (strID.Length > 0)
            {
                query = query + " WHERE ENVIO.MERCADOLIVRE = 0 AND ENVIO.ID = " + strID + "; ";
            }
            else if (strRastreio.Length > 0)
            {
                query = query + " WHERE ITEM.rastreio = '" + strRastreio + "' ";
            }
            else
            {
                query = query + " WHERE ENVIO.DATA_IMPRESSAO IS NULL ";
            }

            query2 = "";

            if (blMercadoLivre == false || blB2W == false || blMagalu == false || blMLPlace == false || blTotalExpress == false || blSequoia == false || blAmazon == false)
            {
                if (blMercadoLivre == true)
                {
                    query2 = " and ( mercadolivre = 1 ";
                }
                if (blB2W == true)
                {
                    if (query2 == "")
                    {
                        query2 = " and ( b2w = 1 ";
                    }
                    else
                    {
                        query2 = query2 + " or b2w = 1 ";
                    }
                }

                if (blAmazon == true)
                {
                    if (query2 == "")
                    {
                        query2 = " and ( amazon = 1 ";
                    }
                    else
                    {
                        query2 = query2 + " or amazon = 1 ";
                    }

                }

                if (blMagalu == true)
                {
                    if (query2 == "")
                    {
                        query2 = " and ( magalu = 1 ";
                    }
                    else
                    {
                        query2 = query2 + " or magalu = 1 ";
                    }

                }
                if (blMLPlace == true)
                {
                    if (query2 == "")
                    {
                        query2 = " and ( mlplace = 1 ";
                    }
                    else
                    {
                        query2 = query2 + " or mlplace = 1 ";
                    }
                }
                if (blTotalExpress == true)
                {
                    if (query2 == "")
                    {
                        query2 = " and ( totalexpress = 1 ";
                    }
                    else
                    {
                        query2 = query2 + " or totalexpress = 1 ";
                    }
                }

                if (blSequoia == true)
                {
                    if (query2 == "")
                    {
                        query2 = " and ( sequoia = 1 ";
                    }
                    else
                    {
                        query2 = query2 + " or sequoia = 1 ";
                    }
                }

                if (query2 != "")
                {
                    query2 = query2 + ")";
                }

            }
            else
            {
                if(blCorreios == true)
                {
                    query2 = " and ( mercadolivre <> 1 and mlplace <> 1 and magalu <> 1 and b2w <> 1 and mercadolivre <> 1 and amazon <> 1 ) ";
                }
                
            }

            query = query + query2;

            if (global.myDB.connection.State == ConnectionState.Closed)
            {
                global.myDB.OpenConnection();
            }

            cmd = new MySqlCommand(query, global.myDB.connection);

            try
            {
                dataReader = cmd.ExecuteReader();
            }
            catch (Exception)
            {
                // dataReader.Close();
                global.myDB.OpenConnection();
                cmd = new MySqlCommand(query, global.myDB.connection);
                dataReader = cmd.ExecuteReader();
            }

            //**************************** CORREIOS ************************************
            if ((blB2W == false && blMercadoLivre == false && blMLPlace == false && blMagalu == false && blTotalExpress == false && blSequoia == false && blAmazon == false) || (blB2W == true && blMercadoLivre == true && blMLPlace == true && blMagalu == true && blTotalExpress == true && blSequoia == true && blAmazon == true))
            {
                lstCorreios.Columns.Clear();
                lstCorreios.Items.Clear();

                lstCorreios.CheckBoxes = true;

                lstCorreios.Columns.Add("RASTREIO", 130);
                lstCorreios.Columns.Add("DATA/HORA", 115);
                lstCorreios.Columns.Add("PLATAFORMA", 100);
                lstCorreios.Columns.Add("SEDEX", 100);
                lstCorreios.Columns.Add("ID", 1);

                i = 0;
                btnNovo.Enabled = true;
                lblDataCriacao.Text = "DATA DE CRIAÇÃO: ";
                lblRegistros.Text = "0";
            }
            //**************************** MERCADO LIVRE ************************************
            if (blMercadoLivre == true)
            {
                lstMercadoLivre.Columns.Clear();
                lstMercadoLivre.Items.Clear();

                lstMercadoLivre.CheckBoxes = true;

                lstMercadoLivre.Columns.Add("RASTREIO", 130);
                lstMercadoLivre.Columns.Add("DATA/HORA", 115);
                lstMercadoLivre.Columns.Add("PLATAFORMA", 100);
                lstMercadoLivre.Columns.Add("SEDEX", 100);
                lstMercadoLivre.Columns.Add("ID", 1);

                iML = 0;
                cmdNovoMercadoLIvre.Enabled = true;
                lblDataCriacaoML.Text = "DATA DE CRIAÇÃO: ";
                lblRegistrosML.Text = "0";
            }
            //************************** B2W ******************************
            if (blB2W == true)
            {
                lstB2W.Columns.Clear();
                lstB2W.Items.Clear();

                lstB2W.CheckBoxes = true;

                lstB2W.Columns.Add("RASTREIO", 130);
                lstB2W.Columns.Add("DATA/HORA", 115);
                lstB2W.Columns.Add("PLATAFORMA", 100);
                lstB2W.Columns.Add("SEDEX", 100);
                lstB2W.Columns.Add("ID", 1);
                lstB2W.Columns.Add("LOJA", 100);
                lstB2W.Columns.Add("CONTALOJA", 100);

                iB2W = 0;
                cmdNovoB2W.Enabled = true;
                lblDataCriacaoB2W.Text = "DATA DE CRIAÇÃO: ";
                lblRegistrosB2W.Text = "0";
                lblRegistrosB2WFacilShopping.Text = "0";
                lblRegistrosB2WAlpha.Text = "0";
                lblRegistrosB2WBela.Text = "0";
                lblRegistrosB2WMundial.Text = "0";
            }
            //************************** MagaLu ******************************
            if (blMagalu == true)
            {
                lstMagalu.Columns.Clear();
                lstMagalu.Items.Clear();

                lstMagalu.CheckBoxes = true;

                lstMagalu.Columns.Add("RASTREIO", 130);
                lstMagalu.Columns.Add("DATA/HORA", 115);
                lstMagalu.Columns.Add("PLATAFORMA", 100);
                lstMagalu.Columns.Add("SEDEX", 100);
                lstMagalu.Columns.Add("ID", 1);

                iMagalu = 0;
                button8.Enabled = true;
                lblDataCriacaoMagalu.Text = "DATA DE CRIAÇÃO: ";
                lblRegistrosMagaLu.Text = "0";

                lblRegistrosMagaluFacilShopping.Text = "0";
                lblRegistrosMagaluAlpha.Text = "0";
                lblRegistrosMagaluBela.Text = "0";
                lblRegistrosMagaluMundial.Text = "0";

            }
            //**************************** MERCADO PLACE ************************************
            if (blMLPlace == true)
            {
                lstMlPlace.Columns.Clear();
                lstMlPlace.Items.Clear();

                lstMlPlace.CheckBoxes = true;

                lstMlPlace.Columns.Add("RASTREIO", 130);
                lstMlPlace.Columns.Add("DATA/HORA", 115);
                lstMlPlace.Columns.Add("PLATAFORMA", 100);
                lstMlPlace.Columns.Add("SEDEX", 100);
                lstMlPlace.Columns.Add("ID", 1);

                iMLPlace = 0;
                cmdNovoMercadoPlace.Enabled = true;
                lblDataCriacaoML.Text = "DATA DE CRIAÇÃO: ";
                lblRegistrosMercadoPlace.Text = "0";
            }
            //**************************** TotalExpress ************************************
            if (blTotalExpress == true)
            {
                lstTotalExpress.Columns.Clear();
                lstTotalExpress.Items.Clear();

                lstTotalExpress.CheckBoxes = true;

                lstTotalExpress.Columns.Add("RASTREIO", 130);
                lstTotalExpress.Columns.Add("DATA/HORA", 115);
                lstTotalExpress.Columns.Add("PLATAFORMA", 100);
                lstTotalExpress.Columns.Add("SEDEX", 100);
                lstTotalExpress.Columns.Add("ID", 1);

                iTotalExpress = 0;
               // cmdNovoTotalExpress.Enabled = true;
                lblDataCriacaoTotalExpress.Text = "DATA DE CRIAÇÃO: ";
                lblRegistrosTotalExpress.Text = "0";
            }
            //**************************** Sequoia ************************************
            if (blSequoia == true)
            {
                lstSequoia.Columns.Clear();
                lstSequoia.Items.Clear();

                lstSequoia.CheckBoxes = true;

                lstSequoia.Columns.Add("RASTREIO", 130);
                lstSequoia.Columns.Add("DATA/HORA", 115);
                lstSequoia.Columns.Add("PLATAFORMA", 100);
                lstSequoia.Columns.Add("SEDEX", 100);
                lstSequoia.Columns.Add("ID", 1);

                iSequoia = 0;
                //cmdNovoTotalExpress.Enabled = true;
                lblDataCriacaoSequoia.Text = "DATA DE CRIAÇÃO: ";
                lblRegistrosSequoia.Text = "0";
            }
            //************************** AMAZON ******************************
            //if (blAmazon == true)
            //{
            //    lstAmazon.Columns.Clear();
            //    lstAmazon.Items.Clear();

            //    lstAmazon.CheckBoxes = true;

            //    lstAmazon.Columns.Add("RASTREIO", 130);
            //    lstAmazon.Columns.Add("DATA/HORA", 115);
            //    lstAmazon.Columns.Add("PLATAFORMA", 100);
            //    lstAmazon.Columns.Add("SEDEX", 100);
            //    lstAmazon.Columns.Add("ID", 1);
            //    lstAmazon.Columns.Add("LOJA", 100);
            //    lstAmazon.Columns.Add("CONTALOJA", 100);

            //    iAmazon = 0;
            //    //cmdNovoB2W.Enabled = true;
            //    lblDataCriacaoAmazon.Text = "DATA DE CRIAÇÃO: ";
            //    lblRegistrosAmazon.Text = "0";
            //    lblRegistrosAmazonFS.Text = "0";
            //    lblRegistrosAmazonAJ.Text = "0";
            //    lblRegistrosAmazonBella.Text = "0";
            //    lblRegistrosAmazonMJ.Text = "0";
            //}

            //********************************************************

            while (dataReader.Read())
            {
                ListViewItem item;
                ListViewItem itemML;

                if (dataReader["mercadolivre"].ToString() == "1")
                {
                    txtNumeroEnvioML.Text = dataReader["id"].ToString();
                    lblDataCriacaoML.Text = "DATA DE CRIAÇÃO: " + dataReader["data_criacao"].ToString();
                   // cmdNovoMercadoLIvre.Enabled = false;
                }
                else if (dataReader["b2w"].ToString() == "1")
                {
                    txtNumeroEnvioB2W.Text = dataReader["id"].ToString();
                    lblDataCriacaoB2W.Text = "DATA DE CRIAÇÃO: " + dataReader["data_criacao"].ToString();
                   // cmdNovoB2W.Enabled = false;
                }
                else if (dataReader["MAGALU"].ToString() == "1")
                {
                    txtNumeroEnvioMagalu.Text = dataReader["id"].ToString();
                    lblDataCriacaoMagalu.Text = "DATA DE CRIAÇÃO: " + dataReader["data_criacao"].ToString();
                   // button8.Enabled = false;
                }
                else if (dataReader["mlplace"].ToString() == "1")
                {
                    txtNumeroEnvioMlPlace.Text = dataReader["id"].ToString();
                    lblDataCriacaoMercadoPlace.Text = "DATA DE CRIAÇÃO: " + dataReader["data_criacao"].ToString();
                   // cmdNovoMercadoPlace.Enabled = false;
                }
                else if (dataReader["totalexpress"].ToString() == "1")
                {
                    txtNumeroEnvioTotalExpress.Text = dataReader["id"].ToString();
                    lblDataCriacaoTotalExpress.Text = "DATA DE CRIAÇÃO: " + dataReader["data_criacao"].ToString();
                    //cmdNovoTotalExpress.Enabled = false;
                }
                else if (dataReader["sequoia"].ToString() == "1")
                {
                    txtNumeroEnvioSequoia.Text = dataReader["id"].ToString();
                    lblDataCriacaoSequoia.Text = "DATA DE CRIAÇÃO: " + dataReader["data_criacao"].ToString();
                   // cmdNovose.Enabled = false;
                }
                //else if (dataReader["AMAZON"].ToString() == "1")
                //{
                //    txtNumeroEnvioAmazon.Text = dataReader["id"].ToString();
                //    lblDataCriacaoAmazon.Text = "DATA DE CRIAÇÃO: " + dataReader["data_criacao"].ToString();
                //    //cmdNovoB2W.Enabled = false;
                //}
                else
                {
                    txtNumeroEnvio.Text = dataReader["id"].ToString();
                    lblDataCriacao.Text = "DATA DE CRIAÇÃO: " + dataReader["data_criacao"].ToString();
                   // btnNovo.Enabled = false;
                }

                if (dataReader["rastreio"].ToString() != "")
                {
                    if (dataReader["mercadolivre"].ToString() == "1")
                    {
                        iML = iML + 1;

                        itemML = new ListViewItem(new[] { dataReader["rastreio"].ToString(), dataReader["data"].ToString(), "MERCADO LIVRE", dataReader["SEDEX"].ToString(), dataReader["IDITEM"].ToString() });
                        lblRegistrosML.Text = Convert.ToString(iML);
                        lstMercadoLivre.Items.Add(itemML);
                    }
                    else if (dataReader["b2w"].ToString() == "1")
                    {
                        iB2W = iB2W + 1;
                        item = new ListViewItem(new[] { dataReader["rastreio"].ToString(), dataReader["data"].ToString(), dataReader["plataforma"].ToString(), dataReader["SEDEX"].ToString(), dataReader["IDITEM"].ToString() });
                        lblRegistrosB2W.Text = Convert.ToString(iB2W);

                        if (dataReader["CONTALOJA"].ToString() == "financeiro@facilcard.com.br")
                        {
                            iB2WFS = iB2WFS + 1;
                        }
                        else if (dataReader["CONTALOJA"].ToString() == "diretoria@facilcard.com.br" || dataReader["CONTALOJA"].ToString() == "alphajogos@facilshopping.com.br")
                        {
                            iB2WAJ = iB2WAJ + 1;
                        }
                        else if (dataReader["CONTALOJA"].ToString() == "midia@facilcard.com.br")
                        {
                            iB2WBS = iB2WBS + 1;
                        }
                        else if (dataReader["CONTALOJA"].ToString() == "comercial@mundialesportes.com.br")
                        {
                            iB2WME = iB2WME + 1;
                        }

                        lblRegistrosB2WFacilShopping.Text = Convert.ToString(iB2WFS);
                        lblRegistrosB2WAlpha.Text = Convert.ToString(iB2WAJ);
                        lblRegistrosB2WBela.Text = Convert.ToString(iB2WBS);
                        lblRegistrosB2WMundial.Text = Convert.ToString(iB2WME);

                        lstB2W.Items.Add(item);
                    }
                    else if (dataReader["magalu"].ToString() == "1")
                    {
                        iMagalu = iMagalu + 1;
                        item = new ListViewItem(new[] { dataReader["rastreio"].ToString(), dataReader["data"].ToString(), dataReader["plataforma"].ToString(), dataReader["SEDEX"].ToString(), dataReader["IDITEM"].ToString() });
                        lblRegistrosMagaLu.Text = Convert.ToString(iMagalu);
                        lstMagalu.Items.Add(item);

                        if (dataReader["CONTALOJA"].ToString() == "facilshopping1api")
                        {
                            iMagaFS = iMagaFS + 1;
                        }
                        else if (dataReader["CONTALOJA"].ToString() == "alphajogosapi" )
                        {
                            iMagaAJ = iMagaAJ + 1;
                        }
                        else if (dataReader["CONTALOJA"].ToString() == "belashoppingapi")
                        {
                            iMagaBS = iMagaBS + 1;
                        }
                        lblRegistrosMagaluFacilShopping.Text = Convert.ToString(iMagaFS);
                        lblRegistrosMagaluAlpha.Text = Convert.ToString(iMagaAJ);
                        lblRegistrosMagaluBela.Text = Convert.ToString(iMagaBS);
                        lblRegistrosMagaluMundial.Text = Convert.ToString(iMagaME);
                    }
                    //else if (dataReader["AMAZON"].ToString() == "1" && dataReader["CONTALOJA"].ToString() == "A121B4BOCPOFLG")
                    //{
                    //    iAmazon = iAmazon + 1;
                    //    item = new ListViewItem(new[] { dataReader["rastreio"].ToString(), dataReader["data"].ToString(), dataReader["plataforma"].ToString(), dataReader["SEDEX"].ToString(), dataReader["IDITEM"].ToString() });
                    //    lblRegistrosAmazon.Text = Convert.ToString(iAmazon);

                    //    if (dataReader["CONTALOJA"].ToString() == "financeiro@facilcard.com.br")
                    //    {
                    //       // iB2WFS = iB2WFS + 1;
                    //    }
                    //    else if (dataReader["CONTALOJA"].ToString() == "diretoria@facilcard.com.br" || dataReader["CONTALOJA"].ToString() == "alphajogos@facilshopping.com.br")
                    //    {
                    //       // iB2WAJ = iB2WAJ + 1;
                    //    }
                    //    else if (dataReader["CONTALOJA"].ToString() == "midia@facilcard.com.br")
                    //    {
                    //       // iB2WBS = iB2WBS + 1;
                    //    }
                    //    else if (dataReader["CONTALOJA"].ToString() == "Mundial Esportes")
                    //    {
                    //        iAmazonME = iAmazonME + 1;
                    //    }

                    //    lblRegistrosAmazonFS.Text = Convert.ToString(iAmazonFS);
                    //    lblRegistrosAmazonAJ.Text = Convert.ToString(iAmazonAJ);
                    //    lblRegistrosAmazonBella.Text = Convert.ToString(iAmazonBS);
                    //    lblRegistrosAmazonMJ.Text = Convert.ToString(iAmazonME);

                    //    lstAmazon.Items.Add(item);
                    //}

                    else if (dataReader["mlplace"].ToString() == "1")
                    {
                        iMLPlace = iMLPlace + 1;
                        item = new ListViewItem(new[] { dataReader["rastreio"].ToString(), dataReader["data"].ToString(), dataReader["plataforma"].ToString(), dataReader["SEDEX"].ToString(), dataReader["IDITEM"].ToString() });
                        lblRegistrosMercadoPlace.Text = Convert.ToString(iMLPlace);
                        lstMlPlace.Items.Add(item);

                        if (dataReader["CONTALOJA"].ToString() == "466531908")
                        {
                            iMPBS = iMPBS + 1;
                        }
                        else if (dataReader["CONTALOJA"].ToString() == "466524535")
                        {
                            iMPAJ = iMPAJ + 1;
                        }

                        else if (dataReader["CONTALOJA"].ToString() == "466533907")
                        {
                            iMPME = iMPME + 1;
                        }

                        lblRegistrosMPlaceFS.Text = Convert.ToString(iMPFS);
                        lblRegistrosMPlaceAlpha.Text = Convert.ToString(iMPAJ);
                        lblRegistrosMPlaceBela.Text = Convert.ToString(iMPBS);
                        lblRegistrosMPlaceMundial.Text = Convert.ToString(iMPME);
                    }
                    else if (dataReader["totalexpress"].ToString() == "1")
                    {
                        iTotalExpress = iTotalExpress + 1;
                        item = new ListViewItem(new[] { dataReader["rastreio"].ToString(), dataReader["data"].ToString(), dataReader["plataforma"].ToString(), dataReader["SEDEX"].ToString(), dataReader["IDITEM"].ToString() });
                        lblRegistrosTotalExpress.Text = Convert.ToString(iTotalExpress);
                        lstTotalExpress.Items.Add(item);
                    }
                    else if (dataReader["sequoia"].ToString() == "1")
                    {
                        iSequoia = iSequoia + 1;
                        item = new ListViewItem(new[] { dataReader["rastreio"].ToString(), dataReader["data"].ToString(), dataReader["plataforma"].ToString(), dataReader["SEDEX"].ToString(), dataReader["IDITEM"].ToString() });
                        lblRegistrosSequoia.Text = Convert.ToString(iSequoia);
                        lstSequoia.Items.Add(item);
                    }
                    
                    else
                    {
                        i = i + 1;
                        item = new ListViewItem(new[] { dataReader["rastreio"].ToString(), dataReader["data"].ToString(), dataReader["plataforma"].ToString(), dataReader["SEDEX"].ToString(), dataReader["IDITEM"].ToString() });
                        lblRegistros.Text = Convert.ToString(i);
                        lstCorreios.Items.Add(item);
                    }
                }
            }

            dataReader.Close();

            if (global.myDB.connection.State != ConnectionState.Closed)
            {
                global.myDB.CloseConnection();
            }

            query = " select count(*) contagem  from ITENS_ENVIO_CORREIOS where DATE_FORMAT(data, '%M %d %Y') = DATE_FORMAT(now(), '%M %d %Y');";

            if (global.myDB.connection.State == ConnectionState.Closed)
            {
                global.myDB.OpenConnection();
            }

            cmd = new MySqlCommand(query, global.myDB.connection);

            try
            {
                dataReader = cmd.ExecuteReader();
            }
            catch (Exception)
            {
                // dataReader.Close();
                global.myDB.OpenConnection();
                cmd = new MySqlCommand(query, global.myDB.connection);
                dataReader = cmd.ExecuteReader();
            }

            lblLidos.Text = "Pedidos Lidos Hoje: 0 ";

            while (dataReader.Read())
            {
                lblLidos.Text = "Pedidos Lidos Hoje: " + dataReader["contagem"].ToString();
            }

            dataReader.Close();

            if (global.myDB.connection.State != ConnectionState.Closed)
            {
                global.myDB.CloseConnection();
            }
        }

        private void label4_Click(object sender, EventArgs e)
        {

        }

        private void radioButton2_CheckedChanged(object sender, EventArgs e)
        {
            txtRastreio.Text = "";
            txtDados.Text = "";
            chkReenvio.Checked = false;
            chkIgnorarData.Checked = false;
            chkPedidoAvulso.Checked = false;
            txtDados.Focus();
        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }

        private void btnNovo_Click(object sender, EventArgs e)
        {

        }

        private void GravaNovoEnvio(bool blMercadoLivre, bool blB2W, bool blMagalu, bool blMLPlace, bool blTotalExpress, bool blSequoia, bool blAmazon)
        {
            int intID;
            String query;



            query = "INSERT INTO ENVIO_CORREIOS (data_criacao, mercadolivre, b2w, magalu, mlplace, shopee, amazon)" +
                   " VALUES ( '" + DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss") + "', " + blMercadoLivre + ", " + blB2W + ", " + blMagalu + ", " + blMLPlace + ", " + blTotalExpress + ", " + blSequoia + ", " + blAmazon + "); ";

            if (global.myDB.connection.State == ConnectionState.Closed)
            {
                global.myDB.OpenConnection();
            }

            MySqlCommand cmd1 = new MySqlCommand(query, global.myDB.connection);

            try
            {
                cmd1.ExecuteNonQuery();
                intID = global.BuscaID("ENVIO_CORREIOS") - 1;

                if (blMercadoLivre == true)
                {
                    txtNumeroEnvioML.Text = Convert.ToString(intID);
                    lblDataCriacaoML.Text = "DATA DE CRIAÇÃO: " + DateTime.Now.ToString("dd-MM-yyyy hh:mm:ss");
                }
                else if (blB2W == true)
                {
                    txtNumeroEnvioB2W.Text = Convert.ToString(intID);
                    lblDataCriacaoB2W.Text = "DATA DE CRIAÇÃO: " + DateTime.Now.ToString("dd-MM-yyyy hh:mm:ss");

                }
                else if (blMagalu == true)
                {
                    txtNumeroEnvioMagalu.Text = Convert.ToString(intID);
                    lblDataCriacaoMagalu.Text = "DATA DE CRIAÇÃO: " + DateTime.Now.ToString("dd-MM-yyyy hh:mm:ss");
                }
                else if (blMLPlace == true)
                {
                    txtNumeroEnvioMlPlace.Text = Convert.ToString(intID);
                    lblDataCriacaoMercadoPlace.Text = "DATA DE CRIAÇÃO: " + DateTime.Now.ToString("dd-MM-yyyy hh:mm:ss");
                }
                else if (blTotalExpress == true)
                {
                    txtNumeroEnvioTotalExpress.Text = Convert.ToString(intID);
                    lblDataCriacaoTotalExpress.Text = "DATA DE CRIAÇÃO: " + DateTime.Now.ToString("dd-MM-yyyy hh:mm:ss");
                }
                else if (blSequoia == true)
                {
                    txtNumeroEnvioSequoia.Text = Convert.ToString(intID);
                    lblDataCriacaoSequoia.Text = "DATA DE CRIAÇÃO: " + DateTime.Now.ToString("dd-MM-yyyy hh:mm:ss");
                }
                else if (blAmazon == true)
                {
                    txtNumeroEnvioAmazon.Text = Convert.ToString(intID);
                    lblDataCriacaoAmazon.Text = "DATA DE CRIAÇÃO: " + DateTime.Now.ToString("dd-MM-yyyy hh:mm:ss");
                }
                else
                {
                    txtNumeroEnvio.Text = Convert.ToString(intID);
                    lblDataCriacao.Text = "DATA DE CRIAÇÃO: " + DateTime.Now.ToString("dd-MM-yyyy hh:mm:ss");
                }


            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "PontoZero Informa");
            }

            if (global.myDB.connection.State != ConnectionState.Closed)
            {
                global.myDB.CloseConnection();
            }
        }

        private void GravaNovoItemEnvio(bool blML, bool blB2W, bool blMagalu, bool blMlPlace, bool blTotalExpress, bool blSequoia, bool blAmazon)
        {
            String query;
            String strRastreio;
            String strPedido = lblPedido.Text;
            Boolean blCorreio = false;

            if (blML == false && blB2W == false && blMagalu == false && blMlPlace == false && blTotalExpress == false && blSequoia == false && blAmazon == false)
            {
                blCorreio = true;
            }

            query = "INSERT INTO ITENS_ENVIO_CORREIOS (data, id_ENVIO_CORREIOS, rastreio, sedex, plataforma, increment_id)";
            query = query + " VALUES ( '" + DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss") + "', ";


            if (blB2W == true)
            {   
                lblRegistrosB2W.Text = Convert.ToString(Convert.ToInt32(lblRegistrosB2W.Text) + 1);

                query = query + txtNumeroEnvioB2W.Text + ", ";
            }
            else if (blAmazon == true)
            {
                lblRegistrosAmazon.Text = Convert.ToString(Convert.ToInt32(lblRegistrosAmazon.Text) + 1);

                query = query + txtNumeroEnvioAmazon.Text + ", ";
            }
            else if (blMagalu == true)
            {
                lblRegistrosMagaLu.Text = Convert.ToString(Convert.ToInt32(lblRegistrosMagaLu.Text) + 1);

                query = query + txtNumeroEnvioMagalu.Text + ", ";
            }
            else if (blML == true)
            {
                lblRegistrosML.Text = Convert.ToString(Convert.ToInt32(lblRegistrosML.Text) + 1);

                query = query + txtNumeroEnvioML.Text + ", ";
            }
            else if (blMlPlace == true)
            {
                lblRegistrosMercadoPlace.Text = Convert.ToString(Convert.ToInt32(lblRegistrosMercadoPlace.Text) + 1);
                query = query + txtNumeroEnvioMlPlace.Text + ", ";
            }
            else if (blTotalExpress == true)
            {
                lblRegistrosTotalExpress.Text = Convert.ToString(Convert.ToInt32(lblRegistrosTotalExpress.Text) + 1);
                query = query + txtNumeroEnvioTotalExpress.Text + ", ";
            }
            else if (blSequoia == true)
            {
                lblRegistrosSequoia.Text = Convert.ToString(Convert.ToInt32(lblRegistrosSequoia.Text) + 1);
                query = query + txtNumeroEnvioSequoia.Text + ", ";
            }
            else
            {
                lblRegistros.Text = Convert.ToString(Convert.ToInt32(lblRegistros.Text) + 1);
                query = query + txtNumeroEnvio.Text + ", ";
            }

            lblLidos.Text = "Pedidos Lidos Hoje: " + (Convert.ToInt32(lblLidos.Text.Substring(19)) + 1);

            query = query + "'" + txtRastreio.Text + "', ";

            strRastreio = txtRastreio.Text;

            if (radioButton6.Checked == false)
            {
                if (strRastreio.Substring(0, 1) == "P")
                {
                    query = query + " 'PAC', ";
                }
                else
                {
                    query = query + " 'SEDEX', ";
                }
            }
            else
            {
                query = query + " 'CARTA REGISTRADA', ";
            }

            query = query + "'" + lblPlataforma.Text + "' ";

            if (strPedido.Substring(0, 7) == "PEDIDO:")
            {
                query = query + ",'" + strPedido.Substring(8) + "'";
            }
            else
            {
                query = query + ",'" + strPedido.Substring(5) + "'";
            }


            query = query + " );";

            if (global.myDB.connection.State == ConnectionState.Closed)
            {
                global.myDB.OpenConnection();
            }

            MySqlCommand cmd1 = new MySqlCommand(query, global.myDB.connection);

            try
            {
                cmd1.ExecuteNonQuery();
                // Carrega_Envio("", "", blML, blB2W, blMagalu, blMlPlace, false, blTotalExpress, blSequoia, blAmazon);


            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "PontoZero Informa");
            }

            if (global.myDB.connection.State != ConnectionState.Closed)
            {
                global.myDB.CloseConnection();
            }
        }

        private void txtRastreio_TextChanged(object sender, EventArgs e)
        {

        }

        private void ExcluiRastreio()
        {
            if (MessageBox.Show("Confirma a exclusão dos itens selecionados?", "PontoZero Informa", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1) == DialogResult.Yes)
            {
                if (lstCorreios.Items.Count > 0)
                {
                    try
                    {
                        foreach (ListViewItem item in lstCorreios.Items)
                        {
                            if (item.Checked == true)
                            {
                                String query;

                                global.myDB.CloseConnection();

                                query = "DELETE FROM ITENS_ENVIO_CORREIOS WHERE ID = " + item.SubItems[4].Text;

                                if (global.myDB.connection.State == ConnectionState.Closed)
                                {
                                    global.myDB.OpenConnection();
                                }

                                MySqlCommand cmd1 = new MySqlCommand(query, global.myDB.connection);

                                try
                                {
                                    cmd1.ExecuteNonQuery();
                                }
                                catch (Exception ex)
                                {
                                    MessageBox.Show(ex.Message, "PontoZero Informa");
                                }
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show("Erro : " + ex.Message, "PontoZero Informa");
                    }
                }
               //Carrega_Envio("", "", false, false, false, false, false, false, false, false);
            }

            if (global.myDB.connection.State != ConnectionState.Closed)
            {
                global.myDB.CloseConnection();
            }
        }

        private void ExcluiRastreioML()
        {
            if (MessageBox.Show("Confirma a exclusão dos itens selecionados do Mercado Livre?", "PontoZero Informa", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1) == DialogResult.Yes)
            {
                if (lstMercadoLivre.Items.Count > 0)
                {
                    try
                    {
                        foreach (ListViewItem item in lstMercadoLivre.Items)
                        {
                            if (item.Checked == true)
                            {
                                String query;

                                global.myDB.CloseConnection();

                                query = "DELETE FROM ITENS_ENVIO_CORREIOS WHERE ID = " + item.SubItems[4].Text;

                                if (global.myDB.connection.State == ConnectionState.Closed)
                                {
                                    global.myDB.OpenConnection();
                                }

                                MySqlCommand cmd1 = new MySqlCommand(query, global.myDB.connection);

                                try
                                {
                                    cmd1.ExecuteNonQuery();
                                }
                                catch (Exception ex)
                                {
                                    MessageBox.Show(ex.Message, "PontoZero Informa");
                                }
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show("Erro : " + ex.Message, "PontoZero Informa");
                    }
                }
               // Carrega_Envio("", "", true, false, false, false, true, false, false, false);
            }

            if (global.myDB.connection.State != ConnectionState.Closed)
            {
                global.myDB.CloseConnection();
            }
        }


        private void ExcluiRastreioAmazon()
        {
            if (MessageBox.Show("Confirma a exclusão dos itens selecionados da Amazon?", "PontoZero Informa", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1) == DialogResult.Yes)
            {
                if (lstAmazon.Items.Count > 0)
                {
                    try
                    {
                        foreach (ListViewItem item in lstAmazon.Items)
                        {
                            if (item.Checked == true)
                            {
                                String query;

                                global.myDB.CloseConnection();

                                query = "DELETE FROM ITENS_ENVIO_CORREIOS WHERE ID = " + item.SubItems[4].Text;

                                if (global.myDB.connection.State == ConnectionState.Closed)
                                {
                                    global.myDB.OpenConnection();
                                }

                                MySqlCommand cmd1 = new MySqlCommand(query, global.myDB.connection);

                                try
                                {
                                    cmd1.ExecuteNonQuery();
                                }
                                catch (Exception ex)
                                {
                                    MessageBox.Show(ex.Message, "PontoZero Informa");
                                }
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show("Erro : " + ex.Message, "PontoZero Informa");
                    }
                }
               // Carrega_Envio("", "", false, false, false, false, true, false, false, true);
            }

            if (global.myDB.connection.State != ConnectionState.Closed)
            {
                global.myDB.CloseConnection();
            }
        }

        private void ExcluiRastreioB2W()
        {
            if (MessageBox.Show("Confirma a exclusão dos itens selecionados da B2W?", "PontoZero Informa", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1) == DialogResult.Yes)
            {
                if (lstB2W.Items.Count > 0)
                {
                    try
                    {
                        foreach (ListViewItem item in lstB2W.Items)
                        {
                            if (item.Checked == true)
                            {
                                String query;

                                global.myDB.CloseConnection();

                                query = "DELETE FROM ITENS_ENVIO_CORREIOS WHERE ID = " + item.SubItems[4].Text;

                                if (global.myDB.connection.State == ConnectionState.Closed)
                                {
                                    global.myDB.OpenConnection();
                                }

                                MySqlCommand cmd1 = new MySqlCommand(query, global.myDB.connection);

                                try
                                {
                                    cmd1.ExecuteNonQuery();
                                }
                                catch (Exception ex)
                                {
                                    MessageBox.Show(ex.Message, "PontoZero Informa");
                                }
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show("Erro : " + ex.Message, "PontoZero Informa");
                    }
                }
               // Carrega_Envio("", "", false, true, false, false, true, false, false, false);
            }

            if (global.myDB.connection.State != ConnectionState.Closed)
            {
                global.myDB.CloseConnection();
            }
        }

        private void ExcluiRastreioMLPlace()
        {
            if (MessageBox.Show("Confirma a exclusão dos itens selecionados da Mercado Place?", "PontoZero Informa", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1) == DialogResult.Yes)
            {
                if (lstMlPlace.Items.Count > 0)
                {
                    try
                    {
                        foreach (ListViewItem item in lstMlPlace.Items)
                        {
                            if (item.Checked == true)
                            {
                                String query;

                                global.myDB.CloseConnection();

                                query = "DELETE FROM ITENS_ENVIO_CORREIOS WHERE ID = " + item.SubItems[4].Text;

                                if (global.myDB.connection.State == ConnectionState.Closed)
                                {
                                    global.myDB.OpenConnection();
                                }

                                MySqlCommand cmd1 = new MySqlCommand(query, global.myDB.connection);

                                try
                                {
                                    cmd1.ExecuteNonQuery();
                                }
                                catch (Exception ex)
                                {
                                    MessageBox.Show(ex.Message, "PontoZero Informa");
                                }
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show("Erro : " + ex.Message, "PontoZero Informa");
                    }
                }
               // Carrega_Envio("", "", false, false, false, true, true, false, false, false);
            }

            if (global.myDB.connection.State != ConnectionState.Closed)
            {
                global.myDB.CloseConnection();
            }
        }

        private void txtRastreio_KeyPress(object sender, KeyPressEventArgs e)
        {
            //if (blTempoDig == false)
            //{
            //    timeDigitacao.Enabled = true;
            //}
            if(e.KeyChar == 13)
            {
                if (chkPedidoAvulso.Checked == false)
                {
                    RastreioDigitado();
                }
                else
                {
                    txtDados.Focus();
                }

                    
            }
            
        }

        private void RastreioDigitado()
        {
            String strDigitado;
            bool blMercadoLivre = false;
            bool blContinua = false;
            bool blB2W = false;
            bool blMagalu = false;
            bool blMlPlace = false;
            bool blErro = false;
            bool blTotalExpress = false;
            bool blSequoia = false;
            bool blAmazon = false;


            strDigitado = txtRastreio.Text;

            if (chkBuscar.Checked == false)
            {
                if (txtRastreio.TextLength > 3)
                {
                    blPressiona = false;
                    blTempoDig = false;
                    timeDigitacao.Enabled = false;

                    if (strDigitado.Substring(strDigitado.Length - 2, 1) == "B")
                    {
                        blMercadoLivre = false;
                    }
                    else if (strDigitado.Substring(0, 2) == "TE" || strDigitado.Substring(0, 2) == "TX")
                    {
                        blMercadoLivre = false;
                    }
                    else
                    {
                        blMercadoLivre = true;
                    }

                    blContinua = false;

                    if (global.BuscaCampo("ITENS_ENVIO_CORREIOS", "RASTREIO", "RASTREIO", txtRastreio.Text) != txtRastreio.Text)
                    {
                        blContinua = true;
                    }
                    else
                    {
                        if (MessageBox.Show("Esse Código de Rastreio já foi registrado em outro momento, confirma a inclusão mesmo assim?", "PontoZero Informa", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1) == DialogResult.Yes)
                        {
                            //DIGITAR SENHA
                            blContinua = true;
                        }
                    }

                    if (blContinua == true)
                    {


                        if (global.BuscaCampo("PEDIDOS_INTEGRADOS", "cd_rastreio", "cd_rastreio", txtRastreio.Text) == txtRastreio.Text)
                        {
                            if (Carrega_Pedido(txtRastreio.Text) == true)
                            {
                                if (txtRastreio.Text == "")
                                {
                                    txtRastreio.Text = txtDados.Text;
                                }

                                if (lblPlataforma.Text == "B2W")
                                {
                                    if (txtNumeroEnvioB2W.TextLength == 0)
                                    {
                                        MessageBox.Show("Não foi aberto o lote do B2W ainda", "PontoZero Informa");
                                        blErro = true;
                                    }
                                    blB2W = true;
                                }

                                //if (lblPlataforma.Text == "Amazon" && lblLoja.Text == "Mundial Esportes")
                                //{
                                //    if (txtNumeroEnvioAmazon.TextLength == 0)
                                //    {
                                //        MessageBox.Show("Não foi aberto o lote da Amazon ainda", "PontoZero Informa");
                                //        blErro = true;
                                //    }
                                //    blAmazon = true;
                                //}

                                if (lblPlataforma.Text == "MAGAZINE LUIZA")
                                {
                                    if (txtNumeroEnvioMagalu.TextLength == 0)
                                    {
                                        MessageBox.Show("Não foi aberto o lote do MagaLu ainda", "PontoZero Informa");
                                        blErro = true;
                                    }

                                    blMagalu = true;
                                }

                                if (blMercadoLivre == true && (lblPlataforma.Text == "Mercado Livre" || lblPlataforma.Text == "MERCADOLIVRE"))
                                {
                                    if (txtNumeroEnvioML.TextLength == 0 && (lblLoja.Text == "FÁCIL_SHOPPING" || lblLoja.Text == "FÁCIL-SHOPPING"))
                                    {
                                        MessageBox.Show("Não foi aberto o lote do Mercado Livre ainda", "PontoZero Informa");
                                        blErro = true;
                                    }

                                    if (txtNumeroEnvioMlPlace.TextLength == 0 && (lblLoja.Text == "ALPHAJOGOSLTDA" || lblLoja.Text == "SHOPPINGBELA" || lblLoja.Text == "MUNDIAL_ESPORTES"))
                                    {
                                        MessageBox.Show("Não foi aberto o lote do Mercado Place  ainda", "PontoZero Informa");
                                        blErro = true;
                                    }

                                    if (lblLoja.Text == "FÁCIL_SHOPPING" || lblLoja.Text == "FÁCIL-SHOPPING")
                                    {
                                        blMercadoLivre = true;
                                    }
                                    else if (lblLoja.Text == "ALPHAJOGOSLTDA" || lblLoja.Text == "SHOPPINGBELA" || lblLoja.Text == "MUNDIAL_ESPORTES")
                                    {
                                        blMlPlace = true;
                                        blMercadoLivre = false;
                                    }
                                    else if (lblLoja.Text == "")
                                    {
                                        if (MessageBox.Show("Esse pedido é do Mercado Coleta " + lblPedido.Text + "!!?? ", "PontoZero Informa", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1) == DialogResult.Yes)
                                        {
                                            blMercadoLivre = true;
                                        }
                                        else
                                        {
                                            blMercadoLivre = false;
                                            if (MessageBox.Show("Esse pedido é do Mercado Place " + lblPedido.Text + "!!?? ", "PontoZero Informa", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1) == DialogResult.Yes)
                                            {
                                                blMlPlace = true;
                                            }
                                            else
                                            {
                                                blMlPlace = false;
                                            }
                                        }
                                    }
                                    else
                                    {
                                        blMercadoLivre = false;
                                    }


                                }
                                else
                                {
                                    blMercadoLivre = false;
                                }

                                if (lblPlataforma.Text == "MERCADO PLACE")
                                {
                                    if (txtNumeroEnvioMlPlace.TextLength == 0)
                                    {
                                        MessageBox.Show("Não foi aberto o lote do Mercado Place ainda", "PontoZero Informa");
                                        blErro = true;
                                    }

                                    blMlPlace = true;
                                }

                                if (lblPlataforma.Text == "SHOPEE")
                                {
                                    if (txtRastreio.Text.Substring(txtRastreio.Text.Length - 2, 2) != "BR")
                                    {
                                        if (txtRastreio.Text.Substring(0, 2) == "TE" || txtRastreio.Text.Substring(0, 2) == "TX")
                                        {
                                            if (txtNumeroEnvioTotalExpress.TextLength == 0)
                                            {
                                                MessageBox.Show("Não foi aberto o lote da TotalExpress ainda", "PontoZero Informa");
                                                blErro = true;
                                            }

                                            blTotalExpress = true;
                                        }
                                        if (txtRastreio.Text.Substring(0, 2) == "BR")
                                        {
                                            if (txtNumeroEnvioSequoia.TextLength == 0)
                                            {
                                                MessageBox.Show("Não foi aberto o lote da Sequoia ainda", "PontoZero Informa");
                                                blErro = true;
                                            }

                                            blSequoia = true;
                                        }
                                    }
                                }

                                if (blMercadoLivre == false && blB2W == false && blMagalu == false && blMlPlace == false && blTotalExpress == false && blSequoia == false && blAmazon == false)
                                {
                                    if (txtNumeroEnvio.TextLength == 0)
                                    {
                                        MessageBox.Show("Não foi aberto o lote do Correio ainda", "PontoZero Informa");
                                        blErro = true;
                                    }

                                }

                                if (blErro == false)
                                {
                                    GravaNovoItemEnvio(blMercadoLivre, blB2W, blMagalu, blMlPlace, blTotalExpress, blSequoia, blAmazon);
                                    global.AtualizaStatus("Aguardando Retirada", lblPedido.Text.Substring(8));
                                }

                                txtRastreio.Text = "";
                                txtDados.Text = "";
                                chkEnvioCorreio.Checked = false;
                                chkReenvio.Checked = false;
                                chkIgnorarData.Checked = false;
                                chkPedidoAvulso.Checked = false;
                                txtRastreio.Focus();
                            }
                        }
                        else
                        {
                            //MessageBox.Show("BUSCA CEP", "PontoZero Informa");
                            txtDados.Text = "";
                            lblPedido.Text = "";
                            lblCliente.Text = "";
                            lblPlataforma.Text = "";
                            lblLoja.Text = "";
                            chkReenvio.Checked = false;
                            chkIgnorarData.Checked = false;
                            chkPedidoAvulso.Checked = false;


                            txtDados.Focus();
                        }

                    }
                    else
                    {
                        //MessageBox.Show("MERCADOLIVRE", "PontoZero Informa");
                    }
                }



            }
            else
            {
                if (txtRastreio.TextLength > 3)
                {
                    //Carrega_Envio("", txtRastreio.Text);
                }
            }

            if (global.myDB.connection.State != ConnectionState.Closed)
            {
                global.myDB.CloseConnection();
            }
        }

        private bool Busca_Grid(String strRastreio)
        {
            if (lstCorreios.Items.Count > 0)
            {
                try
                {
                    foreach (ListViewItem item in lstCorreios.Items)
                    {
                        if (item.SubItems[0].Text == strRastreio)
                        {
                            MessageBox.Show("Código de rastreio já incluído nesse envio!!", "PontoZero Informa");
                            return true;
                        }
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Erro : " + ex.Message, "PontoZero Informa");
                }
            }

            return false;
        }

        private void btnExcluir_Click(object sender, EventArgs e)
        {

        }

        private void btnImprimir_Click(object sender, EventArgs e)
        {
        }

        private void Gera_Correio()
        {

            if (lstCorreios.Items.Count > 0)
            {
                global.PreencherCorreio(Application.StartupPath + "\\" + "Requisição Correio.docx", txtNumeroEnvio.Text, lstCorreios.Items.Count);
            }

        }

        private void AtualizaEnvio(string strEnvio)
        {
            String query = "UPDATE ENVIO_CORREIOS SET data_impressao = '" + DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss") + "' WHERE ID = " + strEnvio + "; ";

            global.myDB.CloseConnection();

            if (global.myDB.connection.State == ConnectionState.Closed)
            {
                global.myDB.OpenConnection();
            }

            MySqlCommand cmd1 = new MySqlCommand(query, global.myDB.connection);

            try
            {
                cmd1.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "PontoZero Informa");
            }

            if (global.myDB.connection.State != ConnectionState.Closed)
            {
                global.myDB.CloseConnection();
            }
        }

        private void txtNumeroEnvio_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtNumeroEnvio_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((e.KeyChar == 13) && txtNumeroEnvio.TextLength > 0)
            {
                //Carrega_Envio(txtNumeroEnvio.Text, "");
            }
        }

        private void chkBuscar_CheckedChanged(object sender, EventArgs e)
        {
            txtNumeroEnvio.Enabled = chkBuscar.Checked;
            btnNovo.Enabled = !chkBuscar.Checked;
        }

        private void radioButton1_CheckedChanged(object sender, EventArgs e)
        {
            txtRastreio.Text = "";
            txtDados.Text = "";
            txtDados.Focus();
        }

        private void radioButton3_CheckedChanged(object sender, EventArgs e)
        {
            txtRastreio.Text = "";
            txtDados.Text = "";
            txtDados.Focus();
        }

        private void radioButton4_CheckedChanged(object sender, EventArgs e)
        {
            txtRastreio.Text = "";
            txtDados.Text = "";
            txtDados.Focus();
        }

        private void radioButton5_CheckedChanged(object sender, EventArgs e)
        {
            txtRastreio.Text = "";
            txtDados.Text = "";
            txtDados.Focus();
        }

        private void radioButton6_CheckedChanged(object sender, EventArgs e)
        {
            txtRastreio.Text = "";
            txtDados.Text = "";
            txtDados.Focus();
        }

        private void lstCorreios_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void txtDados_KeyPress(object sender, KeyPressEventArgs e)
        {
            lblPedido.Text = "";
            lblCliente.Text = "";
            lblPlataforma.Text = "";
            lblLoja.Text = "";

            if (e.KeyChar.ToString() == "-")
            {
                e.KeyChar = '\0';
            }

            //if (txtDados.TextLength > 6)
            //{
            //    Carrega_CEP(txtDados.Text + e.KeyChar);
            //    e.KeyChar = '\0';
            //    //txtRastreio.Text = "";
            //    //txtRastreio.Focus();
            //}

            if (e.KeyChar == 13)
            {
                Carrega_CEP(txtDados.Text);
            }
        }

        private void Carrega_CEP(String strCEP)
        {
            MySqlCommand cmd;
            bool blExibe;
            string strMSG;
            bool blB2W = false;
            bool blMagalu = false;
            bool blMlPlace = false;
            bool blMercadoLivre = false;
            bool blTotalExpress = false;
            bool blSequoia = false;
            bool blAmazon = false;

            bool blErro = false;

            string strPedido = "", strLoja = "", strCanal = "", strConta = "", strNome = "", strSobrenome = "", strPrimeiroNome = "", strNomeMeio = "", strUltimoNome = "";

            blExibe = false;

            if (chkPedidoAvulso.Checked == false)
            {
                string query = " SELECT DISTINCT'LOJA' LOJA, 'LOJA' CONTALOJA, VENDA.CANAL, VENDA.increment_id, VENDA.canal_id , VENDA.customer_firstname, VENDA.customer_middlename, VENDA.customer_lastname, OA.firstname, OA.lastname";

                query = query + " , '' nf, '' serie, '' chave, now() dt_emissao, '' endereco, '' complemento, ' ' bairro, '' cep, '' cidade, '' uf ";

                query = query + " , (  SELECT DISTINCT count(*) ";
                query = query + " FROM sales_flat_order VENDA ";

                query = query + " LEFT JOIN customer_address_entity CE  ON CE.parent_id = VENDA.customer_id ";
                query = query + " LEFT JOIN customer_address_entity_varchar CV  ON CE.entity_id = CV.entity_id AND CV.attribute_id = 30 ";
                query = query + " INNER JOIN sales_flat_order_address OA ON OA.parent_id = VENDA.entity_id ";

                query = query + " WHERE OA.address_type = 'shipping' AND (CV.VALUE = '" + strCEP + "' OR OA.postcode = '" + strCEP + "'";
                query = query + " OR CV.VALUE = '" + strCEP.Substring(0, 5) + "-" + strCEP.Substring(5, 3) + "' OR OA.postcode = '" + strCEP.Substring(0, 5) + "-" + strCEP.Substring(5, 3) + "')";

                query = query + " AND VENDA.STATE NOT IN('canceled', 'closed', 'holded', '') ";// and VENDA.status <> 'complete_shipped' ";
                query = query + " and(VENDA.status <> 'complete' or(VENDA.STATUS IS NULL AND VENDA.STATE = 'processing') or ((VENDA.status = 'complete' and  VENDA.CANAL in ( 'SUBMARINO', 'SHOPTIME', 'LOJASAMERICANAS', 'Loja', 'MagazineLuiza', 'Cnova', 'madeiramadeira', 'Carrefour', 'Dafiti')) or (VENDA.status is null and  VENDA.CANAL in ('Amazon.com.br','MercadoLivre', 'LOJASAMERICANAS'))) )  and VENDA.created_at > (now() - 90000000) ";

                query = query + " ) contador, VENDA.separado ";

                query = query + " FROM sales_flat_order VENDA";
                query = query + " LEFT JOIN customer_address_entity CE  ON CE.parent_id = VENDA.customer_id";

                query = query + " LEFT JOIN customer_address_entity_varchar CV  ON CE.entity_id = CV.entity_id AND CV.attribute_id = 30";
                query = query + " INNER JOIN sales_flat_order_address OA ON OA.parent_id = VENDA.entity_id";

                query = query + " WHERE OA.address_type = 'shipping' AND (CV.VALUE = '" + strCEP + "' OR OA.postcode = '" + strCEP + "'";
                query = query + " OR CV.VALUE = '" + strCEP.Substring(0, 5) + "-" + strCEP.Substring(5, 3) + "' OR OA.postcode = '" + strCEP.Substring(0, 5) + "-" + strCEP.Substring(5, 3) + "')";

                query = query + " AND VENDA.STATE NOT IN('canceled', 'closed', 'holded', '') "; // and VENDA.status <> 'complete_shipped'";

                if (chkReenvio.Checked == false)
                {
                    // query = query + " and VENDA.increment_id not in (Select increment_id from ITENS_ENVIO_CORREIOS where increment_id = VENDA.increment_id) ";
                }

                query = query + " and(VENDA.status <> 'complete' or(VENDA.STATUS IS NULL AND VENDA.STATE = 'processing') or ((VENDA.status = 'complete' and  VENDA.CANAL in ( 'SUBMARINO', 'SHOPTIME', 'LOJASAMERICANAS', 'Loja', 'MagazineLuiza', 'Cnova', 'madeiramadeira', 'Carrefour', 'Dafiti')) or (VENDA.status is null and  VENDA.CANAL in ('Carrefour', 'Amazon.com.br','MercadoLivre', 'LOJASAMERICANAS'))) ) ";

                if (chkIgnorarData.Checked == false)
                {
                    query = query + " and VENDA.created_at > (now() - 90000000) ";
                }


                //*************************************** UNION LUDOSTORE ******************************************
                //query = query + " UNION ";
                //query = query + " SELECT  NOW() dt_emissao, 'LUDOSTORE' LOJA, 'LUDOSTORE' CONTALOJA, 'LUDOSTORE' CANAL, id_pedido INCREMENT_ID, id_pedido CANAL_ID, nome CUSTOMER_FIRSTNAME, '' CUSTOMER_MIDDLENAME, '' CUSTOMER_LASTNAME, nome FIRSTNAME, '' LASTNAME, 1 CONTADOR ";
                //query = query + " from PEDIDO_LUDOSTORE ";
                //query = query + " WHERE STATUS NOT IN('ENTREGUE', 'ENVIADO', 'PAGAMENTO RECUSADO', 'CRIADO') ";

                //if (chkIgnorarData.Checked == false)
                //{
                //    query = query + " and dt_pedido > (date_sub(now(), interval 5 day)) ";
                //}


                //query = query + " AND (CEP = '" + strCEP + "' OR CEP = '" + strCEP.Substring(0, 5) + "-" + strCEP.Substring(5, 3) + "')";

                //if (chkReenvio.Checked == false)
                //{
                //    query = query + " and id_pedido not in (Select increment_id from ITENS_ENVIO_CORREIOS where increment_id = id_pedido) ";
                //}

                //****************************************** UNION IDERIS ************************************

                query = query + " union ";

                query = query + " SELECT  LOJA, CONTALOJA, replace(PEDIDOS_INTEGRADOS.marketplace ,'B2W - ','') CANAL, id_pedido INCREMENT_ID, cod_pedido CANAL_ID, nome CUSTOMER_FIRSTNAME, '' CUSTOMER_MIDDLENAME, '' CUSTOMER_LASTNAME, nome FIRSTNAME, ";
                query = query + " '' LASTNAME,  ";

                query = query + " nf, serie, chave, dt_emissao, endereco, complemento, bairro, cep, cidade, uf, ";

                query = query + " (SELECT COUNT(*) from PEDIDOS_INTEGRADOS ";
                query = query + " WHERE STATUS NOT IN('Pagamento cancelado') ";

                if (strCEP.Length == 9)
                {
                    query = query + " AND (CEP = '" + strCEP + "' OR CEP = '" + strCEP.Substring(0, 5) + "-" + strCEP.Substring(5, 3) + "' or CEP = '" + strCEP.Substring(0, 8) + "' )";
                }
                else
                {
                    query = query + " AND (CEP = '" + strCEP + "' OR CEP = '" + strCEP.Substring(0, 5) + "-" + strCEP.Substring(5, 3) + "')";
                }

                query = query + " and id_pedido not in (Select increment_id from ITENS_ENVIO_CORREIOS where increment_id = id_pedido)  ";

                query = query + ") contador, PEDIDOS_INTEGRADOS.separado";


                query = query + " from PEDIDOS_INTEGRADOS ";

                query = query + " WHERE STATUS NOT IN('Pagamento cancelado')  "; // and dt_pedido > (now() - 90000000) ";
                                                                                 // query = query + " AND PEDIDOS_INTEGRADOS.marketplace NOT LIKE '%MERCADO%' ";
                query = query + " AND (CEP = '" + strCEP.Substring(0, 8) + "' OR CEP = '" + strCEP + "' OR CEP = '" + strCEP.Substring(0, 5) + "-" + strCEP.Substring(5, 3) + "')";

                if (chkReenvio.Checked == false)
                {
                    query = query + " and id_pedido not in (Select increment_id from ITENS_ENVIO_CORREIOS where increment_id = id_pedido)";
                }

                query = query + " order by contador desc ;";
                //*****************************************************************************************************


                if (global.myDB.connection.State == ConnectionState.Closed)
                {
                    global.myDB.OpenConnection();
                }

                cmd = new MySqlCommand(query, global.myDB.connection);

                if (dataReader.IsClosed == false)
                {
                    dataReader.Close();
                }
                try
                {
                    dataReader = cmd.ExecuteReader();
                }
                catch (Exception)
                {
                    global.myDB.OpenConnection();
                    cmd = new MySqlCommand(query, global.myDB.connection);
                    dataReader = cmd.ExecuteReader();
                }

                frmCEPEncontrado frmPedEncontrados = new frmCEPEncontrado();
                frmPedEncontrados.dataReaderPedidos = dataReader;
                frmPedEncontrados.ShowDialog(this);

                blExibe = false;

                if (frmPedEncontrados.strPedido != "")
                {
                    //DIGITAR SENHA VALIDA SE CONFERIDO
                    blExibe = true;
                    strPedido = frmPedEncontrados.strPedido;
                    strLoja = frmPedEncontrados.strLoja;
                    strCanal = frmPedEncontrados.strCanal;
                    strConta = frmPedEncontrados.strConta;
                    strNome = frmPedEncontrados.strNome;
                    strSobrenome = frmPedEncontrados.strSobrenome;
                    strPrimeiroNome = frmPedEncontrados.strPrimeiroNome;
                    strNomeMeio = frmPedEncontrados.strNomeMeio;
                    strUltimoNome = frmPedEncontrados.strUltimoNome;

                    lblPedido.Text = "PEDIDO: " + strPedido;

                    lblLoja.Text = strLoja;

                    if (strCanal == "MERCADOLIVRE")
                    {
                        lblPlataforma.Text = "MERCADOLIVRE";
                        tabPrincipal.SelectedTab = tabPage2;
                    }
                    else if (strCanal == "Outros marketplaces" || strCanal == "SUBMARINO" || strCanal == "SHOPTIME" || strCanal == "AMERICANAS" || strCanal == "LOJASAMERICANAS")
                    {
                        lblPlataforma.Text = "B2W";

                        if (strConta == "financeiro@facilcard.com.br")
                        {
                            lblLoja.Text = "FACILSHOPPING";
                        }
                        else if (strConta == "diretoria@facilcard.com.br" || strConta == "alphajogos@facilshopping.com.br")
                        {
                            lblLoja.Text = "ALPHA JOGOS";
                        }
                        else if (strConta == "midia@facilcard.com.br")
                        {
                            lblLoja.Text = "BELA SHOPPING";
                        }
                        else if (strConta == "comercial@mundialesportes.com.br")
                        {
                            lblLoja.Text = "MUNDIAL";
                        }

                        tabPrincipal.SelectedTab = tabPage3;
                    }

                    //else if (strCanal == "Amazon" && strConta == "Mundial Esportes")
                    //{
                    //    lblPlataforma.Text = "Amazon";

                    //    if (strConta == "financeiro@facilcard.com.br")
                    //    {
                    //    //   lblLoja.Text = "FACILSHOPPING";
                    //    }
                    //    else if (strConta == "diretoria@facilcard.com.br" || strConta == "alphajogos@facilshopping.com.br")
                    //    {
                    //    //    lblLoja.Text = "ALPHA JOGOS";
                    //    }
                    //    else if (strConta == "midia@facilcard.com.br")
                    //    {
                    //    //    lblLoja.Text = "BELA SHOPPING";
                    //    }
                    //    else if (strConta == "Mundial Esportes")
                    //    {
                    //        lblLoja.Text = "MUNDIAL";
                    //    }

                    //    tabPrincipal.SelectedTab = tabPage7;
                    //}

                    else if (strCanal == "LUDOSTORE")
                    {
                        lblPlataforma.Text = "LUDOSTORE";
                        tabPrincipal.SelectedTab = tab1;
                    }
                    else if (strCanal == "Magazine Luiza" || strCanal == "MAGAZINE LUIZA")
                    {
                        lblPlataforma.Text = "MAGAZINE LUIZA";
                        lblLoja.Text = strLoja;

                        //if (strConta == "alphajogosapi")
                        //{
                        //    tabPrincipal.SelectedTab = tabPage1;
                        //}
                        //else
                        {
                            tabPrincipal.SelectedTab = tabPage4;
                        }

                    }
                    else if (strCanal == "SHOPEE")
                    {
                        lblPlataforma.Text = "SHOPEE";
                        lblLoja.Text = strLoja;

                        //if (strConta == "alphajogosapi")
                        //{
                        //    tabPrincipal.SelectedTab = tabPage1;
                        //}
                        //else

                        if (txtRastreio.Text.Substring(txtRastreio.Text.Length - 2, 2) != "BR")
                        {
                            if (txtRastreio.Text.Substring(0, 2) == "TE" || txtRastreio.Text.Substring(0, 2) == "TX")
                            {
                                tabPrincipal.SelectedTab = tabPage6;
                            }
                            if (txtRastreio.Text.Substring(0, 2) == "BR")
                            {
                                tabPrincipal.SelectedTab = tabPage7;
                            }
                        }
                        else
                        {
                            tabPrincipal.SelectedTab = tab1;
                        }
                    }
                    else
                    {
                        lblPlataforma.Text = strCanal;
                        tabPrincipal.SelectedTab = tab1;
                    }

                    if (chkEnvioCorreio.Checked == true)
                    {
                        lblPlataforma.Text = "CORREIO";
                        tabPrincipal.SelectedTab = tab1;
                    }

                    if (strNome.Length > 0)
                    {
                        lblCliente.Text = "CLIENTE: " + strNome + " " + strSobrenome;
                    }
                    else
                    {
                        lblCliente.Text = "CLIENTE: " + strPrimeiroNome + " " + strNomeMeio + " " + strUltimoNome;
                    }
                    // break;
                    if (PedidoValido(strPedido) == true)
                    {
                        blExibe = true;                       
                    }
                    else
                    {
                        if (MessageBox.Show("Já houve um código de rastreio para o pedido " + lblPedido.Text + "!! Grava mesmo assim? ", "PontoZero Informa", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1) == DialogResult.Yes)
                        {
                            //DIGITAR
                            //
                            blExibe = true;
                        }
                        else
                        {
                            blExibe = false;
                        }

                        txtRastreio.Text = "";
                    }


                }
                if (blExibe == false)
                {
                   // if (Carrega_CEP_Antigo(strCEP) == false)
                    {

                    }

                    if (MessageBox.Show("Pedido não encontrado ou com problema!! Grava mesmo assim? ", "PontoZero Informa", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1) == DialogResult.Yes)
                    {
                        //DIGITAR SENHA
                        blExibe = true;
                        lblPedido.Text = "PEDIDO: 00001";
                    }

                }

                //dataReader.Close();
            }
            else
            {
                blExibe = true;
                lblPedido.Text = "PEDIDO: 00001";
                lblPlataforma.Text = "AVULSO";
            }

            if (blExibe == false)
            {
                MessageBox.Show("Não possivel encontrar esse pedido!!", "PontoZero Informa");
                txtDados.Text = "";
                lblPedido.Text = strCEP;
            }
            else
            {
                if (txtRastreio.Text == "")
                {
                    txtRastreio.Text = txtDados.Text;
                }


                if (lblPlataforma.Text == "B2W")
                {
                    if (txtNumeroEnvioB2W.TextLength == 0)
                    {
                        MessageBox.Show("Não foi aberto o lote do B2W ainda", "PontoZero Informa");
                        blErro = true;
                    }

                    blB2W = true;
                }

                //if ((lblPlataforma.Text == "Amazon" || lblPlataforma.Text == "AMAZON") && (lblLoja.Text == "Mundial" || lblLoja.Text == "MUNDIAL ESPORTES"))
                //{
                //    if (txtNumeroEnvioAmazon.TextLength == 0)
                //    {
                //        MessageBox.Show("Não foi aberto o lote do Amazon ainda", "PontoZero Informa");
                //        blErro = true;
                //    }

                //    blAmazon = true;
                //}


                if (lblPlataforma.Text == "Mercado Livre")
                {
                    if (lblLoja.Text == "FÁCIL_SHOPPING" || lblLoja.Text == "FÁCIL-SHOPPING")
                    {
                        blMercadoLivre = true;
                    }
                    else if (lblLoja.Text == "ALPHAJOGOSLTDA" || lblLoja.Text == "SHOPPINGBELA" || lblLoja.Text == "MUNDIAL_ESPORTES")
                    {
                        blMlPlace = true;
                        blMercadoLivre = false;
                    }
                }

                if (lblPlataforma.Text == "MAGAZINE LUIZA")
                {
                    if (txtNumeroEnvioMagalu.TextLength == 0)
                    {
                        MessageBox.Show("Não foi aberto o lote do MagaLu ainda", "PontoZero Informa");
                        blErro = true;
                    }

                    //if (strConta != "alphajogosapi")
                    //{
                        blMagalu = true;
                    //}

                        
                }

                if (lblPlataforma.Text == "SHOPEE")
                {
                    if (txtRastreio.Text.Substring(0, 2) == "TE" || txtRastreio.Text.Substring(0, 2) == "TX")
                    {
                        if (txtNumeroEnvioTotalExpress.TextLength == 0)
                        {
                            MessageBox.Show("Não foi aberto o lote do Shopee ainda", "PontoZero Informa");
                            blErro = true;
                        }

                        blTotalExpress = true;
                     
                    }
                    if (txtRastreio.Text.Substring(0, 2) == "BR")
                    {
                        if (txtNumeroEnvioSequoia.TextLength == 0)
                        {
                            MessageBox.Show("Não foi aberto o lote da Sequoia ainda", "PontoZero Informa");
                            blErro = true;
                        }

                        blSequoia = true;

                    }
                }

                if (blB2W == false && blMagalu == false && blTotalExpress == false && blSequoia == false && blAmazon == false)
                {
                    if (txtNumeroEnvio.TextLength == 0)
                    {
                        MessageBox.Show("Não foi aberto o lote do Correio ainda", "PontoZero Informa");
                        blErro = true;
                    }
                }


                if (blErro == false)
                {
                    GravaNovoItemEnvio(blMercadoLivre, blB2W, blMagalu, blMlPlace, blTotalExpress, blSequoia, blAmazon);
                    global.AtualizaStatus("Aguardando Retirada", lblPedido.Text.Substring(8));
                }

                txtRastreio.Text = "";
                txtDados.Text = "";
                chkEnvioCorreio.Checked = false;
                chkReenvio.Checked = false;
                chkIgnorarData.Checked = false;
                chkPedidoAvulso.Checked = false;
                txtRastreio.Focus();
            }

            if (global.myDB.connection.State != ConnectionState.Closed)
            {
                global.myDB.CloseConnection();
            }

        }


        private void CarregaCertificado()
        {
            MySqlCommand cmd1;
            MySqlDataReader dataPedido;

            string query = "select * from IDERIS_TOKEN;";

            global.myDB.CloseConnection();

            if (global.myDB.connection.State == ConnectionState.Closed)
            {
                global.myDB.OpenConnection();
            }

            cmd1 = new MySqlCommand(query, global.myDB.connection);

            dataPedido = cmd1.ExecuteReader();

            if (dataPedido.Read())
            {
                strCertificado = dataPedido["TOKEN_PROVISORIO"].ToString();
            }

            dataPedido.Close();

            if (global.myDB.connection.State != ConnectionState.Closed)
            {
                global.myDB.CloseConnection();
            }
        }

        private bool Carrega_CEP_Antigo(String strCEP)
        {
            MySqlCommand cmd;
            string strMSG = "";
            bool blRetorno = false;

            string query = " SELECT DISTINCT VENDA.CANAL, VENDA.increment_id, VENDA.canal_id , VENDA.customer_firstname, VENDA.customer_middlename, VENDA.customer_lastname, OA.firstname, OA.lastname";

            query = query + " , (  SELECT DISTINCT count(*) ";
            query = query + " FROM sales_flat_order VENDA ";

            query = query + " LEFT JOIN customer_address_entity CE  ON CE.parent_id = VENDA.customer_id ";
            query = query + " LEFT JOIN customer_address_entity_varchar CV  ON CE.entity_id = CV.entity_id AND CV.attribute_id = 30 ";
            query = query + " INNER JOIN sales_flat_order_address OA ON OA.parent_id = VENDA.entity_id ";

            query = query + " WHERE OA.address_type = 'shipping' AND (CV.VALUE = '" + strCEP + "' OR OA.postcode = '" + strCEP + "'";
            query = query + " OR CV.VALUE = '" + strCEP.Substring(0, 5) + "-" + strCEP.Substring(5, 3) + "' OR OA.postcode = '" + strCEP.Substring(0, 5) + "-" + strCEP.Substring(5, 3) + "')";

            query = query + " AND VENDA.STATE NOT IN('canceled', 'closed', 'holded', '') ";// and VENDA.status <> 'complete_shipped' ";
            query = query + " and(VENDA.status <> 'complete' or(VENDA.STATUS IS NULL AND VENDA.STATE = 'processing') or ((VENDA.status = 'complete' and  VENDA.CANAL in ( 'SUBMARINO', 'SHOPTIME', 'LOJASAMERICANAS', 'Loja', 'MagazineLuiza', 'Cnova', 'madeiramadeira', 'Carrefour', 'Dafiti')) or (VENDA.status is null and  VENDA.CANAL in ('Amazon.com.br','MercadoLivre', 'LOJASAMERICANAS'))) )  and VENDA.created_at > (date_sub(now(), interval 30 day)) ";

            query = query + " ) contador, separado ";

            query = query + " FROM sales_flat_order VENDA";
            query = query + " LEFT JOIN customer_address_entity CE  ON CE.parent_id = VENDA.customer_id";

            query = query + " LEFT JOIN customer_address_entity_varchar CV  ON CE.entity_id = CV.entity_id AND CV.attribute_id = 30";
            query = query + " INNER JOIN sales_flat_order_address OA ON OA.parent_id = VENDA.entity_id";

            query = query + " WHERE OA.address_type = 'shipping' AND (CV.VALUE = '" + strCEP + "' OR OA.postcode = '" + strCEP + "'";
            query = query + " OR CV.VALUE = '" + strCEP.Substring(0, 5) + "-" + strCEP.Substring(5, 3) + "' OR OA.postcode = '" + strCEP.Substring(0, 5) + "-" + strCEP.Substring(5, 3) + "')";

            query = query + " AND VENDA.STATE NOT IN('canceled', 'closed', 'holded', '') "; // and VENDA.status <> 'complete_shipped'";

            query = query + " and(VENDA.status <> 'complete' or(VENDA.STATUS IS NULL AND VENDA.STATE = 'processing') or ((VENDA.status = 'complete' and  VENDA.CANAL in ( 'SUBMARINO', 'SHOPTIME', 'LOJASAMERICANAS', 'Loja', 'MagazineLuiza', 'Cnova', 'madeiramadeira', 'Carrefour', 'Dafiti')) or (VENDA.status is null and  VENDA.CANAL in ('Carrefour', 'Amazon.com.br','MercadoLivre', 'LOJASAMERICANAS'))) ) ";

            query = query + " and VENDA.created_at > (date_sub(now(), interval 30 day)) ";


            //****************************************** UNION IDERIS ************************************

            query = query + " union ";

            query = query + " SELECT replace(PEDIDOS_INTEGRADOS.marketplace ,'B2W - ','') CANAL, id_pedido INCREMENT_ID, cod_pedido CANAL_ID, nome CUSTOMER_FIRSTNAME, '' CUSTOMER_MIDDLENAME, '' CUSTOMER_LASTNAME, nome FIRSTNAME, ";
            query = query + " '' LASTNAME,  ";

            query = query + " (SELECT COUNT(*) from PEDIDOS_INTEGRADOS ";
            query = query + " WHERE STATUS NOT IN('Pagamento cancelado') ";
            query = query + " AND (CEP = '" + strCEP + "' OR CEP = '" + strCEP.Substring(0, 5) + "-" + strCEP.Substring(5, 3) + "')";
            query = query + " and id_pedido not in (Select increment_id from ITENS_ENVIO_CORREIOS where increment_id = id_pedido)  ";

            query = query + ") contador, separado";


            query = query + " from PEDIDOS_INTEGRADOS ";

            query = query + " WHERE STATUS NOT IN('Pagamento cancelado')  "; // and dt_pedido > (now() - 90000000) ";
                                                                             // query = query + " AND PEDIDOS_INTEGRADOS.marketplace NOT LIKE '%MERCADO%' ";
            query = query + " AND (CEP = '" + strCEP.Substring(0, 8) + "' OR CEP = '" + strCEP + "' OR CEP = '" + strCEP.Substring(0, 5) + "-" + strCEP.Substring(5, 3) + "')";

            if (chkReenvio.Checked == false)
            {
                query = query + " and id_pedido not in (Select increment_id from ITENS_ENVIO_CORREIOS where increment_id = id_pedido);";
            }


            //*****************************************************************************************************


            if (global.myDB.connection.State == ConnectionState.Closed)
            {
                global.myDB.OpenConnection();
            }

            cmd = new MySqlCommand(query, global.myDB.connection);

            if (dataReader.IsClosed == false)
            {
                dataReader.Close();
            }
            try
            {
                dataReader = cmd.ExecuteReader();
            }
            catch (Exception)
            {
                global.myDB.OpenConnection();
                cmd = new MySqlCommand(query, global.myDB.connection);
                dataReader = cmd.ExecuteReader();
            }

            while (dataReader.Read())
            {
                blRetorno = true;
                strMSG = strMSG + "Canal: " + dataReader["CANAL"].ToString().ToUpper() + " Cliente " + dataReader["firstname"].ToString() + " Pedido: " + dataReader["increment_id"].ToString() + "\r\n";
            }

            if (strMSG != "")
            {
                MessageBox.Show("PODE SER UM PEDIDO ANTIGO OU DE REENVIO:\r\n\r\n" + strMSG, "PontoZero Informa");
            }

            if (global.myDB.connection.State != ConnectionState.Closed)
            {
                global.myDB.CloseConnection();
            }

            return blRetorno;

        }

        private bool Carrega_Pedido(String strRastreio)
        {
            MySqlCommand cmd;
            bool blExibe;
            string strMSG;

            blExibe = false;

            string query = " SELECT LOJA, CONTALOJA, replace(PEDIDOS_INTEGRADOS.marketplace ,'B2W - ','') CANAL, id_pedido INCREMENT_ID, cod_pedido CANAL_ID, nome CUSTOMER_FIRSTNAME, '' CUSTOMER_MIDDLENAME, '' CUSTOMER_LASTNAME, nome FIRSTNAME, ";
            query = query + " '' LASTNAME, 1 CONTADOR, separado ";

            query = query + " from PEDIDOS_INTEGRADOS ";

            query = query + " WHERE STATUS NOT IN('Pagamento cancelado')  ";

            if (chkIgnorarData.Checked == false)
            {
                query = query + " and PEDIDOS_INTEGRADOS.dt_pedido > (date_sub(now(), interval 5 day)) ";
            }

            query = query + " AND cd_rastreio = '" + strRastreio + "' ";


            if (global.myDB.connection.State == ConnectionState.Closed)
            {
                global.myDB.OpenConnection();
            }

            cmd = new MySqlCommand(query, global.myDB.connection);

            try
            {
                dataReader = cmd.ExecuteReader();
            }
            catch (Exception)
            {
                global.myDB.OpenConnection();
                cmd = new MySqlCommand(query, global.myDB.connection);
                dataReader = cmd.ExecuteReader();
            }

            while (dataReader.Read())
            {

                blExibe = false;
                if (Convert.ToInt32(dataReader["contador"]) >= 2)
                {
                    if (dataReader["CANAL"].ToString() == "SUBMARINO" || dataReader["CANAL"].ToString() == "SHOPTIME" || dataReader["CANAL"].ToString() == "LOJASAMERICANAS")
                    {
                        strMSG = "Confirma o pedido " + dataReader["canal_id"].ToString() + " para o cliente " + dataReader["firstname"].ToString() + " " + dataReader["lastname"].ToString();
                    }
                    else
                    {
                        strMSG = "Confirma o pedido " + dataReader["increment_id"].ToString() + " para o cliente " + dataReader["firstname"].ToString() + " " + dataReader["lastname"].ToString();
                    }

                    if (MessageBox.Show(strMSG, "PontoZero Informa", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1) == DialogResult.Yes)
                    {
                        blExibe = true;
                    }
                    else
                    {
                        blExibe = false;
                    }
                }
                else
                {
                    blExibe = true;
                }

                if (blExibe == true)
                {
                    //DIGITAR SENHA VALIDA SE CONFERIDO

                    lblPedido.Text = "PEDIDO: " + dataReader["increment_id"].ToString();

                    lblLoja.Text = dataReader["LOJA"].ToString().ToUpper();

                    if (dataReader["CANAL"].ToString().ToUpper() == "MERCADOLIVRE" || dataReader["CANAL"].ToString().ToUpper() == "MERCADO LIVRE")
                    {
                        if (dataReader["LOJA"].ToString() == "ALPHAJOGOSLTDA" || dataReader["LOJA"].ToString() == "SHOPPINGBELA" || dataReader["LOJA"].ToString() == "MUNDIAL_ESPORTES")
                        {
                            lblPlataforma.Text = "MERCADO PLACE";
                            tabPrincipal.SelectedTab = tabPage5;

                            lblLoja.Text = dataReader["LOJA"].ToString();

                        }
                        else
                        {
                            lblPlataforma.Text = "MERCADOLIVRE";
                            tabPrincipal.SelectedTab = tabPage2;

                            lblLoja.Text = dataReader["LOJA"].ToString();

                        }

                    }
                    else if (dataReader["CANAL"].ToString().ToUpper() == "SUBMARINO" || dataReader["CANAL"].ToString().ToUpper() == "SHOPTIME" || dataReader["CANAL"].ToString().ToUpper() == "LOJASAMERICANAS" || dataReader["CANAL"].ToString().ToUpper() == "AMERICANAS" || dataReader["CANAL"].ToString().ToUpper() == "Outros marketplaces")
                    {
                        lblPlataforma.Text = "B2W";

                        if (dataReader["CONTALOJA"].ToString() == "financeiro@facilcard.com.br")
                        {
                            lblLoja.Text = "FACILSHOPPING";
                        }
                        else if (dataReader["CONTALOJA"].ToString() == "diretoria@facilcard.com.br")
                        {
                            lblLoja.Text = "ALPHA JOGOS";
                        }
                        else if (dataReader["CONTALOJA"].ToString() == "midia@facilcard.com.br")
                        {
                            lblLoja.Text = "BELA SHOPPING";
                        }
                        else if (dataReader["CONTALOJA"].ToString() == "comercial@mundialesportes.com.br")
                        {
                            lblLoja.Text = "MUNDIAL";
                        }

                        tabPrincipal.SelectedTab = tabPage3;
                    }
                    //else if (dataReader["CANAL"].ToString().ToUpper() == "Amazon" && dataReader["CONTALOJA"].ToString() == "Mundial Esportes")
                    //{
                    //    lblPlataforma.Text = "Amazon";

                    //    if (dataReader["CONTALOJA"].ToString() == "financeiro@facilcard.com.br")
                    //    {
                    //       // lblLoja.Text = "FACILSHOPPING";
                    //    }
                    //    else if (dataReader["CONTALOJA"].ToString() == "diretoria@facilcard.com.br")
                    //    {
                    //        //lblLoja.Text = "ALPHA JOGOS";
                    //    }
                    //    else if (dataReader["CONTALOJA"].ToString() == "midia@facilcard.com.br")
                    //    {
                    //        //lblLoja.Text = "BELA SHOPPING";
                    //    }
                    //    else if (dataReader["CONTALOJA"].ToString() == "Mundial Esportes")
                    //    {
                    //        lblLoja.Text = "MUNDIAL";
                    //    }

                    //    tabPrincipal.SelectedTab = tabPage7;
                    //}
                    else if (dataReader["CANAL"].ToString() == "LUDOSTORE")
                    {
                        lblPlataforma.Text = "LUDOSTORE";
                        tabPrincipal.SelectedTab = tab1;
                    }
                    else if (dataReader["CANAL"].ToString() == "Magazine Luiza")
                    {
                        lblPlataforma.Text = "MAGAZINE LUIZA";
                        tabPrincipal.SelectedTab = tabPage4;
                    }
                    else
                    {
                        lblPlataforma.Text = dataReader["CANAL"].ToString().ToUpper();
                        tabPrincipal.SelectedTab = tab1;
                    }

                    if (chkEnvioCorreio.Checked == true)
                    {
                        lblPlataforma.Text = "CORREIO";
                        tabPrincipal.SelectedTab = tab1;
                    }

                    if (dataReader["firstname"].ToString().Length > 0)
                    {
                        lblCliente.Text = "CLIENTE: " + dataReader["firstname"].ToString() + " " + dataReader["lastname"].ToString();
                    }
                    else
                    {
                        lblCliente.Text = "CLIENTE: " + dataReader["customer_firstname"].ToString() + " " + dataReader["customer_middlename"].ToString() + " " + dataReader["customer_lastname"].ToString();
                    }
                    // break;
                    if (PedidoValido(dataReader["increment_id"].ToString()) == true)
                    {
                        blExibe = true;
                    }
                    else
                    {
                        blExibe = true;
                        MessageBox.Show("Já houve um código de rastreio para o pedido " + lblPedido.Text + "!!", "PontoZero Informa");
                    }
                    break;
                }
            }
            if (blExibe == false)
            {
                MessageBox.Show("Não possivel encontrar esse pedido!!", "PontoZero Informa");
                txtDados.Text = "";
                lblPedido.Text = "";
            }

            dataReader.Close();

            if (global.myDB.connection.State != ConnectionState.Closed)
            {
                global.myDB.CloseConnection();
            }

            return blExibe;
        }


        private bool PedidoValido(string strPedido)
        {
            bool blValido = true;
            MySqlCommand cmd1;
            MySqlDataReader dataPedido;

            string query = "select * from ITENS_ENVIO_CORREIOS where increment_id = '" + strPedido + "';";

            global.myDB.CloseConnection();

            if (global.myDB.connection.State == ConnectionState.Closed)
            {
                global.myDB.OpenConnection();
            }

            cmd1 = new MySqlCommand(query, global.myDB.connection);

            dataPedido = cmd1.ExecuteReader();

            if (dataPedido.Read())
            {
                blValido = false;
            }

            dataPedido.Close();

            if (global.myDB.connection.State != ConnectionState.Closed)
            {
                global.myDB.CloseConnection();
            }




            return blValido;
        }

        private void txtDados_TextChanged(object sender, EventArgs e)
        {

        }

        private void chkEnvioCorreio_CheckedChanged(object sender, EventArgs e)
        {
            txtDados.Focus();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            //frmRel_Correios frmCorreio = new frmRel_Correios();
           // frmCorreio.intNumeroEnvio = Convert.ToInt32(txtNumeroEnvio.Text);
           // frmCorreio.intQuantidade = Convert.ToInt32(lblRegistros.Text);
          //  frmCorreio.ShowDialog(this);

            //frmDeclaracao frmCorreio = new frmDeclaracao();
            //frmCorreio.ShowDialog(this);

        }

        private void lblRegistros_Click(object sender, EventArgs e)
        {

        }

        private void groupBox3_Enter(object sender, EventArgs e)
        {

        }

        private void timeDigitacao_Tick(object sender, EventArgs e)
        {
            blPressiona = true;
            RastreioDigitado();
        }

        private void label10_Click(object sender, EventArgs e)
        {

        }

        private void lblPedido_Click(object sender, EventArgs e)
        {

        }

        private void lblCliente_Click(object sender, EventArgs e)
        {

        }

        private void cmdNovoMercadoLIvre_Click(object sender, EventArgs e)
        {

        }

        private void cmdImprimeMercadoLivre_Click(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {

        }

        private void button1_Click_1(object sender, EventArgs e)
        {

        }

        private void label4_Click_1(object sender, EventArgs e)
        {

        }

        private void lblRegistros_Click_1(object sender, EventArgs e)
        {

        }

        private void label8_Click(object sender, EventArgs e)
        {

        }

        private void label7_Click(object sender, EventArgs e)
        {

        }

        private void btnNovo_Click_1(object sender, EventArgs e)
        {
            //btnNovo.Enabled = false;

            //dataReader.Close();

            //if (global.BuscaNovoEnvio(false, false, false, false, false) > 0)
            //{
            //    Carrega_Envio("", "", false, false, false, false, false, false, false);
            //}
            //else
            //{
            //    GravaNovoEnvio(false, false, false, false, false, false);
            //}
        }

        private void button4_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Confirma a finalização e a impressão do protocolo da B2W?", "PontoZero Informa", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1) == DialogResult.Yes)
            {

                //  frmRel_B2WNew frmB2W = new frmRel_B2WNew();
                //   frmB2W.intNumeroEnvio = 0; // Convert.ToInt32(txtNumeroEnvioB2W.Text);
                //   frmB2W.intQuantidade = 0;// Convert.ToInt32(lblRegistrosB2W.Text);

                //frmB2W.intQtdFS = Convert.ToInt32(lblRegistrosB2WFacilShopping.Text);
                //frmB2W.intQtdAJ = Convert.ToInt32(lblRegistrosB2WAlpha.Text);
                //frmB2W.intQtdBS = Convert.ToInt32(lblRegistrosB2WBela.Text);
                //frmB2W.intQtdMS = Convert.ToInt32(lblRegistrosB2WMundial.Text);
                //   frmB2W.ShowDialog(this);

                if (global.ReciboEnvio("B2W", txtNumeroEnvioB2W.Text, lblRegistrosB2W.Text, lblRegistrosB2WFacilShopping.Text, lblRegistrosB2WAlpha.Text, lblRegistrosB2WBela.Text, lblRegistrosB2WMundial.Text) == true)
                {
                    //if (MessageBox.Show("Impressão do protocolo OK? Pode fechar o lote de envio?", "PontoZero Informa", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1) == DialogResult.Yes)
                    //{
                    //    AtualizaEnvio(txtNumeroEnvioB2W.Text);

                    //    Carrega_Envio("", "", false, true, false, false,false, false);
                    //}
                }
            }
        }

        private void cmdNovoB2W_Click(object sender, EventArgs e)
        {
            //cmdNovoB2W.Enabled = false;

            //dataReader.Close();

            //if (global.BuscaNovoEnvio(false, true, false, false, false) > 0)
            //{
            //    Carrega_Envio("", "", false, true, false, false, true, false, false);
            //}
            //else
            //{
            //    GravaNovoEnvio(false, true, false, false, false, false);
            //}
        }

        private void cmdNovoMercadoLIvre_Click_1(object sender, EventArgs e)
        {
            //cmdNovoMercadoLIvre.Enabled = false;

            //dataReader.Close();

            //if (global.BuscaNovoEnvio(true, false, false, false, false) > 0)
            //{
            //    Carrega_Envio("", "", true, false, false, false, true,false, false);
            //}
            //else
            //{
            //    GravaNovoEnvio(true, false, false, false, false, false);
            //}
        }

        private void btnExcluir_Click_1(object sender, EventArgs e)
        {
            ExcluiRastreio();
        }

        private void button2_Click_1(object sender, EventArgs e)
        {
            ExcluiRastreioML();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            ExcluiRastreioB2W();
        }

        private void cmdImprimeMercadoLivre_Click_1(object sender, EventArgs e)
        {
            if (MessageBox.Show("Confirma a finalização e a impressão do protocolo do Mercado Livre?", "PontoZero Informa", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1) == DialogResult.Yes)
            {
                //frmRel_CorreiosML frmCorreioML = new frmRel_CorreiosML();
                //frmCorreioML.intNumeroEnvioML = Convert.ToInt32(txtNumeroEnvioML.Text);
                //frmCorreioML.intQuantidadeML = Convert.ToInt32(lblRegistrosML.Text);
                //frmCorreioML.ShowDialog(this);

                if (global.ReciboEnvio("MERCADOENVIOS", txtNumeroEnvioML.Text, lblRegistrosML.Text,"", "", "", "") == true)
                {
                    //if (MessageBox.Show("Impressão do protocolo OK? Pode fechar o lote de envio?", "PontoZero Informa", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1) == DialogResult.Yes)
                    //{
                    //    AtualizaEnvio(txtNumeroEnvioML.Text);

                    //    Carrega_Envio("", "", true, false, false, false, false, false);
                    //}
                }
            }
        }

        private void btnImprimir_Click_1(object sender, EventArgs e)
        {
            if (MessageBox.Show("Confirma a impressão do protocolo do Correio?", "PontoZero Informa", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1) == DialogResult.Yes)
            {
                //frmRel_Correios frmCorreio = new frmRel_Correios();
                //frmCorreio.intNumeroEnvio = Convert.ToInt32(txtNumeroEnvio.Text);
                //frmCorreio.intQuantidade = Convert.ToInt32(lblRegistros.Text);
                //frmCorreio.ShowDialog(this);

                if(global.ReciboEnvio("CORREIO", txtNumeroEnvio.Text, lblRegistros.Text, "", "", "", "") == true)
                    {
                    //if (MessageBox.Show("Impressão do protocolo OK? Pode fechar o lote de envio?", "PontoZero Informa", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1) == DialogResult.Yes)
                    //{
                    //    AtualizaEnvio(txtNumeroEnvio.Text);

                    //    Carrega_Envio("", "", false, false, false, false,  true, false);
                    //}
                }
            }

        }

        private void button8_Click(object sender, EventArgs e)
        {
            button8.Enabled = false;

            dataReader.Close();

            if (global.BuscaNovoEnvio(false, false, true, false, false) > 0)
            {
                Carrega_Envio("", "", false, false, false, false, false,false, false, false);
            }
            else
            {
               // GravaNovoEnvio(false, false, true, false, false, false);
            }
        }

        private void button6_Click(object sender, EventArgs e)
        {
            ExcluiRastreioMaGalu();
        }

        private void ExcluiRastreioMaGalu()
        {
            if (MessageBox.Show("Confirma a exclusão dos itens selecionados da MagaLu?", "PontoZero Informa", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1) == DialogResult.Yes)
            {
                if (lstMagalu.Items.Count > 0)
                {
                    try
                    {
                        foreach (ListViewItem item in lstMagalu.Items)
                        {
                            if (item.Checked == true)
                            {
                                String query;

                                global.myDB.CloseConnection();

                                query = "DELETE FROM ITENS_ENVIO_CORREIOS WHERE ID = " + item.SubItems[4].Text;

                                if (global.myDB.connection.State == ConnectionState.Closed)
                                {
                                    global.myDB.OpenConnection();
                                }

                                MySqlCommand cmd1 = new MySqlCommand(query, global.myDB.connection);

                                try
                                {
                                    cmd1.ExecuteNonQuery();
                                }
                                catch (Exception ex)
                                {
                                    MessageBox.Show(ex.Message, "PontoZero Informa");
                                }
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show("Erro : " + ex.Message, "PontoZero Informa");
                    }
                }
                //Carrega_Envio("", "", false, false, true, false, true,false, false, false);
            }

            if (global.myDB.connection.State != ConnectionState.Closed)
            {
                global.myDB.CloseConnection();
            }
        }

        private void button7_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Confirma a finalização e a impressão do protocolo do Magazine Luiza?", "PontoZero Informa", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1) == DialogResult.Yes)
            {
                //frmRel_CorreiosMagalu frmCorreioMagaLu = new frmRel_CorreiosMagalu();
                //frmCorreioMagaLu.intNumeroEnvio = Convert.ToInt32(txtNumeroEnvioMagalu.Text);
                //frmCorreioMagaLu.intQuantidade = Convert.ToInt32(lblRegistrosMagaLu.Text);

                //frmCorreioMagaLu.intQtdFS = Convert.ToInt32(lblRegistrosMagaluFacilShopping.Text);
                //frmCorreioMagaLu.intQtdAJ = Convert.ToInt32(lblRegistrosMagaluAlpha.Text);
                //frmCorreioMagaLu.intQtdBS = Convert.ToInt32(lblRegistrosMagaluBela.Text);
                //frmCorreioMagaLu.intQtdMS = Convert.ToInt32(lblRegistrosMagaluMundial.Text);

                //frmCorreioMagaLu.ShowDialog(this);

                if (global.ReciboEnvio("MAGALU", txtNumeroEnvioMagalu.Text, lblRegistrosMagaLu.Text, lblRegistrosMagaluFacilShopping.Text, lblRegistrosMagaluAlpha.Text, lblRegistrosMagaluBela.Text, lblRegistrosMagaluMundial.Text) == true)
                {
                    //if (MessageBox.Show("Impressão do protocolo OK? Pode fechar o lote de envio?", "PontoZero Informa", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1) == DialogResult.Yes)
                    //{
                    //    AtualizaEnvio(txtNumeroEnvioMagalu.Text);

                    //    Carrega_Envio("", "", false, false, true, false, false,false);
                    //}
                }

            }
        }

        private void label13_Click(object sender, EventArgs e)
        {

        }

        private void label11_Click(object sender, EventArgs e)
        {

        }

        private void cmdNovoMercadoPlace_Click(object sender, EventArgs e)
        {
            //cmdNovoMercadoPlace.Enabled = false;

            //dataReader.Close();

            //if (global.BuscaNovoEnvio(false, false, false, true, false) > 0)
            //{
            //    Carrega_Envio("", "", false, false, false, true, false, false, false, false);
            //}
            //else
            //{
            //   // GravaNovoEnvio(false, false, false, true, false, false);
            //}
        }

        private void cmdExcluirMercadoPlace_Click(object sender, EventArgs e)
        {
            ExcluiRastreioMLPlace();
        }

        private void cmdImprimirMercadoPlace_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Confirma a finalização e a impressão do protocolo do Mercado Place?", "PontoZero Informa", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1) == DialogResult.Yes)
            {
                //frmRel_CorreiosMLPlace frmCorreioMLPlace = new frmRel_CorreiosMLPlace();
                //frmCorreioMLPlace.intNumeroEnvioMLPlace = Convert.ToInt32(txtNumeroEnvioMlPlace.Text);
                //frmCorreioMLPlace.intQuantidadeMLPlace = Convert.ToInt32(lblRegistrosMercadoPlace.Text);
                //frmCorreioMLPlace.ShowDialog(this);

                //frmCorreioMagaLu.intQtdFS = Convert.ToInt32(lblRegistrosMagaluFacilShopping.Text);
                //frmCorreioMagaLu.intQtdAJ = Convert.ToInt32(lblRegistrosMagaluAlpha.Text);
                //frmCorreioMagaLu.intQtdBS = Convert.ToInt32(lblRegistrosMagaluBela.Text);
                //frmCorreioMagaLu.intQtdMS = Convert.ToInt32(lblRegistrosMagaluMundial.Text);

                if (global.ReciboEnvio("MERCADOPLACE", txtNumeroEnvioMlPlace.Text, lblRegistrosMercadoPlace.Text, lblRegistrosMPlaceFS.Text, lblRegistrosMPlaceAlpha.Text, lblRegistrosMPlaceBela.Text, lblRegistrosMPlaceMundial.Text) == true)
                {
                    //if (MessageBox.Show("Impressão do protocolo OK? Pode fechar o lote de envio?", "PontoZero Informa", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1) == DialogResult.Yes)
                    //{

                    //    AtualizaEnvio(txtNumeroEnvioMlPlace.Text);

                    //    Carrega_Envio("", "", false, false, false, true, true, false);
                    //}
                }
            }
        }

        private void button5_Click(object sender, EventArgs e)
        {
            Carrega_Envio("", "", false, false, true, false, false, false, false, false);
        }

        private void btnCarregarMercadoEnvios_Click(object sender, EventArgs e)
        {
            Carrega_Envio("", "",  true, false, false, false, false, false, false, false);
        }

        private void btnCarregarB2W_Click(object sender, EventArgs e)
        {
            Carrega_Envio("", "", false, true, false, false, false, false, false, false);
        }

        private void btnCarregarMagalu_Click(object sender, EventArgs e)
        {
            Carrega_Envio("", "", false, false, true, false, false, false, false, false);
        }

        private void btnCarregarMercadoPlace_Click(object sender, EventArgs e)
        {
            Carrega_Envio("", "", false, false, false, true, false, false, false, false);
        }

        private void cmdNovoShopee_Click(object sender, EventArgs e)
        {
            //cmdNovoTotalExpress.Enabled = false;

            //dataReader.Close();

            //if (global.BuscaNovoEnvio(false, false, false, false, true ) > 0)
            //{
            //    Carrega_Envio("", "", false, false, false, false, false, true, false, false);
            //}
            //else
            //{
            //    GravaNovoEnvio(false, false, false, false, true, false, false);
            //}
        }

        private void btnCarregarShopee_Click(object sender, EventArgs e)
        {
            Carrega_Envio("", "", false, false, false, false, false, true, false, false);
        }

        private void cmdImprimirShopee_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Confirma a impressão do protocolo da Total Express?", "PontoZero Informa", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1) == DialogResult.Yes)
            {
                //frmRel_Correios frmCorreio = new frmRel_Correios();
                //frmCorreio.intNumeroEnvio = Convert.ToInt32(txtNumeroEnvio.Text);
                //frmCorreio.intQuantidade = Convert.ToInt32(lblRegistros.Text);
                //frmCorreio.ShowDialog(this);

                if (global.ReciboEnvio("TOTALEXPRESS", txtNumeroEnvioTotalExpress.Text, lblRegistrosTotalExpress.Text, "", "", "", "") == true)
                {
                    //if (MessageBox.Show("Impressão do protocolo OK? Pode fechar o lote de envio?", "PontoZero Informa", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1) == DialogResult.Yes)
                    //{
                    //    AtualizaEnvio(txtNumeroEnvio.Text);

                    //    Carrega_Envio("", "", false, false, false, false,  true, false);
                    //}
                }
            }
        }

        private void btnCarregarSequoia_Click(object sender, EventArgs e)
        {
            Carrega_Envio("", "", false, false, false, false, false, false, true, false);
        }

        private void cmdImprimirSequoia_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Confirma a impressão do protocolo da Sequoia?", "PontoZero Informa", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1) == DialogResult.Yes)
            {
                //frmRel_Correios frmCorreio = new frmRel_Correios();
                //frmCorreio.intNumeroEnvio = Convert.ToInt32(txtNumeroEnvio.Text);
                //frmCorreio.intQuantidade = Convert.ToInt32(lblRegistros.Text);
                //frmCorreio.ShowDialog(this);

                if (global.ReciboEnvio("SEQUOIA", txtNumeroEnvioSequoia.Text, lblRegistrosSequoia.Text, "", "", "", "") == true)
                {
                    //if (MessageBox.Show("Impressão do protocolo OK? Pode fechar o lote de envio?", "PontoZero Informa", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1) == DialogResult.Yes)
                    //{
                    //    AtualizaEnvio(txtNumeroEnvio.Text);

                    //    Carrega_Envio("", "", false, false, false, false,  true, false);
                    //}
                }
            }
        }

        private void button12_Click(object sender, EventArgs e)
        {

        }

        private void button11_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Confirma e a impressão do protocolo da Amazon?", "PontoZero Informa", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1) == DialogResult.Yes)
            {

                if (global.ReciboEnvio("AMAZON", txtNumeroEnvioAmazon.Text, lblRegistrosAmazon.Text, lblRegistrosAmazonFS.Text, lblRegistrosAmazonAJ.Text, lblRegistrosAmazonBella.Text, lblRegistrosAmazonMJ.Text) == true)
                {
                    //if (MessageBox.Show("Impressão do protocolo OK? Pode fechar o lote de envio?", "PontoZero Informa", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1) == DialogResult.Yes)
                    //{
                    //    AtualizaEnvio(txtNumeroEnvioB2W.Text);

                    //    Carrega_Envio("", "", false, true, false, false,false, false);
                    //}
                }
            }
        }

        private void button9_Click(object sender, EventArgs e)
        {
            Carrega_Envio("", "", false, false, false, false, false, false, false, true);
        }
    }
}
