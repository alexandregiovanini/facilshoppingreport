﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Printing;
using System.Json;
using System.Linq;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FacilShoppingReports
{
    public partial class frmConfSeparacao : Form
    {
        MySqlDataReader dataReader;
        bool blTempoDig;
        String strCertificado;
        String strRastreio;
       
        public frmConfSeparacao()
        {
            InitializeComponent();
        }

        private void txtRastreio_KeyPress(object sender, KeyPressEventArgs e)
        {
            //if ((blTempoDig == false && chkDigitar.Checked == false) || e.KeyChar == 13)
            //{
            //    timer1.Enabled = true;
            //}
            if (e.KeyChar == 13)
            {
                strRastreio = txtRastreio.Text;
                RastreioDigitado();
            }
        }

        private void RastreioDigitado()
        {
            String strDigitado;
            bool blContinua = false;
            
            strDigitado = txtRastreio.Text;

                if (txtRastreio.TextLength > 3)
                {                   
                    blTempoDig = false;
                    timer1.Enabled = false;

                    blContinua = false;

                    if (global.BuscaCampo("ITENS_ENVIO_CORREIOS", "RASTREIO", "RASTREIO", txtRastreio.Text) != txtRastreio.Text)
                    {
                        blContinua = true;
                    }
                    else
                    {
                        if (MessageBox.Show("Esse Rastreio já foi enviado!! Pesquisa mesmo assim??", "PontoZero Informa", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1) == DialogResult.Yes)
                        {
                            //DIGITAR SENHA
                            blContinua = true;
                        }
                    }

                    if (blContinua == true)
                    {
                        if (global.BuscaCampo("PEDIDOS_INTEGRADOS", "cd_rastreio", "cd_rastreio", txtRastreio.Text) == txtRastreio.Text)
                        {
                            if (Carrega_Pedido(txtRastreio.Text) == true)
                            {
                                if (txtRastreio.Text == "")
                                {
                                    txtRastreio.Text = txtRastreio.Text;
                                }

                                PesquisaItensPedido(lblPedido.Text.Substring(8));

                                txtRastreio.Text = "";
                                txtCEP.Text = "";

                                txtEAN.Focus();

                                string strStatus = global.ConsultarStatus(lblPedido.Text.Substring(8), strCertificado);

                                if (strStatus != "0")
                                {
                                    global.AtualizaStatus(strStatus, lblPedido.Text.Substring(8));
                                    if (strStatus != "Aprovado" && strStatus != "Aberto" && strStatus != "Expedição")
                                    {
                                        MessageBox.Show("Esse pedido está com o status: " + strStatus, "PontoZero Informa");
                                    }
                                }
                                else
                                {
                                    MessageBox.Show("Não foi possível encontrar esse pedido no Ideris", "PontoZero Informa");
                                }
                            }
                        }
                        else
                        {
                            //MessageBox.Show("BUSCA CEP", "PontoZero Informa");
                            txtCEP.Text = "";
                            lblPedido.Text = "";
                            lblCliente.Text = "";
                            lblPlataforma.Text = "";
                            lblPedidoPlataforma.Text = "";
                            lblLoja.Text = "";
                            chkReenvio.Checked = false;
                            chkIgnorarData.Checked = false;

                            txtCEP.Focus();
                        }
                    }
                    else
                    {
                        //MessageBox.Show("MERCADOLIVRE", "PontoZero Informa");
                    }
                }

            if (global.myDB.connection.State != ConnectionState.Closed)
            {
                global.myDB.CloseConnection();
            }
        }

        private bool Carrega_Pedido(String strRastreio)
        {
            MySqlCommand cmd;
            bool blExibe;
            string strMSG;

            blExibe = false;

            string query = " SELECT LOJA, CONTALOJA, replace(PEDIDOS_INTEGRADOS.marketplace ,'B2W - ','') CANAL, id_pedido INCREMENT_ID, cod_pedido CANAL_ID, nome CUSTOMER_FIRSTNAME, '' CUSTOMER_MIDDLENAME, '' CUSTOMER_LASTNAME, nome FIRSTNAME, ";
            query = query + " '' LASTNAME, 1 CONTADOR, SEPARADO ";

            query = query + " from PEDIDOS_INTEGRADOS ";

            query = query + " WHERE STATUS NOT IN('Pagamento cancelado')  ";

            if (chkIgnorarData.Checked == false)
            {
                query = query + " and PEDIDOS_INTEGRADOS.dt_pedido > (date_sub(now(), interval 5 day)) ";
            }

            query = query + " AND cd_rastreio = '" + strRastreio + "' ";


            if (global.myDB.connection.State == ConnectionState.Closed)
            {
                global.myDB.OpenConnection();
            }

            cmd = new MySqlCommand(query, global.myDB.connection);

            try
            {
                dataReader = cmd.ExecuteReader();
            }
            catch (Exception)
            {
                global.myDB.OpenConnection();
                cmd = new MySqlCommand(query, global.myDB.connection);
                dataReader = cmd.ExecuteReader();
            }

            while (dataReader.Read())
            {
                blExibe = false;
                if (Convert.ToInt32(dataReader["contador"]) >= 2)
                {
                    if (dataReader["CANAL"].ToString() == "SUBMARINO" || dataReader["CANAL"].ToString() == "SHOPTIME" || dataReader["CANAL"].ToString() == "LOJASAMERICANAS")
                    {
                        strMSG = "Confirma o pedido " + dataReader["canal_id"].ToString() + " para o cliente " + dataReader["firstname"].ToString() + " " + dataReader["lastname"].ToString();
                    }
                    else
                    {
                        strMSG = "Confirma o pedido " + dataReader["increment_id"].ToString() + " para o cliente " + dataReader["firstname"].ToString() + " " + dataReader["lastname"].ToString();
                    }

                    if (MessageBox.Show(strMSG, "PontoZero Informa", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1) == DialogResult.Yes)
                    {
                        blExibe = true;
                    }
                    else
                    {
                        blExibe = false;
                    }
                }
                else
                {
                    blExibe = true;
                }

                if (blExibe == true)
                {
                    if (dataReader["SEPARADO"].ToString() == "1")
                    {
                        MessageBox.Show("ESSE PEDIDO JÁ FOI SEPARADO!!", "PontoZero Informa");
                        blExibe = false;
                    }
                    else
                    { 
                        lblPedido.Text = "PEDIDO: " + dataReader["increment_id"].ToString();

                        lblLoja.Text = dataReader["LOJA"].ToString().ToUpper();

                        lblPedidoPlataforma.Text = dataReader["CANAL_ID"].ToString().ToUpper();

                        if (dataReader["CANAL"].ToString().ToUpper() == "MERCADOLIVRE" || dataReader["CANAL"].ToString().ToUpper() == "MERCADO LIVRE")
                        {
                            if (dataReader["LOJA"].ToString() == "ALPHAJOGOSLTDA" || dataReader["LOJA"].ToString() == "SHOPPINGBELA" || dataReader["LOJA"].ToString() == "MUNDIAL_ESPORTES")
                            {
                                lblPlataforma.Text = "MERCADO PLACE";
                                lblLoja.Text = dataReader["LOJA"].ToString();
                            }
                            else
                            {
                                lblPlataforma.Text = "MERCADOLIVRE";
                                lblLoja.Text = dataReader["LOJA"].ToString();
                            }
                        }
                        else if (dataReader["CANAL"].ToString().ToUpper() == "SUBMARINO" || dataReader["CANAL"].ToString().ToUpper() == "SHOPTIME" || dataReader["CANAL"].ToString().ToUpper() == "LOJASAMERICANAS" || dataReader["CANAL"].ToString().ToUpper() == "AMERICANAS")
                        {
                            lblPlataforma.Text = "B2W";

                            if (dataReader["CONTALOJA"].ToString() == "financeiro@facilcard.com.br")
                            {
                                lblLoja.Text = "FACILSHOPPING";
                            }
                            else if (dataReader["CONTALOJA"].ToString() == "diretoria@facilcard.com.br")
                            {
                                lblLoja.Text = "ALPHA JOGOS";
                            }
                            else if (dataReader["CONTALOJA"].ToString() == "midia@facilcard.com.br")
                            {
                                lblLoja.Text = "BELA SHOPPING";
                            }
                            else if (dataReader["CONTALOJA"].ToString() == "comercial@mundialesportes.com.br")
                            {
                                lblLoja.Text = "MUNDIAL";
                            }
                        }
                        else if (dataReader["CANAL"].ToString() == "LUDOSTORE")
                        {
                            lblPlataforma.Text = "LUDOSTORE";
                        }
                        else if (dataReader["CANAL"].ToString() == "Magazine Luiza")
                        {
                            lblPlataforma.Text = "MAGAZINE LUIZA";
                        }
                        else
                        {
                            lblPlataforma.Text = dataReader["CANAL"].ToString().ToUpper();
                        }

                        if (dataReader["firstname"].ToString().Length > 0)
                        {
                            lblCliente.Text = "CLIENTE: " + dataReader["firstname"].ToString() + " " + dataReader["lastname"].ToString();
                        }
                        else
                        {
                            lblCliente.Text = "CLIENTE: " + dataReader["customer_firstname"].ToString() + " " + dataReader["customer_middlename"].ToString() + " " + dataReader["customer_lastname"].ToString();
                        }
                        // break;
                        if (PedidoValido(dataReader["increment_id"].ToString()) == true)
                        {
                            blExibe = true;
                        }
                        else
                        {
                            blExibe = true;
                            MessageBox.Show("Esse Rastreio já foi enviado!!", "PontoZero Informa");
                        }
                        break;
                    }
                }
            }
            if (blExibe == false)
            {
                MessageBox.Show("Não possivel encontrar esse pedido!!", "PontoZero Informa");
                txtCEP.Text = "";
                lblPedido.Text = "";
            }
 
            dataReader.Close();

            if (global.myDB.connection.State != ConnectionState.Closed)
            {
                global.myDB.CloseConnection();
            }

            return blExibe;
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            RastreioDigitado();
        }


        private bool PedidoValido(string strPedido)
        {
            bool blValido = true;
            MySqlCommand cmd1;
            MySqlDataReader dataPedido;

            string query = "select * from ITENS_ENVIO_CORREIOS where increment_id = '" + strPedido + "';";

            global.myDB.CloseConnection();

            if (global.myDB.connection.State == ConnectionState.Closed)
            {
                global.myDB.OpenConnection();
            }

            cmd1 = new MySqlCommand(query, global.myDB.connection);

            dataPedido = cmd1.ExecuteReader();

            if (dataPedido.Read())
            {
                blValido = false;
            }

            dataPedido.Close();

            if (global.myDB.connection.State != ConnectionState.Closed)
            {
                global.myDB.CloseConnection();
            }

            return blValido;
        }

        private void txtCEP_KeyPress(object sender, KeyPressEventArgs e)
        {
            lblPedido.Text = "";
            lblCliente.Text = "";
            lblPlataforma.Text = "";
            lblLoja.Text = "";
            lblPedidoPlataforma.Text = "";

            if (e.KeyChar.ToString() == "-")
            {
                e.KeyChar = '\0';
            }

            if (e.KeyChar == 13)
            {
                Carrega_CEP(txtCEP.Text);
            }
            //    Carrega_CEP(txtCEP.Text + e.KeyChar);
            //    e.KeyChar = '\0';
            //}
        }

        private void Carrega_CEP(String strCEP)
        {
            MySqlCommand cmd;
            bool blExibe;

            string strPedido = "", strLoja = "", strCanal = "", strConta = "", strNome = "", strSobrenome = "", strPrimeiroNome = "", strNomeMeio = "", strUltimoNome = "";

            blExibe = false;

            string query = " SELECT DISTINCT 'LOJA' LOJA, 'LOJA' CONTALOJA, VENDA.CANAL, VENDA.increment_id, VENDA.canal_id , VENDA.customer_firstname, VENDA.customer_middlename, VENDA.customer_lastname, OA.firstname, OA.lastname";

            query = query + ", '' nf , '' serie, '' chave, now() dt_emissao, '' endereco, '' complemento, '' bairro, '' cep, '' cidade, '' uf  ";
            

            query = query + " , (  SELECT DISTINCT count(*) ";
            query = query + " FROM sales_flat_order VENDA ";

            query = query + " LEFT JOIN customer_address_entity CE  ON CE.parent_id = VENDA.customer_id ";
            query = query + " LEFT JOIN customer_address_entity_varchar CV  ON CE.entity_id = CV.entity_id AND CV.attribute_id = 30 ";
            query = query + " INNER JOIN sales_flat_order_address OA ON OA.parent_id = VENDA.entity_id ";

            query = query + " WHERE OA.address_type = 'shipping' AND (CV.VALUE = '" + strCEP + "' OR OA.postcode = '" + strCEP + "'";
            query = query + " OR CV.VALUE = '" + strCEP.Substring(0, 5) + "-" + strCEP.Substring(5, 3) + "' OR OA.postcode = '" + strCEP.Substring(0, 5) + "-" + strCEP.Substring(5, 3) + "')";

            query = query + " AND VENDA.STATE NOT IN('canceled', 'closed', 'holded', '') ";// and VENDA.status <> 'complete_shipped' ";
            query = query + " and(VENDA.status <> 'complete' or(VENDA.STATUS IS NULL AND VENDA.STATE = 'processing') or ((VENDA.status = 'complete' and  VENDA.CANAL in ( 'SUBMARINO', 'SHOPTIME', 'LOJASAMERICANAS', 'Loja', 'MagazineLuiza', 'Cnova', 'madeiramadeira', 'Carrefour', 'Dafiti')) or (VENDA.status is null and  VENDA.CANAL in ('Amazon.com.br','MercadoLivre', 'LOJASAMERICANAS'))) )  and VENDA.created_at > (now() - 90000000) ";

            query = query + " ) contador, VENDA.separado ";

            query = query + " FROM sales_flat_order VENDA";
            query = query + " LEFT JOIN customer_address_entity CE  ON CE.parent_id = VENDA.customer_id";

            query = query + " LEFT JOIN customer_address_entity_varchar CV  ON CE.entity_id = CV.entity_id AND CV.attribute_id = 30";
            query = query + " INNER JOIN sales_flat_order_address OA ON OA.parent_id = VENDA.entity_id";

            query = query + " WHERE OA.address_type = 'shipping' AND (CV.VALUE = '" + strCEP + "' OR OA.postcode = '" + strCEP + "'";
            query = query + " OR CV.VALUE = '" + strCEP.Substring(0, 5) + "-" + strCEP.Substring(5, 3) + "' OR OA.postcode = '" + strCEP.Substring(0, 5) + "-" + strCEP.Substring(5, 3) + "')";

            query = query + " AND VENDA.STATE NOT IN('canceled', 'closed', 'holded', '') "; // and VENDA.status <> 'complete_shipped'";

            if (chkReenvio.Checked == false)
            {
                // query = query + " and VENDA.increment_id not in (Select increment_id from ITENS_ENVIO_CORREIOS where increment_id = VENDA.increment_id) ";
            }

            query = query + " and(VENDA.status <> 'complete' or(VENDA.STATUS IS NULL AND VENDA.STATE = 'processing') or ((VENDA.status = 'complete' and  VENDA.CANAL in ( 'SUBMARINO', 'SHOPTIME', 'LOJASAMERICANAS', 'Loja', 'MagazineLuiza', 'Cnova', 'madeiramadeira', 'Carrefour', 'Dafiti')) or (VENDA.status is null and  VENDA.CANAL in ('Carrefour', 'Amazon.com.br','MercadoLivre', 'LOJASAMERICANAS'))) ) ";

            if (chkIgnorarData.Checked == false)
            {
                query = query + " and VENDA.created_at > (now() - 90000000) ";
            }


            //*************************************** UNION LUDOSTORE ******************************************
            //query = query + " UNION ";
            //query = query + " SELECT  'LUDOSTORE' LOJA, 'LUDOSTORE' CONTALOJA, 'LUDOSTORE' CANAL, id_pedido INCREMENT_ID, id_pedido CANAL_ID, nome CUSTOMER_FIRSTNAME, '' CUSTOMER_MIDDLENAME, '' CUSTOMER_LASTNAME, nome FIRSTNAME, '' LASTNAME, 1 CONTADOR ";
            //query = query + " from PEDIDO_LUDOSTORE ";
            //query = query + " WHERE STATUS NOT IN('ENTREGUE', 'ENVIADO', 'PAGAMENTO RECUSADO', 'CRIADO') ";

            //if (chkIgnorarData.Checked == false)
            //{
            //    query = query + " and dt_pedido > (date_sub(now(), interval 5 day)) ";
            //}


            //query = query + " AND (CEP = '" + strCEP + "' OR CEP = '" + strCEP.Substring(0, 5) + "-" + strCEP.Substring(5, 3) + "')";

            //if (chkReenvio.Checked == false)
            //{
            //    query = query + " and id_pedido not in (Select increment_id from ITENS_ENVIO_CORREIOS where increment_id = id_pedido) ";
            //}

            //****************************************** UNION IDERIS ************************************

            query = query + " union ";

            query = query + " SELECT  LOJA, CONTALOJA, replace(PEDIDOS_INTEGRADOS.marketplace ,'B2W - ','') CANAL, id_pedido INCREMENT_ID, cod_pedido CANAL_ID, nome CUSTOMER_FIRSTNAME, '' CUSTOMER_MIDDLENAME, '' CUSTOMER_LASTNAME, nome FIRSTNAME, ";
            query = query + " '' LASTNAME,  ";

            query = query + "  nf, serie, chave, dt_emissao, endereco, complemento, bairro, cep, cidade, uf ,";

            query = query + " (SELECT COUNT(*) from PEDIDOS_INTEGRADOS ";
            query = query + " WHERE STATUS NOT IN('Pagamento cancelado') ";

            if (strCEP.Length == 9)
            {
                query = query + " AND (CEP = '" + strCEP + "' OR CEP = '" + strCEP.Substring(0, 5) + "-" + strCEP.Substring(5, 3) + "' or CEP = '" + strCEP.Substring(0, 8) + "' )";
            }
            else
            {
                query = query + " AND (CEP = '" + strCEP + "' OR CEP = '" + strCEP.Substring(0, 5) + "-" + strCEP.Substring(5, 3) + "')";
            }

            query = query + " and id_pedido not in (Select increment_id from ITENS_ENVIO_CORREIOS where increment_id = id_pedido)  ";

            query = query + ") contador, PEDIDOS_INTEGRADOS.separado";

            query = query + " from PEDIDOS_INTEGRADOS ";

            query = query + " WHERE STATUS NOT IN('Pagamento cancelado')  "; // and dt_pedido > (now() - 90000000) ";
                                                                             // query = query + " AND PEDIDOS_INTEGRADOS.marketplace NOT LIKE '%MERCADO%' ";
            query = query + " AND (CEP = '" + strCEP.Substring(0, 8) + "' OR CEP = '" + strCEP + "' OR CEP = '" + strCEP.Substring(0, 5) + "-" + strCEP.Substring(5, 3) + "')";

            if (chkReenvio.Checked == false)
            {
                query = query + " and id_pedido not in (Select increment_id from ITENS_ENVIO_CORREIOS where increment_id = id_pedido)";
            }

            query = query + " order by contador desc ;";
            //*****************************************************************************************************


            if (global.myDB.connection.State == ConnectionState.Closed)
            {
                global.myDB.OpenConnection();
            }

            cmd = new MySqlCommand(query, global.myDB.connection);

            //if (dataReader.IsClosed == false)
            //{
            //    dataReader.Close();
            //}
            try
            {
                dataReader = cmd.ExecuteReader();
            }
            catch (Exception)
            {
                global.myDB.OpenConnection();
                cmd = new MySqlCommand(query, global.myDB.connection);
                dataReader = cmd.ExecuteReader();
            }

            frmCEPEncontrado frmPedEncontrados = new frmCEPEncontrado();
            frmPedEncontrados.dataReaderPedidos = dataReader;
            frmPedEncontrados.ShowDialog(this);

            blExibe = false;

            if (frmPedEncontrados.strPedido != "")
            {
                blExibe = true;

                if (frmPedEncontrados.strSeparado != "0")
                {
                    MessageBox.Show("ESSE PEDIDO JÁ FOI SEPARADO!!", "PontoZero Informa");
                    blExibe = false;
                }
                else
                {
                    strPedido = frmPedEncontrados.strPedido;
                    strLoja = frmPedEncontrados.strLoja;
                    strCanal = frmPedEncontrados.strCanal;
                    strConta = frmPedEncontrados.strConta;
                    strNome = frmPedEncontrados.strNome;
                    strSobrenome = frmPedEncontrados.strSobrenome;
                    strPrimeiroNome = frmPedEncontrados.strPrimeiroNome;
                    strNomeMeio = frmPedEncontrados.strNomeMeio;
                    strUltimoNome = frmPedEncontrados.strUltimoNome;
                    lblPedidoPlataforma.Text = frmPedEncontrados.strPedidoPlataforma;

                    lblPedido.Text = "PEDIDO: " + strPedido;

                    lblLoja.Text = strLoja;

                    if (strCanal == "MERCADOLIVRE")
                    {
                        lblPlataforma.Text = "MERCADOLIVRE";
                    }
                    else if (strCanal == "SUBMARINO" || strCanal == "SHOPTIME" || strCanal == "AMERICANAS" || strCanal == "LOJASAMERICANAS" || strCanal == "Outros marketplaces")
                    {
                        lblPlataforma.Text = "B2W";

                        if (strConta == "financeiro@facilcard.com.br")
                        {
                            lblLoja.Text = "FACILSHOPPING";
                        }
                        else if (strConta == "diretoria@facilcard.com.br" || strConta == "alphajogos@facilshopping.com.br")
                        {
                            lblLoja.Text = "ALPHA JOGOS";
                        }
                        else if (strConta == "midia@facilcard.com.br")
                        {
                            lblLoja.Text = "BELA SHOPPING";
                        }
                        else if (strConta == "comercial@mundialesportes.com.br")
                        {
                            lblLoja.Text = "MUNDIAL";
                        }
                    }
                    else if (strCanal == "LUDOSTORE")
                    {
                        lblPlataforma.Text = "LUDOSTORE";
                    }
                    else if (strCanal == "Magazine Luiza" || strCanal == "MAGAZINE LUIZA")
                    {
                        lblPlataforma.Text = "MAGAZINE LUIZA";
                        lblLoja.Text = strLoja;
                    }
                    else
                    {
                        lblPlataforma.Text = strCanal;
                    }

                    if (strNome.Length > 0)
                    {
                        lblCliente.Text = "CLIENTE: " + strNome + " " + strSobrenome;
                    }
                    else
                    {
                        lblCliente.Text = "CLIENTE: " + strPrimeiroNome + " " + strNomeMeio + " " + strUltimoNome;
                    }
                    // break;
                    if (PedidoValido(strPedido) == true)
                    {
                        blExibe = true;
                    }
                    else
                    {
                        if (MessageBox.Show("Esse Rastreio para o pedido " + lblPedido.Text + " já foi enviado!! Pesquisa mesmo assim? ", "PontoZero Informa", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1) == DialogResult.Yes)
                        {
                            //DIGITAR SENHA
                            blExibe = true;
                        }
                        else
                        {
                            blExibe = false;
                        }
                        txtRastreio.Text = "";
                    }
                }
            }
            if (blExibe == false)
            {
                //if (Carrega_CEP_Antigo(strCEP) == false)
                //{
                //}

                MessageBox.Show("Não possivel encontrar esse pedido!!", "PontoZero Informa");
            }

            //dataReader.Close();

            if (blExibe == false)
            {
                MessageBox.Show("Não possivel encontrar esse pedido!!", "PontoZero Informa");
                txtCEP.Text = "";
                lblPedido.Text = strCEP;
            }
            else
            {
                if (txtRastreio.Text == "")
                {
                    txtRastreio.Text = txtCEP.Text;
                }

                txtRastreio.Text = "";
                txtCEP.Text = "";
                chkReenvio.Checked = false;
                chkIgnorarData.Checked = false;
                txtEAN.Focus();

                PesquisaItensPedido(strPedido);

                if (lblLoja.Text != "LOJA")
                {
                    string strStatus = global.ConsultarStatus(lblPedido.Text.Substring(8), strCertificado);

                    if (strStatus != "0")
                    {
                        global.AtualizaStatus(strStatus, lblPedido.Text.Substring(8));
                        if (strStatus != "Aprovado" && strStatus != "Aberto" && strStatus != "Expedição")
                        {
                            MessageBox.Show("Esse pedido está com o status: " + strStatus, "PontoZero Informa");
                        }
                    }
                    else
                    {
                        MessageBox.Show("Não foi possível encontrar esse pedido no Ideris", "PontoZero Informa");
                    }
                }
            }

            if (global.myDB.connection.State != ConnectionState.Closed)
            {
                global.myDB.CloseConnection();
            }
        }

        private void label5_Click(object sender, EventArgs e)
        {

        }

        private void PesquisaItensPedido(String strPedido)
        {            
            MySqlCommand cmd1;
            MySqlDataReader dataPedido;
            string query;

            lstItensPedido.Columns.Clear();
            lstItensPedido.Items.Clear();

            lstItensPedido.CheckBoxes = false;

            lstItensPedido.Columns.Add("SKU", 130);
            lstItensPedido.Columns.Add("PRODUTO", 500);
            lstItensPedido.Columns.Add("QTDE", 100);
            lstItensPedido.Columns.Add("EAN", 100);

            if (lblPlataforma.Text == "LOJA")
            {
                query = " select IP.SKU, IP.NAME nm_produto, IP.qty_ordered qtde,  COALESCE(trim(EAN.VALUE), 0) EAN ";
                query = query + " from sales_flat_order_item IP ";
                query = query + " left JOIN catalog_product_entity SKU ";
                query = query + " ON SKU.SKU = IP.sku ";
                query = query + " INNER JOIN sales_flat_order VENDA ";
                query = query + " ON IP.order_id = VENDA.entity_id ";
                query = query + " left JOIN catalog_product_entity_varchar EAN ";
                query = query + " ON EAN.attribute_id = 169 ";
                query = query + " AND SKU.entity_id = EAN.entity_id ";

                query = query + " where VENDA.increment_id = '" + strPedido + "'  AND IP.PRODUCT_TYPE = 'simple';";              

            }
            else
            {
                query = " select IP.SKU, IP.nm_produto, IP.qtde,  COALESCE(trim(EAN.VALUE), 0) EAN ";
                query = query + " from ITENS_PEDIDOS_INTEGRADOS IP ";
                query = query + " left JOIN catalog_product_entity SKU ";
                query = query + " ON SKU.SKU = IP.sku ";
                query = query + " left JOIN catalog_product_entity_varchar EAN ";
                query = query + " ON EAN.attribute_id = 169 ";
                query = query + " AND SKU.entity_id = EAN.entity_id ";

                if (lblPlataforma.Text == "MERCADOLIVRE" || lblPlataforma.Text == "MERCADO PLACE")
                {
                    query = query + " where id_pedido in (SELECT id_pedido FROM PEDIDOS_INTEGRADOS WHERE cd_rastreio = '" + txtRastreio.Text + "'); ";
                }
                else
                {
                    query = query + " where id_pedido = '" + strPedido + "';";
                }

            }

            global.myDB.CloseConnection();

            if (global.myDB.connection.State == ConnectionState.Closed)
            {
                global.myDB.OpenConnection();
            }

            cmd1 = new MySqlCommand(query, global.myDB.connection);

            try
            {
                dataPedido = cmd1.ExecuteReader();
            }
            catch (Exception)
            {                
                global.myDB.OpenConnection();
                cmd1 = new MySqlCommand(query, global.myDB.connection);
                dataPedido = cmd1.ExecuteReader();
            }

            while (dataPedido.Read())
            {
                ListViewItem item;
                
                for (int i = 0; i < Convert.ToInt32(dataPedido["QTDE"]); i++)
                {
                    item = new ListViewItem(new[] { dataPedido["SKU"].ToString(), dataPedido["NM_PRODUTO"].ToString(), "1", dataPedido["EAN"].ToString() });

                    if(dataPedido["EAN"].ToString() == "0")
                    {
                        item.ForeColor = Color.Red;
                    }
                    else
                    {
                        //item.ForeColor = item.BackColor;
                    }

                    lstItensPedido.Items.Add(item);
                }
                        
            }

            dataPedido.Close();

            if (global.myDB.connection.State != ConnectionState.Closed)
            {
                global.myDB.CloseConnection();
            }

            lblTotal.Text = lstItensPedido.Items.Count.ToString();

            if(lstItensPedido.Items.Count.ToString()=="0")
            {
              //  BuscaItemIderis(strPedido);
            }            
        }

        private void frmConfSeparacao_Load(object sender, EventArgs e)
        {
            lblPedido.Text = "";
            lblCliente.Text = "";
            lblPlataforma.Text = "";
            lblLoja.Text = "";
            lblPedidoPlataforma.Text = "";

            CarregaCertificado();
            QuantidadeSeparado();
        }

        private void CarregaCertificado()
        {            
            MySqlCommand cmd1;
            MySqlDataReader dataPedido;            

            string query = "select * from IDERIS_TOKEN;";

            global.myDB.CloseConnection();

            if (global.myDB.connection.State == ConnectionState.Closed)
            {
                global.myDB.OpenConnection();
            }

            cmd1 = new MySqlCommand(query, global.myDB.connection);

            dataPedido = cmd1.ExecuteReader();

            if (dataPedido.Read())
            {
                strCertificado = dataPedido["TOKEN_PROVISORIO"].ToString();
            }

            dataPedido.Close();

            if (global.myDB.connection.State != ConnectionState.Closed)
            {
                global.myDB.CloseConnection();
            }            
        }

        private void label7_Click(object sender, EventArgs e)
        {

        }

        private void label6_Click(object sender, EventArgs e)
        {

        }

        private void txtRastreio_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtEAN_KeyPress(object sender, KeyPressEventArgs e)
        {
            //if ((blTempoDig == false && chkDigitar.Checked == false) || e.KeyChar == 13)
            //{
            //    timer2.Enabled = true;
            //}
            if (e.KeyChar == 13)
            {
                txtEAN.Text = txtEAN.Text.Replace(" ", "");
                BuscaEAN();
            }
        }

        private void timer2_Tick(object sender, EventArgs e)
        {
            timer2.Enabled = false;

            txtEAN.Text = txtEAN.Text.Replace(" ", "");


            BuscaEAN();

            timer2.Enabled = false;
        }

        private void BuscaEAN()
        {
            bool blAchou = false;
            int intCont = 0;

            if (lstItensPedido.Items.Count > 0)
            {
                foreach (ListViewItem item in lstItensPedido.Items)
                {
                    if (item.SubItems[3].Text == txtEAN.Text && item.ForeColor != Color.Black && blAchou != true)
                    {
                        item.ForeColor = Color.Black;
                        blAchou = true;
                    }

                    if(item.ForeColor == Color.Black)
                    {
                        intCont = intCont + 1;
                    }
                }
            }

            txtEAN.Text = "";

            if (blAchou==false)
            {
                MessageBox.Show("EAN não encontrado ou não faz parte desse pedido", "PontoZero Informa");
                txtEAN.Focus();
            }
            else
            {
                if(lstItensPedido.Items.Count == intCont)
                {
                    MarcarSeparado();

                    lblPedido.Text = "";
                    lblCliente.Text = "";
                    lblPlataforma.Text = "";
                    lblLoja.Text = "";
                    lblPedidoPlataforma.Text = "";
                    lblTotal.Text = "";

                    lstItensPedido.Columns.Clear();
                    lstItensPedido.Items.Clear();
                    lstItensPedido.CheckBoxes = false;

                    frmMensagemGeral frMSG = new frmMensagemGeral();
                    frMSG.strMensagem = "Pedido conferido com exito!!";
                    frMSG.ShowDialog(this);

                    

                    int intC = Convert.ToInt32(lblLidos.Text) + 1;
                    lblLidos.Text = intC.ToString();

                    txtRastreio.Focus();
                }
            }            
        }

        private void MarcarSeparado()
        {
            string query = "";

           //global.AtualizaStatus("Separado", lblPedido.Text.Substring(8));

            if (query == "1")
            {
                query = "UPDATE sales_flat_order SET separado = 1";
                query = query + " WHERE item_id = '" + lblPedido.Text + "'";
            }
            else
            {
                query = "UPDATE PEDIDOS_INTEGRADOS SET separado = 1, status = 'Separado', data_separado = now(), cd_rastreio =  '" + strRastreio.ToString().ToUpper() + "' " ;
                query = query + " WHERE id_pedido = '" + lblPedido.Text.Substring(8) + "'";
            }

            global.myDB.CloseConnection();

            if (global.myDB.connection.State == ConnectionState.Closed)
            {
                global.myDB.OpenConnection();
            }

            MySqlCommand cmd1 = new MySqlCommand(query, global.myDB.connection);

            try
            {
                cmd1.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "PontoZero Informa");
            }
        }


        private void QuantidadeSeparado()
        {
            MySqlCommand cmd;

            lblLidos.Text = "0";

            string query = "SELECT COUNT(*) contagem FROM PEDIDOS_INTEGRADOS WHERE  DATA_SEPARADO >= DATE_FORMAT(NOW(), '%Y-%m-%d');";

            if (global.myDB.connection.State == ConnectionState.Closed)
            {
                global.myDB.OpenConnection();
            }

            cmd = new MySqlCommand(query, global.myDB.connection);

            try
            {
                dataReader = cmd.ExecuteReader();
            }
            catch (Exception)
            {
                global.myDB.OpenConnection();
                cmd = new MySqlCommand(query, global.myDB.connection);
                dataReader = cmd.ExecuteReader();
            }

            while (dataReader.Read())
            {
                lblLidos.Text= dataReader["contagem"].ToString();
            }
        }



        private void txtCEP_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtEAN_TextChanged(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
         
        }

        private void printDocument1_PrintPage(object sender, PrintPageEventArgs e)
        {

        }

        private void chkProblema_CheckedChanged(object sender, EventArgs e)
        {
            txtProblema.Enabled = chkProblema.Checked;
        }
    }
}
