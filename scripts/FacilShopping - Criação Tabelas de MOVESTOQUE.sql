CREATE TABLE MOVESTOQUE (id INT NOT NULL PRIMARY KEY AUTO_INCREMENT, 
					     SKU CHAR(50) NOT NULL,
                         OBSERVACAO CHAR(255) NULL,
                         DATA datetime NOT NULL,
						 QUANTIDADE DOUBLE NOT NULL,
                         ENTRADA BIT NOT NULL DEFAULT TRUE,
                         QUANT_ESTOQUE DOUBLE NOT NULL);                                             