﻿namespace FacilShoppingReports
{
    partial class frmEnvio_Correios
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.label2 = new System.Windows.Forms.Label();
            this.txtRastreio = new System.Windows.Forms.TextBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.radioButton6 = new System.Windows.Forms.RadioButton();
            this.radioButton4 = new System.Windows.Forms.RadioButton();
            this.radioButton3 = new System.Windows.Forms.RadioButton();
            this.radioButton2 = new System.Windows.Forms.RadioButton();
            this.radioButton1 = new System.Windows.Forms.RadioButton();
            this.txtDados = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.lblPedido = new System.Windows.Forms.Label();
            this.lblCliente = new System.Windows.Forms.Label();
            this.lblPlataforma = new System.Windows.Forms.Label();
            this.timeDigitacao = new System.Windows.Forms.Timer(this.components);
            this.label10 = new System.Windows.Forms.Label();
            this.chkReenvio = new System.Windows.Forms.CheckBox();
            this.chkIgnorarData = new System.Windows.Forms.CheckBox();
            this.tabPrincipal = new System.Windows.Forms.TabControl();
            this.tab1 = new System.Windows.Forms.TabPage();
            this.btnCarregarCorreio = new System.Windows.Forms.Button();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.lstCorreios = new System.Windows.Forms.ListView();
            this.chkEnvioCorreio = new System.Windows.Forms.CheckBox();
            this.button1 = new System.Windows.Forms.Button();
            this.chkBuscar = new System.Windows.Forms.CheckBox();
            this.btnExcluir = new System.Windows.Forms.Button();
            this.lblDataCriacao = new System.Windows.Forms.Label();
            this.txtNumeroEnvio = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.btnImprimir = new System.Windows.Forms.Button();
            this.btnNovo = new System.Windows.Forms.Button();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.btnCarregarMercadoEnvios = new System.Windows.Forms.Button();
            this.button5 = new System.Windows.Forms.Button();
            this.cmdImprimeMercadoLivre = new System.Windows.Forms.Button();
            this.cmdNovoMercadoLIvre = new System.Windows.Forms.Button();
            this.lblDataCriacaoML = new System.Windows.Forms.Label();
            this.txtNumeroEnvioML = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.lstMercadoLivre = new System.Windows.Forms.ListView();
            this.button2 = new System.Windows.Forms.Button();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.btnCarregarB2W = new System.Windows.Forms.Button();
            this.lblRegistrosB2WMundial = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.lblRegistrosB2WBela = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.lblRegistrosB2WAlpha = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.lblRegistrosB2WFacilShopping = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.lstB2W = new System.Windows.Forms.ListView();
            this.lblDataCriacaoB2W = new System.Windows.Forms.Label();
            this.txtNumeroEnvioB2W = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.button3 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.cmdNovoB2W = new System.Windows.Forms.Button();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.btnCarregarMagalu = new System.Windows.Forms.Button();
            this.lblRegistrosMagaluMundial = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.lblRegistrosMagaluBela = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.lblRegistrosMagaluAlpha = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.lblRegistrosMagaluFacilShopping = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.lstMagalu = new System.Windows.Forms.ListView();
            this.lblDataCriacaoMagalu = new System.Windows.Forms.Label();
            this.txtNumeroEnvioMagalu = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.button6 = new System.Windows.Forms.Button();
            this.button7 = new System.Windows.Forms.Button();
            this.button8 = new System.Windows.Forms.Button();
            this.tabPage5 = new System.Windows.Forms.TabPage();
            this.btnCarregarMercadoPlace = new System.Windows.Forms.Button();
            this.lblRegistrosMPlaceMundial = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.lblRegistrosMPlaceBela = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.lblRegistrosMPlaceAlpha = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.lblRegistrosMPlaceFS = new System.Windows.Forms.Label();
            this.label30 = new System.Windows.Forms.Label();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.lstMlPlace = new System.Windows.Forms.ListView();
            this.lblDataCriacaoMercadoPlace = new System.Windows.Forms.Label();
            this.txtNumeroEnvioMlPlace = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.cmdExcluirMercadoPlace = new System.Windows.Forms.Button();
            this.cmdImprimirMercadoPlace = new System.Windows.Forms.Button();
            this.cmdNovoMercadoPlace = new System.Windows.Forms.Button();
            this.tabPage6 = new System.Windows.Forms.TabPage();
            this.btnCarregarTotalExpress = new System.Windows.Forms.Button();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this.lstTotalExpress = new System.Windows.Forms.ListView();
            this.lblDataCriacaoTotalExpress = new System.Windows.Forms.Label();
            this.txtNumeroEnvioTotalExpress = new System.Windows.Forms.TextBox();
            this.label36 = new System.Windows.Forms.Label();
            this.cmdExcluirTotalExpress = new System.Windows.Forms.Button();
            this.cmdImprimirTotalExpress = new System.Windows.Forms.Button();
            this.cmdNovoTotalExpress = new System.Windows.Forms.Button();
            this.tabPage7 = new System.Windows.Forms.TabPage();
            this.btnCarregarSequoia = new System.Windows.Forms.Button();
            this.groupBox8 = new System.Windows.Forms.GroupBox();
            this.lstSequoia = new System.Windows.Forms.ListView();
            this.lblDataCriacaoSequoia = new System.Windows.Forms.Label();
            this.txtNumeroEnvioSequoia = new System.Windows.Forms.TextBox();
            this.label29 = new System.Windows.Forms.Label();
            this.cmdExcluirSequoia = new System.Windows.Forms.Button();
            this.cmdImprimirSequoia = new System.Windows.Forms.Button();
            this.button12 = new System.Windows.Forms.Button();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.button9 = new System.Windows.Forms.Button();
            this.lblRegistrosAmazonMJ = new System.Windows.Forms.Label();
            this.label32 = new System.Windows.Forms.Label();
            this.lblRegistrosAmazonBella = new System.Windows.Forms.Label();
            this.label34 = new System.Windows.Forms.Label();
            this.lblRegistrosAmazonAJ = new System.Windows.Forms.Label();
            this.label37 = new System.Windows.Forms.Label();
            this.lblRegistrosAmazonFS = new System.Windows.Forms.Label();
            this.label39 = new System.Windows.Forms.Label();
            this.groupBox9 = new System.Windows.Forms.GroupBox();
            this.lstAmazon = new System.Windows.Forms.ListView();
            this.lblDataCriacaoAmazon = new System.Windows.Forms.Label();
            this.txtNumeroEnvioAmazon = new System.Windows.Forms.TextBox();
            this.label41 = new System.Windows.Forms.Label();
            this.button10 = new System.Windows.Forms.Button();
            this.button11 = new System.Windows.Forms.Button();
            this.button13 = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.lblRegistros = new System.Windows.Forms.Label();
            this.lblRegistrosML = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.lblRegistrosMagaLu = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.lblRegistrosB2W = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.lblLoja = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.lblLidos = new System.Windows.Forms.Label();
            this.lblRegistrosMercadoPlace = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.chkPedidoAvulso = new System.Windows.Forms.CheckBox();
            this.lblRegistrosTotalExpress = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.lblRegistrosAmazon = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.lblRegistrosSequoia = new System.Windows.Forms.Label();
            this.label31 = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            this.tabPrincipal.SuspendLayout();
            this.tab1.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.tabPage3.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.tabPage4.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.tabPage5.SuspendLayout();
            this.groupBox6.SuspendLayout();
            this.tabPage6.SuspendLayout();
            this.groupBox7.SuspendLayout();
            this.tabPage7.SuspendLayout();
            this.groupBox8.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.groupBox9.SuspendLayout();
            this.SuspendLayout();
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(11, 11);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(49, 13);
            this.label2.TabIndex = 5;
            this.label2.Text = "Rastreio:";
            this.label2.Click += new System.EventHandler(this.label2_Click);
            // 
            // txtRastreio
            // 
            this.txtRastreio.Location = new System.Drawing.Point(66, 7);
            this.txtRastreio.Name = "txtRastreio";
            this.txtRastreio.Size = new System.Drawing.Size(191, 20);
            this.txtRastreio.TabIndex = 6;
            this.txtRastreio.TextChanged += new System.EventHandler(this.txtRastreio_TextChanged);
            this.txtRastreio.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtRastreio_KeyPress);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.radioButton6);
            this.groupBox1.Controls.Add(this.radioButton4);
            this.groupBox1.Controls.Add(this.radioButton3);
            this.groupBox1.Controls.Add(this.radioButton2);
            this.groupBox1.Controls.Add(this.radioButton1);
            this.groupBox1.Location = new System.Drawing.Point(342, 109);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(144, 139);
            this.groupBox1.TabIndex = 10;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Plataforma";
            this.groupBox1.Visible = false;
            this.groupBox1.Enter += new System.EventHandler(this.groupBox1_Enter);
            // 
            // radioButton6
            // 
            this.radioButton6.AutoSize = true;
            this.radioButton6.Location = new System.Drawing.Point(15, 115);
            this.radioButton6.Name = "radioButton6";
            this.radioButton6.Size = new System.Drawing.Size(104, 17);
            this.radioButton6.TabIndex = 5;
            this.radioButton6.TabStop = true;
            this.radioButton6.Text = "Carta Registrada";
            this.radioButton6.UseVisualStyleBackColor = true;
            this.radioButton6.CheckedChanged += new System.EventHandler(this.radioButton6_CheckedChanged);
            // 
            // radioButton4
            // 
            this.radioButton4.AutoSize = true;
            this.radioButton4.Location = new System.Drawing.Point(14, 94);
            this.radioButton4.Name = "radioButton4";
            this.radioButton4.Size = new System.Drawing.Size(87, 17);
            this.radioButton4.TabIndex = 3;
            this.radioButton4.Text = "Melhor Envio";
            this.radioButton4.UseVisualStyleBackColor = true;
            this.radioButton4.CheckedChanged += new System.EventHandler(this.radioButton4_CheckedChanged);
            // 
            // radioButton3
            // 
            this.radioButton3.AutoSize = true;
            this.radioButton3.Location = new System.Drawing.Point(14, 69);
            this.radioButton3.Name = "radioButton3";
            this.radioButton3.Size = new System.Drawing.Size(49, 17);
            this.radioButton3.TabIndex = 2;
            this.radioButton3.TabStop = true;
            this.radioButton3.Text = "B2W";
            this.radioButton3.UseVisualStyleBackColor = true;
            this.radioButton3.CheckedChanged += new System.EventHandler(this.radioButton3_CheckedChanged);
            // 
            // radioButton2
            // 
            this.radioButton2.AutoSize = true;
            this.radioButton2.Location = new System.Drawing.Point(14, 44);
            this.radioButton2.Name = "radioButton2";
            this.radioButton2.Size = new System.Drawing.Size(74, 17);
            this.radioButton2.TabIndex = 1;
            this.radioButton2.TabStop = true;
            this.radioButton2.Text = "LudoStore";
            this.radioButton2.UseVisualStyleBackColor = true;
            this.radioButton2.CheckedChanged += new System.EventHandler(this.radioButton2_CheckedChanged);
            // 
            // radioButton1
            // 
            this.radioButton1.AutoSize = true;
            this.radioButton1.Checked = true;
            this.radioButton1.Location = new System.Drawing.Point(14, 19);
            this.radioButton1.Name = "radioButton1";
            this.radioButton1.Size = new System.Drawing.Size(93, 17);
            this.radioButton1.TabIndex = 0;
            this.radioButton1.TabStop = true;
            this.radioButton1.Text = "Mercado Livre";
            this.radioButton1.UseVisualStyleBackColor = true;
            this.radioButton1.CheckedChanged += new System.EventHandler(this.radioButton1_CheckedChanged);
            // 
            // txtDados
            // 
            this.txtDados.Location = new System.Drawing.Point(317, 8);
            this.txtDados.Name = "txtDados";
            this.txtDados.Size = new System.Drawing.Size(113, 20);
            this.txtDados.TabIndex = 17;
            this.txtDados.TextChanged += new System.EventHandler(this.txtDados_TextChanged);
            this.txtDados.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtDados_KeyPress);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(270, 12);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(41, 13);
            this.label3.TabIndex = 16;
            this.label3.Text = "Dados:";
            // 
            // lblPedido
            // 
            this.lblPedido.AutoSize = true;
            this.lblPedido.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPedido.Location = new System.Drawing.Point(206, 35);
            this.lblPedido.Name = "lblPedido";
            this.lblPedido.Size = new System.Drawing.Size(51, 16);
            this.lblPedido.TabIndex = 18;
            this.lblPedido.Text = "label3";
            this.lblPedido.Click += new System.EventHandler(this.lblPedido_Click);
            // 
            // lblCliente
            // 
            this.lblCliente.AutoSize = true;
            this.lblCliente.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCliente.Location = new System.Drawing.Point(12, 65);
            this.lblCliente.Name = "lblCliente";
            this.lblCliente.Size = new System.Drawing.Size(51, 16);
            this.lblCliente.TabIndex = 19;
            this.lblCliente.Text = "label3";
            this.lblCliente.Click += new System.EventHandler(this.lblCliente_Click);
            // 
            // lblPlataforma
            // 
            this.lblPlataforma.AutoSize = true;
            this.lblPlataforma.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPlataforma.Location = new System.Drawing.Point(327, 35);
            this.lblPlataforma.Name = "lblPlataforma";
            this.lblPlataforma.Size = new System.Drawing.Size(51, 16);
            this.lblPlataforma.TabIndex = 21;
            this.lblPlataforma.Text = "label3";
            // 
            // timeDigitacao
            // 
            this.timeDigitacao.Interval = 1000;
            this.timeDigitacao.Tick += new System.EventHandler(this.timeDigitacao_Tick);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(8, 87);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(985, 13);
            this.label10.TabIndex = 33;
            this.label10.Text = "_________________________________________________________________________________" +
    "________________________________________________________________________________" +
    "__";
            this.label10.Click += new System.EventHandler(this.label10_Click);
            // 
            // chkReenvio
            // 
            this.chkReenvio.AutoSize = true;
            this.chkReenvio.Location = new System.Drawing.Point(14, 34);
            this.chkReenvio.Name = "chkReenvio";
            this.chkReenvio.Size = new System.Drawing.Size(66, 17);
            this.chkReenvio.TabIndex = 36;
            this.chkReenvio.Text = "Reenvio";
            this.chkReenvio.UseVisualStyleBackColor = true;
            // 
            // chkIgnorarData
            // 
            this.chkIgnorarData.AutoSize = true;
            this.chkIgnorarData.Location = new System.Drawing.Point(86, 34);
            this.chkIgnorarData.Name = "chkIgnorarData";
            this.chkIgnorarData.Size = new System.Drawing.Size(85, 17);
            this.chkIgnorarData.TabIndex = 37;
            this.chkIgnorarData.Text = "Ignorar Data";
            this.chkIgnorarData.UseVisualStyleBackColor = true;
            // 
            // tabPrincipal
            // 
            this.tabPrincipal.Controls.Add(this.tab1);
            this.tabPrincipal.Controls.Add(this.tabPage2);
            this.tabPrincipal.Controls.Add(this.tabPage3);
            this.tabPrincipal.Controls.Add(this.tabPage4);
            this.tabPrincipal.Controls.Add(this.tabPage5);
            this.tabPrincipal.Controls.Add(this.tabPage6);
            this.tabPrincipal.Controls.Add(this.tabPage7);
            this.tabPrincipal.Controls.Add(this.tabPage1);
            this.tabPrincipal.Location = new System.Drawing.Point(11, 109);
            this.tabPrincipal.Name = "tabPrincipal";
            this.tabPrincipal.SelectedIndex = 0;
            this.tabPrincipal.Size = new System.Drawing.Size(626, 406);
            this.tabPrincipal.TabIndex = 38;
            // 
            // tab1
            // 
            this.tab1.BackColor = System.Drawing.SystemColors.Control;
            this.tab1.Controls.Add(this.btnCarregarCorreio);
            this.tab1.Controls.Add(this.groupBox3);
            this.tab1.Controls.Add(this.chkEnvioCorreio);
            this.tab1.Controls.Add(this.button1);
            this.tab1.Controls.Add(this.chkBuscar);
            this.tab1.Controls.Add(this.btnExcluir);
            this.tab1.Controls.Add(this.lblDataCriacao);
            this.tab1.Controls.Add(this.txtNumeroEnvio);
            this.tab1.Controls.Add(this.label1);
            this.tab1.Controls.Add(this.btnImprimir);
            this.tab1.Controls.Add(this.btnNovo);
            this.tab1.Location = new System.Drawing.Point(4, 22);
            this.tab1.Name = "tab1";
            this.tab1.Padding = new System.Windows.Forms.Padding(3);
            this.tab1.Size = new System.Drawing.Size(618, 380);
            this.tab1.TabIndex = 0;
            this.tab1.Text = "CORREIO";
            // 
            // btnCarregarCorreio
            // 
            this.btnCarregarCorreio.Image = global::FacilShoppingReports.Properties.Resources.search_icon1;
            this.btnCarregarCorreio.Location = new System.Drawing.Point(524, 290);
            this.btnCarregarCorreio.Name = "btnCarregarCorreio";
            this.btnCarregarCorreio.Size = new System.Drawing.Size(75, 63);
            this.btnCarregarCorreio.TabIndex = 36;
            this.btnCarregarCorreio.Text = "Carregar Envios";
            this.btnCarregarCorreio.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.btnCarregarCorreio.UseVisualStyleBackColor = true;
            this.btnCarregarCorreio.Click += new System.EventHandler(this.button5_Click);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.lstCorreios);
            this.groupBox3.Location = new System.Drawing.Point(22, 40);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(492, 305);
            this.groupBox3.TabIndex = 24;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Correios";
            // 
            // lstCorreios
            // 
            this.lstCorreios.HideSelection = false;
            this.lstCorreios.Location = new System.Drawing.Point(5, 15);
            this.lstCorreios.Name = "lstCorreios";
            this.lstCorreios.Size = new System.Drawing.Size(479, 283);
            this.lstCorreios.TabIndex = 21;
            this.lstCorreios.UseCompatibleStateImageBehavior = false;
            this.lstCorreios.View = System.Windows.Forms.View.Details;
            // 
            // chkEnvioCorreio
            // 
            this.chkEnvioCorreio.AutoSize = true;
            this.chkEnvioCorreio.Location = new System.Drawing.Point(258, 16);
            this.chkEnvioCorreio.Name = "chkEnvioCorreio";
            this.chkEnvioCorreio.Size = new System.Drawing.Size(112, 17);
            this.chkEnvioCorreio.TabIndex = 34;
            this.chkEnvioCorreio.Text = "Envio pelo Correio";
            this.chkEnvioCorreio.UseVisualStyleBackColor = true;
            this.chkEnvioCorreio.Visible = false;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(584, 297);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(87, 30);
            this.button1.TabIndex = 35;
            this.button1.Text = "button1";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Visible = false;
            this.button1.Click += new System.EventHandler(this.button1_Click_1);
            // 
            // chkBuscar
            // 
            this.chkBuscar.AutoSize = true;
            this.chkBuscar.Location = new System.Drawing.Point(455, 18);
            this.chkBuscar.Name = "chkBuscar";
            this.chkBuscar.Size = new System.Drawing.Size(59, 17);
            this.chkBuscar.TabIndex = 33;
            this.chkBuscar.Text = "Buscar";
            this.chkBuscar.UseVisualStyleBackColor = true;
            this.chkBuscar.Visible = false;
            // 
            // btnExcluir
            // 
            this.btnExcluir.Image = global::FacilShoppingReports.Properties.Resources.exit_icon;
            this.btnExcluir.Location = new System.Drawing.Point(524, 215);
            this.btnExcluir.Name = "btnExcluir";
            this.btnExcluir.Size = new System.Drawing.Size(75, 63);
            this.btnExcluir.TabIndex = 31;
            this.btnExcluir.Text = "Excluir Selecionado";
            this.btnExcluir.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.btnExcluir.UseVisualStyleBackColor = true;
            this.btnExcluir.Click += new System.EventHandler(this.btnExcluir_Click_1);
            // 
            // lblDataCriacao
            // 
            this.lblDataCriacao.AutoSize = true;
            this.lblDataCriacao.Location = new System.Drawing.Point(182, 19);
            this.lblDataCriacao.Name = "lblDataCriacao";
            this.lblDataCriacao.Size = new System.Drawing.Size(35, 13);
            this.lblDataCriacao.TabIndex = 29;
            this.lblDataCriacao.Text = "label3";
            // 
            // txtNumeroEnvio
            // 
            this.txtNumeroEnvio.Enabled = false;
            this.txtNumeroEnvio.Location = new System.Drawing.Point(109, 16);
            this.txtNumeroEnvio.Name = "txtNumeroEnvio";
            this.txtNumeroEnvio.Size = new System.Drawing.Size(67, 20);
            this.txtNumeroEnvio.TabIndex = 28;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(25, 19);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(78, 13);
            this.label1.TabIndex = 27;
            this.label1.Text = "Envio numero: ";
            // 
            // btnImprimir
            // 
            this.btnImprimir.Image = global::FacilShoppingReports.Properties.Resources.print_icon;
            this.btnImprimir.Location = new System.Drawing.Point(522, 140);
            this.btnImprimir.Name = "btnImprimir";
            this.btnImprimir.Size = new System.Drawing.Size(75, 63);
            this.btnImprimir.TabIndex = 26;
            this.btnImprimir.Text = "&Imprimir";
            this.btnImprimir.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.btnImprimir.UseVisualStyleBackColor = true;
            this.btnImprimir.Click += new System.EventHandler(this.btnImprimir_Click_1);
            // 
            // btnNovo
            // 
            this.btnNovo.Image = global::FacilShoppingReports.Properties.Resources.new_icon;
            this.btnNovo.Location = new System.Drawing.Point(522, 68);
            this.btnNovo.Name = "btnNovo";
            this.btnNovo.Size = new System.Drawing.Size(75, 63);
            this.btnNovo.TabIndex = 25;
            this.btnNovo.Text = "&Novo";
            this.btnNovo.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.btnNovo.UseVisualStyleBackColor = true;
            this.btnNovo.Visible = false;
            this.btnNovo.Click += new System.EventHandler(this.btnNovo_Click_1);
            // 
            // tabPage2
            // 
            this.tabPage2.BackColor = System.Drawing.Color.WhiteSmoke;
            this.tabPage2.Controls.Add(this.btnCarregarMercadoEnvios);
            this.tabPage2.Controls.Add(this.button5);
            this.tabPage2.Controls.Add(this.cmdImprimeMercadoLivre);
            this.tabPage2.Controls.Add(this.cmdNovoMercadoLIvre);
            this.tabPage2.Controls.Add(this.lblDataCriacaoML);
            this.tabPage2.Controls.Add(this.txtNumeroEnvioML);
            this.tabPage2.Controls.Add(this.label9);
            this.tabPage2.Controls.Add(this.groupBox2);
            this.tabPage2.Controls.Add(this.button2);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(618, 380);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "MERCADO ENVIOS";
            // 
            // btnCarregarMercadoEnvios
            // 
            this.btnCarregarMercadoEnvios.Image = global::FacilShoppingReports.Properties.Resources.search_icon1;
            this.btnCarregarMercadoEnvios.Location = new System.Drawing.Point(524, 293);
            this.btnCarregarMercadoEnvios.Name = "btnCarregarMercadoEnvios";
            this.btnCarregarMercadoEnvios.Size = new System.Drawing.Size(75, 63);
            this.btnCarregarMercadoEnvios.TabIndex = 46;
            this.btnCarregarMercadoEnvios.Text = "Carregar Envios";
            this.btnCarregarMercadoEnvios.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.btnCarregarMercadoEnvios.UseVisualStyleBackColor = true;
            this.btnCarregarMercadoEnvios.Click += new System.EventHandler(this.btnCarregarMercadoEnvios_Click);
            // 
            // button5
            // 
            this.button5.Location = new System.Drawing.Point(0, 0);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(75, 23);
            this.button5.TabIndex = 0;
            this.button5.Visible = false;
            // 
            // cmdImprimeMercadoLivre
            // 
            this.cmdImprimeMercadoLivre.Image = global::FacilShoppingReports.Properties.Resources.print_icon;
            this.cmdImprimeMercadoLivre.Location = new System.Drawing.Point(522, 140);
            this.cmdImprimeMercadoLivre.Name = "cmdImprimeMercadoLivre";
            this.cmdImprimeMercadoLivre.Size = new System.Drawing.Size(75, 63);
            this.cmdImprimeMercadoLivre.TabIndex = 45;
            this.cmdImprimeMercadoLivre.Text = "&Imprimir";
            this.cmdImprimeMercadoLivre.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.cmdImprimeMercadoLivre.UseVisualStyleBackColor = true;
            this.cmdImprimeMercadoLivre.Click += new System.EventHandler(this.cmdImprimeMercadoLivre_Click_1);
            // 
            // cmdNovoMercadoLIvre
            // 
            this.cmdNovoMercadoLIvre.Image = global::FacilShoppingReports.Properties.Resources.new_icon;
            this.cmdNovoMercadoLIvre.Location = new System.Drawing.Point(522, 68);
            this.cmdNovoMercadoLIvre.Name = "cmdNovoMercadoLIvre";
            this.cmdNovoMercadoLIvre.Size = new System.Drawing.Size(75, 63);
            this.cmdNovoMercadoLIvre.TabIndex = 44;
            this.cmdNovoMercadoLIvre.Text = "&Novo";
            this.cmdNovoMercadoLIvre.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.cmdNovoMercadoLIvre.UseVisualStyleBackColor = true;
            this.cmdNovoMercadoLIvre.Visible = false;
            this.cmdNovoMercadoLIvre.Click += new System.EventHandler(this.cmdNovoMercadoLIvre_Click_1);
            // 
            // lblDataCriacaoML
            // 
            this.lblDataCriacaoML.AutoSize = true;
            this.lblDataCriacaoML.Location = new System.Drawing.Point(182, 19);
            this.lblDataCriacaoML.Name = "lblDataCriacaoML";
            this.lblDataCriacaoML.Size = new System.Drawing.Size(35, 13);
            this.lblDataCriacaoML.TabIndex = 42;
            this.lblDataCriacaoML.Text = "label3";
            // 
            // txtNumeroEnvioML
            // 
            this.txtNumeroEnvioML.Enabled = false;
            this.txtNumeroEnvioML.Location = new System.Drawing.Point(109, 16);
            this.txtNumeroEnvioML.Name = "txtNumeroEnvioML";
            this.txtNumeroEnvioML.Size = new System.Drawing.Size(67, 20);
            this.txtNumeroEnvioML.TabIndex = 41;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(25, 19);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(78, 13);
            this.label9.TabIndex = 40;
            this.label9.Text = "Envio numero: ";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.lstMercadoLivre);
            this.groupBox2.Location = new System.Drawing.Point(22, 40);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(496, 305);
            this.groupBox2.TabIndex = 39;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Mercado Livre";
            // 
            // lstMercadoLivre
            // 
            this.lstMercadoLivre.HideSelection = false;
            this.lstMercadoLivre.Location = new System.Drawing.Point(5, 15);
            this.lstMercadoLivre.Name = "lstMercadoLivre";
            this.lstMercadoLivre.Size = new System.Drawing.Size(480, 283);
            this.lstMercadoLivre.TabIndex = 26;
            this.lstMercadoLivre.UseCompatibleStateImageBehavior = false;
            this.lstMercadoLivre.View = System.Windows.Forms.View.Details;
            // 
            // button2
            // 
            this.button2.Image = global::FacilShoppingReports.Properties.Resources.exit_icon;
            this.button2.Location = new System.Drawing.Point(524, 215);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 63);
            this.button2.TabIndex = 36;
            this.button2.Text = "Excluir Selecionado";
            this.button2.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click_1);
            // 
            // tabPage3
            // 
            this.tabPage3.BackColor = System.Drawing.Color.WhiteSmoke;
            this.tabPage3.Controls.Add(this.btnCarregarB2W);
            this.tabPage3.Controls.Add(this.lblRegistrosB2WMundial);
            this.tabPage3.Controls.Add(this.label20);
            this.tabPage3.Controls.Add(this.lblRegistrosB2WBela);
            this.tabPage3.Controls.Add(this.label18);
            this.tabPage3.Controls.Add(this.lblRegistrosB2WAlpha);
            this.tabPage3.Controls.Add(this.label16);
            this.tabPage3.Controls.Add(this.lblRegistrosB2WFacilShopping);
            this.tabPage3.Controls.Add(this.label13);
            this.tabPage3.Controls.Add(this.groupBox4);
            this.tabPage3.Controls.Add(this.lblDataCriacaoB2W);
            this.tabPage3.Controls.Add(this.txtNumeroEnvioB2W);
            this.tabPage3.Controls.Add(this.label12);
            this.tabPage3.Controls.Add(this.button3);
            this.tabPage3.Controls.Add(this.button4);
            this.tabPage3.Controls.Add(this.cmdNovoB2W);
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Size = new System.Drawing.Size(618, 380);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "B2W";
            // 
            // btnCarregarB2W
            // 
            this.btnCarregarB2W.Image = global::FacilShoppingReports.Properties.Resources.search_icon1;
            this.btnCarregarB2W.Location = new System.Drawing.Point(524, 288);
            this.btnCarregarB2W.Name = "btnCarregarB2W";
            this.btnCarregarB2W.Size = new System.Drawing.Size(75, 63);
            this.btnCarregarB2W.TabIndex = 55;
            this.btnCarregarB2W.Text = "Carregar Envios";
            this.btnCarregarB2W.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.btnCarregarB2W.UseVisualStyleBackColor = true;
            this.btnCarregarB2W.Click += new System.EventHandler(this.btnCarregarB2W_Click);
            // 
            // lblRegistrosB2WMundial
            // 
            this.lblRegistrosB2WMundial.AutoSize = true;
            this.lblRegistrosB2WMundial.Location = new System.Drawing.Point(575, 354);
            this.lblRegistrosB2WMundial.Name = "lblRegistrosB2WMundial";
            this.lblRegistrosB2WMundial.Size = new System.Drawing.Size(13, 13);
            this.lblRegistrosB2WMundial.TabIndex = 54;
            this.lblRegistrosB2WMundial.Text = "0";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(490, 354);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(47, 13);
            this.label20.TabIndex = 53;
            this.label20.Text = "Mundial:";
            // 
            // lblRegistrosB2WBela
            // 
            this.lblRegistrosB2WBela.AutoSize = true;
            this.lblRegistrosB2WBela.Location = new System.Drawing.Point(266, 354);
            this.lblRegistrosB2WBela.Name = "lblRegistrosB2WBela";
            this.lblRegistrosB2WBela.Size = new System.Drawing.Size(13, 13);
            this.lblRegistrosB2WBela.TabIndex = 52;
            this.lblRegistrosB2WBela.Text = "0";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(339, 354);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(65, 13);
            this.label18.TabIndex = 51;
            this.label18.Text = "AlphaJogos:";
            // 
            // lblRegistrosB2WAlpha
            // 
            this.lblRegistrosB2WAlpha.AutoSize = true;
            this.lblRegistrosB2WAlpha.Location = new System.Drawing.Point(410, 354);
            this.lblRegistrosB2WAlpha.Name = "lblRegistrosB2WAlpha";
            this.lblRegistrosB2WAlpha.Size = new System.Drawing.Size(13, 13);
            this.lblRegistrosB2WAlpha.TabIndex = 50;
            this.lblRegistrosB2WAlpha.Text = "0";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(182, 354);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(78, 13);
            this.label16.TabIndex = 49;
            this.label16.Text = "BellaShopping:";
            // 
            // lblRegistrosB2WFacilShopping
            // 
            this.lblRegistrosB2WFacilShopping.AutoSize = true;
            this.lblRegistrosB2WFacilShopping.Location = new System.Drawing.Point(106, 354);
            this.lblRegistrosB2WFacilShopping.Name = "lblRegistrosB2WFacilShopping";
            this.lblRegistrosB2WFacilShopping.Size = new System.Drawing.Size(13, 13);
            this.lblRegistrosB2WFacilShopping.TabIndex = 48;
            this.lblRegistrosB2WFacilShopping.Text = "0";
            this.lblRegistrosB2WFacilShopping.Click += new System.EventHandler(this.label11_Click);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(24, 354);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(78, 13);
            this.label13.TabIndex = 47;
            this.label13.Text = "Facilshopping :";
            this.label13.Click += new System.EventHandler(this.label13_Click);
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.lstB2W);
            this.groupBox4.Location = new System.Drawing.Point(22, 40);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(492, 305);
            this.groupBox4.TabIndex = 35;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "B2W";
            // 
            // lstB2W
            // 
            this.lstB2W.HideSelection = false;
            this.lstB2W.Location = new System.Drawing.Point(5, 15);
            this.lstB2W.Name = "lstB2W";
            this.lstB2W.Size = new System.Drawing.Size(479, 283);
            this.lstB2W.TabIndex = 21;
            this.lstB2W.UseCompatibleStateImageBehavior = false;
            this.lstB2W.View = System.Windows.Forms.View.Details;
            // 
            // lblDataCriacaoB2W
            // 
            this.lblDataCriacaoB2W.AutoSize = true;
            this.lblDataCriacaoB2W.Location = new System.Drawing.Point(182, 19);
            this.lblDataCriacaoB2W.Name = "lblDataCriacaoB2W";
            this.lblDataCriacaoB2W.Size = new System.Drawing.Size(35, 13);
            this.lblDataCriacaoB2W.TabIndex = 40;
            this.lblDataCriacaoB2W.Text = "label3";
            // 
            // txtNumeroEnvioB2W
            // 
            this.txtNumeroEnvioB2W.Enabled = false;
            this.txtNumeroEnvioB2W.Location = new System.Drawing.Point(109, 16);
            this.txtNumeroEnvioB2W.Name = "txtNumeroEnvioB2W";
            this.txtNumeroEnvioB2W.Size = new System.Drawing.Size(67, 20);
            this.txtNumeroEnvioB2W.TabIndex = 39;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(25, 19);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(78, 13);
            this.label12.TabIndex = 38;
            this.label12.Text = "Envio numero: ";
            // 
            // button3
            // 
            this.button3.Image = global::FacilShoppingReports.Properties.Resources.exit_icon;
            this.button3.Location = new System.Drawing.Point(524, 215);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(75, 63);
            this.button3.TabIndex = 42;
            this.button3.Text = "Excluir Selecionado";
            this.button3.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // button4
            // 
            this.button4.Image = global::FacilShoppingReports.Properties.Resources.print_icon;
            this.button4.Location = new System.Drawing.Point(522, 140);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(75, 63);
            this.button4.TabIndex = 37;
            this.button4.Text = "&Imprimir";
            this.button4.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // cmdNovoB2W
            // 
            this.cmdNovoB2W.Image = global::FacilShoppingReports.Properties.Resources.new_icon;
            this.cmdNovoB2W.Location = new System.Drawing.Point(522, 68);
            this.cmdNovoB2W.Name = "cmdNovoB2W";
            this.cmdNovoB2W.Size = new System.Drawing.Size(75, 63);
            this.cmdNovoB2W.TabIndex = 36;
            this.cmdNovoB2W.Text = "&Novo";
            this.cmdNovoB2W.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.cmdNovoB2W.UseVisualStyleBackColor = true;
            this.cmdNovoB2W.Visible = false;
            this.cmdNovoB2W.Click += new System.EventHandler(this.cmdNovoB2W_Click);
            // 
            // tabPage4
            // 
            this.tabPage4.BackColor = System.Drawing.Color.WhiteSmoke;
            this.tabPage4.Controls.Add(this.btnCarregarMagalu);
            this.tabPage4.Controls.Add(this.lblRegistrosMagaluMundial);
            this.tabPage4.Controls.Add(this.label19);
            this.tabPage4.Controls.Add(this.lblRegistrosMagaluBela);
            this.tabPage4.Controls.Add(this.label22);
            this.tabPage4.Controls.Add(this.lblRegistrosMagaluAlpha);
            this.tabPage4.Controls.Add(this.label24);
            this.tabPage4.Controls.Add(this.lblRegistrosMagaluFacilShopping);
            this.tabPage4.Controls.Add(this.label26);
            this.tabPage4.Controls.Add(this.groupBox5);
            this.tabPage4.Controls.Add(this.lblDataCriacaoMagalu);
            this.tabPage4.Controls.Add(this.txtNumeroEnvioMagalu);
            this.tabPage4.Controls.Add(this.label15);
            this.tabPage4.Controls.Add(this.button6);
            this.tabPage4.Controls.Add(this.button7);
            this.tabPage4.Controls.Add(this.button8);
            this.tabPage4.Location = new System.Drawing.Point(4, 22);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Size = new System.Drawing.Size(618, 380);
            this.tabPage4.TabIndex = 3;
            this.tabPage4.Text = "MAGALU";
            // 
            // btnCarregarMagalu
            // 
            this.btnCarregarMagalu.Image = global::FacilShoppingReports.Properties.Resources.search_icon1;
            this.btnCarregarMagalu.Location = new System.Drawing.Point(524, 288);
            this.btnCarregarMagalu.Name = "btnCarregarMagalu";
            this.btnCarregarMagalu.Size = new System.Drawing.Size(75, 63);
            this.btnCarregarMagalu.TabIndex = 63;
            this.btnCarregarMagalu.Text = "Carregar Envios";
            this.btnCarregarMagalu.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.btnCarregarMagalu.UseVisualStyleBackColor = true;
            this.btnCarregarMagalu.Click += new System.EventHandler(this.btnCarregarMagalu_Click);
            // 
            // lblRegistrosMagaluMundial
            // 
            this.lblRegistrosMagaluMundial.AutoSize = true;
            this.lblRegistrosMagaluMundial.Location = new System.Drawing.Point(560, 354);
            this.lblRegistrosMagaluMundial.Name = "lblRegistrosMagaluMundial";
            this.lblRegistrosMagaluMundial.Size = new System.Drawing.Size(13, 13);
            this.lblRegistrosMagaluMundial.TabIndex = 62;
            this.lblRegistrosMagaluMundial.Text = "0";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(475, 354);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(47, 13);
            this.label19.TabIndex = 61;
            this.label19.Text = "Mundial:";
            // 
            // lblRegistrosMagaluBela
            // 
            this.lblRegistrosMagaluBela.AutoSize = true;
            this.lblRegistrosMagaluBela.Location = new System.Drawing.Point(251, 354);
            this.lblRegistrosMagaluBela.Name = "lblRegistrosMagaluBela";
            this.lblRegistrosMagaluBela.Size = new System.Drawing.Size(13, 13);
            this.lblRegistrosMagaluBela.TabIndex = 60;
            this.lblRegistrosMagaluBela.Text = "0";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(324, 354);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(65, 13);
            this.label22.TabIndex = 59;
            this.label22.Text = "AlphaJogos:";
            // 
            // lblRegistrosMagaluAlpha
            // 
            this.lblRegistrosMagaluAlpha.AutoSize = true;
            this.lblRegistrosMagaluAlpha.Location = new System.Drawing.Point(395, 354);
            this.lblRegistrosMagaluAlpha.Name = "lblRegistrosMagaluAlpha";
            this.lblRegistrosMagaluAlpha.Size = new System.Drawing.Size(13, 13);
            this.lblRegistrosMagaluAlpha.TabIndex = 58;
            this.lblRegistrosMagaluAlpha.Text = "0";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(167, 354);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(78, 13);
            this.label24.TabIndex = 57;
            this.label24.Text = "BellaShopping:";
            // 
            // lblRegistrosMagaluFacilShopping
            // 
            this.lblRegistrosMagaluFacilShopping.AutoSize = true;
            this.lblRegistrosMagaluFacilShopping.Location = new System.Drawing.Point(91, 354);
            this.lblRegistrosMagaluFacilShopping.Name = "lblRegistrosMagaluFacilShopping";
            this.lblRegistrosMagaluFacilShopping.Size = new System.Drawing.Size(13, 13);
            this.lblRegistrosMagaluFacilShopping.TabIndex = 56;
            this.lblRegistrosMagaluFacilShopping.Text = "0";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(9, 354);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(78, 13);
            this.label26.TabIndex = 55;
            this.label26.Text = "Facilshopping :";
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.lstMagalu);
            this.groupBox5.Location = new System.Drawing.Point(22, 40);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(492, 305);
            this.groupBox5.TabIndex = 35;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "MagaLu";
            // 
            // lstMagalu
            // 
            this.lstMagalu.HideSelection = false;
            this.lstMagalu.Location = new System.Drawing.Point(5, 15);
            this.lstMagalu.Name = "lstMagalu";
            this.lstMagalu.Size = new System.Drawing.Size(479, 283);
            this.lstMagalu.TabIndex = 21;
            this.lstMagalu.UseCompatibleStateImageBehavior = false;
            this.lstMagalu.View = System.Windows.Forms.View.Details;
            // 
            // lblDataCriacaoMagalu
            // 
            this.lblDataCriacaoMagalu.AutoSize = true;
            this.lblDataCriacaoMagalu.Location = new System.Drawing.Point(182, 19);
            this.lblDataCriacaoMagalu.Name = "lblDataCriacaoMagalu";
            this.lblDataCriacaoMagalu.Size = new System.Drawing.Size(35, 13);
            this.lblDataCriacaoMagalu.TabIndex = 40;
            this.lblDataCriacaoMagalu.Text = "label3";
            // 
            // txtNumeroEnvioMagalu
            // 
            this.txtNumeroEnvioMagalu.Enabled = false;
            this.txtNumeroEnvioMagalu.Location = new System.Drawing.Point(109, 16);
            this.txtNumeroEnvioMagalu.Name = "txtNumeroEnvioMagalu";
            this.txtNumeroEnvioMagalu.Size = new System.Drawing.Size(67, 20);
            this.txtNumeroEnvioMagalu.TabIndex = 39;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(25, 19);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(78, 13);
            this.label15.TabIndex = 38;
            this.label15.Text = "Envio numero: ";
            // 
            // button6
            // 
            this.button6.Image = global::FacilShoppingReports.Properties.Resources.exit_icon;
            this.button6.Location = new System.Drawing.Point(524, 215);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(75, 63);
            this.button6.TabIndex = 42;
            this.button6.Text = "Excluir Selecionado";
            this.button6.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.button6.UseVisualStyleBackColor = true;
            this.button6.Click += new System.EventHandler(this.button6_Click);
            // 
            // button7
            // 
            this.button7.Image = global::FacilShoppingReports.Properties.Resources.print_icon;
            this.button7.Location = new System.Drawing.Point(522, 140);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(75, 63);
            this.button7.TabIndex = 37;
            this.button7.Text = "&Imprimir";
            this.button7.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.button7.UseVisualStyleBackColor = true;
            this.button7.Click += new System.EventHandler(this.button7_Click);
            // 
            // button8
            // 
            this.button8.Image = global::FacilShoppingReports.Properties.Resources.new_icon;
            this.button8.Location = new System.Drawing.Point(522, 68);
            this.button8.Name = "button8";
            this.button8.Size = new System.Drawing.Size(75, 63);
            this.button8.TabIndex = 36;
            this.button8.Text = "&Novo";
            this.button8.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.button8.UseVisualStyleBackColor = true;
            this.button8.Visible = false;
            this.button8.Click += new System.EventHandler(this.button8_Click);
            // 
            // tabPage5
            // 
            this.tabPage5.BackColor = System.Drawing.Color.WhiteSmoke;
            this.tabPage5.Controls.Add(this.btnCarregarMercadoPlace);
            this.tabPage5.Controls.Add(this.lblRegistrosMPlaceMundial);
            this.tabPage5.Controls.Add(this.label21);
            this.tabPage5.Controls.Add(this.lblRegistrosMPlaceBela);
            this.tabPage5.Controls.Add(this.label25);
            this.tabPage5.Controls.Add(this.lblRegistrosMPlaceAlpha);
            this.tabPage5.Controls.Add(this.label28);
            this.tabPage5.Controls.Add(this.lblRegistrosMPlaceFS);
            this.tabPage5.Controls.Add(this.label30);
            this.tabPage5.Controls.Add(this.groupBox6);
            this.tabPage5.Controls.Add(this.lblDataCriacaoMercadoPlace);
            this.tabPage5.Controls.Add(this.txtNumeroEnvioMlPlace);
            this.tabPage5.Controls.Add(this.label14);
            this.tabPage5.Controls.Add(this.cmdExcluirMercadoPlace);
            this.tabPage5.Controls.Add(this.cmdImprimirMercadoPlace);
            this.tabPage5.Controls.Add(this.cmdNovoMercadoPlace);
            this.tabPage5.Location = new System.Drawing.Point(4, 22);
            this.tabPage5.Name = "tabPage5";
            this.tabPage5.Size = new System.Drawing.Size(618, 380);
            this.tabPage5.TabIndex = 4;
            this.tabPage5.Text = "MERCADO PLACE";
            // 
            // btnCarregarMercadoPlace
            // 
            this.btnCarregarMercadoPlace.Image = global::FacilShoppingReports.Properties.Resources.search_icon1;
            this.btnCarregarMercadoPlace.Location = new System.Drawing.Point(524, 287);
            this.btnCarregarMercadoPlace.Name = "btnCarregarMercadoPlace";
            this.btnCarregarMercadoPlace.Size = new System.Drawing.Size(75, 63);
            this.btnCarregarMercadoPlace.TabIndex = 71;
            this.btnCarregarMercadoPlace.Text = "Carregar Envios";
            this.btnCarregarMercadoPlace.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.btnCarregarMercadoPlace.UseVisualStyleBackColor = true;
            this.btnCarregarMercadoPlace.Click += new System.EventHandler(this.btnCarregarMercadoPlace_Click);
            // 
            // lblRegistrosMPlaceMundial
            // 
            this.lblRegistrosMPlaceMundial.AutoSize = true;
            this.lblRegistrosMPlaceMundial.Location = new System.Drawing.Point(534, 353);
            this.lblRegistrosMPlaceMundial.Name = "lblRegistrosMPlaceMundial";
            this.lblRegistrosMPlaceMundial.Size = new System.Drawing.Size(13, 13);
            this.lblRegistrosMPlaceMundial.TabIndex = 70;
            this.lblRegistrosMPlaceMundial.Text = "0";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(487, 353);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(47, 13);
            this.label21.TabIndex = 69;
            this.label21.Text = "Mundial:";
            // 
            // lblRegistrosMPlaceBela
            // 
            this.lblRegistrosMPlaceBela.AutoSize = true;
            this.lblRegistrosMPlaceBela.Location = new System.Drawing.Point(263, 353);
            this.lblRegistrosMPlaceBela.Name = "lblRegistrosMPlaceBela";
            this.lblRegistrosMPlaceBela.Size = new System.Drawing.Size(13, 13);
            this.lblRegistrosMPlaceBela.TabIndex = 68;
            this.lblRegistrosMPlaceBela.Text = "0";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(336, 353);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(65, 13);
            this.label25.TabIndex = 67;
            this.label25.Text = "AlphaJogos:";
            // 
            // lblRegistrosMPlaceAlpha
            // 
            this.lblRegistrosMPlaceAlpha.AutoSize = true;
            this.lblRegistrosMPlaceAlpha.Location = new System.Drawing.Point(407, 353);
            this.lblRegistrosMPlaceAlpha.Name = "lblRegistrosMPlaceAlpha";
            this.lblRegistrosMPlaceAlpha.Size = new System.Drawing.Size(13, 13);
            this.lblRegistrosMPlaceAlpha.TabIndex = 66;
            this.lblRegistrosMPlaceAlpha.Text = "0";
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Location = new System.Drawing.Point(179, 353);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(78, 13);
            this.label28.TabIndex = 65;
            this.label28.Text = "BellaShopping:";
            // 
            // lblRegistrosMPlaceFS
            // 
            this.lblRegistrosMPlaceFS.AutoSize = true;
            this.lblRegistrosMPlaceFS.Location = new System.Drawing.Point(103, 353);
            this.lblRegistrosMPlaceFS.Name = "lblRegistrosMPlaceFS";
            this.lblRegistrosMPlaceFS.Size = new System.Drawing.Size(13, 13);
            this.lblRegistrosMPlaceFS.TabIndex = 64;
            this.lblRegistrosMPlaceFS.Text = "0";
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Location = new System.Drawing.Point(21, 353);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(78, 13);
            this.label30.TabIndex = 63;
            this.label30.Text = "Facilshopping :";
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.lstMlPlace);
            this.groupBox6.Location = new System.Drawing.Point(22, 40);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(492, 305);
            this.groupBox6.TabIndex = 43;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "Mercado Place";
            // 
            // lstMlPlace
            // 
            this.lstMlPlace.HideSelection = false;
            this.lstMlPlace.Location = new System.Drawing.Point(6, 16);
            this.lstMlPlace.Name = "lstMlPlace";
            this.lstMlPlace.Size = new System.Drawing.Size(479, 283);
            this.lstMlPlace.TabIndex = 21;
            this.lstMlPlace.UseCompatibleStateImageBehavior = false;
            this.lstMlPlace.View = System.Windows.Forms.View.Details;
            // 
            // lblDataCriacaoMercadoPlace
            // 
            this.lblDataCriacaoMercadoPlace.AutoSize = true;
            this.lblDataCriacaoMercadoPlace.Location = new System.Drawing.Point(182, 19);
            this.lblDataCriacaoMercadoPlace.Name = "lblDataCriacaoMercadoPlace";
            this.lblDataCriacaoMercadoPlace.Size = new System.Drawing.Size(35, 13);
            this.lblDataCriacaoMercadoPlace.TabIndex = 48;
            this.lblDataCriacaoMercadoPlace.Text = "label3";
            // 
            // txtNumeroEnvioMlPlace
            // 
            this.txtNumeroEnvioMlPlace.Enabled = false;
            this.txtNumeroEnvioMlPlace.Location = new System.Drawing.Point(109, 16);
            this.txtNumeroEnvioMlPlace.Name = "txtNumeroEnvioMlPlace";
            this.txtNumeroEnvioMlPlace.Size = new System.Drawing.Size(67, 20);
            this.txtNumeroEnvioMlPlace.TabIndex = 47;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(25, 19);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(78, 13);
            this.label14.TabIndex = 46;
            this.label14.Text = "Envio numero: ";
            // 
            // cmdExcluirMercadoPlace
            // 
            this.cmdExcluirMercadoPlace.Image = global::FacilShoppingReports.Properties.Resources.exit_icon;
            this.cmdExcluirMercadoPlace.Location = new System.Drawing.Point(524, 215);
            this.cmdExcluirMercadoPlace.Name = "cmdExcluirMercadoPlace";
            this.cmdExcluirMercadoPlace.Size = new System.Drawing.Size(75, 63);
            this.cmdExcluirMercadoPlace.TabIndex = 49;
            this.cmdExcluirMercadoPlace.Text = "Excluir Selecionado";
            this.cmdExcluirMercadoPlace.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.cmdExcluirMercadoPlace.UseVisualStyleBackColor = true;
            this.cmdExcluirMercadoPlace.Click += new System.EventHandler(this.cmdExcluirMercadoPlace_Click);
            // 
            // cmdImprimirMercadoPlace
            // 
            this.cmdImprimirMercadoPlace.Image = global::FacilShoppingReports.Properties.Resources.print_icon;
            this.cmdImprimirMercadoPlace.Location = new System.Drawing.Point(522, 140);
            this.cmdImprimirMercadoPlace.Name = "cmdImprimirMercadoPlace";
            this.cmdImprimirMercadoPlace.Size = new System.Drawing.Size(75, 63);
            this.cmdImprimirMercadoPlace.TabIndex = 45;
            this.cmdImprimirMercadoPlace.Text = "&Imprimir";
            this.cmdImprimirMercadoPlace.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.cmdImprimirMercadoPlace.UseVisualStyleBackColor = true;
            this.cmdImprimirMercadoPlace.Click += new System.EventHandler(this.cmdImprimirMercadoPlace_Click);
            // 
            // cmdNovoMercadoPlace
            // 
            this.cmdNovoMercadoPlace.Image = global::FacilShoppingReports.Properties.Resources.new_icon;
            this.cmdNovoMercadoPlace.Location = new System.Drawing.Point(522, 68);
            this.cmdNovoMercadoPlace.Name = "cmdNovoMercadoPlace";
            this.cmdNovoMercadoPlace.Size = new System.Drawing.Size(75, 63);
            this.cmdNovoMercadoPlace.TabIndex = 44;
            this.cmdNovoMercadoPlace.Text = "&Novo";
            this.cmdNovoMercadoPlace.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.cmdNovoMercadoPlace.UseVisualStyleBackColor = true;
            this.cmdNovoMercadoPlace.Click += new System.EventHandler(this.cmdNovoMercadoPlace_Click);
            // 
            // tabPage6
            // 
            this.tabPage6.BackColor = System.Drawing.Color.WhiteSmoke;
            this.tabPage6.Controls.Add(this.btnCarregarTotalExpress);
            this.tabPage6.Controls.Add(this.groupBox7);
            this.tabPage6.Controls.Add(this.lblDataCriacaoTotalExpress);
            this.tabPage6.Controls.Add(this.txtNumeroEnvioTotalExpress);
            this.tabPage6.Controls.Add(this.label36);
            this.tabPage6.Controls.Add(this.cmdExcluirTotalExpress);
            this.tabPage6.Controls.Add(this.cmdImprimirTotalExpress);
            this.tabPage6.Controls.Add(this.cmdNovoTotalExpress);
            this.tabPage6.Location = new System.Drawing.Point(4, 22);
            this.tabPage6.Name = "tabPage6";
            this.tabPage6.Size = new System.Drawing.Size(618, 380);
            this.tabPage6.TabIndex = 5;
            this.tabPage6.Text = "TOTAL EXPRESS";
            // 
            // btnCarregarTotalExpress
            // 
            this.btnCarregarTotalExpress.Image = global::FacilShoppingReports.Properties.Resources.search_icon1;
            this.btnCarregarTotalExpress.Location = new System.Drawing.Point(523, 286);
            this.btnCarregarTotalExpress.Name = "btnCarregarTotalExpress";
            this.btnCarregarTotalExpress.Size = new System.Drawing.Size(75, 63);
            this.btnCarregarTotalExpress.TabIndex = 87;
            this.btnCarregarTotalExpress.Text = "Carregar Envios";
            this.btnCarregarTotalExpress.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.btnCarregarTotalExpress.UseVisualStyleBackColor = true;
            this.btnCarregarTotalExpress.Click += new System.EventHandler(this.btnCarregarShopee_Click);
            // 
            // groupBox7
            // 
            this.groupBox7.Controls.Add(this.lstTotalExpress);
            this.groupBox7.Location = new System.Drawing.Point(21, 39);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Size = new System.Drawing.Size(492, 305);
            this.groupBox7.TabIndex = 72;
            this.groupBox7.TabStop = false;
            this.groupBox7.Text = "Total Express";
            // 
            // lstTotalExpress
            // 
            this.lstTotalExpress.HideSelection = false;
            this.lstTotalExpress.Location = new System.Drawing.Point(6, 16);
            this.lstTotalExpress.Name = "lstTotalExpress";
            this.lstTotalExpress.Size = new System.Drawing.Size(479, 283);
            this.lstTotalExpress.TabIndex = 21;
            this.lstTotalExpress.UseCompatibleStateImageBehavior = false;
            this.lstTotalExpress.View = System.Windows.Forms.View.Details;
            // 
            // lblDataCriacaoTotalExpress
            // 
            this.lblDataCriacaoTotalExpress.AutoSize = true;
            this.lblDataCriacaoTotalExpress.Location = new System.Drawing.Point(181, 18);
            this.lblDataCriacaoTotalExpress.Name = "lblDataCriacaoTotalExpress";
            this.lblDataCriacaoTotalExpress.Size = new System.Drawing.Size(35, 13);
            this.lblDataCriacaoTotalExpress.TabIndex = 77;
            this.lblDataCriacaoTotalExpress.Text = "label3";
            // 
            // txtNumeroEnvioTotalExpress
            // 
            this.txtNumeroEnvioTotalExpress.Enabled = false;
            this.txtNumeroEnvioTotalExpress.Location = new System.Drawing.Point(108, 15);
            this.txtNumeroEnvioTotalExpress.Name = "txtNumeroEnvioTotalExpress";
            this.txtNumeroEnvioTotalExpress.Size = new System.Drawing.Size(67, 20);
            this.txtNumeroEnvioTotalExpress.TabIndex = 76;
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Location = new System.Drawing.Point(24, 18);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(78, 13);
            this.label36.TabIndex = 75;
            this.label36.Text = "Envio numero: ";
            // 
            // cmdExcluirTotalExpress
            // 
            this.cmdExcluirTotalExpress.Image = global::FacilShoppingReports.Properties.Resources.exit_icon;
            this.cmdExcluirTotalExpress.Location = new System.Drawing.Point(523, 214);
            this.cmdExcluirTotalExpress.Name = "cmdExcluirTotalExpress";
            this.cmdExcluirTotalExpress.Size = new System.Drawing.Size(75, 63);
            this.cmdExcluirTotalExpress.TabIndex = 78;
            this.cmdExcluirTotalExpress.Text = "Excluir Selecionado";
            this.cmdExcluirTotalExpress.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.cmdExcluirTotalExpress.UseVisualStyleBackColor = true;
            // 
            // cmdImprimirTotalExpress
            // 
            this.cmdImprimirTotalExpress.Image = global::FacilShoppingReports.Properties.Resources.print_icon;
            this.cmdImprimirTotalExpress.Location = new System.Drawing.Point(521, 139);
            this.cmdImprimirTotalExpress.Name = "cmdImprimirTotalExpress";
            this.cmdImprimirTotalExpress.Size = new System.Drawing.Size(75, 63);
            this.cmdImprimirTotalExpress.TabIndex = 74;
            this.cmdImprimirTotalExpress.Text = "&Imprimir";
            this.cmdImprimirTotalExpress.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.cmdImprimirTotalExpress.UseVisualStyleBackColor = true;
            this.cmdImprimirTotalExpress.Click += new System.EventHandler(this.cmdImprimirShopee_Click);
            // 
            // cmdNovoTotalExpress
            // 
            this.cmdNovoTotalExpress.Image = global::FacilShoppingReports.Properties.Resources.new_icon;
            this.cmdNovoTotalExpress.Location = new System.Drawing.Point(521, 67);
            this.cmdNovoTotalExpress.Name = "cmdNovoTotalExpress";
            this.cmdNovoTotalExpress.Size = new System.Drawing.Size(75, 63);
            this.cmdNovoTotalExpress.TabIndex = 73;
            this.cmdNovoTotalExpress.Text = "&Novo";
            this.cmdNovoTotalExpress.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.cmdNovoTotalExpress.UseVisualStyleBackColor = true;
            this.cmdNovoTotalExpress.Visible = false;
            this.cmdNovoTotalExpress.Click += new System.EventHandler(this.cmdNovoShopee_Click);
            // 
            // tabPage7
            // 
            this.tabPage7.BackColor = System.Drawing.Color.WhiteSmoke;
            this.tabPage7.Controls.Add(this.btnCarregarSequoia);
            this.tabPage7.Controls.Add(this.groupBox8);
            this.tabPage7.Controls.Add(this.lblDataCriacaoSequoia);
            this.tabPage7.Controls.Add(this.txtNumeroEnvioSequoia);
            this.tabPage7.Controls.Add(this.label29);
            this.tabPage7.Controls.Add(this.cmdExcluirSequoia);
            this.tabPage7.Controls.Add(this.cmdImprimirSequoia);
            this.tabPage7.Controls.Add(this.button12);
            this.tabPage7.Location = new System.Drawing.Point(4, 22);
            this.tabPage7.Name = "tabPage7";
            this.tabPage7.Size = new System.Drawing.Size(618, 380);
            this.tabPage7.TabIndex = 6;
            this.tabPage7.Text = "SEQUOIA";
            // 
            // btnCarregarSequoia
            // 
            this.btnCarregarSequoia.Image = global::FacilShoppingReports.Properties.Resources.search_icon1;
            this.btnCarregarSequoia.Location = new System.Drawing.Point(523, 294);
            this.btnCarregarSequoia.Name = "btnCarregarSequoia";
            this.btnCarregarSequoia.Size = new System.Drawing.Size(75, 63);
            this.btnCarregarSequoia.TabIndex = 95;
            this.btnCarregarSequoia.Text = "Carregar Envios";
            this.btnCarregarSequoia.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.btnCarregarSequoia.UseVisualStyleBackColor = true;
            this.btnCarregarSequoia.Click += new System.EventHandler(this.btnCarregarSequoia_Click);
            // 
            // groupBox8
            // 
            this.groupBox8.Controls.Add(this.lstSequoia);
            this.groupBox8.Location = new System.Drawing.Point(21, 47);
            this.groupBox8.Name = "groupBox8";
            this.groupBox8.Size = new System.Drawing.Size(492, 305);
            this.groupBox8.TabIndex = 88;
            this.groupBox8.TabStop = false;
            this.groupBox8.Text = "Total Express";
            // 
            // lstSequoia
            // 
            this.lstSequoia.HideSelection = false;
            this.lstSequoia.Location = new System.Drawing.Point(6, 16);
            this.lstSequoia.Name = "lstSequoia";
            this.lstSequoia.Size = new System.Drawing.Size(479, 283);
            this.lstSequoia.TabIndex = 21;
            this.lstSequoia.UseCompatibleStateImageBehavior = false;
            this.lstSequoia.View = System.Windows.Forms.View.Details;
            // 
            // lblDataCriacaoSequoia
            // 
            this.lblDataCriacaoSequoia.AutoSize = true;
            this.lblDataCriacaoSequoia.Location = new System.Drawing.Point(181, 26);
            this.lblDataCriacaoSequoia.Name = "lblDataCriacaoSequoia";
            this.lblDataCriacaoSequoia.Size = new System.Drawing.Size(35, 13);
            this.lblDataCriacaoSequoia.TabIndex = 93;
            this.lblDataCriacaoSequoia.Text = "label3";
            // 
            // txtNumeroEnvioSequoia
            // 
            this.txtNumeroEnvioSequoia.Enabled = false;
            this.txtNumeroEnvioSequoia.Location = new System.Drawing.Point(108, 23);
            this.txtNumeroEnvioSequoia.Name = "txtNumeroEnvioSequoia";
            this.txtNumeroEnvioSequoia.Size = new System.Drawing.Size(67, 20);
            this.txtNumeroEnvioSequoia.TabIndex = 92;
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Location = new System.Drawing.Point(25, 26);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(78, 13);
            this.label29.TabIndex = 91;
            this.label29.Text = "Envio numero: ";
            // 
            // cmdExcluirSequoia
            // 
            this.cmdExcluirSequoia.Image = global::FacilShoppingReports.Properties.Resources.exit_icon;
            this.cmdExcluirSequoia.Location = new System.Drawing.Point(523, 222);
            this.cmdExcluirSequoia.Name = "cmdExcluirSequoia";
            this.cmdExcluirSequoia.Size = new System.Drawing.Size(75, 63);
            this.cmdExcluirSequoia.TabIndex = 94;
            this.cmdExcluirSequoia.Text = "Excluir Selecionado";
            this.cmdExcluirSequoia.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.cmdExcluirSequoia.UseVisualStyleBackColor = true;
            // 
            // cmdImprimirSequoia
            // 
            this.cmdImprimirSequoia.Image = global::FacilShoppingReports.Properties.Resources.print_icon;
            this.cmdImprimirSequoia.Location = new System.Drawing.Point(521, 147);
            this.cmdImprimirSequoia.Name = "cmdImprimirSequoia";
            this.cmdImprimirSequoia.Size = new System.Drawing.Size(75, 63);
            this.cmdImprimirSequoia.TabIndex = 90;
            this.cmdImprimirSequoia.Text = "&Imprimir";
            this.cmdImprimirSequoia.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.cmdImprimirSequoia.UseVisualStyleBackColor = true;
            this.cmdImprimirSequoia.Click += new System.EventHandler(this.cmdImprimirSequoia_Click);
            // 
            // button12
            // 
            this.button12.Image = global::FacilShoppingReports.Properties.Resources.new_icon;
            this.button12.Location = new System.Drawing.Point(521, 75);
            this.button12.Name = "button12";
            this.button12.Size = new System.Drawing.Size(75, 63);
            this.button12.TabIndex = 89;
            this.button12.Text = "&Novo";
            this.button12.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.button12.UseVisualStyleBackColor = true;
            this.button12.Visible = false;
            this.button12.Click += new System.EventHandler(this.button12_Click);
            // 
            // tabPage1
            // 
            this.tabPage1.BackColor = System.Drawing.Color.WhiteSmoke;
            this.tabPage1.Controls.Add(this.button9);
            this.tabPage1.Controls.Add(this.lblRegistrosAmazonMJ);
            this.tabPage1.Controls.Add(this.label32);
            this.tabPage1.Controls.Add(this.lblRegistrosAmazonBella);
            this.tabPage1.Controls.Add(this.label34);
            this.tabPage1.Controls.Add(this.lblRegistrosAmazonAJ);
            this.tabPage1.Controls.Add(this.label37);
            this.tabPage1.Controls.Add(this.lblRegistrosAmazonFS);
            this.tabPage1.Controls.Add(this.label39);
            this.tabPage1.Controls.Add(this.groupBox9);
            this.tabPage1.Controls.Add(this.lblDataCriacaoAmazon);
            this.tabPage1.Controls.Add(this.txtNumeroEnvioAmazon);
            this.tabPage1.Controls.Add(this.label41);
            this.tabPage1.Controls.Add(this.button10);
            this.tabPage1.Controls.Add(this.button11);
            this.tabPage1.Controls.Add(this.button13);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Size = new System.Drawing.Size(618, 380);
            this.tabPage1.TabIndex = 7;
            this.tabPage1.Text = "AMAZON";
            // 
            // button9
            // 
            this.button9.Image = global::FacilShoppingReports.Properties.Resources.search_icon1;
            this.button9.Location = new System.Drawing.Point(523, 287);
            this.button9.Name = "button9";
            this.button9.Size = new System.Drawing.Size(75, 63);
            this.button9.TabIndex = 71;
            this.button9.Text = "Carregar Envios";
            this.button9.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.button9.UseVisualStyleBackColor = true;
            this.button9.Click += new System.EventHandler(this.button9_Click);
            // 
            // lblRegistrosAmazonMJ
            // 
            this.lblRegistrosAmazonMJ.AutoSize = true;
            this.lblRegistrosAmazonMJ.Location = new System.Drawing.Point(574, 353);
            this.lblRegistrosAmazonMJ.Name = "lblRegistrosAmazonMJ";
            this.lblRegistrosAmazonMJ.Size = new System.Drawing.Size(13, 13);
            this.lblRegistrosAmazonMJ.TabIndex = 70;
            this.lblRegistrosAmazonMJ.Text = "0";
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Location = new System.Drawing.Point(489, 353);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(47, 13);
            this.label32.TabIndex = 69;
            this.label32.Text = "Mundial:";
            // 
            // lblRegistrosAmazonBella
            // 
            this.lblRegistrosAmazonBella.AutoSize = true;
            this.lblRegistrosAmazonBella.Location = new System.Drawing.Point(265, 353);
            this.lblRegistrosAmazonBella.Name = "lblRegistrosAmazonBella";
            this.lblRegistrosAmazonBella.Size = new System.Drawing.Size(13, 13);
            this.lblRegistrosAmazonBella.TabIndex = 68;
            this.lblRegistrosAmazonBella.Text = "0";
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Location = new System.Drawing.Point(338, 353);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(65, 13);
            this.label34.TabIndex = 67;
            this.label34.Text = "AlphaJogos:";
            // 
            // lblRegistrosAmazonAJ
            // 
            this.lblRegistrosAmazonAJ.AutoSize = true;
            this.lblRegistrosAmazonAJ.Location = new System.Drawing.Point(409, 353);
            this.lblRegistrosAmazonAJ.Name = "lblRegistrosAmazonAJ";
            this.lblRegistrosAmazonAJ.Size = new System.Drawing.Size(13, 13);
            this.lblRegistrosAmazonAJ.TabIndex = 66;
            this.lblRegistrosAmazonAJ.Text = "0";
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Location = new System.Drawing.Point(181, 353);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(78, 13);
            this.label37.TabIndex = 65;
            this.label37.Text = "BellaShopping:";
            // 
            // lblRegistrosAmazonFS
            // 
            this.lblRegistrosAmazonFS.AutoSize = true;
            this.lblRegistrosAmazonFS.Location = new System.Drawing.Point(105, 353);
            this.lblRegistrosAmazonFS.Name = "lblRegistrosAmazonFS";
            this.lblRegistrosAmazonFS.Size = new System.Drawing.Size(13, 13);
            this.lblRegistrosAmazonFS.TabIndex = 64;
            this.lblRegistrosAmazonFS.Text = "0";
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Location = new System.Drawing.Point(23, 353);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(78, 13);
            this.label39.TabIndex = 63;
            this.label39.Text = "Facilshopping :";
            // 
            // groupBox9
            // 
            this.groupBox9.Controls.Add(this.lstAmazon);
            this.groupBox9.Location = new System.Drawing.Point(21, 39);
            this.groupBox9.Name = "groupBox9";
            this.groupBox9.Size = new System.Drawing.Size(492, 305);
            this.groupBox9.TabIndex = 56;
            this.groupBox9.TabStop = false;
            this.groupBox9.Text = "Amazon";
            // 
            // lstAmazon
            // 
            this.lstAmazon.HideSelection = false;
            this.lstAmazon.Location = new System.Drawing.Point(5, 15);
            this.lstAmazon.Name = "lstAmazon";
            this.lstAmazon.Size = new System.Drawing.Size(479, 283);
            this.lstAmazon.TabIndex = 21;
            this.lstAmazon.UseCompatibleStateImageBehavior = false;
            this.lstAmazon.View = System.Windows.Forms.View.Details;
            // 
            // lblDataCriacaoAmazon
            // 
            this.lblDataCriacaoAmazon.AutoSize = true;
            this.lblDataCriacaoAmazon.Location = new System.Drawing.Point(181, 18);
            this.lblDataCriacaoAmazon.Name = "lblDataCriacaoAmazon";
            this.lblDataCriacaoAmazon.Size = new System.Drawing.Size(35, 13);
            this.lblDataCriacaoAmazon.TabIndex = 61;
            this.lblDataCriacaoAmazon.Text = "label3";
            // 
            // txtNumeroEnvioAmazon
            // 
            this.txtNumeroEnvioAmazon.Enabled = false;
            this.txtNumeroEnvioAmazon.Location = new System.Drawing.Point(108, 15);
            this.txtNumeroEnvioAmazon.Name = "txtNumeroEnvioAmazon";
            this.txtNumeroEnvioAmazon.Size = new System.Drawing.Size(67, 20);
            this.txtNumeroEnvioAmazon.TabIndex = 60;
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.Location = new System.Drawing.Point(24, 18);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(78, 13);
            this.label41.TabIndex = 59;
            this.label41.Text = "Envio numero: ";
            // 
            // button10
            // 
            this.button10.Image = global::FacilShoppingReports.Properties.Resources.exit_icon;
            this.button10.Location = new System.Drawing.Point(523, 214);
            this.button10.Name = "button10";
            this.button10.Size = new System.Drawing.Size(75, 63);
            this.button10.TabIndex = 62;
            this.button10.Text = "Excluir Selecionado";
            this.button10.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.button10.UseVisualStyleBackColor = true;
            // 
            // button11
            // 
            this.button11.Image = global::FacilShoppingReports.Properties.Resources.print_icon;
            this.button11.Location = new System.Drawing.Point(521, 139);
            this.button11.Name = "button11";
            this.button11.Size = new System.Drawing.Size(75, 63);
            this.button11.TabIndex = 58;
            this.button11.Text = "&Imprimir";
            this.button11.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.button11.UseVisualStyleBackColor = true;
            this.button11.Click += new System.EventHandler(this.button11_Click);
            // 
            // button13
            // 
            this.button13.Image = global::FacilShoppingReports.Properties.Resources.new_icon;
            this.button13.Location = new System.Drawing.Point(521, 67);
            this.button13.Name = "button13";
            this.button13.Size = new System.Drawing.Size(75, 63);
            this.button13.TabIndex = 57;
            this.button13.Text = "&Novo";
            this.button13.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.button13.UseVisualStyleBackColor = true;
            this.button13.Visible = false;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(73, 528);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(78, 13);
            this.label4.TabIndex = 40;
            this.label4.Text = "Total Correios: ";
            this.label4.Click += new System.EventHandler(this.label4_Click_1);
            // 
            // lblRegistros
            // 
            this.lblRegistros.AutoSize = true;
            this.lblRegistros.Location = new System.Drawing.Point(157, 528);
            this.lblRegistros.Name = "lblRegistros";
            this.lblRegistros.Size = new System.Drawing.Size(13, 13);
            this.lblRegistros.TabIndex = 39;
            this.lblRegistros.Text = "0";
            this.lblRegistros.Click += new System.EventHandler(this.lblRegistros_Click_1);
            // 
            // lblRegistrosML
            // 
            this.lblRegistrosML.AutoSize = true;
            this.lblRegistrosML.Location = new System.Drawing.Point(157, 551);
            this.lblRegistrosML.Name = "lblRegistrosML";
            this.lblRegistrosML.Size = new System.Drawing.Size(13, 13);
            this.lblRegistrosML.TabIndex = 42;
            this.lblRegistrosML.Text = "0";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(19, 551);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(132, 13);
            this.label5.TabIndex = 41;
            this.label5.Text = "Total de Mercado Envios: ";
            // 
            // lblRegistrosMagaLu
            // 
            this.lblRegistrosMagaLu.AutoSize = true;
            this.lblRegistrosMagaLu.Location = new System.Drawing.Point(284, 528);
            this.lblRegistrosMagaLu.Name = "lblRegistrosMagaLu";
            this.lblRegistrosMagaLu.Size = new System.Drawing.Size(13, 13);
            this.lblRegistrosMagaLu.TabIndex = 44;
            this.lblRegistrosMagaLu.Text = "0";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(184, 528);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(91, 13);
            this.label7.TabIndex = 43;
            this.label7.Text = "Total de MagaLu:";
            this.label7.Click += new System.EventHandler(this.label7_Click);
            // 
            // lblRegistrosB2W
            // 
            this.lblRegistrosB2W.AutoSize = true;
            this.lblRegistrosB2W.Location = new System.Drawing.Point(284, 551);
            this.lblRegistrosB2W.Name = "lblRegistrosB2W";
            this.lblRegistrosB2W.Size = new System.Drawing.Size(13, 13);
            this.lblRegistrosB2W.TabIndex = 46;
            this.lblRegistrosB2W.Text = "0";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(187, 551);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(79, 13);
            this.label8.TabIndex = 45;
            this.label8.Text = "Total de B2W :";
            this.label8.Click += new System.EventHandler(this.label8_Click);
            // 
            // lblLoja
            // 
            this.lblLoja.AutoSize = true;
            this.lblLoja.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblLoja.Location = new System.Drawing.Point(450, 65);
            this.lblLoja.Name = "lblLoja";
            this.lblLoja.Size = new System.Drawing.Size(0, 16);
            this.lblLoja.TabIndex = 47;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(410, 65);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(0, 16);
            this.label6.TabIndex = 48;
            // 
            // lblLidos
            // 
            this.lblLidos.AutoSize = true;
            this.lblLidos.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblLidos.Location = new System.Drawing.Point(416, 67);
            this.lblLidos.Name = "lblLidos";
            this.lblLidos.Size = new System.Drawing.Size(67, 20);
            this.lblLidos.TabIndex = 49;
            this.lblLidos.Text = "label11";
            // 
            // lblRegistrosMercadoPlace
            // 
            this.lblRegistrosMercadoPlace.AutoSize = true;
            this.lblRegistrosMercadoPlace.Location = new System.Drawing.Point(436, 528);
            this.lblRegistrosMercadoPlace.Name = "lblRegistrosMercadoPlace";
            this.lblRegistrosMercadoPlace.Size = new System.Drawing.Size(13, 13);
            this.lblRegistrosMercadoPlace.TabIndex = 51;
            this.lblRegistrosMercadoPlace.Text = "0";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(306, 528);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(124, 13);
            this.label17.TabIndex = 50;
            this.label17.Text = "Total de Mercado Place:";
            // 
            // chkPedidoAvulso
            // 
            this.chkPedidoAvulso.AutoSize = true;
            this.chkPedidoAvulso.Location = new System.Drawing.Point(446, 9);
            this.chkPedidoAvulso.Name = "chkPedidoAvulso";
            this.chkPedidoAvulso.Size = new System.Drawing.Size(94, 17);
            this.chkPedidoAvulso.TabIndex = 52;
            this.chkPedidoAvulso.Text = "Pedido Avulso";
            this.chkPedidoAvulso.UseVisualStyleBackColor = true;
            // 
            // lblRegistrosTotalExpress
            // 
            this.lblRegistrosTotalExpress.AutoSize = true;
            this.lblRegistrosTotalExpress.Location = new System.Drawing.Point(436, 550);
            this.lblRegistrosTotalExpress.Name = "lblRegistrosTotalExpress";
            this.lblRegistrosTotalExpress.Size = new System.Drawing.Size(13, 13);
            this.lblRegistrosTotalExpress.TabIndex = 54;
            this.lblRegistrosTotalExpress.Text = "0";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(306, 550);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(74, 13);
            this.label27.TabIndex = 53;
            this.label27.Text = "Total Express:";
            // 
            // lblRegistrosAmazon
            // 
            this.lblRegistrosAmazon.AutoSize = true;
            this.lblRegistrosAmazon.Location = new System.Drawing.Point(585, 550);
            this.lblRegistrosAmazon.Name = "lblRegistrosAmazon";
            this.lblRegistrosAmazon.Size = new System.Drawing.Size(13, 13);
            this.lblRegistrosAmazon.TabIndex = 58;
            this.lblRegistrosAmazon.Text = "0";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(455, 550);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(72, 13);
            this.label23.TabIndex = 57;
            this.label23.Text = "Total Amazon";
            // 
            // lblRegistrosSequoia
            // 
            this.lblRegistrosSequoia.AutoSize = true;
            this.lblRegistrosSequoia.Location = new System.Drawing.Point(585, 528);
            this.lblRegistrosSequoia.Name = "lblRegistrosSequoia";
            this.lblRegistrosSequoia.Size = new System.Drawing.Size(13, 13);
            this.lblRegistrosSequoia.TabIndex = 56;
            this.lblRegistrosSequoia.Text = "0";
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Location = new System.Drawing.Point(455, 528);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(91, 13);
            this.label31.TabIndex = 55;
            this.label31.Text = "Total de Sequoia:";
            // 
            // frmEnvio_Correios
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(648, 572);
            this.Controls.Add(this.lblRegistrosAmazon);
            this.Controls.Add(this.label23);
            this.Controls.Add(this.lblRegistrosSequoia);
            this.Controls.Add(this.label31);
            this.Controls.Add(this.lblRegistrosTotalExpress);
            this.Controls.Add(this.label27);
            this.Controls.Add(this.chkPedidoAvulso);
            this.Controls.Add(this.lblRegistrosMercadoPlace);
            this.Controls.Add(this.label17);
            this.Controls.Add(this.lblLidos);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.lblLoja);
            this.Controls.Add(this.lblRegistrosB2W);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.lblRegistrosMagaLu);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.lblRegistrosML);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.lblRegistros);
            this.Controls.Add(this.tabPrincipal);
            this.Controls.Add(this.chkIgnorarData);
            this.Controls.Add(this.chkReenvio);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.lblPlataforma);
            this.Controls.Add(this.lblCliente);
            this.Controls.Add(this.lblPedido);
            this.Controls.Add(this.txtDados);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.txtRastreio);
            this.Controls.Add(this.label2);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmEnvio_Correios";
            this.Text = "Envio aos Correios";
            this.Load += new System.EventHandler(this.Envio_Correios_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.tabPrincipal.ResumeLayout(false);
            this.tab1.ResumeLayout(false);
            this.tab1.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.tabPage3.ResumeLayout(false);
            this.tabPage3.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.tabPage4.ResumeLayout(false);
            this.tabPage4.PerformLayout();
            this.groupBox5.ResumeLayout(false);
            this.tabPage5.ResumeLayout(false);
            this.tabPage5.PerformLayout();
            this.groupBox6.ResumeLayout(false);
            this.tabPage6.ResumeLayout(false);
            this.tabPage6.PerformLayout();
            this.groupBox7.ResumeLayout(false);
            this.tabPage7.ResumeLayout(false);
            this.tabPage7.PerformLayout();
            this.groupBox8.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.groupBox9.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtRastreio;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.RadioButton radioButton2;
        private System.Windows.Forms.RadioButton radioButton1;
        private System.Windows.Forms.RadioButton radioButton4;
        private System.Windows.Forms.RadioButton radioButton3;
        private System.Windows.Forms.RadioButton radioButton6;
        private System.Windows.Forms.TextBox txtDados;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label lblPedido;
        private System.Windows.Forms.Label lblCliente;
        private System.Windows.Forms.Label lblPlataforma;
        private System.Windows.Forms.Timer timeDigitacao;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.CheckBox chkReenvio;
        private System.Windows.Forms.CheckBox chkIgnorarData;
        private System.Windows.Forms.TabControl tabPrincipal;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.Button cmdImprimeMercadoLivre;
        private System.Windows.Forms.Button cmdNovoMercadoLIvre;
        private System.Windows.Forms.Label lblDataCriacaoML;
        private System.Windows.Forms.TextBox txtNumeroEnvioML;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.ListView lstMercadoLivre;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.TabPage tab1;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.ListView lstCorreios;
        private System.Windows.Forms.CheckBox chkEnvioCorreio;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.CheckBox chkBuscar;
        private System.Windows.Forms.Button btnExcluir;
        private System.Windows.Forms.Label lblDataCriacao;
        private System.Windows.Forms.TextBox txtNumeroEnvio;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnImprimir;
        private System.Windows.Forms.Button btnNovo;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label lblRegistros;
        private System.Windows.Forms.Label lblRegistrosML;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label lblRegistrosMagaLu;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label lblRegistrosB2W;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.TabPage tabPage4;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.ListView lstB2W;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Label lblDataCriacaoB2W;
        private System.Windows.Forms.TextBox txtNumeroEnvioB2W;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button cmdNovoB2W;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.ListView lstMagalu;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.Label lblDataCriacaoMagalu;
        private System.Windows.Forms.TextBox txtNumeroEnvioMagalu;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Button button7;
        private System.Windows.Forms.Button button8;
        private System.Windows.Forms.Label lblLoja;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label lblRegistrosB2WFacilShopping;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label lblRegistrosB2WMundial;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label lblRegistrosB2WBela;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label lblRegistrosB2WAlpha;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label lblLidos;
        private System.Windows.Forms.TabPage tabPage5;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.ListView lstMlPlace;
        private System.Windows.Forms.Label lblDataCriacaoMercadoPlace;
        private System.Windows.Forms.TextBox txtNumeroEnvioMlPlace;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Button cmdExcluirMercadoPlace;
        private System.Windows.Forms.Button cmdImprimirMercadoPlace;
        private System.Windows.Forms.Button cmdNovoMercadoPlace;
        private System.Windows.Forms.Label lblRegistrosMercadoPlace;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label lblRegistrosMagaluMundial;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label lblRegistrosMagaluBela;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label lblRegistrosMagaluAlpha;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label lblRegistrosMagaluFacilShopping;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label lblRegistrosMPlaceMundial;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label lblRegistrosMPlaceBela;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label lblRegistrosMPlaceAlpha;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label lblRegistrosMPlaceFS;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Button btnCarregarCorreio;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Button btnCarregarMercadoEnvios;
        private System.Windows.Forms.Button btnCarregarB2W;
        private System.Windows.Forms.Button btnCarregarMagalu;
        private System.Windows.Forms.Button btnCarregarMercadoPlace;
        private System.Windows.Forms.CheckBox chkPedidoAvulso;
        private System.Windows.Forms.TabPage tabPage6;
        private System.Windows.Forms.Button btnCarregarTotalExpress;
        private System.Windows.Forms.GroupBox groupBox7;
        private System.Windows.Forms.ListView lstTotalExpress;
        private System.Windows.Forms.Label lblDataCriacaoTotalExpress;
        private System.Windows.Forms.TextBox txtNumeroEnvioTotalExpress;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.Button cmdExcluirTotalExpress;
        private System.Windows.Forms.Button cmdImprimirTotalExpress;
        private System.Windows.Forms.Button cmdNovoTotalExpress;
        private System.Windows.Forms.Label lblRegistrosTotalExpress;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label lblRegistrosAmazon;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label lblRegistrosSequoia;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.TabPage tabPage7;
        private System.Windows.Forms.Button btnCarregarSequoia;
        private System.Windows.Forms.GroupBox groupBox8;
        private System.Windows.Forms.ListView lstSequoia;
        private System.Windows.Forms.Label lblDataCriacaoSequoia;
        private System.Windows.Forms.TextBox txtNumeroEnvioSequoia;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Button cmdExcluirSequoia;
        private System.Windows.Forms.Button cmdImprimirSequoia;
        private System.Windows.Forms.Button button12;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.Button button9;
        private System.Windows.Forms.Label lblRegistrosAmazonMJ;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.Label lblRegistrosAmazonBella;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.Label lblRegistrosAmazonAJ;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.Label lblRegistrosAmazonFS;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.GroupBox groupBox9;
        private System.Windows.Forms.ListView lstAmazon;
        private System.Windows.Forms.Label lblDataCriacaoAmazon;
        private System.Windows.Forms.TextBox txtNumeroEnvioAmazon;
        private System.Windows.Forms.Label label41;
        private System.Windows.Forms.Button button10;
        private System.Windows.Forms.Button button11;
        private System.Windows.Forms.Button button13;
    }
}