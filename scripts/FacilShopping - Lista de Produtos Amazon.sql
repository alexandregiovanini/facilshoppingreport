SELECT catalog_product_entity.sku SKU, 
(SELECT value FROM catalog_product_entity_varchar WHERE catalog_product_entity_varchar.attribute_id = 84 AND cataloginventory_stock_item.product_id = catalog_product_entity_varchar.entity_id) FORNECEDOR,  
catalog_product_entity_varchar.value value,
catalog_product_entity_text.value fabricante, 
`catalog_product_entity_varchar`.entity_id,
'',
catalog_product_entity_decimal.value VALOR, 
`cataloginventory_stock_item`.qty,

CONCAT('https://facilshopping.com.br/media/catalog/product',
(SELECT value FROM catalog_product_entity_media_gallery WHERE entity_id = catalog_product_entity_varchar.entity_id limit 0,1)) IMAGEM1, 
CONCAT('https://facilshopping.com.br/media/catalog/product',
(SELECT catalog_product_entity_media_gallery.value FROM catalog_product_entity_media_gallery 
INNER JOIN catalog_product_entity_media_gallery_value ON catalog_product_entity_media_gallery_value.value_id = catalog_product_entity_media_gallery.value_id 
WHERE catalog_product_entity_media_gallery.entity_id = catalog_product_entity_varchar.entity_id ORDER BY catalog_product_entity_media_gallery_value.position LIMIT 0,1)) IMAGEM2 ,
'',
'',
'',
'',
'',
'',
'',
'',
Descricao.value Descricao,
'',
'',
'',
'',
'',
'CM',
'CM',

(SELECT value from catalog_product_entity_int where catalog_product_entity_int.attribute_id = 158 and catalog_product_entity_int.entity_id = catalog_product_entity_varchar.entity_id) ALTURA,
(SELECT value from catalog_product_entity_int where catalog_product_entity_int.attribute_id = 157 and catalog_product_entity_int.entity_id = catalog_product_entity_varchar.entity_id) COMPRIMENTO,
(SELECT value from catalog_product_entity_int where catalog_product_entity_int.attribute_id = 159 and catalog_product_entity_int.entity_id = catalog_product_entity_varchar.entity_id) LARGURA,
'KG',
(SELECT value from catalog_product_entity_decimal where catalog_product_entity_decimal.attribute_id = 80 and catalog_product_entity_decimal.entity_id = catalog_product_entity_varchar.entity_id) PESO,
''

FROM `catalog_product_entity_varchar`   

INNER JOIN `cataloginventory_stock_item`   
ON `cataloginventory_stock_item`.product_id = `catalog_product_entity_varchar`.entity_id 

INNER JOIN  `cataloginventory_stock_status` 
ON `cataloginventory_stock_status`.product_id = `catalog_product_entity_varchar`.entity_id 

LEFT JOIN catalog_product_entity_decimal 
ON catalog_product_entity_decimal.attribute_id = 75 
AND catalog_product_entity_decimal.entity_id = catalog_product_entity_varchar.entity_id 

LEFT JOIN catalog_product_entity_decimal CUSTO 
ON catalog_product_entity_decimal.attribute_id = 173 
AND catalog_product_entity_decimal.entity_id = catalog_product_entity_varchar.entity_id 

INNER JOIN  `catalog_product_entity_text`   
ON `catalog_product_entity_text`.entity_id = `catalog_product_entity_varchar`.entity_id 
AND catalog_product_entity_text.attribute_id = 83 


INNER JOIN  `catalog_product_entity_text`  Descricao 
ON Descricao.entity_id = `catalog_product_entity_varchar`.entity_id 
AND Descricao.attribute_id = 73

INNER JOIN  `catalog_product_entity`    
ON `catalog_product_entity`.entity_id = `catalog_product_entity_varchar`.entity_id 

LEFT JOIN catalog_product_entity_decimal PROMOCAO
ON PROMOCAO.attribute_id = 76
AND PROMOCAO.entity_id = catalog_product_entity_varchar.entity_id

LEFT JOIN catalog_product_entity_datetime FIMDAPROMOCAO
ON FIMDAPROMOCAO.entity_id = PROMOCAO.entity_id
AND FIMDAPROMOCAO.attribute_id = 78


where catalog_product_entity_varchar.attribute_id = '71' 
and catalog_product_entity.sku is not null
limit 0,5000; 

