SELECT substring(catalog_product_entity_varchar.value ,1,60) TITULO, substring(catalog_product_entity.sku,1,50) SKU,


    coalesce(itemEstoque.localidade, '') LOCAL , 
coalesce(itemEstoque.quantidade, '') QUANTIDADE,  
(SELECT catalog_product_entity_media_gallery.value FROM catalog_product_entity_media_gallery INNER JOIN catalog_product_entity_media_gallery_value  ON catalog_product_entity_media_gallery_value.value_id = catalog_product_entity_media_gallery.value_id  WHERE catalog_product_entity_media_gallery.entity_id = catalog_product_entity_varchar.entity_id ORDER BY catalog_product_entity_media_gallery_value.position LIMIT 0,1) IMAGEM  ,  
`cataloginventory_stock_item`.qty, 
catalog_product_entity_text.value fabricante,  
EAN.VALUE EAN  

FROM `catalog_product_entity_varchar`   

INNER JOIN catalog_product_entity_varchar EAN  
ON EAN.attribute_id = 169  
AND EAN.entity_id = catalog_product_entity_varchar.entity_id 

INNER JOIN `cataloginventory_stock_item`   
ON `cataloginventory_stock_item`.product_id = `catalog_product_entity_varchar`.entity_id  

INNER JOIN  `cataloginventory_stock_status`  
ON `cataloginventory_stock_status`.product_id = `catalog_product_entity_varchar`.entity_id  

LEFT JOIN catalog_product_entity_decimal  ON catalog_product_entity_decimal.attribute_id = 75  
AND catalog_product_entity_decimal.entity_id = catalog_product_entity_varchar.entity_id  

INNER JOIN  `catalog_product_entity_text`  
ON `catalog_product_entity_text`.entity_id = `catalog_product_entity_varchar`.entity_id  
AND catalog_product_entity_text.attribute_id = 83  

INNER JOIN  `catalog_product_entity`  
ON `catalog_product_entity`.entity_id = `catalog_product_entity_varchar`.entity_id  

LEFT JOIN itemEstoque  ON itemEstoque.SKU = catalog_product_entity.sku  

where catalog_product_entity_varchar.attribute_id = '71'  limit 0,10000;
