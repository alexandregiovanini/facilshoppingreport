select VENDA.customer_firstname, VENDA.customer_lastname, VENDA.customer_email, ENDERECO.fax CEL, ENDERECO.telephone TEL, ENDERECO.city CIDADE, 
count(VENDA.increment_id) PEDIDOS, ROUND(sum(VENDA.total_paid),0)  TOTAL
from sales_flat_order VENDA
LEFT JOIN sales_flat_order_address ENDERECO 
ON VENDA.entity_id = ENDERECO.parent_id
AND ENDERECO.address_type = 'shipping' 
                
WHERE VENDA.canal = 'Loja'

group by  VENDA.customer_email
order by 7 desc;

SELECT * 
from sales_flat_order VENDA
WHERE VENDA.canal = 'Loja';

select * from sales_flat_order_address;