﻿namespace FacilShoppingReports
{
    partial class frmCEPEncontrado
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.lstPedidos = new System.Windows.Forms.ListView();
            this.btnSair2 = new System.Windows.Forms.Button();
            this.groupBox3.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.lstPedidos);
            this.groupBox3.Location = new System.Drawing.Point(6, 3);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(854, 305);
            this.groupBox3.TabIndex = 25;
            this.groupBox3.TabStop = false;
            // 
            // lstPedidos
            // 
            this.lstPedidos.Location = new System.Drawing.Point(5, 15);
            this.lstPedidos.Name = "lstPedidos";
            this.lstPedidos.Size = new System.Drawing.Size(843, 283);
            this.lstPedidos.TabIndex = 21;
            this.lstPedidos.UseCompatibleStateImageBehavior = false;
            this.lstPedidos.View = System.Windows.Forms.View.Details;
            this.lstPedidos.DoubleClick += new System.EventHandler(this.lstPedidos_DoubleClick);
            // 
            // btnSair2
            // 
            this.btnSair2.Image = global::FacilShoppingReports.Properties.Resources.exit_icon;
            this.btnSair2.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnSair2.Location = new System.Drawing.Point(773, 314);
            this.btnSair2.Name = "btnSair2";
            this.btnSair2.Size = new System.Drawing.Size(87, 40);
            this.btnSair2.TabIndex = 26;
            this.btnSair2.Text = "Sair";
            this.btnSair2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnSair2.UseVisualStyleBackColor = true;
            this.btnSair2.Click += new System.EventHandler(this.btnSair2_Click);
            // 
            // frmCEPEncontrado
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(868, 361);
            this.ControlBox = false;
            this.Controls.Add(this.btnSair2);
            this.Controls.Add(this.groupBox3);
            this.Name = "frmCEPEncontrado";
            this.Text = "Pedidos Encontrados";
            this.Load += new System.EventHandler(this.frmCEPEncontrado_Load);
            this.groupBox3.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.ListView lstPedidos;
        private System.Windows.Forms.Button btnSair2;
    }
}