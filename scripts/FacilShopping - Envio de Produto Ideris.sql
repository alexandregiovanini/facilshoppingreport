SELECT substring(catalog_product_entity_varchar.value ,1,60) TITULO, substring(catalog_product_entity.sku,1,50) SKU,  

'Geral' CATEGORIA, 'Geral' SUBCATEGORIA, 


(select  substring(fnStripTags(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(
value
,CHAR(13),' '),'&ccedil;','ç'),'&aacute;','á') ,'&acirc;','â'),'&nbsp;','') , '&eacute;','é') , '&oacute;','ó'), '&iacute;','í'),'&ecirc;','ê'),'&amp;','&'),'&atilde;','ã'),'&otilde;','õ'), '&uacute;','ú'),'&ocirc;','ô'),'&agrave;','à')
			  ,'&Ccedil;','Ç'),'&Aacute;','Á') ,'&Acirc;','Â'),     '"','') , '&Eacute;','É') , '&Oacute;','Ó'), '&Iacute;','Í'),'&Ecirc;','Ê'), '&deg;',''),'&Atilde;','Ã'),'&Otilde;','Õ'), '&Uacute;','Ú'),'&Ocirc;','Õ'),'&Agrave;','À')
),1,2000)


from catalog_product_entity_text where attribute_id = 72 and entity_id = catalog_product_entity_varchar.entity_id) DESCRICAO_COMPLETA,       

REPLACE(catalog_product_entity_text.value,'FABRICANTE:','') 'MARCA/FABRICANTE', 'GERAL' DEPARTAMENTO,

REPLACE(CUSTO.VALUE,'.',',') CUSTO, 

REPLACE(catalog_product_entity_decimal.value,'.',',') VALOR_VENDA, 

cast(`cataloginventory_stock_item`.qty as unsigned integer) Qtde,



(SELECT CONCAT("https://facilshopping.com.br/media/catalog/product"  , catalog_product_entity_media_gallery.value) FROM catalog_product_entity_media_gallery INNER JOIN catalog_product_entity_media_gallery_value  ON catalog_product_entity_media_gallery_value.value_id = catalog_product_entity_media_gallery.value_id  WHERE catalog_product_entity_media_gallery.entity_id = catalog_product_entity_varchar.entity_id ORDER BY catalog_product_entity_media_gallery_value.position LIMIT 0,1) IMAGEM_1  ,   

'' IMAGEM_2, '' IMAGEM_3, '' IMAGEM_4, '' IMAGEM_5, '' IMAGEM_6, '' IMAGEM_7, '' IMAGEM_8, '' IMAGEM_9, '' IMAGEM_10,


substring(EAN.VALUE,1,20) EAN,

'' YOUTUBE,  '' MODELO, '' GARANTIA,
(SELECT coalesce(REPLACE(value,'.',','),' ') from catalog_product_entity_decimal where catalog_product_entity_decimal.attribute_id = 80 and catalog_product_entity_decimal.entity_id = catalog_product_entity_varchar.entity_id) PESO,
(SELECT coalesce(REPLACE(value,'.',','),' ') from catalog_product_entity_int where catalog_product_entity_int.attribute_id = 157 and catalog_product_entity_int.entity_id = catalog_product_entity_varchar.entity_id) COMPRIMENTO,
(SELECT coalesce(REPLACE(value,'.',','),' ') from catalog_product_entity_int where catalog_product_entity_int.attribute_id = 159 and catalog_product_entity_int.entity_id = catalog_product_entity_varchar.entity_id) LARGURA,
(SELECT coalesce(REPLACE(value,'.',','),' ') from catalog_product_entity_int where catalog_product_entity_int.attribute_id = 158 and catalog_product_entity_int.entity_id = catalog_product_entity_varchar.entity_id) ALTURA,

(SELECT coalesce(REPLACE(value + 0.1,'.',','),' ') from catalog_product_entity_decimal where catalog_product_entity_decimal.attribute_id = 80 and catalog_product_entity_decimal.entity_id = catalog_product_entity_varchar.entity_id) PESO_EMBALADO,

(SELECT coalesce(REPLACE(value + 2,'.',','),' ') from catalog_product_entity_int where catalog_product_entity_int.attribute_id = 157 and catalog_product_entity_int.entity_id = catalog_product_entity_varchar.entity_id) COMPRIMENTO_EMBALADO,
(SELECT coalesce(REPLACE(value + 2,'.',','),' ') from catalog_product_entity_int where catalog_product_entity_int.attribute_id = 159 and catalog_product_entity_int.entity_id = catalog_product_entity_varchar.entity_id) LARGURA_EMBALADO,
(SELECT coalesce(REPLACE(value + 2,'.',','),' ') from catalog_product_entity_int where catalog_product_entity_int.attribute_id = 158 and catalog_product_entity_int.entity_id = catalog_product_entity_varchar.entity_id) ALTURA_EMBALADO,

NCM.value NCM,

'' DESCRICAO_NCM, '' CEST, 0 ORIGEM

FROM `catalog_product_entity_varchar`     

INNER JOIN catalog_product_entity_varchar EAN   
ON EAN.attribute_id = 169   
AND EAN.entity_id = catalog_product_entity_varchar.entity_id   

inner join catalog_product_entity_decimal CUSTO
on CUSTO.attribute_id = 173 
AND CUSTO.entity_id = catalog_product_entity_varchar.entity_id     


INNER JOIN `cataloginventory_stock_item`    
ON `cataloginventory_stock_item`.product_id = `catalog_product_entity_varchar`.entity_id    

INNER JOIN  `cataloginventory_stock_status`   
ON `cataloginventory_stock_status`.product_id = `catalog_product_entity_varchar`.entity_id    

LEFT JOIN catalog_product_entity_decimal  
ON catalog_product_entity_decimal.attribute_id = 75   
AND catalog_product_entity_decimal.entity_id = catalog_product_entity_varchar.entity_id    

INNER JOIN  `catalog_product_entity_text`   
ON `catalog_product_entity_text`.entity_id = `catalog_product_entity_varchar`.entity_id   
AND catalog_product_entity_text.attribute_id = 83    

INNER JOIN  `catalog_product_entity`   
ON `catalog_product_entity`.entity_id = `catalog_product_entity_varchar`.entity_id    

inner join catalog_product_entity_varchar NCM 
on NCM.attribute_id = 174 AND 
NCM.entity_id =  catalog_product_entity_varchar.entity_id

LEFT JOIN itemEstoque  
ON itemEstoque.SKU = catalog_product_entity.sku    

where catalog_product_entity_varchar.attribute_id = '71' 
and catalog_product_entity_text.value is not null
AND (
catalog_product_entity_varchar.value NOT LIKE '%CALCINHA%'
AND catalog_product_entity_varchar.value NOT LIKE '%CAMISOLA%'
AND catalog_product_entity_varchar.value NOT LIKE '%BICO DE PATO%'
AND catalog_product_entity_varchar.value NOT LIKE '%PIJAMA%'
AND catalog_product_entity_varchar.value NOT LIKE '%SUTI%'
AND catalog_product_entity_varchar.value NOT LIKE '%TIARA%'
AND catalog_product_entity_varchar.value NOT LIKE '%Rucci%'
)
AND 
catalog_product_entity.sku NOT IN (
'1308-CANDIDE-Vermelho',
'1308-CANDIDE-Branco',
'2394-CANDIDE-Batman',
'2394-CANDIDE-Superman',
'6114-CANDIDE-Camaro',
'6114-CANDIDE-Ford Mustang',
'1001-1002 - MAGICTOYS-Rosa',
'1001-1002 - MAGICTOYS-Azul',
'0709-Betoneira',
'0709-Caçamba',
'0709-Bombeiro',
'0709-Surf',
'1517-Gato-Azul claro',
'1517-Gato-Rosa',
'1517-CACHORRO-Azul claro',
'1517-CACHORRO-Rosa',
'SK4019-Whats Up',
'SK4019-Milk Shake',
'001kitfruit',
'8525',
'DEV22001',
'M017',
'5189-BELFIX',
'ZR0207002'
)
order by catalog_product_entity_varchar.value
limit 0,10000	;



select catalog_product_entity.SKU, NCM.value NCM, EAN.VALUE, NOME.value
from catalog_product_entity
inner join catalog_product_entity_varchar NCM 
on NCM.attribute_id = 174 
AND NCM.entity_id =  catalog_product_entity.entity_id 
INNER JOIN catalog_product_entity_varchar NOME
ON NOME.attribute_id = '71'
AND NCM.entity_id =  NOME.entity_id
INNER JOIN catalog_product_entity_varchar EAN   
ON EAN.attribute_id = 169   
AND EAN.entity_id = NOME.entity_id LIMIT 5000;
