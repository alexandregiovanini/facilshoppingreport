﻿namespace FacilShoppingReports
{
    partial class frmRelatorio
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmRelatorio));
            this.lstEstoque = new System.Windows.Forms.ListView();
            this.frameEstoque = new System.Windows.Forms.GroupBox();
            this.txtEAN = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.chkXML = new System.Windows.Forms.CheckBox();
            this.cbCategorias = new System.Windows.Forms.ComboBox();
            this.chkCategoria = new System.Windows.Forms.CheckBox();
            this.txtFornecedor = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.chkAtivos = new System.Windows.Forms.CheckBox();
            this.txtFabricante = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.chkQtdeMinima = new System.Windows.Forms.CheckBox();
            this.txtDescricao = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.btnExportar = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.btnSair = new System.Windows.Forms.Button();
            this.btnLimpar = new System.Windows.Forms.Button();
            this.btPesquisar = new System.Windows.Forms.Button();
            this.lblRegistros = new System.Windows.Forms.Label();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.optDeclaração = new System.Windows.Forms.RadioButton();
            this.optContinental = new System.Windows.Forms.RadioButton();
            this.optVenda = new System.Windows.Forms.RadioButton();
            this.optEstoque = new System.Windows.Forms.RadioButton();
            this.frameVendas = new System.Windows.Forms.GroupBox();
            this.cbFabricante = new System.Windows.Forms.ComboBox();
            this.chkFabricante = new System.Windows.Forms.CheckBox();
            this.cbCategoriasVenda = new System.Windows.Forms.ComboBox();
            this.chkCategoriaVenda = new System.Windows.Forms.CheckBox();
            this.dcLoja = new System.Windows.Forms.ComboBox();
            this.chkLoja = new System.Windows.Forms.CheckBox();
            this.cbProcesso = new System.Windows.Forms.ComboBox();
            this.chkProcesso = new System.Windows.Forms.CheckBox();
            this.imgCodigoBarraCEP = new System.Windows.Forms.PictureBox();
            this.chkSKUPluggto = new System.Windows.Forms.CheckBox();
            this.chkPesquisarPedido = new System.Windows.Forms.CheckBox();
            this.chkCancelados = new System.Windows.Forms.CheckBox();
            this.lblBusca = new System.Windows.Forms.Label();
            this.txtProduto = new System.Windows.Forms.TextBox();
            this.cbStatus = new System.Windows.Forms.ComboBox();
            this.chkStatus = new System.Windows.Forms.CheckBox();
            this.cbCanal = new System.Windows.Forms.ComboBox();
            this.chkCanal = new System.Windows.Forms.CheckBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.optStatus = new System.Windows.Forms.RadioButton();
            this.optPedido = new System.Windows.Forms.RadioButton();
            this.mskDtFinal = new System.Windows.Forms.MaskedTextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.mskDtInicial = new System.Windows.Forms.MaskedTextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.lblValorTotal = new System.Windows.Forms.Label();
            this.lblFreteTotal = new System.Windows.Forms.Label();
            this.lblQuantidadeTotal = new System.Windows.Forms.Label();
            this.cmdLidos = new System.Windows.Forms.Button();
            this.cmdEnviado = new System.Windows.Forms.Button();
            this.cmdParado = new System.Windows.Forms.Button();
            this.imgCodigoBarra = new System.Windows.Forms.PictureBox();
            this.cmdMarcar = new System.Windows.Forms.Button();
            this.cmdSeparar = new System.Windows.Forms.Button();
            this.cmdSeparado = new System.Windows.Forms.Button();
            this.cmdLiberar = new System.Windows.Forms.Button();
            this.lblCusto = new System.Windows.Forms.Label();
            this.lblAviso = new System.Windows.Forms.Label();
            this.picQRCode = new System.Windows.Forms.PictureBox();
            this.cmdEmail = new System.Windows.Forms.Button();
            this.frameEstoque.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.frameVendas.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.imgCodigoBarraCEP)).BeginInit();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.imgCodigoBarra)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picQRCode)).BeginInit();
            this.SuspendLayout();
            // 
            // lstEstoque
            // 
            this.lstEstoque.HideSelection = false;
            this.lstEstoque.Location = new System.Drawing.Point(18, 162);
            this.lstEstoque.Name = "lstEstoque";
            this.lstEstoque.Size = new System.Drawing.Size(1124, 362);
            this.lstEstoque.Sorting = System.Windows.Forms.SortOrder.Ascending;
            this.lstEstoque.TabIndex = 0;
            this.lstEstoque.UseCompatibleStateImageBehavior = false;
            this.lstEstoque.View = System.Windows.Forms.View.Details;
            this.lstEstoque.ColumnClick += new System.Windows.Forms.ColumnClickEventHandler(this.lstEstoque_ColumnClick);
            this.lstEstoque.SelectedIndexChanged += new System.EventHandler(this.listView1_SelectedIndexChanged);
            this.lstEstoque.DoubleClick += new System.EventHandler(this.lstEstoque_DoubleClick);
            // 
            // frameEstoque
            // 
            this.frameEstoque.Controls.Add(this.txtEAN);
            this.frameEstoque.Controls.Add(this.label5);
            this.frameEstoque.Controls.Add(this.chkXML);
            this.frameEstoque.Controls.Add(this.cbCategorias);
            this.frameEstoque.Controls.Add(this.chkCategoria);
            this.frameEstoque.Controls.Add(this.txtFornecedor);
            this.frameEstoque.Controls.Add(this.label3);
            this.frameEstoque.Controls.Add(this.chkAtivos);
            this.frameEstoque.Controls.Add(this.txtFabricante);
            this.frameEstoque.Controls.Add(this.label2);
            this.frameEstoque.Controls.Add(this.chkQtdeMinima);
            this.frameEstoque.Controls.Add(this.txtDescricao);
            this.frameEstoque.Controls.Add(this.label1);
            this.frameEstoque.Location = new System.Drawing.Point(19, 12);
            this.frameEstoque.Name = "frameEstoque";
            this.frameEstoque.Size = new System.Drawing.Size(687, 119);
            this.frameEstoque.TabIndex = 8;
            this.frameEstoque.TabStop = false;
            this.frameEstoque.Text = "Estoque";
            this.frameEstoque.Enter += new System.EventHandler(this.groupBox1_Enter);
            // 
            // txtEAN
            // 
            this.txtEAN.Location = new System.Drawing.Point(77, 92);
            this.txtEAN.Name = "txtEAN";
            this.txtEAN.Size = new System.Drawing.Size(260, 20);
            this.txtEAN.TabIndex = 22;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(15, 95);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(29, 13);
            this.label5.TabIndex = 21;
            this.label5.Text = "EAN";
            // 
            // chkXML
            // 
            this.chkXML.AutoSize = true;
            this.chkXML.Location = new System.Drawing.Point(599, 21);
            this.chkXML.Name = "chkXML";
            this.chkXML.Size = new System.Drawing.Size(77, 17);
            this.chkXML.TabIndex = 20;
            this.chkXML.Text = "Gerar XML";
            this.chkXML.UseVisualStyleBackColor = true;
            this.chkXML.Visible = false;
            // 
            // cbCategorias
            // 
            this.cbCategorias.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cbCategorias.FormattingEnabled = true;
            this.cbCategorias.Location = new System.Drawing.Point(432, 57);
            this.cbCategorias.Name = "cbCategorias";
            this.cbCategorias.Size = new System.Drawing.Size(245, 21);
            this.cbCategorias.TabIndex = 14;
            this.cbCategorias.Visible = false;
            this.cbCategorias.SelectedIndexChanged += new System.EventHandler(this.cbCategorias_SelectedIndexChanged);
            // 
            // chkCategoria
            // 
            this.chkCategoria.AutoSize = true;
            this.chkCategoria.Checked = true;
            this.chkCategoria.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkCategoria.Location = new System.Drawing.Point(365, 59);
            this.chkCategoria.Name = "chkCategoria";
            this.chkCategoria.Size = new System.Drawing.Size(71, 17);
            this.chkCategoria.TabIndex = 19;
            this.chkCategoria.Text = "Categoria";
            this.chkCategoria.UseVisualStyleBackColor = true;
            this.chkCategoria.Visible = false;
            this.chkCategoria.CheckedChanged += new System.EventHandler(this.chkCategoria_CheckedChanged);
            // 
            // txtFornecedor
            // 
            this.txtFornecedor.Location = new System.Drawing.Point(77, 64);
            this.txtFornecedor.Name = "txtFornecedor";
            this.txtFornecedor.Size = new System.Drawing.Size(260, 20);
            this.txtFornecedor.TabIndex = 18;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(15, 67);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(61, 13);
            this.label3.TabIndex = 17;
            this.label3.Text = "Fornecedor";
            // 
            // chkAtivos
            // 
            this.chkAtivos.AutoSize = true;
            this.chkAtivos.Checked = true;
            this.chkAtivos.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkAtivos.Location = new System.Drawing.Point(487, 22);
            this.chkAtivos.Name = "chkAtivos";
            this.chkAtivos.Size = new System.Drawing.Size(112, 17);
            this.chkAtivos.TabIndex = 13;
            this.chkAtivos.Text = "só produtos ativos";
            this.chkAtivos.UseVisualStyleBackColor = true;
            this.chkAtivos.Visible = false;
            this.chkAtivos.CheckedChanged += new System.EventHandler(this.chkAtivos_CheckedChanged);
            // 
            // txtFabricante
            // 
            this.txtFabricante.Location = new System.Drawing.Point(77, 39);
            this.txtFabricante.Name = "txtFabricante";
            this.txtFabricante.Size = new System.Drawing.Size(260, 20);
            this.txtFabricante.TabIndex = 12;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(14, 42);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(57, 13);
            this.label2.TabIndex = 11;
            this.label2.Text = "Fabricante";
            // 
            // chkQtdeMinima
            // 
            this.chkQtdeMinima.AutoSize = true;
            this.chkQtdeMinima.Location = new System.Drawing.Point(365, 22);
            this.chkQtdeMinima.Name = "chkQtdeMinima";
            this.chkQtdeMinima.Size = new System.Drawing.Size(116, 17);
            this.chkQtdeMinima.TabIndex = 10;
            this.chkQtdeMinima.Text = "Quantidade minima";
            this.chkQtdeMinima.UseVisualStyleBackColor = true;
            // 
            // txtDescricao
            // 
            this.txtDescricao.Location = new System.Drawing.Point(77, 14);
            this.txtDescricao.Name = "txtDescricao";
            this.txtDescricao.Size = new System.Drawing.Size(260, 20);
            this.txtDescricao.TabIndex = 9;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(14, 17);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(55, 13);
            this.label1.TabIndex = 8;
            this.label1.Text = "Descrição";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.btnExportar);
            this.groupBox2.Controls.Add(this.button1);
            this.groupBox2.Controls.Add(this.btnSair);
            this.groupBox2.Controls.Add(this.btnLimpar);
            this.groupBox2.Controls.Add(this.btPesquisar);
            this.groupBox2.Location = new System.Drawing.Point(710, 12);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(432, 57);
            this.groupBox2.TabIndex = 10;
            this.groupBox2.TabStop = false;
            this.groupBox2.Enter += new System.EventHandler(this.groupBox2_Enter_1);
            // 
            // btnExportar
            // 
            this.btnExportar.Image = global::FacilShoppingReports.Properties.Resources.new_icon;
            this.btnExportar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnExportar.Location = new System.Drawing.Point(257, 11);
            this.btnExportar.Name = "btnExportar";
            this.btnExportar.Size = new System.Drawing.Size(85, 40);
            this.btnExportar.TabIndex = 14;
            this.btnExportar.Text = "Exportar";
            this.btnExportar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnExportar.UseVisualStyleBackColor = true;
            this.btnExportar.Click += new System.EventHandler(this.btnExportar_Click);
            // 
            // button1
            // 
            this.button1.Image = global::FacilShoppingReports.Properties.Resources.print_icon;
            this.button1.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button1.Location = new System.Drawing.Point(172, 11);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(85, 40);
            this.button1.TabIndex = 13;
            this.button1.Text = "Imprimir";
            this.button1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // btnSair
            // 
            this.btnSair.Image = global::FacilShoppingReports.Properties.Resources.exit_icon;
            this.btnSair.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnSair.Location = new System.Drawing.Point(342, 11);
            this.btnSair.Name = "btnSair";
            this.btnSair.Size = new System.Drawing.Size(85, 40);
            this.btnSair.TabIndex = 12;
            this.btnSair.Text = "Sair";
            this.btnSair.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnSair.UseVisualStyleBackColor = true;
            this.btnSair.Click += new System.EventHandler(this.btnSair_Click);
            // 
            // btnLimpar
            // 
            this.btnLimpar.Image = global::FacilShoppingReports.Properties.Resources.delete_icon;
            this.btnLimpar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnLimpar.Location = new System.Drawing.Point(87, 11);
            this.btnLimpar.Name = "btnLimpar";
            this.btnLimpar.Size = new System.Drawing.Size(85, 40);
            this.btnLimpar.TabIndex = 11;
            this.btnLimpar.Text = "Limpar";
            this.btnLimpar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnLimpar.UseVisualStyleBackColor = true;
            this.btnLimpar.Click += new System.EventHandler(this.btnLimpar_Click);
            // 
            // btPesquisar
            // 
            this.btPesquisar.Image = global::FacilShoppingReports.Properties.Resources.search_icon1;
            this.btPesquisar.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
            this.btPesquisar.Location = new System.Drawing.Point(2, 11);
            this.btPesquisar.Name = "btPesquisar";
            this.btPesquisar.Size = new System.Drawing.Size(85, 40);
            this.btPesquisar.TabIndex = 10;
            this.btPesquisar.Text = "Pesquisar";
            this.btPesquisar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btPesquisar.UseVisualStyleBackColor = true;
            this.btPesquisar.Click += new System.EventHandler(this.btPesquisar_Click);
            // 
            // lblRegistros
            // 
            this.lblRegistros.AutoSize = true;
            this.lblRegistros.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRegistros.Location = new System.Drawing.Point(20, 570);
            this.lblRegistros.Name = "lblRegistros";
            this.lblRegistros.Size = new System.Drawing.Size(236, 24);
            this.lblRegistros.TabIndex = 11;
            this.lblRegistros.Text = "TOTAL DE REGISTROS: 0";
            this.lblRegistros.Click += new System.EventHandler(this.lblRegistros_Click);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.optDeclaração);
            this.groupBox3.Controls.Add(this.optContinental);
            this.groupBox3.Controls.Add(this.optVenda);
            this.groupBox3.Controls.Add(this.optEstoque);
            this.groupBox3.Location = new System.Drawing.Point(713, 70);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(429, 39);
            this.groupBox3.TabIndex = 12;
            this.groupBox3.TabStop = false;
            // 
            // optDeclaração
            // 
            this.optDeclaração.AutoSize = true;
            this.optDeclaração.Location = new System.Drawing.Point(215, 13);
            this.optDeclaração.Name = "optDeclaração";
            this.optDeclaração.Size = new System.Drawing.Size(124, 17);
            this.optDeclaração.TabIndex = 17;
            this.optDeclaração.Text = "Etiqueta/Declaração";
            this.optDeclaração.UseVisualStyleBackColor = true;
            this.optDeclaração.CheckedChanged += new System.EventHandler(this.radioButton1_CheckedChanged);
            // 
            // optContinental
            // 
            this.optContinental.AutoSize = true;
            this.optContinental.Location = new System.Drawing.Point(137, 13);
            this.optContinental.Name = "optContinental";
            this.optContinental.Size = new System.Drawing.Size(77, 17);
            this.optContinental.TabIndex = 16;
            this.optContinental.Text = "Separação";
            this.optContinental.UseVisualStyleBackColor = true;
            this.optContinental.CheckedChanged += new System.EventHandler(this.optContinental_CheckedChanged);
            // 
            // optVenda
            // 
            this.optVenda.AutoSize = true;
            this.optVenda.Location = new System.Drawing.Point(77, 13);
            this.optVenda.Name = "optVenda";
            this.optVenda.Size = new System.Drawing.Size(56, 17);
            this.optVenda.TabIndex = 15;
            this.optVenda.Text = "Venda";
            this.optVenda.UseVisualStyleBackColor = true;
            this.optVenda.CheckedChanged += new System.EventHandler(this.optVenda_CheckedChanged);
            // 
            // optEstoque
            // 
            this.optEstoque.AutoSize = true;
            this.optEstoque.Checked = true;
            this.optEstoque.Location = new System.Drawing.Point(9, 13);
            this.optEstoque.Name = "optEstoque";
            this.optEstoque.Size = new System.Drawing.Size(64, 17);
            this.optEstoque.TabIndex = 14;
            this.optEstoque.TabStop = true;
            this.optEstoque.Text = "Estoque";
            this.optEstoque.UseVisualStyleBackColor = true;
            this.optEstoque.CheckedChanged += new System.EventHandler(this.optEstoque_CheckedChanged);
            // 
            // frameVendas
            // 
            this.frameVendas.Controls.Add(this.cbFabricante);
            this.frameVendas.Controls.Add(this.chkFabricante);
            this.frameVendas.Controls.Add(this.cbCategoriasVenda);
            this.frameVendas.Controls.Add(this.chkCategoriaVenda);
            this.frameVendas.Controls.Add(this.dcLoja);
            this.frameVendas.Controls.Add(this.chkLoja);
            this.frameVendas.Controls.Add(this.cbProcesso);
            this.frameVendas.Controls.Add(this.chkProcesso);
            this.frameVendas.Controls.Add(this.imgCodigoBarraCEP);
            this.frameVendas.Controls.Add(this.chkSKUPluggto);
            this.frameVendas.Controls.Add(this.chkPesquisarPedido);
            this.frameVendas.Controls.Add(this.chkCancelados);
            this.frameVendas.Controls.Add(this.lblBusca);
            this.frameVendas.Controls.Add(this.txtProduto);
            this.frameVendas.Controls.Add(this.cbStatus);
            this.frameVendas.Controls.Add(this.chkStatus);
            this.frameVendas.Controls.Add(this.cbCanal);
            this.frameVendas.Controls.Add(this.chkCanal);
            this.frameVendas.Controls.Add(this.groupBox1);
            this.frameVendas.Controls.Add(this.mskDtFinal);
            this.frameVendas.Controls.Add(this.label7);
            this.frameVendas.Controls.Add(this.mskDtInicial);
            this.frameVendas.Controls.Add(this.label4);
            this.frameVendas.Location = new System.Drawing.Point(19, 12);
            this.frameVendas.Name = "frameVendas";
            this.frameVendas.Size = new System.Drawing.Size(686, 147);
            this.frameVendas.TabIndex = 13;
            this.frameVendas.TabStop = false;
            this.frameVendas.Text = "Vendas";
            this.frameVendas.Visible = false;
            this.frameVendas.Enter += new System.EventHandler(this.frameVendas_Enter);
            // 
            // cbFabricante
            // 
            this.cbFabricante.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cbFabricante.Enabled = false;
            this.cbFabricante.FormattingEnabled = true;
            this.cbFabricante.Location = new System.Drawing.Point(406, 123);
            this.cbFabricante.Name = "cbFabricante";
            this.cbFabricante.Size = new System.Drawing.Size(245, 21);
            this.cbFabricante.TabIndex = 58;
            // 
            // chkFabricante
            // 
            this.chkFabricante.AutoSize = true;
            this.chkFabricante.Location = new System.Drawing.Point(324, 127);
            this.chkFabricante.Name = "chkFabricante";
            this.chkFabricante.Size = new System.Drawing.Size(76, 17);
            this.chkFabricante.TabIndex = 57;
            this.chkFabricante.Text = "Fabricante";
            this.chkFabricante.UseVisualStyleBackColor = true;
            this.chkFabricante.CheckedChanged += new System.EventHandler(this.chkFabricante_CheckedChanged);
            // 
            // cbCategoriasVenda
            // 
            this.cbCategoriasVenda.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cbCategoriasVenda.Enabled = false;
            this.cbCategoriasVenda.FormattingEnabled = true;
            this.cbCategoriasVenda.Items.AddRange(new object[] {
            "PARADOS",
            "SEPARAR",
            "SEPARADOS",
            "LIDOS",
            "ENVIADOS"});
            this.cbCategoriasVenda.Location = new System.Drawing.Point(406, 99);
            this.cbCategoriasVenda.Name = "cbCategoriasVenda";
            this.cbCategoriasVenda.Size = new System.Drawing.Size(245, 21);
            this.cbCategoriasVenda.TabIndex = 56;
            // 
            // chkCategoriaVenda
            // 
            this.chkCategoriaVenda.AutoSize = true;
            this.chkCategoriaVenda.Location = new System.Drawing.Point(324, 103);
            this.chkCategoriaVenda.Name = "chkCategoriaVenda";
            this.chkCategoriaVenda.Size = new System.Drawing.Size(71, 17);
            this.chkCategoriaVenda.TabIndex = 55;
            this.chkCategoriaVenda.Text = "Categoria";
            this.chkCategoriaVenda.UseVisualStyleBackColor = true;
            this.chkCategoriaVenda.CheckedChanged += new System.EventHandler(this.chkCategoriaVenda_CheckedChanged);
            // 
            // dcLoja
            // 
            this.dcLoja.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.dcLoja.Enabled = false;
            this.dcLoja.FormattingEnabled = true;
            this.dcLoja.Items.AddRange(new object[] {
            "PARADOS",
            "SEPARAR",
            "SEPARADOS",
            "LIDOS",
            "ENVIADOS"});
            this.dcLoja.Location = new System.Drawing.Point(406, 77);
            this.dcLoja.Name = "dcLoja";
            this.dcLoja.Size = new System.Drawing.Size(245, 21);
            this.dcLoja.TabIndex = 52;
            // 
            // chkLoja
            // 
            this.chkLoja.AutoSize = true;
            this.chkLoja.Location = new System.Drawing.Point(323, 79);
            this.chkLoja.Name = "chkLoja";
            this.chkLoja.Size = new System.Drawing.Size(46, 17);
            this.chkLoja.TabIndex = 53;
            this.chkLoja.Text = "Loja";
            this.chkLoja.UseVisualStyleBackColor = true;
            this.chkLoja.CheckedChanged += new System.EventHandler(this.chkLoja_CheckedChanged);
            // 
            // cbProcesso
            // 
            this.cbProcesso.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cbProcesso.Enabled = false;
            this.cbProcesso.FormattingEnabled = true;
            this.cbProcesso.Items.AddRange(new object[] {
            "PARADOS",
            "SEPARAR",
            "SEPARADOS",
            "LIDOS",
            "ENVIADOS"});
            this.cbProcesso.Location = new System.Drawing.Point(406, 53);
            this.cbProcesso.Name = "cbProcesso";
            this.cbProcesso.Size = new System.Drawing.Size(245, 21);
            this.cbProcesso.TabIndex = 50;
            // 
            // chkProcesso
            // 
            this.chkProcesso.AutoSize = true;
            this.chkProcesso.Location = new System.Drawing.Point(323, 55);
            this.chkProcesso.Name = "chkProcesso";
            this.chkProcesso.Size = new System.Drawing.Size(70, 17);
            this.chkProcesso.TabIndex = 51;
            this.chkProcesso.Text = "Processo";
            this.chkProcesso.UseVisualStyleBackColor = true;
            this.chkProcesso.CheckedChanged += new System.EventHandler(this.chkProcesso_CheckedChanged);
            // 
            // imgCodigoBarraCEP
            // 
            this.imgCodigoBarraCEP.ImageLocation = "c:\\temp\\test.bmp";
            this.imgCodigoBarraCEP.Location = new System.Drawing.Point(607, -3);
            this.imgCodigoBarraCEP.Name = "imgCodigoBarraCEP";
            this.imgCodigoBarraCEP.Size = new System.Drawing.Size(44, 41);
            this.imgCodigoBarraCEP.TabIndex = 21;
            this.imgCodigoBarraCEP.TabStop = false;
            this.imgCodigoBarraCEP.Visible = false;
            // 
            // chkSKUPluggto
            // 
            this.chkSKUPluggto.AutoSize = true;
            this.chkSKUPluggto.Location = new System.Drawing.Point(217, 49);
            this.chkSKUPluggto.Name = "chkSKUPluggto";
            this.chkSKUPluggto.Size = new System.Drawing.Size(90, 17);
            this.chkSKUPluggto.TabIndex = 49;
            this.chkSKUPluggto.Text = "SKU Plugg.to";
            this.chkSKUPluggto.UseVisualStyleBackColor = true;
            this.chkSKUPluggto.Visible = false;
            // 
            // chkPesquisarPedido
            // 
            this.chkPesquisarPedido.AutoSize = true;
            this.chkPesquisarPedido.Location = new System.Drawing.Point(126, 96);
            this.chkPesquisarPedido.Name = "chkPesquisarPedido";
            this.chkPesquisarPedido.Size = new System.Drawing.Size(126, 17);
            this.chkPesquisarPedido.TabIndex = 48;
            this.chkPesquisarPedido.Text = "Pesquisar por Pedido";
            this.chkPesquisarPedido.UseVisualStyleBackColor = true;
            // 
            // chkCancelados
            // 
            this.chkCancelados.AutoSize = true;
            this.chkCancelados.Location = new System.Drawing.Point(6, 96);
            this.chkCancelados.Name = "chkCancelados";
            this.chkCancelados.Size = new System.Drawing.Size(120, 17);
            this.chkCancelados.TabIndex = 47;
            this.chkCancelados.Text = "Mostrar Cancelados";
            this.chkCancelados.UseVisualStyleBackColor = true;
            // 
            // lblBusca
            // 
            this.lblBusca.AutoSize = true;
            this.lblBusca.Location = new System.Drawing.Point(25, 72);
            this.lblBusca.Name = "lblBusca";
            this.lblBusca.Size = new System.Drawing.Size(44, 13);
            this.lblBusca.TabIndex = 46;
            this.lblBusca.Text = "Produto";
            // 
            // txtProduto
            // 
            this.txtProduto.Location = new System.Drawing.Point(75, 68);
            this.txtProduto.Name = "txtProduto";
            this.txtProduto.Size = new System.Drawing.Size(232, 20);
            this.txtProduto.TabIndex = 45;
            // 
            // cbStatus
            // 
            this.cbStatus.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cbStatus.Enabled = false;
            this.cbStatus.FormattingEnabled = true;
            this.cbStatus.Location = new System.Drawing.Point(406, 31);
            this.cbStatus.Name = "cbStatus";
            this.cbStatus.Size = new System.Drawing.Size(245, 21);
            this.cbStatus.TabIndex = 43;
            // 
            // chkStatus
            // 
            this.chkStatus.AutoSize = true;
            this.chkStatus.Location = new System.Drawing.Point(323, 33);
            this.chkStatus.Name = "chkStatus";
            this.chkStatus.Size = new System.Drawing.Size(56, 17);
            this.chkStatus.TabIndex = 44;
            this.chkStatus.Text = "Status";
            this.chkStatus.UseVisualStyleBackColor = true;
            this.chkStatus.CheckedChanged += new System.EventHandler(this.chkStatus_CheckedChanged);
            // 
            // cbCanal
            // 
            this.cbCanal.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cbCanal.Enabled = false;
            this.cbCanal.FormattingEnabled = true;
            this.cbCanal.Location = new System.Drawing.Point(406, 8);
            this.cbCanal.Name = "cbCanal";
            this.cbCanal.Size = new System.Drawing.Size(245, 21);
            this.cbCanal.TabIndex = 41;
            // 
            // chkCanal
            // 
            this.chkCanal.AutoSize = true;
            this.chkCanal.Location = new System.Drawing.Point(323, 10);
            this.chkCanal.Name = "chkCanal";
            this.chkCanal.Size = new System.Drawing.Size(53, 17);
            this.chkCanal.TabIndex = 42;
            this.chkCanal.Text = "Canal";
            this.chkCanal.UseVisualStyleBackColor = true;
            this.chkCanal.CheckedChanged += new System.EventHandler(this.chkCanal_CheckedChanged);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.optStatus);
            this.groupBox1.Controls.Add(this.optPedido);
            this.groupBox1.Location = new System.Drawing.Point(172, 11);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(135, 39);
            this.groupBox1.TabIndex = 40;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Pesquisar por Data do";
            // 
            // optStatus
            // 
            this.optStatus.AutoSize = true;
            this.optStatus.Checked = true;
            this.optStatus.Location = new System.Drawing.Point(75, 15);
            this.optStatus.Name = "optStatus";
            this.optStatus.Size = new System.Drawing.Size(55, 17);
            this.optStatus.TabIndex = 15;
            this.optStatus.TabStop = true;
            this.optStatus.Text = "Status";
            this.optStatus.UseVisualStyleBackColor = true;
            // 
            // optPedido
            // 
            this.optPedido.AutoSize = true;
            this.optPedido.Location = new System.Drawing.Point(9, 15);
            this.optPedido.Name = "optPedido";
            this.optPedido.Size = new System.Drawing.Size(58, 17);
            this.optPedido.TabIndex = 14;
            this.optPedido.Text = "Pedido";
            this.optPedido.UseVisualStyleBackColor = true;
            // 
            // mskDtFinal
            // 
            this.mskDtFinal.Location = new System.Drawing.Point(75, 38);
            this.mskDtFinal.Mask = "00/00/0000";
            this.mskDtFinal.Name = "mskDtFinal";
            this.mskDtFinal.Size = new System.Drawing.Size(91, 20);
            this.mskDtFinal.TabIndex = 39;
            this.mskDtFinal.ValidatingType = typeof(System.DateTime);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(16, 43);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(55, 13);
            this.label7.TabIndex = 38;
            this.label7.Text = "Data Final";
            // 
            // mskDtInicial
            // 
            this.mskDtInicial.Location = new System.Drawing.Point(75, 14);
            this.mskDtInicial.Mask = "00/00/0000";
            this.mskDtInicial.Name = "mskDtInicial";
            this.mskDtInicial.Size = new System.Drawing.Size(91, 20);
            this.mskDtInicial.TabIndex = 37;
            this.mskDtInicial.ValidatingType = typeof(System.DateTime);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(11, 18);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(60, 13);
            this.label4.TabIndex = 36;
            this.label4.Text = "Data Inicial";
            // 
            // lblValorTotal
            // 
            this.lblValorTotal.AutoSize = true;
            this.lblValorTotal.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblValorTotal.Location = new System.Drawing.Point(505, 570);
            this.lblValorTotal.Name = "lblValorTotal";
            this.lblValorTotal.Size = new System.Drawing.Size(161, 24);
            this.lblValorTotal.TabIndex = 14;
            this.lblValorTotal.Text = "VALOR TOTAL: 0";
            this.lblValorTotal.Click += new System.EventHandler(this.lblValorTotal_Click);
            // 
            // lblFreteTotal
            // 
            this.lblFreteTotal.AutoSize = true;
            this.lblFreteTotal.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFreteTotal.Location = new System.Drawing.Point(506, 594);
            this.lblFreteTotal.Name = "lblFreteTotal";
            this.lblFreteTotal.Size = new System.Drawing.Size(160, 24);
            this.lblFreteTotal.TabIndex = 15;
            this.lblFreteTotal.Text = "FRETE TOTAL: 0";
            this.lblFreteTotal.Click += new System.EventHandler(this.lblFreteTotal_Click);
            // 
            // lblQuantidadeTotal
            // 
            this.lblQuantidadeTotal.AutoSize = true;
            this.lblQuantidadeTotal.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblQuantidadeTotal.Location = new System.Drawing.Point(20, 594);
            this.lblQuantidadeTotal.Name = "lblQuantidadeTotal";
            this.lblQuantidadeTotal.Size = new System.Drawing.Size(220, 24);
            this.lblQuantidadeTotal.TabIndex = 16;
            this.lblQuantidadeTotal.Text = "QUANTIDADE TOTAL: 0";
            this.lblQuantidadeTotal.Click += new System.EventHandler(this.lblQuantidadeTotal_Click);
            // 
            // cmdLidos
            // 
            this.cmdLidos.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmdLidos.Location = new System.Drawing.Point(489, 530);
            this.cmdLidos.Name = "cmdLidos";
            this.cmdLidos.Size = new System.Drawing.Size(151, 33);
            this.cmdLidos.TabIndex = 17;
            this.cmdLidos.Text = "LIDOS";
            this.cmdLidos.UseVisualStyleBackColor = true;
            this.cmdLidos.Visible = false;
            this.cmdLidos.Click += new System.EventHandler(this.cmdLidos_Click);
            // 
            // cmdEnviado
            // 
            this.cmdEnviado.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmdEnviado.Location = new System.Drawing.Point(646, 530);
            this.cmdEnviado.Name = "cmdEnviado";
            this.cmdEnviado.Size = new System.Drawing.Size(151, 33);
            this.cmdEnviado.TabIndex = 18;
            this.cmdEnviado.Text = "ENVIADOS";
            this.cmdEnviado.UseVisualStyleBackColor = true;
            this.cmdEnviado.Visible = false;
            this.cmdEnviado.Click += new System.EventHandler(this.cmdEnviado_Click);
            // 
            // cmdParado
            // 
            this.cmdParado.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmdParado.Location = new System.Drawing.Point(19, 530);
            this.cmdParado.Name = "cmdParado";
            this.cmdParado.Size = new System.Drawing.Size(151, 33);
            this.cmdParado.TabIndex = 19;
            this.cmdParado.Text = "PARADOS";
            this.cmdParado.UseVisualStyleBackColor = true;
            this.cmdParado.Visible = false;
            this.cmdParado.Click += new System.EventHandler(this.cmdParado_Click);
            // 
            // imgCodigoBarra
            // 
            this.imgCodigoBarra.ImageLocation = "c:\\temp\\test.bmp";
            this.imgCodigoBarra.Location = new System.Drawing.Point(676, 10);
            this.imgCodigoBarra.Name = "imgCodigoBarra";
            this.imgCodigoBarra.Size = new System.Drawing.Size(44, 41);
            this.imgCodigoBarra.TabIndex = 20;
            this.imgCodigoBarra.TabStop = false;
            this.imgCodigoBarra.Visible = false;
            // 
            // cmdMarcar
            // 
            this.cmdMarcar.Location = new System.Drawing.Point(1004, 109);
            this.cmdMarcar.Name = "cmdMarcar";
            this.cmdMarcar.Size = new System.Drawing.Size(138, 27);
            this.cmdMarcar.TabIndex = 21;
            this.cmdMarcar.Tag = "false";
            this.cmdMarcar.Text = "Marcar/Desmarcar Todos";
            this.cmdMarcar.UseVisualStyleBackColor = true;
            this.cmdMarcar.Click += new System.EventHandler(this.cmdMarcar_Click);
            // 
            // cmdSeparar
            // 
            this.cmdSeparar.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmdSeparar.Location = new System.Drawing.Point(175, 530);
            this.cmdSeparar.Name = "cmdSeparar";
            this.cmdSeparar.Size = new System.Drawing.Size(151, 33);
            this.cmdSeparar.TabIndex = 22;
            this.cmdSeparar.Text = "SEPARAR";
            this.cmdSeparar.UseVisualStyleBackColor = true;
            this.cmdSeparar.Visible = false;
            this.cmdSeparar.Click += new System.EventHandler(this.cmdSeparar_Click);
            // 
            // cmdSeparado
            // 
            this.cmdSeparado.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmdSeparado.Location = new System.Drawing.Point(332, 530);
            this.cmdSeparado.Name = "cmdSeparado";
            this.cmdSeparado.Size = new System.Drawing.Size(151, 33);
            this.cmdSeparado.TabIndex = 23;
            this.cmdSeparado.Text = "SEPARADOS";
            this.cmdSeparado.UseVisualStyleBackColor = true;
            this.cmdSeparado.Visible = false;
            this.cmdSeparado.Click += new System.EventHandler(this.cmdSeparado_Click);
            // 
            // cmdLiberar
            // 
            this.cmdLiberar.Location = new System.Drawing.Point(713, 155);
            this.cmdLiberar.Name = "cmdLiberar";
            this.cmdLiberar.Size = new System.Drawing.Size(138, 27);
            this.cmdLiberar.TabIndex = 24;
            this.cmdLiberar.Tag = "false";
            this.cmdLiberar.Text = "Liberar LudoStore";
            this.cmdLiberar.UseVisualStyleBackColor = true;
            this.cmdLiberar.Visible = false;
            this.cmdLiberar.Click += new System.EventHandler(this.cmdLiberar_Click);
            // 
            // lblCusto
            // 
            this.lblCusto.AutoSize = true;
            this.lblCusto.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCusto.Location = new System.Drawing.Point(843, 570);
            this.lblCusto.Name = "lblCusto";
            this.lblCusto.Size = new System.Drawing.Size(162, 24);
            this.lblCusto.TabIndex = 25;
            this.lblCusto.Text = "CUSTO TOTAL: 0";
            this.lblCusto.Click += new System.EventHandler(this.label5_Click);
            // 
            // lblAviso
            // 
            this.lblAviso.AutoSize = true;
            this.lblAviso.BackColor = System.Drawing.SystemColors.Control;
            this.lblAviso.Font = new System.Drawing.Font("Microsoft Sans Serif", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAviso.Location = new System.Drawing.Point(298, 284);
            this.lblAviso.Name = "lblAviso";
            this.lblAviso.Size = new System.Drawing.Size(516, 42);
            this.lblAviso.TabIndex = 26;
            this.lblAviso.Text = "AGUARDE... PESQUISANDO";
            this.lblAviso.Click += new System.EventHandler(this.label5_Click_1);
            // 
            // picQRCode
            // 
            this.picQRCode.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.picQRCode.Location = new System.Drawing.Point(465, 208);
            this.picQRCode.Name = "picQRCode";
            this.picQRCode.Size = new System.Drawing.Size(225, 213);
            this.picQRCode.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picQRCode.TabIndex = 27;
            this.picQRCode.TabStop = false;
            this.picQRCode.Visible = false;
            // 
            // cmdEmail
            // 
            this.cmdEmail.Location = new System.Drawing.Point(912, 109);
            this.cmdEmail.Name = "cmdEmail";
            this.cmdEmail.Size = new System.Drawing.Size(86, 28);
            this.cmdEmail.TabIndex = 28;
            this.cmdEmail.Text = "Enviar Email";
            this.cmdEmail.UseVisualStyleBackColor = true;
            this.cmdEmail.Visible = false;
            this.cmdEmail.Click += new System.EventHandler(this.cmdEmail_Click);
            // 
            // frmRelatorio
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.ClientSize = new System.Drawing.Size(1175, 628);
            this.ControlBox = false;
            this.Controls.Add(this.cmdEmail);
            this.Controls.Add(this.picQRCode);
            this.Controls.Add(this.lblCusto);
            this.Controls.Add(this.cmdLiberar);
            this.Controls.Add(this.cmdSeparado);
            this.Controls.Add(this.cmdSeparar);
            this.Controls.Add(this.cmdMarcar);
            this.Controls.Add(this.imgCodigoBarra);
            this.Controls.Add(this.cmdParado);
            this.Controls.Add(this.cmdEnviado);
            this.Controls.Add(this.cmdLidos);
            this.Controls.Add(this.lblQuantidadeTotal);
            this.Controls.Add(this.lblFreteTotal);
            this.Controls.Add(this.lblValorTotal);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.lblRegistros);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.lstEstoque);
            this.Controls.Add(this.lblAviso);
            this.Controls.Add(this.frameVendas);
            this.Controls.Add(this.frameEstoque);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmRelatorio";
            this.Text = "Relatórios";
            this.Load += new System.EventHandler(this.frmRelatorio_Load);
            this.frameEstoque.ResumeLayout(false);
            this.frameEstoque.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.frameVendas.ResumeLayout(false);
            this.frameVendas.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.imgCodigoBarraCEP)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.imgCodigoBarra)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picQRCode)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListView lstEstoque;
        private System.Windows.Forms.GroupBox frameEstoque;
        private System.Windows.Forms.CheckBox chkAtivos;
        private System.Windows.Forms.TextBox txtFabricante;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.CheckBox chkQtdeMinima;
        private System.Windows.Forms.TextBox txtDescricao;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button btnLimpar;
        private System.Windows.Forms.Button btPesquisar;
        private System.Windows.Forms.Button btnSair;
        private System.Windows.Forms.Label lblRegistros;
        private System.Windows.Forms.ComboBox cbCategorias;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox txtFornecedor;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.RadioButton optVenda;
        private System.Windows.Forms.RadioButton optEstoque;
        private System.Windows.Forms.CheckBox chkCategoria;
        private System.Windows.Forms.GroupBox frameVendas;
        private System.Windows.Forms.CheckBox chkXML;
        private System.Windows.Forms.MaskedTextBox mskDtFinal;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.MaskedTextBox mskDtInicial;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.RadioButton optStatus;
        private System.Windows.Forms.RadioButton optPedido;
        private System.Windows.Forms.ComboBox cbCanal;
        private System.Windows.Forms.CheckBox chkCanal;
        private System.Windows.Forms.ComboBox cbStatus;
        private System.Windows.Forms.CheckBox chkStatus;
        private System.Windows.Forms.Label lblBusca;
        private System.Windows.Forms.TextBox txtProduto;
        private System.Windows.Forms.Label lblValorTotal;
        private System.Windows.Forms.Label lblFreteTotal;
        private System.Windows.Forms.CheckBox chkCancelados;
        private System.Windows.Forms.CheckBox chkPesquisarPedido;
        private System.Windows.Forms.RadioButton optContinental;
        private System.Windows.Forms.RadioButton optDeclaração;
        private System.Windows.Forms.Label lblQuantidadeTotal;
        private System.Windows.Forms.CheckBox chkSKUPluggto;
        private System.Windows.Forms.Button cmdLidos;
        private System.Windows.Forms.Button cmdEnviado;
        private System.Windows.Forms.Button cmdParado;
        private System.Windows.Forms.PictureBox imgCodigoBarra;
        private System.Windows.Forms.PictureBox imgCodigoBarraCEP;
        private System.Windows.Forms.Button btnExportar;
        private System.Windows.Forms.Button cmdMarcar;
        private System.Windows.Forms.Button cmdSeparar;
        private System.Windows.Forms.Button cmdSeparado;
        private System.Windows.Forms.Button cmdLiberar;
        private System.Windows.Forms.ComboBox cbProcesso;
        private System.Windows.Forms.CheckBox chkProcesso;
        private System.Windows.Forms.Label lblCusto;
        private System.Windows.Forms.Label lblAviso;
        private System.Windows.Forms.PictureBox picQRCode;
        private System.Windows.Forms.Button cmdEmail;
        private System.Windows.Forms.ComboBox dcLoja;
        private System.Windows.Forms.CheckBox chkLoja;
        private System.Windows.Forms.TextBox txtEAN;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.CheckBox chkCategoriaVenda;
        private System.Windows.Forms.ComboBox cbCategoriasVenda;
        private System.Windows.Forms.CheckBox chkFabricante;
        private System.Windows.Forms.ComboBox cbFabricante;
    }
}

