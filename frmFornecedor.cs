﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;



namespace FacilShoppingReports
{
    public partial class frmFornecedor : Form
    {
        private global obj;
        MySqlDataReader dataLista;
        Boolean blAlteracao;

        public frmFornecedor()
        {
            InitializeComponent();
            obj = new global();
            
        }
        

        private void btnSair_Click_1(object sender, EventArgs e)
        {
            if (btnSair.Text == "Sair")
            {
                this.Close();

            }
            else
            {
                if(blAlteracao==false)
                {
                    Limpar();
                }
                

                Muda_Status();
                

            }

            
        }

        private void label14_Click(object sender, EventArgs e)
        {

        }

        private void Muda_Status()
        {
            if(btnSair.Text == "Sair")
            {
                btnSair.Text = "Cancelar";
            }
            else
            {
                btnSair.Text = "Sair";
                blAlteracao = false;
            }

            btnGrava.Enabled = !btnGrava.Enabled;
            btPesquisar.Enabled = !btPesquisar.Enabled;
            txtPesquisa.Enabled = !txtPesquisa.Enabled;
            btnNovo.Enabled = !btnNovo.Enabled;
            btnDireita.Enabled = !btnDireita.Enabled;
            btnEsquerda.Enabled = !btnEsquerda.Enabled;
            btnSair2.Enabled = !btnSair2.Enabled;
            btnAlterar.Enabled = !btnAlterar.Enabled;


            groupBox1.Enabled = !groupBox1.Enabled;
            groupBox3.Enabled = !groupBox3.Enabled;
            groupBox4.Enabled = !groupBox4.Enabled;
            groupBox5.Enabled = !groupBox5.Enabled;
            groupBox6.Enabled = !groupBox6.Enabled;

            lstLista.Enabled = !lstLista.Enabled;

        }

        private void Limpar()
        {

            txtPesquisa.Text = "";
            txtRazao.Text = "";
            txtFantasia.Text = "";
            txtID.Text = "";
            txtNumero.Text = "";
            txtEndereco.Text = "";
            txtCidade.Text = "";
            txtBairro.Text = "";
            txtComplemento.Text = "";
            txtContato1.Text = "";
            txtContato2.Text = "";
            txtObservacao.Text = "";
            mskTelefone.Text = "";
            mskTelefone2.Text = "";
            mskTelefone1.Text = "";
            mskCNPJ.Text = "";
            mskData.Text = "";
            mskCEP.Text = "";
            cbUF.Text = "";
            chkFabricante.Checked = false;
            chkFornecedor.Checked = false;


        }

        public bool Gravar()
        {
            string query;

            if (blAlteracao == false)
            {
                query = "INSERT INTO agentes (id, data, fantasia, razao, endereco, numero, complemento, bairro, cidade, estado, cep, " +
                        "fornecedor, fabricante, cnpj, telefone, contato1, telefone1, contato2, telefone2, observacao) " +
                        " VALUES ( " +
                        txtID.Text + ", '" + System.DateTime.Parse(mskData.Text).ToString("yyyy-MM-dd") + "', '" + txtFantasia.Text + "', " +
                        "'" + txtRazao.Text + "', '" + txtEndereco.Text + "', '" + txtNumero.Text + "', '" + txtComplemento.Text + "', " +
                        "'" + txtBairro.Text + "', '" + txtCidade.Text + "', '" + cbUF.Text + "', '" + mskCEP.Text + "', " +
                        chkFornecedor.Checked + ", " + chkFabricante.Checked + " , '" + mskCNPJ.Text + "', " +
                        "'" + mskTelefone.Text + "', '" + txtContato1.Text + "', '" + mskTelefone1.Text + "', " +
                        "'" + txtContato2.Text + "', '" + mskTelefone2.Text + "', '" + txtObservacao.Text + "'); ";
            }
            else
            {

                query = "UPDATE agentes " +
                        " SET fantasia = '" + txtFantasia.Text + "', " +
                        " razao = '" + txtRazao.Text + "', " +
                        " endereco = '" + txtEndereco.Text + "', " +
                        " numero = '" + txtNumero.Text + "', " +
                        " complemento = '" + txtComplemento.Text + "', " +
                        " bairro = '" + txtBairro.Text + "', " +
                        " cidade = '" + txtCidade.Text + "', " +
                        " estado = '" + mskCNPJ.Text + "', " +
                        " cep = '" + mskCNPJ.Text + "', " +
                        " fornecedor = " + chkFornecedor.Checked + ", " +
                        " fabricante = " + chkFabricante.Checked + ", " +
                        " cnpj = '" + mskCNPJ.Text + "', " +
                        " telefone = '" + mskTelefone.Text + "', " +
                        " contato1 = '" + txtContato1.Text + "', " +
                        " telefone1 = '" + mskTelefone1.Text + "', " +
                        " contato2 = '" + txtContato2.Text + "', " +
                        " telefone2 = '" + mskTelefone2.Text + "', " +
                        " observacao = '" + txtObservacao.Text + "' " +
                        " WHERE ID = " + txtID.Text + "; ";
            }

            if (global.myDB.connection.State == ConnectionState.Closed)
            {
                global.myDB.OpenConnection();
            }

            
           MySqlCommand cmd1 = new MySqlCommand(query, global.myDB.connection);

            

            try
            {
                cmd1.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return false;
            }



            return true;
        }


        

        private void btnGrava_Click(object sender, EventArgs e)
        {
            if(Valida()==true)
            {
                    if (Gravar() == true)
                    {
                        if (blAlteracao == false)
                        {
                            Limpar();
                        }

                        Muda_Status();
                    }
       
            }
        }

        private bool Valida()
        {

            if(global.validarCNPJ(mskCNPJ.Text)==false)
            {
                MessageBox.Show("CNPJ inválido, favor inserir informações válidas!!","Validação!!");
                return false;
            }

            return true;
        }

        private void btnNovo_Click(object sender, EventArgs e)
        {
            Limpar();
            Muda_Status();

            //txtID.Text = obj.BuscaID("agentes");
            mskData.Text = DateTime.Now.ToShortDateString();
        }

        private void btPesquisar_Click(object sender, EventArgs e)
        {
            lstLista.Columns.Clear();
            lstLista.Items.Clear();

            Pesquisa();
        }
        

        private void Pesquisa()
        {
            MySqlCommand cmd;
            

            string query = " SELECT * FROM agentes " ;

            if (txtPesquisa.Text.Length != 0)
            {
                query = query + " WHERE(FANTASIA LIKE '%" + txtPesquisa.Text + "' OR RAZAO LIKE '%" + txtPesquisa.Text + "%'); ";
            }


            if (global.myDB.connection.State == ConnectionState.Closed)
            {
                global.myDB.OpenConnection();
            }

            cmd = new MySqlCommand(query, global.myDB.connection);


            try
            {
                dataLista = cmd.ExecuteReader();
            }
            catch (Exception)
            {
                global.myDB.OpenConnection();
                cmd = new MySqlCommand(query, global.myDB.connection);
                dataLista.Close();

                dataLista = cmd.ExecuteReader();
            }


            lstLista.Columns.Add("ID", 60);
            lstLista.Columns.Add("DATA", 60);
            lstLista.Columns.Add("FANTASIA", 60);
            lstLista.Columns.Add("RAZAO", 60);
            lstLista.Columns.Add("ENDERECO", 60);
            lstLista.Columns.Add("NUMERO", 60);
            lstLista.Columns.Add("COMPLEMENTO", 60);
            lstLista.Columns.Add("BAIRRO", 60);
            lstLista.Columns.Add("CIDADE", 60);
            lstLista.Columns.Add("ESTADO", 60);
            lstLista.Columns.Add("CEP", 60);
            lstLista.Columns.Add("FORNECEDOR", 60);
            lstLista.Columns.Add("FABRICANTE", 60);
            lstLista.Columns.Add("CNPJ", 60);
            lstLista.Columns.Add("TELEFONE", 60);
            lstLista.Columns.Add("CONTATO1", 60);
            lstLista.Columns.Add("TELEFONE1", 60);
            lstLista.Columns.Add("CONTATO2", 60);
            lstLista.Columns.Add("TELEFONE2", 60);
            lstLista.Columns.Add("OBSERVACAO", 60);


            while (dataLista.Read())
            {
               
                ListViewItem item = new ListViewItem(new[] { dataLista["id"].ToString(), dataLista["data"].ToString(), dataLista["fantasia"].ToString(), dataLista["razao"].ToString(), dataLista["endereco"].ToString(), dataLista["numero"].ToString(), dataLista["complemento"].ToString(), dataLista["bairro"].ToString(), dataLista["cidade"].ToString(), dataLista["estado"].ToString(), dataLista["cep"].ToString(), dataLista["fornecedor"].ToString(), dataLista["fabricante"].ToString(), dataLista["cnpj"].ToString(), dataLista["telefone"].ToString(), dataLista["contato1"].ToString(), dataLista["telefone1"].ToString(), dataLista["contato2"].ToString(), dataLista["telefone2"].ToString(), dataLista["observacao"].ToString() });

                lstLista.Items.Add(item);

            }

            dataLista.Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void lstLista_SelectedIndexChanged(object sender, EventArgs e)
        {
            //lstLista.Rows[e.RowIndex].Cells[0].Value.ToString();
        }

        private void lstLista_DoubleClick(object sender, EventArgs e)
        {
            Exibe_Registros();
            tabPrincipal.SelectedTab = tabPage2; 
        }

        private void lstLista_MouseDoubleClick(object sender, MouseEventArgs e)
        {
           // MessageBox.Show(lstLista.SelectedItems[0].SubItems[0].Text);
        }

        private void Exibe_Registros()
        {

            txtID.Text = lstLista.SelectedItems[0].SubItems[0].Text;
            mskData.Text = lstLista.SelectedItems[0].SubItems[1].Text;
            txtFantasia.Text = lstLista.SelectedItems[0].SubItems[2].Text;
            txtRazao.Text = lstLista.SelectedItems[0].SubItems[3].Text;
            txtEndereco.Text = lstLista.SelectedItems[0].SubItems[4].Text;
            txtNumero.Text = lstLista.SelectedItems[0].SubItems[5].Text;
            txtComplemento.Text = lstLista.SelectedItems[0].SubItems[6].Text;
            txtBairro.Text = lstLista.SelectedItems[0].SubItems[7].Text;
            txtCidade.Text = lstLista.SelectedItems[0].SubItems[8].Text;
            cbUF.Text = lstLista.SelectedItems[0].SubItems[9].Text;
            mskCEP.Text = lstLista.SelectedItems[0].SubItems[10].Text;

            if (lstLista.SelectedItems[0].SubItems[11].Text == "1")
            {
                chkFornecedor.Checked = true;
            }
            else
            { 
                chkFornecedor.Checked = false;
            }

            if (lstLista.SelectedItems[0].SubItems[12].Text == "1")
            {
                chkFabricante.Checked = true;
            }
            else
            {
                chkFabricante.Checked = false;
            }

           
            mskCNPJ.Text = lstLista.SelectedItems[0].SubItems[13].Text;
            mskTelefone.Text = lstLista.SelectedItems[0].SubItems[14].Text;
            txtContato1.Text = lstLista.SelectedItems[0].SubItems[15].Text;
            mskTelefone1.Text = lstLista.SelectedItems[0].SubItems[16].Text;
            txtContato2.Text = lstLista.SelectedItems[0].SubItems[17].Text;
            mskTelefone2.Text = lstLista.SelectedItems[0].SubItems[18].Text;
            txtObservacao.Text = lstLista.SelectedItems[0].SubItems[19].Text;

        }

        private void txtFantasia_TextChanged(object sender, EventArgs e)
        {
            txtFantasia.CharacterCasing = CharacterCasing.Upper;

        }

        private void txtRazao_TextChanged(object sender, EventArgs e)
        {
            txtRazao.CharacterCasing = CharacterCasing.Upper;
        }

        private void txtEndereco_TextChanged(object sender, EventArgs e)
        {
            txtEndereco.CharacterCasing = CharacterCasing.Upper;
        }

        private void txtComplemento_TextChanged(object sender, EventArgs e)
        {
            txtComplemento.CharacterCasing = CharacterCasing.Upper;
        }

        private void txtBairro_TextChanged(object sender, EventArgs e)
        {
            txtBairro.CharacterCasing = CharacterCasing.Upper;
        }

        private void txtCidade_TextChanged(object sender, EventArgs e)
        {
            txtCidade.CharacterCasing = CharacterCasing.Upper;
        }

        private void txtObservacao_TextChanged(object sender, EventArgs e)
        {
            txtObservacao.CharacterCasing = CharacterCasing.Upper;
        }

        private void txtContato1_TextChanged(object sender, EventArgs e)
        {
            txtContato1.CharacterCasing = CharacterCasing.Upper;
        }

        private void txtContato2_TextChanged(object sender, EventArgs e)
        {
            txtContato2.CharacterCasing = CharacterCasing.Upper;
        }

        private void btnDireita_Click(object sender, EventArgs e)
        {
            //lstLista.EnsureVisible(3);

            //lstLista.SelectedItems[0].Index
             
            //Exibe_Registros();
        }

        private void btnAlterar_Click(object sender, EventArgs e)
        {
            if(txtID.Text.Length != 0)
            {
                Muda_Status();
                blAlteracao = true;
            }
        }

        private void lstLista_SelectedIndexChanged_1(object sender, EventArgs e)
        {

        }
    }
}
