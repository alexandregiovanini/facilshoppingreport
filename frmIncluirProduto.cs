﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;


namespace FacilShoppingReports
{
    public partial class frmIncluirProduto : Form
    {
        MySqlDataReader dataReader;
        public ListViewItem itemList;
        public int countLista;

        public frmIncluirProduto()
        {
            InitializeComponent();
            CarregaProdutos();
        }

        private void btnSair_Click(object sender, EventArgs e)
        {
            this.Close();
        }


        private void CarregaProdutos()
        {
            MySqlCommand cmd;
            DataTable dataTable = new DataTable();

            string query = "select entity_id, value from catalog_product_entity_varchar where attribute_id = 71" +
                           " order by value; ";


            if (global.myDB.connection.State == ConnectionState.Closed)
            {
                global.myDB.OpenConnection();
            }

            //cmd = new MySqlCommand(query, connection);

            cmd = new MySqlCommand(query, global.myDB.connection);

            //dataReader.Close();

            dataReader = cmd.ExecuteReader();

            dataTable.Load(dataReader);

            // cbCategorias.Items.Add(dataReader["value"].ToString());
            cbProduto.DisplayMember = "value";
            cbProduto.ValueMember = "entity_id";
            cbProduto.DataSource = dataTable;

            dataReader.Close();

        }

        private void txtUnidade_KeyPress(object sender, KeyPressEventArgs e)
        {

        }

        private void txtQuantidade_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && (e.KeyChar != ','))
            {
                e.Handled = true;
            }
            if ((e.KeyChar == ',') && ((sender as TextBox).Text.IndexOf(',') > -1))
            {
                e.Handled = true;
            }
        }

        private void txtValorUnitario_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && (e.KeyChar != ','))
            {
                e.Handled = true;
            }
            if ((e.KeyChar == ',') && ((sender as TextBox).Text.IndexOf(',') > -1))
            {
                e.Handled = true;
            }
        }

        private void txtValorDesconto_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtValorDesconto_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && (e.KeyChar != ','))
            {
                e.Handled = true;
            }
            if ((e.KeyChar == ',') && ((sender as TextBox).Text.IndexOf(',') > -1))
            {
                e.Handled = true;
            }
        }

        private void txtValorLiquido_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && (e.KeyChar != ','))
            {
                e.Handled = true;
            }
            if ((e.KeyChar == ',') && ((sender as TextBox).Text.IndexOf(',') > -1))
            {
                e.Handled = true;
            }
        }

        private void txtBaseICMS_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && (e.KeyChar != ','))
            {
                e.Handled = true;
            }
            if ((e.KeyChar == ',') && ((sender as TextBox).Text.IndexOf(',') > -1))
            {
                e.Handled = true;
            }
        }

        private void txtValorICMS_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && (e.KeyChar != ','))
            {
                e.Handled = true;
            }
            if ((e.KeyChar == ',') && ((sender as TextBox).Text.IndexOf(',') > -1))
            {
                e.Handled = true;
            }
        }

        private void txtValorIPI_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && (e.KeyChar != ','))
            {
                e.Handled = true;
            }
            if ((e.KeyChar == ',') && ((sender as TextBox).Text.IndexOf(',') > -1))
            {
                e.Handled = true;
            }
        }

        private void txtAliquotaICMS_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && (e.KeyChar != ','))
            {
                e.Handled = true;
            }
            if ((e.KeyChar == ',') && ((sender as TextBox).Text.IndexOf(',') > -1))
            {
                e.Handled = true;
            }
        }

        private void txtAliquotaIPI_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && (e.KeyChar != ','))
            {
                e.Handled = true;
            }
            if ((e.KeyChar == ',') && ((sender as TextBox).Text.IndexOf(',') > -1))
            {
                e.Handled = true;
            }
        }

        private void btnAdicionar_Click(object sender, EventArgs e)
        {            
            itemList = new ListViewItem(new[] { countLista.ToString(), txtCodigo.Text, cbProduto.SelectedValue.ToString(), cbProduto.Text, txtNCM.Text, txtSosn.Text, txtCFOP.Text, txtUnidade.Text, txtQuantidade.Text, txtValorUnitario.Text, txtValorDesconto.Text, txtValorLiquido.Text, txtValorICMS.Text, txtValorIPI.Text, txtAliquotaICMS.Text, txtAliquotaIPI.Text });
            
            this.Close();
        }

        private void txtUnidade_TextChanged(object sender, EventArgs e)
        {

        }

        private void label6_Click(object sender, EventArgs e)
        {

        }

        private void txtValorICMS_TextChanged(object sender, EventArgs e)
        {

        }

        private void label5_Click(object sender, EventArgs e)
        {

        }

        private void txtValorIPI_TextChanged(object sender, EventArgs e)
        {

        }

        private void label4_Click(object sender, EventArgs e)
        {

        }

        private void txtAliquotaICMS_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtAliquotaIPI_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
