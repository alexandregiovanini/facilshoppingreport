﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FacilShoppingReports
{
    public partial class frmMensagemGeral : Form
    {
        public string strMensagem;

        public frmMensagemGeral()
        {
            InitializeComponent();
        }

        private void frmMensagemGeral_Load(object sender, EventArgs e)
        {

            timer1.Interval = timer1.Interval * 3;
            txtMensagem.Enabled = false;
            txtMensagem.Text = strMensagem;
            
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
