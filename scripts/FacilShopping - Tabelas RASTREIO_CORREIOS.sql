CREATE TABLE RASTREIO_CORREIOS (id INT NOT NULL PRIMARY KEY AUTO_INCREMENT, 
   							    prefixo CHAR(30) NULL,
                                numero char (15) NULL,
                                sufixo CHAR(30) NULL,
                                pedido CHAR(30) NULL,
                                sedex bit NULL,
                                pac bit NULL,
                                PLP CHAR(30) NULL,
                                sincronizado bit NULL);
drop table RASTREIO_CORREIOS;

SELECT * FROM RASTREIO_CORREIOS order by sedex, numero;

UPDATE RASTREIO_CORREIOS SET SINCRONIZADO = 1, PLP = '19762559' WHERE ID = 1;

SELECT numero + 1 numero, prefixo, sufixo  FROM RASTREIO_CORREIOS WHERE SEDEX = 0 ORDER BY NUMERO DESC  limit 0,2;
SELECT numero + 1 numero, prefixo, sufixo FROM RASTREIO_CORREIOS WHERE PAC = 1  ORDER BY NUMERO DESC  limit 0,1;

INSERT INTO RASTREIO_CORREIOS (prefixo, numero, sufixo, sedex, pac) VALUES ("OG", "62528255", "BR", 1, 0);
INSERT INTO RASTREIO_CORREIOS (prefixo, numero, sufixo, sedex, pac) VALUES ("PT", "09739225", "BR", 0, 1);  
																				

delete from RASTREIO_CORREIOS where id = 5;

SELECT CONCAT(PREFIXO, NUMERO, SUFIXO) RASTREIO   FROM RASTREIO_CORREIOS WHERE SINCRONIZADO IS NOT TRUE;

SELECT * FROM SEDEX_CIDADES WHERE '99940000' between CEPINICIAL AND CEPFINAL;

select * from ITENS_ENVIO_CORREIOS where rastreio like '%2176br%';