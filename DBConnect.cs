﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;
using System.Windows.Forms;
using System.Data;

namespace FacilShoppingReports
{
    public class DBConnect
    {
        public MySqlConnection connection;
        public MySqlDataReader dataDB;
        
        private string server;
        private string database;
        private string uid;
        private string password;

        //Constructor
        public DBConnect()
        {
            Initialize();
        }

        //Initialize values
        private void Initialize()
        {
            server = "179.188.50.108";
            database = "magento_fs";
            uid = "magento_fs";
            password = "facil32245";
            string connectionString;
            connectionString = "SERVER=" + server + ";" + "DATABASE=" +
            database + ";" + "UID=" + uid + ";" + "PASSWORD=" + password + ";";

            connection = new MySqlConnection(connectionString);
        }

        //open connection to database
        public bool OpenConnection()
        {
            try
            {
                if (global.myDB.connection.State == ConnectionState.Closed)
                {
                    connection.Open();
                }

                
                return true;
            }
            catch (MySqlException ex)
            {
                
                switch (ex.Number)
                {
                    case 0:
                        MessageBox.Show("Não foi possivel conectar no serfvidor.  Contacte o gerenciado da REDE");
                        break;

                    case 1045:
                        MessageBox.Show("Usuário ou senha inválida para acesso ao banco, tente novamente");
                        break;
                }
                return false;
            }
        }

        //Close connection
        public bool CloseConnection()
        {
            try
            {
                connection.Close();
                return true;
            }
            catch (MySqlException ex)
            {
               // MessageBox.Show(ex.Message);
                return false;
            }
        }

        //Insert statement
        public void Insert()
        {
        }

        //Update statement
        public void Update()
        {
        }

        //Delete statement
        public void Delete()
        {
        }

        //Select statement
        public void Pesquisa() 
        {
            
            MySqlCommand cmd;

           string query = " SELECT  * FROM catalog_product_entity_varchar";

            cmd = new MySqlCommand(query, global.myDB.connection);
            
            try
            {
                dataDB = cmd.ExecuteReader();
            }
            catch (Exception)
            {
                global.myDB.OpenConnection();
                cmd = new MySqlCommand(query, global.myDB.connection);
                dataDB = cmd.ExecuteReader();
            }
            
        }

        //Count statement
        //public int Count()
        //{
        //}

        //Backup
        public void Backup()
        {
        }

        //Restore
        public void Restore()
        {
        }
    }
}
