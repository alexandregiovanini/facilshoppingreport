﻿using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Text;
//using System.Threading.Tasks;
using MySql.Data.MySqlClient;
using System.Data;
using System.Windows.Forms;

using System.Text.RegularExpressions;

using System.Diagnostics;
using System.Drawing;


//using Office = Microsoft.Office;
//using Word = Microsoft.Office.Interop.Word;

using System.ServiceModel.Channels;

using System.Security.Cryptography;
using System.IO;
using System.Text;
using System.Net;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace FacilShoppingReports
{
    public class global
    {
        //public static int VarGlobal = 10;
        public static DBConnect myDB;

        public int globalID;
        
        public static string strIdServicoSedex = "";
        public static string strIdServicoPAC = "";

        public static string strIdContrato = "9992157880";
        public static string strIdCartaoPostagem = "0067599079";
        public static string strUser = "sigep";
        public static string strSenha = "n5f9t8";
        public static string strCodigoAdm = "17000190";
        public static string strCNPJ = "34028316000103";


        public static string strRemetente_Nome = "FACILSHOPPING LTDA";
        public static string strRemetente_Logradouro = "Rua Ipiranga";
        public static string strRemetente_Numero = "957";
        public static string strRemetente_Complemento = "SALA 1";
        public static string strRemetente_Bairro = "Jardim Santista";
        public static string strRemetente_CEP = "08730000";
        public static string strRemetente_Cidade = "MOGI DAS CRUZES";
        public static string strRemetente_UF = "SP";
        public static string strRemetente_Telefone = "11-3883-0115";
        public static string strRemetente_Fax = "11-3883-0115";
        public static string strRemetente_Email = "diretoria@facilcard.com.br";


        static MySqlDataReader dataGlobal;


        public static string AtualizaStatus(string strStatus, string strPedido)
        {
            string query = "";

            //if(strStatus == "Expedição")
            //{
            //    strStatus = "Expedido";
            //}

            query = "UPDATE PEDIDOS_INTEGRADOS SET status = '" + strStatus + "'";
            query = query + " WHERE id_pedido = '" + strPedido + "'";

            global.myDB.CloseConnection();

            if (global.myDB.connection.State == ConnectionState.Closed)
            {
                global.myDB.OpenConnection();
            }

            MySqlCommand cmd1 = new MySqlCommand(query, global.myDB.connection);

            try
            {
                
                cmd1.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "PontoZero Informa");
            }
            return "OK";
        }


        public static void ConsultarPedidoIderis(string strPedido, Boolean blIdIderis, string strCertificado)
        {
            string strUF;
            string strSQL;
            string strSKU;

            var requisicaoWeb = WebRequest.CreateHttp("http://api.ideris.com.br/pedido/" + strPedido);

            if (blIdIderis == false)
            {
                requisicaoWeb = WebRequest.CreateHttp("http://api.ideris.com.br/pedido?codigo=" + strPedido);
            }
            

            requisicaoWeb.Method = "GET";
            requisicaoWeb.ContentType = "application/json";
            requisicaoWeb.Headers["Authorization"] = strCertificado;

            using (var resposta = requisicaoWeb.GetResponse())
            {
                var streamDados = resposta.GetResponseStream();
                StreamReader reader = new StreamReader(streamDados);
                object objResponse = reader.ReadToEnd();

                var post = JsonConvert.DeserializeObject<Pedido>(objResponse.ToString());

                streamDados.Close();
                resposta.Close();

                if (post.paging.count != "0")
                {
                    //***********************************************************************************************************************

                    strUF = post.result[0].enderecoEntregaEstado;

                    strSQL = "INSERT INTO PEDIDOS_INTEGRADOS (contaloja, loja, id_pedido, cod_pedido, dt_pedido, dt_integracao,nome, cpf, endereco, complemento,cep,bairro,numero,cidade,UF,pagamento,cd_rastreio,marketplace,vl_total, Status)";
                    strSQL = strSQL + " VALUES (";
                    strSQL = strSQL + "'" + post.result[0].idContaMarketplace + "',";
                    strSQL = strSQL + "'" + post.result[0].nomeContaMarketplace + "',";
                    strSQL = strSQL + "'" + post.result[0].id + "',";
                    strSQL = strSQL + "'" + post.result[0].codigo + "',"; 
                    strSQL = strSQL  + "'" + System.DateTime.Parse(post.result[0].data).ToString("YYYY/MM/DD  hh:mm:ss") + "',";
                    strSQL = strSQL + "NOW(),";

                    if (post.result[0].enderecoEntrega_NomeResponsavelRecebimento == null || post.result[0].enderecoEntrega_NomeResponsavelRecebimento == "")
                    {
                        strSQL = strSQL + "'" + post.result[0].compradorPrimeiroNome.Replace("'", "") + " " + post.result[0].compradorSobrenome.Replace("'", "") + "',";
                    }
                    else
                    {
                        strSQL = strSQL + "'" + post.result[0].enderecoEntrega_NomeResponsavelRecebimento.Replace("'", "") + "',";
                    }

                    strSQL = strSQL + "'" + post.result[0].compradorDocumento + "',";

                    if(post.result[0].enderecoEntregaRua == null)
                    {
                        strSQL = strSQL + "' ',";
                    }
                    else
                    {
                        strSQL = strSQL + "'" + post.result[0].enderecoEntregaRua.Replace("'", "") + "',";
                    }


                    if(post.result[0].enderecoEntregaComentario == null)
                    {
                        strSQL = strSQL + "' ',";
                    }
                    else
                    {
                        strSQL = strSQL + "'" + post.result[0].enderecoEntregaComentario.Replace("'", "") + "',";
                    }
                    
                    strSQL = strSQL + "'" + post.result[0].enderecoEntregaCep + "',";


                    if(post.result[0].enderecoEntregaBairro == null)
                    {
                        strSQL = strSQL + "' ',";
                    }
                    else
                    {
                        strSQL = strSQL + "'" + post.result[0].enderecoEntregaBairro.Replace("'", "") + "',";
                    }

                    if (post.result[0].enderecoEntregaNumero == null)
                    {
                        strSQL = strSQL + "' ',";
                    }
                    else
                    {
                        strSQL = strSQL + "'" + post.result[0].enderecoEntregaNumero.Replace("'", "") + "',";
                    }

                    if (post.result[0].enderecoEntregaCidade == null)
                    {
                        strSQL = strSQL + "' ',";
                    }
                    else
                    {
                        strSQL = strSQL + "'" + post.result[0].enderecoEntregaCidade.Replace("'", "") + "',";
                    }


                    strSQL = strSQL + "'" + CorrigeUF(strUF) + "',";


                    if (post.result[0].Pagamento.Count > 0)
                    {
                        if (post.result[0].Pagamento[0].codigoPagamento == null)
                        {
                            strSQL = strSQL + "' ',";
                        }
                        else
                        {
                            strSQL = strSQL + "'" + post.result[0].Pagamento[0].codigoPagamento + "',";
                        }
                    }
                    else
                    {
                        strSQL = strSQL + "' ',";
                    }



                    strSQL = strSQL + "'" + post.result[0].numeroRastreio + "',";
                    strSQL = strSQL + "'" + post.result[0].marketplace + "',";
                    strSQL = strSQL + post.result[0].valorTotal.Replace("'", "") + ",";
                    strSQL = strSQL + "'" + post.result[0].status + "'";
                    strSQL = strSQL + ")";

                    global.myDB.CloseConnection();

                    if (global.myDB.connection.State == ConnectionState.Closed)
                    {
                        global.myDB.OpenConnection();
                    }

                    MySqlCommand cmd1 = new MySqlCommand(strSQL, global.myDB.connection);

                    try
                    {
                        cmd1.ExecuteNonQuery();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message, "Informa");
                    }

                    if (global.myDB.connection.State != ConnectionState.Closed)
                    {
                        global.myDB.CloseConnection();
                    }

                    for (int i = 1; i <= post.result[0].Item.Count; i++)     
                    {

                        if (post.result[0].Item[i].skuProdutoItem == null)
                        {
                            strSKU = "IDERIS NULO";
                        }
                        else
                        {
                            strSKU = post.result[0].Item[i].skuProdutoItem;
                        }

                        String query = "update cataloginventory_stock_item set data_integracao = now(), qty = qty - " + post.result[0].Item[i].quantidadeItem + " where product_id = (select entity_id from catalog_product_entity where sku = '" + strSKU + "')";

                        dataGlobal.Close();

                        if (global.myDB.connection.State == ConnectionState.Closed)
                        {
                            global.myDB.OpenConnection();
                        }

                        MySqlCommand cmd = new MySqlCommand(query, global.myDB.connection);

                        try
                        {
                            cmd1.ExecuteNonQuery();
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show(ex.Message);
                        }


                        if (post.result[0].Item[i].tituloProdutoItem.Contains("kit") || post.result[0].Item[i].tituloProdutoItem.Contains("combo"))
                        {
                            MessageBox.Show("O produto " + post.result[0].Item[i].tituloProdutoItem + " é um kit ou combo, apenas nesse caso deverá ser tirado do estoque ") ;
                        }

                        strSQL = "INSERT INTO ITENS_PEDIDOS_INTEGRADOS (id_pedido, sku, id_produto, nm_produto, qtde, vl_unitario, marketplace, vl_total)";
                        strSQL = strSQL + "VALUES (";

                        strSQL = strSQL + "'" + post.result[0].id + "',";

                        strSQL = strSQL + "'" + strSKU + "',";

                        strSQL = strSQL + "'" + post.result[0].Item[i].codigoProdutoItem + "',";

                        if(post.result[0].Item[i].tituloProdutoItem == null)
                        {
                            strSQL = strSQL + "'',";
                        }
                        else
                        {
                            strSQL = strSQL + "'" + post.result[0].Item[i].tituloProdutoItem.Replace("'", "").Replace(",", "") + "',";
                        }

                        strSQL = strSQL + post.result[0].Item[i].quantidadeItem + ",";


                        strSQL = strSQL + post.result[0].Item[i].precoUnitarioItem + ",";
                        strSQL = strSQL + "'" + post.result[0].marketplace + "',";

                        strSQL = strSQL + Convert.ToInt32(post.result[0].Item[i].quantidadeItem) * Convert.ToDouble(post.result[0].Item[i].precoUnitarioItem.Replace(".", ","));
                        strSQL = strSQL + ")";


                        cmd1 = new MySqlCommand(strSQL, global.myDB.connection);

                        try
                        {
                            cmd1.ExecuteNonQuery();
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show(ex.Message, "Informa");
                        }

                        if (global.myDB.connection.State != ConnectionState.Closed)
                        {
                            global.myDB.CloseConnection();
                        }


                    }   //        Next


                    //***********************************************************************************************************************                    
                }
                else
                {
                    MessageBox.Show("Pedido não encontrado, verificar se exite no Ideris", "PontoZero Informa");
                }
            }
        }


        public static string ConsultarStatus(string strPedido, string strCertificado)
        {            
            var requisicaoWeb = WebRequest.CreateHttp("http://api.ideris.com.br/pedido/" + strPedido);
            requisicaoWeb.Method = "GET";
            requisicaoWeb.ContentType = "application/json";
            requisicaoWeb.Headers["Authorization"] = strCertificado;

            using (var resposta = requisicaoWeb.GetResponse())
            {
                var streamDados = resposta.GetResponseStream();
                StreamReader reader = new StreamReader(streamDados);
                object objResponse = reader.ReadToEnd();

                var post = JsonConvert.DeserializeObject<Pedido>(objResponse.ToString());

                streamDados.Close();
                resposta.Close();

                if (post.paging.count != "0")
                {
                    return post.result[0].status;
                }
                else
                {
                    return "0";
                }


            }                  
        }

        public class Pedido
        {
            public paging paging { get; set; }
            public result[] result { get; set; }

        }

        public class paging
        {
            public string limit { get; set; }
            public string offset { get; set; }
            public string count { get; set; }
            public string total { get; set; }

        }

        public class result
        {
            public string id { get; set; }
            public string data { get; set; }
            public string codigo { get; set; }
            public string valorTotalComFrete { get; set; }
            public string valorPago { get; set; }
            public string valorTotal { get; set; }
            public string tarifaEnvio { get; set; }
            public string tarifaVenda { get; set; }
            public string totalDebitado { get; set; }
            public string custoProduto { get; set; }
            public string valorSobrou { get; set; }
            public string totalLiquido { get; set; }
            public string valorDesconto { get; set; }
            public string freteComprador { get; set; }
            public string valorJuros { get; set; }
            public string moeda { get; set; }
            public string tipoEntrega { get; set; }
            public string Outro { get; set; }
            public string marketplace { get; set; }
            public string idAutenticacaoIderis { get; set; }
            public string idContaMarketplace { get; set; }
            public string nomeContaMarketplace { get; set; }
            public string documentoContaRemetente { get; set; }
            public string status { get; set; }
            public string codigoEntrega { get; set; }
            public string metodoRastreio { get; set; }
            public string numeroRastreio { get; set; }
            public string compradorPrimeiroNome { get; set; }
            public string compradorSobrenome { get; set; }
            public string compradorApelido { get; set; }
            public string compradorEmail { get; set; }
            public string compradorTipoDocumento { get; set; }
            public string compradorDocumento { get; set; }
            public string compradorCodigoAreaTelefone { get; set; }
            public string compradorTelefone { get; set; }
            public string enderecoEntregaCompleto { get; set; }
            public string enderecoEntregaRua { get; set; }
            public string enderecoEntregaNumero { get; set; }
            public string enderecoEntregaCep { get; set; }
            public string enderecoEntregaBairro { get; set; }
            public string enderecoEntregaCidade { get; set; }
            public string enderecoEntregaEstado { get; set; }
            public string enderecoEntregaPais { get; set; }
            public string enderecoEntregaComentario { get; set; }
            public string enderecoEntrega_NomeResponsavelRecebimento { get; set; }
            public string enderecoEntrega_TelefoneResponsavelRecebimento { get; set; }
            public string codigoCarrinhoCompras { get; set; }
            public string intermediadorNome { get; set; }
            public string intermediadorCnpj { get; set; }
            public string dataCriacao { get; set; }
            public string dataSeparacao { get; set; }
            public string dataExpedicao { get; set; }
            public string dataExpedido { get; set; }
            public string dataEntregue { get; set; }

            public List<Item> Item { get; set; }
            public List<Pagamento> Pagamento { get; set; }


        }

        public class Item
        {
            public string tituloProdutoItem { get; set; }
            public string skuProdutoItem { get; set; }
            public string codigoProduto { get; set; }
            public string codigoProdutoItem { get; set; }
            public string quantidadeItem { get; set; }
            public string precoUnitarioItem { get; set; }
            public string caminhoImagemItem { get; set; }
            public string variacaoProdutoItem { get; set; }
            public string codigoItem { get; set; }
        }

        public class Pagamento
        {
            public string codigoPagamento { get; set; }
            public string dataAprovacaoPagamento { get; set; }
            public string statusPagamento { get; set; }
            public string formaPagamento { get; set; }
            public string numeroParcelasPagamento { get; set; }
            public string taxaMercadoPago { get; set; }
            public string taxaMercadoLivre { get; set; }
            public string valorFrete { get; set; }
            public string strPaymentMethod { get; set; }
        }


            public static bool validarData(string data)
        {
            Regex r = new Regex(@"(\d{2}\/\d{2}\/\d{4} \d{2}:\d{2})");
            return r.Match(data).Success;
        }


        public static bool ReciboEnvio(string strEmpresa, string strLote, string strQuantidade, string strFacilshopping, string strAlpha, string strBela, string strMundial)
        {
            string strHtml;
            

            strHtml = "<!DOCTYPE html>";
            strHtml = strHtml + "<div style=\"text-align: left;\">";
            
            if(strEmpresa == "CORREIO")
            {
                strHtml = strHtml + "	<img src=\"file:correio.png\" border=\"0\" width=\"159\" height=\"66\" style=\"font-size: 12pt;\" />&nbsp; &nbsp; &nbsp;<font style=\"font-size: 14pt;\" size=\"4\">REQUISI&Ccedil;&Atilde;O DE SERVI&Ccedil;OS</font>";
            }
            else if (strEmpresa == "MERCADOENVIOS")
            {
                strHtml = strHtml + "	<img src=\"file:mercadoenvio.png\" border=\"0\" width=\"159\" height=\"66\" style=\"font-size: 12pt;\" />&nbsp; &nbsp; &nbsp;<font style=\"font-size: 14pt;\" size=\"4\">REQUISI&Ccedil;&Atilde;O DE SERVI&Ccedil;OS</font>";
            }
            else if (strEmpresa == "B2W")
            {
                strHtml = strHtml + "	<img src=\"file:B2W.png\" border=\"0\" width=\"159\" height=\"66\" style=\"font-size: 12pt;\" />&nbsp; &nbsp; &nbsp;<font style=\"font-size: 14pt;\" size=\"4\">REQUISI&Ccedil;&Atilde;O DE SERVI&Ccedil;OS</font>";
            }
            else if (strEmpresa == "MAGALU")
            {
                strHtml = strHtml + "	<img src=\"file:magalu.png\" border=\"0\" width=\"159\" height=\"66\" style=\"font-size: 12pt;\" />&nbsp; &nbsp; &nbsp;<font style=\"font-size: 14pt;\" size=\"4\">REQUISI&Ccedil;&Atilde;O DE SERVI&Ccedil;OS</font>";
            }            
            else if (strEmpresa == "MERCADOPLACE")
            {
                strHtml = strHtml + "	<img src=\"file:mercadoplace.png\" border=\"0\" width=\"159\" height=\"66\" style=\"font-size: 12pt;\" />&nbsp; &nbsp; &nbsp;<font style=\"font-size: 14pt;\" size=\"4\">REQUISI&Ccedil;&Atilde;O DE SERVI&Ccedil;OS</font>";
            }
            else if (strEmpresa == "TOTALEXPRESS")
            {
                strHtml = strHtml + "	<img src=\"file:totalexpress.png\" border=\"0\" width=\"159\" height=\"66\" style=\"font-size: 12pt;\" />&nbsp; &nbsp; &nbsp;<font style=\"font-size: 14pt;\" size=\"4\">REQUISI&Ccedil;&Atilde;O DE SERVI&Ccedil;OS</font>";
            }
            else if (strEmpresa == "SEQUOIA")
            {
                strHtml = strHtml + "	<img src=\"file:sequoia.png\" border=\"0\" width=\"159\" height=\"66\" style=\"font-size: 12pt;\" />&nbsp; &nbsp; &nbsp;<font style=\"font-size: 14pt;\" size=\"4\">REQUISI&Ccedil;&Atilde;O DE SERVI&Ccedil;OS</font>";
            }
            else if (strEmpresa == "AMAZON")
            {
                strHtml = strHtml + "	<img src=\"file:amazon3.png\" border=\"0\" width=\"159\" height=\"66\" style=\"font-size: 12pt;\" />&nbsp; &nbsp; &nbsp;<font style=\"font-size: 14pt;\" size=\"4\">REQUISI&Ccedil;&Atilde;O DE SERVI&Ccedil;OS</font>";
            }

            strHtml = strHtml + "</div>";
            strHtml = strHtml + "<div style=\"text-align: right;\">";
            
            strHtml = strHtml + "	<span style=\"font-size: 21.3333px;\">N&deg;:" + strLote + " &nbsp;</span>";
            
            strHtml = strHtml + "</div>";
            strHtml = strHtml + "<div style=\"text-align: right;\">";
            strHtml = strHtml + "</div>";
            strHtml = strHtml + "<div style=\"text-align: left;\">";
            strHtml = strHtml + "	<font size=\"3\" style=\"font-size: 12pt;\">Empresa: Facilcard&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;Depto: Atend. FacilShopping&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; Fone/Ramal: 3883-0117</font>";
            strHtml = strHtml + "</div>";
            strHtml = strHtml + "<div style=\"text-align: left;\">";
            strHtml = strHtml + "</div>";
            strHtml = strHtml + "<div style=\"text-align: left;\">";
            strHtml = strHtml + "	<table border=\"1\" align=\"default\" cellpadding=\"2\" cellspacing=\"0\" title=\"\" summary=\"\" style=\"width: 98.9076%;\">";
            strHtml = strHtml + "		<caption>";
            strHtml = strHtml + "			<br />";
            strHtml = strHtml + "		</caption>";
            strHtml = strHtml + "		<tbody>";
            strHtml = strHtml + "			<tr>";
            strHtml = strHtml + "				<td style=\"text-align: center; width: 22.8114%;\">SERVI&Ccedil;OS</td>";
            strHtml = strHtml + "				<td style=\"text-align: center; width: 10%;\">QTDE</td>";
            strHtml = strHtml + "				<td style=\"text-align: center; width: 56%;\">SERVI&Ccedil;OS</td>";
            strHtml = strHtml + "				<td style=\"text-align: center; width: 99%;\">QTDE</td>";
            strHtml = strHtml + "			</tr>";
            strHtml = strHtml + "			<tr>";
            strHtml = strHtml + "				<td style=\"width: 22.8114%;\">";
            strHtml = strHtml + "					<font size=\"1\" style=\"font-size: 8pt;\">CARTA REGISTRADA</font>";
            strHtml = strHtml + "				</td>";
            strHtml = strHtml + "				<td style=\"width: 10%;\">";
            strHtml = strHtml + "					<font size=\"1\" style=\"font-size: 8pt;\">";
            strHtml = strHtml + "						<br />";
            strHtml = strHtml + "				</font>";
            strHtml = strHtml + "				</td>";
            strHtml = strHtml + "				<td style=\"width: 56%;\">";
            strHtml = strHtml + "					<font size=\"1\" style=\"font-size: 8pt;\">CARTA REGISTRADA(COM OPCIONAIS)</font>";
            strHtml = strHtml + "				</td>";
            strHtml = strHtml + "				<td style=\"width: 99%;\">";
            strHtml = strHtml + "					<font size=\"1\" style=\"font-size: 8pt;\">";
            strHtml = strHtml + "						<br />";
            strHtml = strHtml + "				</font>";
            strHtml = strHtml + "				</td>";
            strHtml = strHtml + "			</tr>";
            strHtml = strHtml + "			<tr>";
            strHtml = strHtml + "				<td style=\"width: 22.8114%;\">";
            strHtml = strHtml + "					<font size=\"1\" style=\"font-size: 8pt;\">SEDEX INTERNACIONAL EMS</font>";
            strHtml = strHtml + "				</td>";
            strHtml = strHtml + "				<td style=\"width: 10%;\">";
            strHtml = strHtml + "					<font size=\"1\" style=\"font-size: 8pt;\">";
            strHtml = strHtml + "						<br />";
            strHtml = strHtml + "				</font>";
            strHtml = strHtml + "				</td>";
            strHtml = strHtml + "				<td style=\"width: 56%;\">";
            strHtml = strHtml + "					<font size=\"1\" style=\"font-size: 8pt;\">SEDEX PAGAMENTO NA ENTREGA C/VD</font>";
            strHtml = strHtml + "				</td>";
            strHtml = strHtml + "				<td style=\"width: 99%;\">";
            strHtml = strHtml + "					<font size=\"1\" style=\"font-size: 8pt;\">";
            strHtml = strHtml + "						<br />";
            strHtml = strHtml + "				</font>";
            strHtml = strHtml + "				</td>";
            strHtml = strHtml + "			</tr>";
            strHtml = strHtml + "			<tr>";
            strHtml = strHtml + "				<td style=\"width: 22.8114%;\">";
            strHtml = strHtml + "					<font size=\"1\" style=\"font-size: 8pt;\">E-SEDEX</font>";
            strHtml = strHtml + "				</td>";
            strHtml = strHtml + "				<td style=\"width: 10%;\">";
            strHtml = strHtml + "					<font size=\"1\" style=\"font-size: 8pt;\">";
            strHtml = strHtml + "						<br />";
            strHtml = strHtml + "				</font>";
            strHtml = strHtml + "				</td>";
            strHtml = strHtml + "				<td style=\"width: 56%;\">";
            strHtml = strHtml + "					<font size=\"1\" style=\"font-size: 8pt;\">E-SEDEX(COM OPCIONAIS)</font>";
            strHtml = strHtml + "				</td>";
            strHtml = strHtml + "				<td style=\"width: 99%;\">";
            strHtml = strHtml + "					<font size=\"1\" style=\"font-size: 8pt;\">";
            strHtml = strHtml + "						<br />";
            strHtml = strHtml + "				</font>";
            strHtml = strHtml + "				</td>";
            strHtml = strHtml + "			</tr>";
            strHtml = strHtml + "			<tr>";
            strHtml = strHtml + "				<td style=\"width: 22.8114%;\">";
            strHtml = strHtml + "					<font size=\"1\" style=\"font-size: 8pt;\">PAC</font>";
            strHtml = strHtml + "				</td>";
            strHtml = strHtml + "				<td style=\"width: 10%;\">";
            strHtml = strHtml + "					<font size=\"1\" style=\"font-size: 8pt;\">";
            strHtml = strHtml + "						<br />";
            strHtml = strHtml + "				</font>";
            strHtml = strHtml + "				</td>";
            strHtml = strHtml + "				<td style=\"width: 56%;\">";
            strHtml = strHtml + "					<font size=\"1\" style=\"font-size: 8pt;\">PAC (COM OPCIONAIS)</font>";
            strHtml = strHtml + "				</td>";
            strHtml = strHtml + "				<td style=\"width: 99%;\">";
            strHtml = strHtml + "					<font size=\"1\" style=\"font-size: 8pt;\">";
            strHtml = strHtml + "						<br />";
            strHtml = strHtml + "				</font>";
            strHtml = strHtml + "				</td>";
            strHtml = strHtml + "			</tr>";
            strHtml = strHtml + "			<tr>";
            strHtml = strHtml + "				<td style=\"width: 22.8114%;\">";

            if (strEmpresa == "B2W" || strEmpresa == "MAGALU" || strEmpresa == "MERCADOPLACE" || strEmpresa == "AMAZON")
            {
                strHtml = strHtml + "					<font size=\"1\" style=\"font-size: 8pt;\">FACILSHOPPING</font>";
                strHtml = strHtml + "				</td>";
                strHtml = strHtml + "				<td style=\"text-align: center; width: 10%; height: 16px;\">";
                strHtml = strHtml + "					<font size=\"2\" style=\"font-size: 10pt;\">";
                strHtml = strHtml + "						" + strFacilshopping + "<br />";
                strHtml = strHtml + "				</font>";
            }
            else
            {
                strHtml = strHtml + "					<font size=\"1\" style=\"font-size: 8pt;\">SEDEX</font>";
                strHtml = strHtml + "				</td>";
                strHtml = strHtml + "				<td style=\"width: 10%;\">";
                strHtml = strHtml + "					<font size=\"1\" style=\"font-size: 8pt;\">";
                strHtml = strHtml + "						<br />";
                strHtml = strHtml + "				</font>";
            }            

            strHtml = strHtml + "				</td>";
            strHtml = strHtml + "				<td style=\"width: 56%;\">";
            strHtml = strHtml + "					<font size=\"1\" style=\"font-size: 8pt;\">SEDEX(COM OPCIONAIS)</font>";
            strHtml = strHtml + "				</td>";
            strHtml = strHtml + "				<td style=\"width: 99%;\">";
            strHtml = strHtml + "					<font size=\"1\" style=\"font-size: 8pt;\">";
            strHtml = strHtml + "						<br />";
            strHtml = strHtml + "				</font>";
            strHtml = strHtml + "				</td>";
            strHtml = strHtml + "			</tr>";
            strHtml = strHtml + "			<tr>";
            strHtml = strHtml + "				<td style=\"width: 22.8114%;\">";

            if (strEmpresa == "B2W" || strEmpresa == "MAGALU" || strEmpresa == "MERCADOPLACE" || strEmpresa == "AMAZON")
            {
                strHtml = strHtml + "					<font size=\"1\" style=\"font-size: 8pt;\">ALPHA JOGOS</font>";
                strHtml = strHtml + "				</td>";
                strHtml = strHtml + "				<td style=\"text-align: center; width: 10%; height: 16px;\">";
                strHtml = strHtml + "					<font size=\"2\" style=\"font-size: 10pt;\">";
                strHtml = strHtml + "                        " + strAlpha ;
                strHtml = strHtml + "				</font>";
                strHtml = strHtml + "				</td>";
            }
            else
            {
                strHtml = strHtml + "					<font size=\"1\" style=\"font-size: 8pt;\">SEDEX 10</font>";

                strHtml = strHtml + "				</td>";
                strHtml = strHtml + "				<td style=\"width: 10%;\">";
                strHtml = strHtml + "					<font size=\"1\" style=\"font-size: 8pt;\">";
                strHtml = strHtml + "						<br />";
                strHtml = strHtml + "				</font>";
                strHtml = strHtml + "				</td>";
            }            

            strHtml = strHtml + "				<td style=\"width: 56%;\">";
            strHtml = strHtml + "					<font size=\"1\" style=\"font-size: 8pt;\">SEDEX 10 (COM OPCIONAIS)</font>";
            strHtml = strHtml + "				</td>";
            strHtml = strHtml + "				<td style=\"width: 99%;\">";
            strHtml = strHtml + "					<font size=\"1\" style=\"font-size: 8pt;\">";
            strHtml = strHtml + "						<br />";
            strHtml = strHtml + "				</font>";
            strHtml = strHtml + "				</td>";
            strHtml = strHtml + "			</tr>";
            strHtml = strHtml + "		</tbody>";
            strHtml = strHtml + "		<tfoot>";
            strHtml = strHtml + "		</tfoot>";
            strHtml = strHtml + "	</table>";
            strHtml = strHtml + "	<table border=\"1\" align=\"default\" cellpadding=\"2\" cellspacing=\"0\" title=\"\" summary=\"\" style=\"width: 98.9076%;\">";
            strHtml = strHtml + "		<tbody>";
            strHtml = strHtml + "			<tr style=\"height: 17px;\">";
            strHtml = strHtml + "				<td style=\"width: 23%; height: 17px;\">";

            if (strEmpresa == "B2W" || strEmpresa == "MAGALU" || strEmpresa == "MERCADOPLACE" || strEmpresa == "AMAZON")
            {
                strHtml = strHtml + "					<font size=\"1\" style=\"font-size: 8pt;\">BELLA</font>";

                strHtml = strHtml + "				</td>";
                strHtml = strHtml + "				<td style=\"text-align: center; width: 10%; height: 16px;\">";
                strHtml = strHtml + "					<font size=\"2\" style=\"font-size: 10pt;\">";
                strHtml = strHtml + "                        " + strBela;
                strHtml = strHtml + "				</font>";
                strHtml = strHtml + "				</td>";
            }
            else
            {
                strHtml = strHtml + "					<font size=\"1\" style=\"font-size: 8pt;\">CARTAS SIMPLES</font>";
                strHtml = strHtml + "				</td>";
                strHtml = strHtml + "				<td style=\"width: 10%; height: 17px;\">";
                strHtml = strHtml + "					<font size=\"2\" style=\"font-size: 10pt;\">";
                strHtml = strHtml + "						<br />";
                strHtml = strHtml + "				</font>";
                strHtml = strHtml + "				</td>";
            }           

            strHtml = strHtml + "				<td style=\"width: 193%; height: 17px;\">";
            strHtml = strHtml + "					<font size=\"2\" style=\"font-size: 10pt;\">";
            strHtml = strHtml + "						<br />";
            strHtml = strHtml + "				</font>";
            strHtml = strHtml + "				</td>";
            strHtml = strHtml + "			</tr>";
            strHtml = strHtml + "			<tr style=\"height: 17px;\">";
            strHtml = strHtml + "				<td style=\"width: 23%; height: 17px;\">";

            if (strEmpresa == "B2W" || strEmpresa == "MAGALU" || strEmpresa == "MERCADOPLACE" || strEmpresa == "AMAZON")
            {
                strHtml = strHtml + "					<font size=\"1\" style=\"font-size: 8pt;\">MUNDIAL</font>";
                strHtml = strHtml + "				</td>";
                strHtml = strHtml + "				<td style=\"text-align: center; width: 10%; height: 16px;\">";
                strHtml = strHtml + "					<font size=\"2\" style=\"font-size: 10pt;\">";
                strHtml = strHtml + "                        " + strMundial ;
                strHtml = strHtml + "				</font>";
                strHtml = strHtml + "				</td>";
            }
            else
            {
                strHtml = strHtml + "					<font size=\"1\" style=\"font-size: 8pt;\">IMPRESSO</font>";
                strHtml = strHtml + "				</td>";
                strHtml = strHtml + "				<td style=\"width: 10%; height: 17px;\">";
                strHtml = strHtml + "					<font size=\"2\" style=\"font-size: 10pt;\">";
                strHtml = strHtml + "						<br />";
                strHtml = strHtml + "				</font>";
                strHtml = strHtml + "				</td>";
            }
            

            strHtml = strHtml + "				<td style=\"text-align: center; width: 193%; height: 17px;\">";
            strHtml = strHtml + "					<font style=\"\">";
            strHtml = strHtml + "						<font size=\"1\" style=\"font-size: 7pt;\">Obs:. AR = Aviso de Recebimento e VD Valor Declarado(Seguro= 1,5% do valor da postagem)&nbsp;</font>";
            strHtml = strHtml + "					</font>";
            strHtml = strHtml + "				</td>";
            strHtml = strHtml + "			</tr>";
            strHtml = strHtml + "			<tr style=\"height: 16px;\">";
            strHtml = strHtml + "				<td style=\"width: 23%; height: 16px;\">";
            if (strEmpresa == "CORREIO")
            {
                strHtml = strHtml + "					<font size=\"1\" style=\"font-size: 8pt;\">CORREIOS</font>";
            }
            else if (strEmpresa == "MERCADOENVIOS")
            {
                strHtml = strHtml + "					<font size=\"1\" style=\"font-size: 8pt;\">MERCADO LIVRE</font>";
            }
            else if (strEmpresa == "B2W")
            {
                strHtml = strHtml + "					<font size=\"1\" style=\"font-size: 8pt;\">B2W</font>";
            }
            else if (strEmpresa == "MAGALU")
            {
                strHtml = strHtml + "					<font size=\"1\" style=\"font-size: 8pt;\">MAGALU</font>";
            }
            else if (strEmpresa == "MERCADOPLACE")
            {
                strHtml = strHtml + "					<font size=\"1\" style=\"font-size: 8pt;\">MERCADO PLACE</font>";
            }
            else if (strEmpresa == "TOTALEXPRESS")
            {
                strHtml = strHtml + "					<font size=\"1\" style=\"font-size: 8pt;\">TOTAL EXPRESS</font>";
            }
            else if (strEmpresa == "SEQUOIA")
            {
                strHtml = strHtml + "					<font size=\"1\" style=\"font-size: 8pt;\">SEQUOIA</font>";
            }
            else if (strEmpresa == "AMAZON")
            {
                strHtml = strHtml + "					<font size=\"1\" style=\"font-size: 8pt;\">AMAZON</font>";
            }

            strHtml = strHtml + "				</td>";
            strHtml = strHtml + "				<td style=\"text-align: center; width: 10%; height: 16px;\">";
            strHtml = strHtml + "					<font size=\"2\" style=\"font-size: 10pt;\">" + strQuantidade + "</font>";
            strHtml = strHtml + "				</td>";
            strHtml = strHtml + "				<td style=\"width: 193%; font-size: 10pt; height: 16px;\">";
            strHtml = strHtml + "					<br />";
            strHtml = strHtml + "				</td>";
            strHtml = strHtml + "			</tr>";
            strHtml = strHtml + "		</tbody>";
            strHtml = strHtml + "		<tfoot>";
            strHtml = strHtml + "		</tfoot>";
            strHtml = strHtml + "	</table>";
            strHtml = strHtml + "	<table border=\"1\" align=\"default\" cellpadding=\"2\" cellspacing=\"0\" title=\"\" summary=\"\" style=\"width: 98.6987%;\">";
            strHtml = strHtml + "		<tbody>";
            strHtml = strHtml + "			<tr>";
            strHtml = strHtml + "				<td style=\"text-align: center; width: 23%;\">";
            strHtml = strHtml + "					<font size=\"2\" style=\"font-size: 10pt;\">&nbsp; " + DateTime.Today.ToShortDateString() + "</font>";
            strHtml = strHtml + "				</td>";
            strHtml = strHtml + "				<td style=\"text-align: center; width: 10%;\">";
            strHtml = strHtml + "					<font size=\"2\" style=\"font-size: 10pt;\">" + DateTime.Now.ToShortTimeString() + "</font>";
            strHtml = strHtml + "				</td>";
            strHtml = strHtml + "				<td style=\"width: 152%;\">";
            strHtml = strHtml + "					<br />";
            strHtml = strHtml + "				</td>";
            strHtml = strHtml + "			</tr>";
            strHtml = strHtml + "			<tr>";
            strHtml = strHtml + "				<td style=\"text-align: center; width: 23%;\">";
            strHtml = strHtml + "					<i>";
            strHtml = strHtml + "						<font size=\"2\" style=\"font-size: 10pt;\">ALVARO DE MATTOS</font>";
            strHtml = strHtml + "					</i>";
            strHtml = strHtml + "				</td>";
            strHtml = strHtml + "				<td style=\"width: 10%;\">";
            strHtml = strHtml + "					<br />";
            strHtml = strHtml + "				</td>";

            if (strEmpresa == "CORREIO")
            {
                strHtml = strHtml + "				<td style=\"text-align: center; width: 152%;\">CORREIOS</td>";
            }
            else if (strEmpresa == "MERCADOENVIOS")
            {
                strHtml = strHtml + "				<td style=\"text-align: center; width: 152%;\">MERCADO LIVRE</td>";
            }
            else if (strEmpresa == "B2W")
            {
                strHtml = strHtml + "				<td style=\"text-align: center; width: 152%;\">B2W</td>";
            }
            else if (strEmpresa == "MAGALU")
            {
                strHtml = strHtml + "				<td style=\"text-align: center; width: 152%;\">MAGALU</td>";
            }
            else if (strEmpresa == "MERCADOPLACE")
            {
                strHtml = strHtml + "				<td style=\"text-align: center; width: 152%;\">MERCADO PLACE</td>";
            }
            else if (strEmpresa == "AMAZON")
            {
                strHtml = strHtml + "				<td style=\"text-align: center; width: 152%;\">AMAZON</td>";
            }
            strHtml = strHtml + "			</tr>";
            strHtml = strHtml + "			<tr>";
            strHtml = strHtml + "				<td style=\"text-align: center; width: 23%;\">";
            strHtml = strHtml + "					<font size=\"1\" style=\"font-size: 9pt;\">SOLICITANTE</font>";
            strHtml = strHtml + "				</td>";
            strHtml = strHtml + "				<td style=\"text-align: center; width: 10%;\">";
            strHtml = strHtml + "					<font size=\"1\" style=\"font-size: 8pt;\">COLETOR</font>";
            strHtml = strHtml + "				</td>";
            strHtml = strHtml + "				<td style=\"width: 152%;\">";
            strHtml = strHtml + "					<br />";
            strHtml = strHtml + "				</td>";
            strHtml = strHtml + "			</tr>";
            strHtml = strHtml + "		</tbody>";
            strHtml = strHtml + "		<tfoot>";
            strHtml = strHtml + "		</tfoot>";
            strHtml = strHtml + "	</table>";
            strHtml = strHtml + "	<br />";

            //********************************************************************
            if (strEmpresa == "CORREIO")
            {
                strHtml = strHtml + "	<div>";
                strHtml = strHtml + "		<img src=\"file:correio.png\" border=\"0\" width=\"159\" height=\"66\" style=\"font-size:12pt;\" />&nbsp; &nbsp; &nbsp;<font style=\"font-size:14pt;\">REQUISI&Ccedil;&Atilde;O DE SERVI&Ccedil;OS</font>";
                strHtml = strHtml + "	</div>";
                strHtml = strHtml + "	<div style=\"text-align:right;\">";

                strHtml = strHtml + "	<span style=\"font-size: 21.3333px;\">N&deg;:" + strLote + " &nbsp;</span>";
                

                strHtml = strHtml + "	</div>";
                strHtml = strHtml + "	<div style=\"text-align:right;\">";
                strHtml = strHtml + "	</div>";
                strHtml = strHtml + "	<div>";
                strHtml = strHtml + "	<font style=\"font-size:12pt;\">Empresa: Facilcard&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;Depto: Atend. FacilShopping&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; Fone/Ramal: 3883-0117</font>";
                strHtml = strHtml + "	</div>";
                //strHtml = strHtml + "	<div>";
                //strHtml = strHtml + "		<br />";
                //strHtml = strHtml + "	</div>";
                strHtml = strHtml + "	<div>";
                strHtml = strHtml + "	<table border=\"1\" align=\"default\" cellpadding=\"2\" cellspacing=\"0\" title=\"\" summary=\"\" style=\"width:98.9076%;\">";
                strHtml = strHtml + "		<caption>";
                strHtml = strHtml + "			<br />";
                strHtml = strHtml + "		</caption>";
                strHtml = strHtml + "		<tbody>";
                strHtml = strHtml + "			<tr>";
                strHtml = strHtml + "				<td style=\"text-align:center;width:22.8114%;\">SERVI&Ccedil;OS</td>";
                strHtml = strHtml + "				<td style=\"text-align:center;width:10%;\">QTDE</td>";
                strHtml = strHtml + "				<td style=\"text-align:center;width:56%;\">SERVI&Ccedil;OS</td>";
                strHtml = strHtml + "				<td style=\"text-align:center;width:99%;\">QTDE</td>";
                strHtml = strHtml + "			</tr>";
                strHtml = strHtml + "			<tr>";
                strHtml = strHtml + "				<td style=\"width:22.8114%;\">";
                strHtml = strHtml + "						<font style=\"font-size:8pt;\">CARTA REGISTRADA</font>";
                strHtml = strHtml + "					</td>";
                strHtml = strHtml + "				<td style=\"width:10%;\">";
                strHtml = strHtml + "					<font style=\"font-size:8pt;\">";
                strHtml = strHtml + "							<br />";
                strHtml = strHtml + "				</font>";
                strHtml = strHtml + "					</td>";
                strHtml = strHtml + "				<td style=\"width:56%;\">";
                strHtml = strHtml + "						<font style=\"font-size:8pt;\">CARTA REGISTRADA(COM OPCIONAIS)</font>";
                strHtml = strHtml + "					</td>";
                strHtml = strHtml + "				<td style=\"width:99%;\">";
                strHtml = strHtml + "					<font style=\"font-size:8pt;\">";
                strHtml = strHtml + "							<br />";
                strHtml = strHtml + "				</font>";
                strHtml = strHtml + "					</td>";
                strHtml = strHtml + "			</tr>";
                strHtml = strHtml + "			<tr>";
                strHtml = strHtml + "				<td style=\"width:22.8114%;\">";
                strHtml = strHtml + "						<font style=\"font-size:8pt;\">SEDEX INTERNACIONAL EMS</font>";
                strHtml = strHtml + "					</td>";
                strHtml = strHtml + "				<td style=\"width:10%;\">";
                strHtml = strHtml + "					<font style=\"font-size:8pt;\">";
                strHtml = strHtml + "							<br />";
                strHtml = strHtml + "				</font>";
                strHtml = strHtml + "					</td>";
                strHtml = strHtml + "				<td style=\"width:56%;\">";
                strHtml = strHtml + "						<font style=\"font-size:8pt;\">SEDEX PAGAMENTO NA ENTREGA C/VD</font>";
                strHtml = strHtml + "					</td>";
                strHtml = strHtml + "				<td style=\"width:99%;\">";
                strHtml = strHtml + "					<font style=\"font-size:8pt;\">";
                strHtml = strHtml + "							<br />";
                strHtml = strHtml + "				</font>";
                strHtml = strHtml + "					</td>";
                strHtml = strHtml + "			</tr>";
                strHtml = strHtml + "			<tr>";
                strHtml = strHtml + "				<td style=\"width:22.8114%;\">";
                strHtml = strHtml + "						<font style=\"font-size:8pt;\">E-SEDEX</font>";
                strHtml = strHtml + "					</td>";
                strHtml = strHtml + "				<td style=\"width:10%;\">";
                strHtml = strHtml + "					<font style=\"font-size:8pt;\">";
                strHtml = strHtml + "							<br />";
                strHtml = strHtml + "				</font>";
                strHtml = strHtml + "					</td>";
                strHtml = strHtml + "				<td style=\"width:56%;\">";
                strHtml = strHtml + "						<font style=\"font-size:8pt;\">E-SEDEX(COM OPCIONAIS)</font>";
                strHtml = strHtml + "					</td>";
                strHtml = strHtml + "				<td style=\"width:99%;\">";
                strHtml = strHtml + "					<font style=\"font-size:8pt;\">";
                strHtml = strHtml + "							<br />";
                strHtml = strHtml + "				</font>";
                strHtml = strHtml + "					</td>";
                strHtml = strHtml + "			</tr>";
                strHtml = strHtml + "			<tr>";
                strHtml = strHtml + "				<td style=\"width:22.8114%;\">";
                strHtml = strHtml + "						<font style=\"font-size:8pt;\">PAC</font>";
                strHtml = strHtml + "					</td>";
                strHtml = strHtml + "				<td style=\"width:10%;\">";
                strHtml = strHtml + "					<font style=\"font-size:8pt;\">";
                strHtml = strHtml + "							<br />";
                strHtml = strHtml + "				</font>";
                strHtml = strHtml + "					</td>";
                strHtml = strHtml + "				<td style=\"width:56%;\">";
                strHtml = strHtml + "						<font style=\"font-size:8pt;\">PAC (COM OPCIONAIS)</font>";
                strHtml = strHtml + "					</td>";
                strHtml = strHtml + "				<td style=\"width:99%;\">";
                strHtml = strHtml + "					<font style=\"font-size:8pt;\">";
                strHtml = strHtml + "							<br />";
                strHtml = strHtml + "				</font>";
                strHtml = strHtml + "					</td>";
                strHtml = strHtml + "			</tr>";
                strHtml = strHtml + "			<tr>";
                strHtml = strHtml + "				<td style=\"width:22.8114%;\">";
                strHtml = strHtml + "						<font style=\"font-size:8pt;\">SEDEX</font>";
                strHtml = strHtml + "					</td>";
                strHtml = strHtml + "				<td style=\"width:10%;\">";
                strHtml = strHtml + "					<font style=\"font-size:8pt;\">";
                strHtml = strHtml + "							<br />";
                strHtml = strHtml + "				</font>";
                strHtml = strHtml + "					</td>";
                strHtml = strHtml + "				<td style=\"width:56%;\">";
                strHtml = strHtml + "						<font style=\"font-size:8pt;\">SEDEX(COM OPCIONAIS)</font>";
                strHtml = strHtml + "					</td>";
                strHtml = strHtml + "				<td style=\"width:99%;\">";
                strHtml = strHtml + "					<font style=\"font-size:8pt;\">";
                strHtml = strHtml + "							<br />";
                strHtml = strHtml + "				</font>";
                strHtml = strHtml + "					</td>";
                strHtml = strHtml + "			</tr>";
                strHtml = strHtml + "			<tr>";
                strHtml = strHtml + "				<td style=\"width:22.8114%;\">";
                strHtml = strHtml + "						<font style=\"font-size:8pt;\">SEDEX 10</font>";
                strHtml = strHtml + "					</td>";
                strHtml = strHtml + "				<td style=\"width:10%;\">";
                strHtml = strHtml + "					<font style=\"font-size:8pt;\">";
                strHtml = strHtml + "							<br />";
                strHtml = strHtml + "				</font>";
                strHtml = strHtml + "					</td>";
                strHtml = strHtml + "				<td style=\"width:56%;\">";
                strHtml = strHtml + "						<font style=\"font-size:8pt;\">SEDEX 10 (COM OPCIONAIS)</font>";
                strHtml = strHtml + "					</td>";
                strHtml = strHtml + "				<td style=\"width:99%;\">";
                strHtml = strHtml + "					<font style=\"font-size:8pt;\">";
                strHtml = strHtml + "							<br />";
                strHtml = strHtml + "				</font>";
                strHtml = strHtml + "					</td>";
                strHtml = strHtml + "			</tr>";
                strHtml = strHtml + "		</tbody>";
                strHtml = strHtml + "		<tfoot>";
                strHtml = strHtml + "		</tfoot>";
                strHtml = strHtml + "	</table>";
                strHtml = strHtml + "	<table border=\"1\" align=\"default\" cellpadding=\"2\" cellspacing=\"0\" title=\"\" summary=\"\" style=\"width:98.9076%;\">";
                strHtml = strHtml + "		<tbody>";
                strHtml = strHtml + "			<tr style=\"height:17px;\">";
                strHtml = strHtml + "				<td style=\"width:23%;height:17px;\">";
                strHtml = strHtml + "						<font style=\"font-size:8pt;\">CARTAS SIMPLES</font>";
                strHtml = strHtml + "					</td>";
                strHtml = strHtml + "				<td style=\"width:10%;height:17px;\">";
                strHtml = strHtml + "					<font style=\"font-size:10pt;\">";
                strHtml = strHtml + "							<br />";
                strHtml = strHtml + "				</font>";
                strHtml = strHtml + "					</td>";
                strHtml = strHtml + "				<td style=\"width:193%;height:17px;\">";
                strHtml = strHtml + "					<font style=\"font-size:10pt;\">";
                strHtml = strHtml + "							<br />";
                strHtml = strHtml + "				</font>";
                strHtml = strHtml + "					</td>";
                strHtml = strHtml + "			</tr>";
                strHtml = strHtml + "			<tr style=\"height:17px;\">";
                strHtml = strHtml + "				<td style=\"width:23%;height:17px;\">";
                strHtml = strHtml + "						<font style=\"font-size:8pt;\">IMPRESSO</font>";
                strHtml = strHtml + "					</td>";
                strHtml = strHtml + "				<td style=\"width:10%;height:17px;\">";
                strHtml = strHtml + "					<font style=\"font-size:10pt;\">";
                strHtml = strHtml + "							<br />";
                strHtml = strHtml + "				</font>";
                strHtml = strHtml + "					</td>";
                strHtml = strHtml + "				<td style=\"text-align:center;width:193%;height:17px;\">";
                strHtml = strHtml + "					<font style=\"font-size:7pt;\">Obs:. AR = Aviso de Recebimento e VD Valor Declarado(Seguro= 1,5% do valor da postagem)&nbsp;</font>";
                strHtml = strHtml + "				</td>";
                strHtml = strHtml + "			</tr>";
                strHtml = strHtml + "			<tr style=\"height:16px;\">";
                strHtml = strHtml + "				<td style=\"width:23%;height:16px;\">";
                strHtml = strHtml + "						<font style=\"font-size:8pt;\">CORREIOS</font>";
                strHtml = strHtml + "					</td>";
                strHtml = strHtml + "				<td style=\"text-align:center;width:10%;height:16px;\">";
                strHtml = strHtml + "						<font style=\"font-size:10pt;\">" + strQuantidade + "</font>";
                strHtml = strHtml + "					</td>";
                strHtml = strHtml + "				<td style=\"width:193%;font-size:10pt;height:16px;\">";
                strHtml = strHtml + "					<br />";
                strHtml = strHtml + "				</td>";
                strHtml = strHtml + "			</tr>";
                strHtml = strHtml + "		</tbody>";
                strHtml = strHtml + "		<tfoot>";
                strHtml = strHtml + "		</tfoot>";
                strHtml = strHtml + "	</table>";
                strHtml = strHtml + "	<table border=\"1\" align=\"default\" cellpadding=\"2\" cellspacing=\"0\" title=\"\" summary=\"\" style=\"width:98.6987%;\">";
                strHtml = strHtml + "		<tbody>";
                strHtml = strHtml + "			<tr>";
                strHtml = strHtml + "				<td style=\"text-align:center;width:23%;\">";
                strHtml = strHtml + "						<font style=\"font-size:10pt;\">&nbsp; " + DateTime.Today.ToShortDateString() + "</font>";
                strHtml = strHtml + "					</td>";
                strHtml = strHtml + "				<td style=\"text-align:center;width:10%;\">";
                strHtml = strHtml + "						<font style=\"font-size:10pt;\">" + DateTime.Now.ToShortTimeString() + "</font>";
                strHtml = strHtml + "					</td>";
                strHtml = strHtml + "				<td style=\"width:152%;\">";
                strHtml = strHtml + "					<br />";
                strHtml = strHtml + "				</td>";
                strHtml = strHtml + "			</tr>";
                strHtml = strHtml + "			<tr>";
                strHtml = strHtml + "				<td style=\"text-align:center;width:23%;\">";
                strHtml = strHtml + "					<i>";
                strHtml = strHtml + "							<font style=\"font-size:10pt;\">ALVARO DE MATTOS</font>";
                strHtml = strHtml + "						</i>";
                strHtml = strHtml + "				</td>";
                strHtml = strHtml + "				<td style=\"width:10%;\">";
                strHtml = strHtml + "					<br />";
                strHtml = strHtml + "				</td>";
                strHtml = strHtml + "				<td style=\"text-align:center;width:152%;\">CORREIOS</td>";
                strHtml = strHtml + "			</tr>";
                strHtml = strHtml + "			<tr>";
                strHtml = strHtml + "				<td style=\"text-align:center;width:23%;\">";
                strHtml = strHtml + "						<font style=\"font-size:9pt;\">SOLICITANTE</font>";
                strHtml = strHtml + "				</td>";
                strHtml = strHtml + "				<td style=\"text-align:center;width:10%;\">";
                strHtml = strHtml + "						<font style=\"font-size:8pt;\">COLETOR</font>";
                strHtml = strHtml + "				</td>";
                strHtml = strHtml + "				<td style=\"width:152%;\">";
                strHtml = strHtml + "					<br />";
                strHtml = strHtml + "				</td>";
                strHtml = strHtml + "			</tr>";
                strHtml = strHtml + "		</tbody>";
                strHtml = strHtml + "		<tfoot>";
                strHtml = strHtml + "		</tfoot>";
                strHtml = strHtml + "	</table>";
                strHtml = strHtml + "	<br />";
                strHtml = strHtml + "	</div>";
                strHtml = strHtml + "	<div>";
                strHtml = strHtml + "		<br />";
                strHtml = strHtml + "	</div>";
                strHtml = strHtml + "	<div>";
                strHtml = strHtml + "		<div style=\"text-align:center;\">";
                strHtml = strHtml + "			<br />";
                strHtml = strHtml + "			<br />";
                strHtml = strHtml + "		</div>";
                strHtml = strHtml + "	</div>";
                strHtml = strHtml + "</div>";
                strHtml = strHtml + "<div style=\"text-align: left;\">";
                strHtml = strHtml + "	<br />";
                strHtml = strHtml + "</div>";
                strHtml = strHtml + "<div style=\"text-align: left;\">";
                strHtml = strHtml + "	<div style=\"text-align:center;\">";
                //strHtml = strHtml + "		<br />";
                //strHtml = strHtml + "		<br />";
                strHtml = strHtml + "	</div>";
                strHtml = strHtml + "</div>";
            }
            //********************************************************************
            strHtml = strHtml + "</html>";



            StreamWriter escritor = new StreamWriter(@"envio.html");
            escritor.WriteLine(strHtml);
            escritor.Close();

            // Pronto, sua pagina foi criada e está pronta para ser exibida, agora é só exibí-la.
            System.Diagnostics.Process.Start(@"envio.html");

            return true;
        }

        public static bool ImprimeWord(String CaminhoArquivo)
        {
            ProcessStartInfo info = new ProcessStartInfo(CaminhoArquivo);

            info.Verb = "Print";

            info.CreateNoWindow = true;

            info.WindowStyle = ProcessWindowStyle.Hidden;

            Process.Start(info);

            return true;
        }
        
        public static void ApagarItemEntrada(String strId)
        {
            String query;

            global.myDB.CloseConnection();

            query = "DELETE FROM ITENS_ENTRADA_PRODUTOS WHERE ID = " + strId;

            if (global.myDB.connection.State == ConnectionState.Closed)
            {
                global.myDB.OpenConnection();
            }

            MySqlCommand cmd1 = new MySqlCommand(query, global.myDB.connection);

            try
            {
                cmd1.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "PontoZero Informa");
            }

            
        }



        public static string CarregaDescricao(String strSKU)
        {
            String query;
            string strRetorno = "";
            MySqlCommand cmd;
            MySqlDataReader dataLista;

            query = " select catalog_product_entity_varchar.value ";
            query = query + " from catalog_product_entity_varchar";
            query = query + " INNER JOIN  `catalog_product_entity` ";
            query = query + " ON `catalog_product_entity`.entity_id = `catalog_product_entity_varchar`.entity_id";
            query = query + " where catalog_product_entity_varchar.attribute_id = '71'";
            query = query + " AND catalog_product_entity.sku = '" + strSKU + "'; ";


            if (global.myDB.connection.State == ConnectionState.Closed)
            {
                global.myDB.OpenConnection();
            }

            cmd = new MySqlCommand(query, global.myDB.connection);

            try
            {
                dataLista = cmd.ExecuteReader();
            }
            catch (Exception)
            {
                global.myDB.OpenConnection();
                cmd = new MySqlCommand(query, global.myDB.connection);


                dataLista = cmd.ExecuteReader();
            }

            while (dataLista.Read())
            {
                strRetorno = dataLista["value"].ToString();

            }

            dataLista.Close();

            return strRetorno;
        }

        public static void PreencherPorReplace(string CaminhoDocMatriz, string strPedido, string strNome, string strEndereco, string strCEP, string strMunicipio, string strUF, string strCPF, string strPeso, ListView lstList)

        {
   //         int i;
   //         double dblQuant = 0;
   //         double dblValor = 0;

   //         //Objeto a ser usado nos parâmetros opcionais
   //         object missing = System.Reflection.Missing.Value;

   //         //Abre a aplicação Word e faz uma cópia do documento mapeado
   //         Word.Application oApp = new Word.Application();

   //         object template = CaminhoDocMatriz;

   //         Word.Document oDoc = oApp.Documents.Add(ref template, ref missing, ref missing, ref missing);

   //         //Troca o conteúdo de alguns tags

   //         Word.Range oRng = oDoc.Range(ref missing, ref missing);
   //         object FindText = "facilNome";
   //         object ReplaceWith = strNome;
   //         object MatchWholeWord = true;
   //         object Forward = false;

   //         oRng.Find.Execute(ref FindText, ref missing, ref MatchWholeWord, ref missing, ref missing, ref missing, ref Forward,
   //         ref missing, ref missing, ref ReplaceWith, ref missing, ref missing, ref missing, ref missing, ref missing);

   //         oRng = oDoc.Range(ref missing, ref missing);
   //         FindText = "facilEndereco1";
   //         ReplaceWith = Regex.Replace(strEndereco, "\n", " ");
   //         oRng.Find.Execute(ref FindText, ref missing, ref MatchWholeWord, ref missing, ref missing, ref missing, ref Forward,
   //         ref missing, ref missing, ref ReplaceWith, ref missing, ref missing, ref missing, ref missing, ref missing);

   //         oRng = oDoc.Range(ref missing, ref missing);
   //         FindText = "facilCEP";
   //         ReplaceWith = strCEP;
   //         oRng.Find.Execute(ref FindText, ref missing, ref MatchWholeWord, ref missing, ref missing, ref missing, ref Forward,
   //         ref missing, ref missing, ref ReplaceWith, ref missing, ref missing, ref missing, ref missing, ref missing);

   //         oRng = oDoc.Range(ref missing, ref missing);
   //         FindText = "facilCidade";
   //         ReplaceWith = strMunicipio;
   //         oRng.Find.Execute(ref FindText, ref missing, ref MatchWholeWord, ref missing, ref missing, ref missing, ref Forward,
   //         ref missing, ref missing, ref ReplaceWith, ref missing, ref missing, ref missing, ref missing, ref missing);

   //         oRng = oDoc.Range(ref missing, ref missing);
   //         FindText = "facilUF";
   //         ReplaceWith = strUF;
   //         oRng.Find.Execute(ref FindText, ref missing, ref MatchWholeWord, ref missing, ref missing, ref missing, ref Forward,
   //         ref missing, ref missing, ref ReplaceWith, ref missing, ref missing, ref missing, ref missing, ref missing);

   //         oRng = oDoc.Range(ref missing, ref missing);
   //         FindText = "facilCPF";
   //         ReplaceWith = strCPF;
   //         oRng.Find.Execute(ref FindText, ref missing, ref MatchWholeWord, ref missing, ref missing, ref missing, ref Forward,
   //         ref missing, ref missing, ref ReplaceWith, ref missing, ref missing, ref missing, ref missing, ref missing);

   //         i = 0;

   //         foreach (ListViewItem item in lstList.Items)
   //         {
   //             if (item.SubItems[21].Text == strPedido)
   //             {

   //                 i = i + 1;

   //                 oRng = oDoc.Range(ref missing, ref missing);

   //                 FindText = "Fac" + i;

   //                 ReplaceWith = i;

   //                 oRng.Find.Execute(ref FindText, ref missing, ref MatchWholeWord, ref missing, ref missing, ref missing, ref Forward,
   //                 ref missing, ref missing, ref ReplaceWith, ref missing, ref missing, ref missing, ref missing, ref missing);

   //                 oRng = oDoc.Range(ref missing, ref missing);
   //                 FindText = "Conteudo" + i;

   //                 if (item.SubItems[4].Text.Length > 85)
   //                 {
   //                     ReplaceWith = item.SubItems[4].Text.Substring(0, 85);
   //                 }
   //                 else
   //                 {
   //                     ReplaceWith = item.SubItems[4].Text;
   //                 }

   //                 oRng.Find.Execute(ref FindText, ref missing, ref MatchWholeWord, ref missing, ref missing, ref missing, ref Forward,
   //                 ref missing, ref missing, ref ReplaceWith, ref missing, ref missing, ref missing, ref missing, ref missing);

   //                 oRng = oDoc.Range(ref missing, ref missing);
   //                 FindText = "Quant" + i;
   //                 ReplaceWith = item.SubItems[5].Text;
   //                 dblQuant = dblQuant + Convert.ToDouble(item.SubItems[5].Text);
   //                 oRng.Find.Execute(ref FindText, ref missing, ref MatchWholeWord, ref missing, ref missing, ref missing, ref Forward,
   //                 ref missing, ref missing, ref ReplaceWith, ref missing, ref missing, ref missing, ref missing, ref missing);

   //                 oRng = oDoc.Range(ref missing, ref missing);
   //                 FindText = "Valor" + i;
   //                 ReplaceWith = item.SubItems[10].Text;
   //                 dblValor = dblValor + Convert.ToDouble(item.SubItems[10].Text);
   //                 oRng.Find.Execute(ref FindText, ref missing, ref MatchWholeWord, ref missing, ref missing, ref missing, ref Forward,
   //                 ref missing, ref missing, ref ReplaceWith, ref missing, ref missing, ref missing, ref missing, ref missing);

   //             }
   //         }

   //         if (i < 15)
   //         {
   //             while (i <= 15)
   //             {
   //                 i = i + 1;

   //                 oRng = oDoc.Range(ref missing, ref missing);

   //                 FindText = "Fac" + i;

   //                 ReplaceWith = "";

   //                 oRng.Find.Execute(ref FindText, ref missing, ref MatchWholeWord, ref missing, ref missing, ref missing, ref Forward,
   //                 ref missing, ref missing, ref ReplaceWith, ref missing, ref missing, ref missing, ref missing, ref missing);

   //                 oRng = oDoc.Range(ref missing, ref missing);
   //                 FindText = "Conteudo" + i;
   //                 ReplaceWith = "";
   //                 oRng.Find.Execute(ref FindText, ref missing, ref MatchWholeWord, ref missing, ref missing, ref missing, ref Forward,
   //                 ref missing, ref missing, ref ReplaceWith, ref missing, ref missing, ref missing, ref missing, ref missing);

   //                 oRng = oDoc.Range(ref missing, ref missing);
   //                 FindText = "Quant" + i;
   //                 ReplaceWith = "";
   //                 oRng.Find.Execute(ref FindText, ref missing, ref MatchWholeWord, ref missing, ref missing, ref missing, ref Forward,
   //                 ref missing, ref missing, ref ReplaceWith, ref missing, ref missing, ref missing, ref missing, ref missing);

   //                 oRng = oDoc.Range(ref missing, ref missing);
   //                 FindText = "Valor" + i;
   //                 ReplaceWith = "";
   //                 oRng.Find.Execute(ref FindText, ref missing, ref MatchWholeWord, ref missing, ref missing, ref missing, ref Forward,
   //                 ref missing, ref missing, ref ReplaceWith, ref missing, ref missing, ref missing, ref missing, ref missing);
   //             }
   //         }

   //         oRng = oDoc.Range(ref missing, ref missing);
   //         FindText = "QuantTotal";
   //         ReplaceWith = String.Format("{0:N}", dblQuant);
   //         oRng.Find.Execute(ref FindText, ref missing, ref MatchWholeWord, ref missing, ref missing, ref missing, ref Forward,
   //         ref missing, ref missing, ref ReplaceWith, ref missing, ref missing, ref missing, ref missing, ref missing);

   //         oRng = oDoc.Range(ref missing, ref missing);
   //         FindText = "ValorTotal";
   //         ReplaceWith = String.Format("{0:N}", dblValor);
   //         oRng.Find.Execute(ref FindText, ref missing, ref MatchWholeWord, ref missing, ref missing, ref missing, ref Forward,
   //         ref missing, ref missing, ref ReplaceWith, ref missing, ref missing, ref missing, ref missing, ref missing);

   //         oRng = oDoc.Range(ref missing, ref missing);
   //         FindText = "PesoTotal";
   //         ReplaceWith = String.Format("{0:N}", Convert.ToDouble(strPeso));
   //         oRng.Find.Execute(ref FindText, ref missing, ref MatchWholeWord, ref missing, ref missing, ref missing, ref Forward,
   //         ref missing, ref missing, ref ReplaceWith, ref missing, ref missing, ref missing, ref missing, ref missing);

   //         //oApp.Visible = true;
   //         //oApp.PrintOut();

   //         //********************************************************************

           
   //         // Arquivo de Origem
   //       //  object filename = "nomeDoArquivo.doc";

   //         // Arquivo de Destino
   //         object newFileName = strPedido + ".pdf";

          

   //         // Abrir documento
   //        /*Microsoft.Office.Interop.Word.Document doc = oApp.Documents.Open(ref filename, ref missing, ref missing, ref missing, ref missing, ref missing,
   //             ref missing, ref missing, ref missing, ref missing, ref missing, ref missing, ref missing,
   //             ref missing, ref missing, ref missing);*/

   //         // Formato para Salvar o Arquivo – Destino  – No caso, PDF
   //         object formatoArquivo = Microsoft.Office.Interop.Word.WdSaveFormat.wdFormatPDF;

   //         // Alterações no tamanho do Papel – No caso, utilizando o formato A5
   //         oDoc.PageSetup.PaperSize = Microsoft.Office.Interop.Word.WdPaperSize.wdPaperA4;

   //         // Alterações na Orientação do Papel
   ////         oDoc.PageSetup.Orientation = Microsoft.Office.Interop.Word.WdOrientation.wdOrientLandscape;

   //         // Salvar Arquivo
   //         oDoc.SaveAs(ref newFileName, ref formatoArquivo, ref missing, ref missing, ref missing, ref missing, ref missing,
   //             ref missing, ref missing, ref missing, ref missing, ref missing, ref missing, ref missing, ref missing, ref missing);

   //         // Não salvar alterações no arquivo original
   //         // object salvarAlteracoesArqOriginal = false;
   //         // wordApp.Quit(ref salvarAlteracoesArqOriginal, ref missing, ref missing);

            //********************************************************************

        }


        public static void PreencherRastreio(string CaminhoDocMatriz, string strfacilNota, string strfacilPedido, string strfacilRastreio, string strfacilNome, string strfacilEndereco, string strfacilBairro, string strfacilCEP, string strfacilCEPBarra, Image imgImagem, Image imgImagemCEP)

        {

            ////Objeto a ser usado nos parâmetros opcionais
            //object missing = System.Reflection.Missing.Value;

            ////Abre a aplicação Word e faz uma cópia do documento mapeado
            //Word.Application oApp = new Word.Application();

            //object template = CaminhoDocMatriz;

            //Word.Document oDoc = oApp.Documents.Add(ref template, ref missing, ref missing, ref missing);

            ////Troca o conteúdo de alguns tags

            //Word.Range oRng = oDoc.Range(ref missing, ref missing);
            //object FindText = "facilNota";
            //object ReplaceWith = strfacilNota;
            //object MatchWholeWord = true;
            //object Forward = false;

            //oRng.Find.Execute(ref FindText, ref missing, ref MatchWholeWord, ref missing, ref missing, ref missing, ref Forward,
            //ref missing, ref missing, ref ReplaceWith, ref missing, ref missing, ref missing, ref missing, ref missing);

            //oRng = oDoc.Range(ref missing, ref missing);
            //FindText = "facilPedido";
            //ReplaceWith = strfacilPedido;
            //oRng.Find.Execute(ref FindText, ref missing, ref MatchWholeWord, ref missing, ref missing, ref missing, ref Forward,
            //ref missing, ref missing, ref ReplaceWith, ref missing, ref missing, ref missing, ref missing, ref missing);

            //oRng = oDoc.Range(ref missing, ref missing);
            //FindText = "facilRastreio";
            //ReplaceWith = strfacilRastreio;
            //oRng.Find.Execute(ref FindText, ref missing, ref MatchWholeWord, ref missing, ref missing, ref missing, ref Forward,
            //ref missing, ref missing, ref ReplaceWith, ref missing, ref missing, ref missing, ref missing, ref missing);

            ////*************************************************************************************************************************************************
            //oRng = oDoc.Range(ref missing, ref missing);
            //FindText = "facilCodigoBarra";
            //ReplaceWith = "";
            //oRng.Find.Execute(ref FindText, ref missing, ref MatchWholeWord, ref missing, ref missing, ref missing, ref Forward,
            //ref missing, ref missing, ref ReplaceWith, ref missing, ref missing, ref missing, ref missing, ref missing);

            //Clipboard.SetImage(imgImagem);

            //oRng.Paste();

            ////*************************************************************************************************************************************************


            //oRng = oDoc.Range(ref missing, ref missing);
            //FindText = "facilNome";
            //ReplaceWith = strfacilNome;
            //oRng.Find.Execute(ref FindText, ref missing, ref MatchWholeWord, ref missing, ref missing, ref missing, ref Forward,
            //  ref missing, ref missing, ref ReplaceWith, ref missing, ref missing, ref missing, ref missing, ref missing);


            //oRng = oDoc.Range(ref missing, ref missing);
            //FindText = "facilEndereco";
            //ReplaceWith = strfacilEndereco;
            //oRng.Find.Execute(ref FindText, ref missing, ref MatchWholeWord, ref missing, ref missing, ref missing, ref Forward,
            //ref missing, ref missing, ref ReplaceWith, ref missing, ref missing, ref missing, ref missing, ref missing);

            //oRng = oDoc.Range(ref missing, ref missing);
            //FindText = "facilBairro";
            //ReplaceWith = strfacilBairro;
            //oRng.Find.Execute(ref FindText, ref missing, ref MatchWholeWord, ref missing, ref missing, ref missing, ref Forward,
            //ref missing, ref missing, ref ReplaceWith, ref missing, ref missing, ref missing, ref missing, ref missing);

            //oRng = oDoc.Range(ref missing, ref missing);
            //FindText = "facilCEP";
            //ReplaceWith = strfacilCEP;
            //oRng.Find.Execute(ref FindText, ref missing, ref MatchWholeWord, ref missing, ref missing, ref missing, ref Forward,
            //ref missing, ref missing, ref ReplaceWith, ref missing, ref missing, ref missing, ref missing, ref missing);

            ////*************************************************************************************************************************************************
            //oRng = oDoc.Range(ref missing, ref missing);
            //FindText = "facilCEPBarra";
            //ReplaceWith = "";
            //oRng.Find.Execute(ref FindText, ref missing, ref MatchWholeWord, ref missing, ref missing, ref missing, ref Forward,
            //ref missing, ref missing, ref ReplaceWith, ref missing, ref missing, ref missing, ref missing, ref missing);

            //Clipboard.SetImage(imgImagemCEP);

            //oRng.Paste();

            ////*************************************************************************************************************************************************

            //oApp.Visible = true;

            ////  oApp.Visible = false;

            ////oApp.ActivePrinter = "ZDesigner GC420t (EPL)";

            ////  oApp.PrintOut(true, ref missing, ref missing, ref missing, ref missing, ref missing, ref missing, ref missing, ref missing, ref missing, ref missing, ref missing, ref missing, ref missing);

            //// oApp.ActivePrinter = "RICOH SP 310SFNw PCL 6";
        }


        public static void PreencherEtiquetaNF(string CaminhoDocMatriz, string strfacilNota, string strfacilData, string strfacilChave, string strfacilNome, string strfacilEndereco, string strfacilBairro, string strfacilCEP, string strfacilCEPBarra, Image imgImagem)

        {

            ////Objeto a ser usado nos parâmetros opcionais
            //object missing = System.Reflection.Missing.Value;

            ////Abre a aplicação Word e faz uma cópia do documento mapeado
            //Word.Application oApp = new Word.Application();

            //object template = CaminhoDocMatriz;

            //Word.Document oDoc = oApp.Documents.Add(ref template, ref missing, ref missing, ref missing);

            ////Troca o conteúdo de alguns tags

            //Word.Range oRng = oDoc.Range(ref missing, ref missing);
            //object FindText = "facilNota";
            //object ReplaceWith = strfacilNota;
            //object MatchWholeWord = true;
            //object Forward = false;

            //oRng.Find.Execute(ref FindText, ref missing, ref MatchWholeWord, ref missing, ref missing, ref missing, ref Forward,
            //ref missing, ref missing, ref ReplaceWith, ref missing, ref missing, ref missing, ref missing, ref missing);

            //oRng = oDoc.Range(ref missing, ref missing);
            //FindText = "facilData";
            //ReplaceWith = strfacilData;
            //oRng.Find.Execute(ref FindText, ref missing, ref MatchWholeWord, ref missing, ref missing, ref missing, ref Forward,
            //ref missing, ref missing, ref ReplaceWith, ref missing, ref missing, ref missing, ref missing, ref missing);

            //oRng = oDoc.Range(ref missing, ref missing);
            //FindText = "facilChave";
            //ReplaceWith = strfacilChave;
            //oRng.Find.Execute(ref FindText, ref missing, ref MatchWholeWord, ref missing, ref missing, ref missing, ref Forward,
            //ref missing, ref missing, ref ReplaceWith, ref missing, ref missing, ref missing, ref missing, ref missing);

            ////*************************************************************************************************************************************************
            //oRng = oDoc.Range(ref missing, ref missing);
            //FindText = "facilCodigoBarra";
            //ReplaceWith = "";
            //oRng.Find.Execute(ref FindText, ref missing, ref MatchWholeWord, ref missing, ref missing, ref missing, ref Forward,
            //ref missing, ref missing, ref ReplaceWith, ref missing, ref missing, ref missing, ref missing, ref missing);

            //Clipboard.SetImage(imgImagem);

            //oRng.Paste();

            ////*************************************************************************************************************************************************


            //oRng = oDoc.Range(ref missing, ref missing);
            //FindText = "facilNome";
            //ReplaceWith = strfacilNome;
            //oRng.Find.Execute(ref FindText, ref missing, ref MatchWholeWord, ref missing, ref missing, ref missing, ref Forward,
            //  ref missing, ref missing, ref ReplaceWith, ref missing, ref missing, ref missing, ref missing, ref missing);


            //oRng = oDoc.Range(ref missing, ref missing);
            //FindText = "facilEndereco";
            //ReplaceWith = strfacilEndereco;
            //oRng.Find.Execute(ref FindText, ref missing, ref MatchWholeWord, ref missing, ref missing, ref missing, ref Forward,
            //ref missing, ref missing, ref ReplaceWith, ref missing, ref missing, ref missing, ref missing, ref missing);

            //oRng = oDoc.Range(ref missing, ref missing);
            //FindText = "facilBairro";
            //ReplaceWith = strfacilBairro;
            //oRng.Find.Execute(ref FindText, ref missing, ref MatchWholeWord, ref missing, ref missing, ref missing, ref Forward,
            //ref missing, ref missing, ref ReplaceWith, ref missing, ref missing, ref missing, ref missing, ref missing);

            //oRng = oDoc.Range(ref missing, ref missing);
            //FindText = "facilCEP";
            //ReplaceWith = strfacilCEP;
            //oRng.Find.Execute(ref FindText, ref missing, ref MatchWholeWord, ref missing, ref missing, ref missing, ref Forward,
            //ref missing, ref missing, ref ReplaceWith, ref missing, ref missing, ref missing, ref missing, ref missing);

            ////*************************************************************************************************************************************************
            ////oRng = oDoc.Range(ref missing, ref missing);
            ////FindText = "facilCEPBarra";
            ////ReplaceWith = "";
            ////oRng.Find.Execute(ref FindText, ref missing, ref MatchWholeWord, ref missing, ref missing, ref missing, ref Forward,
            ////ref missing, ref missing, ref ReplaceWith, ref missing, ref missing, ref missing, ref missing, ref missing);

            ////Clipboard.SetImage(imgImagem);

            ////oRng.Paste();

            ////*************************************************************************************************************************************************

            //oApp.Visible = true;

            ////  oApp.Visible = false;

            ////oApp.ActivePrinter = "ZDesigner GC420t (EPL)";

            ////  oApp.PrintOut(true, ref missing, ref missing, ref missing, ref missing, ref missing, ref missing, ref missing, ref missing, ref missing, ref missing, ref missing, ref missing, ref missing);

            //// oApp.ActivePrinter = "RICOH SP 310SFNw PCL 6";
        }

        public static void PreencherCorreio(string CaminhoDocMatriz, string strID, int strQuantidade)

        {

            ////Objeto a ser usado nos parâmetros opcionais
            //object missing = System.Reflection.Missing.Value;

            ////Abre a aplicação Word e faz uma cópia do documento mapeado
            //Word.Application oApp = new Word.Application();

            //object template = CaminhoDocMatriz;

            //Word.Document oDoc = oApp.Documents.Add(ref template, ref missing, ref missing, ref missing);

            ////Troca o conteúdo de alguns tags

            //Word.Range oRng = oDoc.Range(ref missing, ref missing);
            //object FindText = "strID";
            //object ReplaceWith = strID;
            //object MatchWholeWord = true;
            //object Forward = false;

            //oRng.Find.Execute(ref FindText, ref missing, ref MatchWholeWord, ref missing, ref missing, ref missing, ref Forward,
            //ref missing, ref missing, ref ReplaceWith, ref missing, ref missing, ref missing, ref missing, ref missing);

            //oRng = oDoc.Range(ref missing, ref missing);
            //FindText = "strQuant";
            //ReplaceWith = strQuantidade;
            //oRng.Find.Execute(ref FindText, ref missing, ref MatchWholeWord, ref missing, ref missing, ref missing, ref Forward,
            //ref missing, ref missing, ref ReplaceWith, ref missing, ref missing, ref missing, ref missing, ref missing);

            //oRng = oDoc.Range(ref missing, ref missing);
            //FindText = "strData";
            //ReplaceWith = DateTime.Now.ToString("dd-MM-yyyy");
            //oRng.Find.Execute(ref FindText, ref missing, ref MatchWholeWord, ref missing, ref missing, ref missing, ref Forward,
            //ref missing, ref missing, ref ReplaceWith, ref missing, ref missing, ref missing, ref missing, ref missing);

            //oRng = oDoc.Range(ref missing, ref missing);
            //FindText = "strHora";
            //ReplaceWith = DateTime.Now.ToString("HH:mm:ss");
            //oRng.Find.Execute(ref FindText, ref missing, ref MatchWholeWord, ref missing, ref missing, ref missing, ref Forward,
            //ref missing, ref missing, ref ReplaceWith, ref missing, ref missing, ref missing, ref missing, ref missing);

            //oRng = oDoc.Range(ref missing, ref missing);
            //FindText = "strID1";
            //ReplaceWith = strID;
            //MatchWholeWord = true;
            //Forward = false;

            //oRng.Find.Execute(ref FindText, ref missing, ref MatchWholeWord, ref missing, ref missing, ref missing, ref Forward,
            //ref missing, ref missing, ref ReplaceWith, ref missing, ref missing, ref missing, ref missing, ref missing);

            //oRng = oDoc.Range(ref missing, ref missing);
            //FindText = "strQuant1";
            //ReplaceWith = strQuantidade;
            //oRng.Find.Execute(ref FindText, ref missing, ref MatchWholeWord, ref missing, ref missing, ref missing, ref Forward,
            //ref missing, ref missing, ref ReplaceWith, ref missing, ref missing, ref missing, ref missing, ref missing);

            //oRng = oDoc.Range(ref missing, ref missing);
            //FindText = "strData1";
            //ReplaceWith = DateTime.Now.ToString("dd-MM-yyyy");
            //oRng.Find.Execute(ref FindText, ref missing, ref MatchWholeWord, ref missing, ref missing, ref missing, ref Forward,
            //ref missing, ref missing, ref ReplaceWith, ref missing, ref missing, ref missing, ref missing, ref missing);

            //oRng = oDoc.Range(ref missing, ref missing);
            //FindText = "strHora1";
            //ReplaceWith = DateTime.Now.ToString("HH:mm:ss");
            //oRng.Find.Execute(ref FindText, ref missing, ref MatchWholeWord, ref missing, ref missing, ref missing, ref Forward,
            //ref missing, ref missing, ref ReplaceWith, ref missing, ref missing, ref missing, ref missing, ref missing);

            //oApp.Visible = true;
        }

        public static int BuscaID(String strTabela)
        {
            int id = 0;

            MySqlCommand cmd;
            MySqlDataReader dataReader;

            string query = " SELECT coalesce(MAX(ID),0) MAX " +
                           " FROM " + strTabela;

            if (global.myDB.connection.State == ConnectionState.Closed)
            {
                myDB.OpenConnection();
            }

            cmd = new MySqlCommand(query, myDB.connection);

            dataReader = cmd.ExecuteReader();

            dataReader.Read();

            id = Convert.ToInt32(dataReader["MAX"]) + 1;

            dataReader.Close();

            if (global.myDB.connection.State != ConnectionState.Closed)
            {
                global.myDB.CloseConnection();
            }

            return id;
        }


        public static int BuscaNovoEnvio(bool blMercadoLivre, bool blB2W, bool blMagaLu, bool blMlPlace, bool blTotalExpress)
        {
            int id = 0;
            MySqlDataReader drDataReader;
            MySqlCommand cmd;

            string query = "select coalesce(id_ENVIO_CORREIOS,0) id_ENVIO_CORREIOS";

            query = query + " FROM ENVIO_CORREIOS ENVIO "; 
            query = query + " LEFT JOIN ITENS_ENVIO_CORREIOS ITEM "; 
            query = query + " ON ITEM.ID_ENVIO_CORREIOS = ENVIO.ID ";
            query = query + " WHERE ENVIO.MERCADOLIVRE = " + blMercadoLivre;
            query = query + " AND ENVIO.b2w = " + blB2W;
            query = query + " AND ENVIO.magalu = " + blMagaLu;
            query = query + " AND ENVIO.mlplace = " + blMlPlace;
            query = query + " AND ENVIO.totalexpress = " + blTotalExpress;
            query = query + " AND ENVIO.DATA_IMPRESSAO IS NULL; ";


            myDB.CloseConnection();

            if (global.myDB.connection.State == ConnectionState.Closed)
            {
                myDB.OpenConnection();
            }

            cmd = new MySqlCommand(query, myDB.connection);

            drDataReader = cmd.ExecuteReader();

            drDataReader.Read();

            id = 0;

            while (drDataReader.Read())
            {
                id = Convert.ToInt32(drDataReader["id_ENVIO_CORREIOS"]);
            }


            drDataReader.Close();

            if (global.myDB.connection.State != ConnectionState.Closed)
            {
                global.myDB.CloseConnection();
            }

            return id;
        }

        public static String BuscaCampo(string strTabela, string strRetorno, string strCriterio, string strBusca)
        {
            MySqlDataReader drDataReader;
            MySqlCommand cmd;
            String strReturn;

            strReturn = "";

            string query = "select " + strRetorno + " retorno ";


            query = query + " FROM " + strTabela;
            query = query + " WHERE " + strCriterio + " = '" + strBusca + "'";

            myDB.CloseConnection();

            cmd = new MySqlCommand(query, myDB.connection);


            if (global.myDB.connection.State == ConnectionState.Closed)
            {
                myDB.OpenConnection();
                cmd = new MySqlCommand(query, myDB.connection);
                drDataReader = cmd.ExecuteReader();
            }
            else
            {
                drDataReader = cmd.ExecuteReader();
            }


            drDataReader.Read();



            //while (drDataReader.Read())
            //{
            try
            {
                strReturn = drDataReader["retorno"].ToString();
            }
            catch (Exception ex)
            {
                strReturn = "";
            }
                //}


            drDataReader.Close();

            if (global.myDB.connection.State != ConnectionState.Closed)
            {
                global.myDB.CloseConnection();
            }

            return strReturn;
        }

        public static bool validarCPF(string CPF)
        {
            int[] mt1 = new int[9] { 10, 9, 8, 7, 6, 5, 4, 3, 2 };
            int[] mt2 = new int[10] { 11, 10, 9, 8, 7, 6, 5, 4, 3, 2 };
            string TempCPF;
            string Digito;
            int soma;
            int resto;

            CPF = CPF.Trim();
            CPF = CPF.Replace(".", "").Replace("-", "");

            if (CPF.Length != 11)
                return false;

            TempCPF = CPF.Substring(0, 9);
            soma = 0;
            for (int i = 0; i < 9; i++)
                soma += int.Parse(TempCPF[i].ToString()) * mt1[i];

            resto = soma % 11;
            if (resto < 2)
                resto = 0;
            else
                resto = 11 - resto;

            Digito = resto.ToString();
            TempCPF = TempCPF + Digito;
            soma = 0;

            for (int i = 0; i < 10; i++)
                soma += int.Parse(TempCPF[i].ToString()) * mt2[i];

            resto = soma % 11;
            if (resto < 2)
                resto = 0;
            else
                resto = 11 - resto;

            Digito = Digito + resto.ToString();

            return CPF.EndsWith(Digito);
        }

        public static bool validarCNPJ(string cnpj)
        {
            int[] multiplicador1 = new int[12] { 5, 4, 3, 2, 9, 8, 7, 6, 5, 4, 3, 2 };
            int[] multiplicador2 = new int[13] { 6, 5, 4, 3, 2, 9, 8, 7, 6, 5, 4, 3, 2 };
            int soma;
            int resto;
            string digito;
            string tempCnpj;
            cnpj = cnpj.Trim();
            cnpj = cnpj.Replace(".", "").Replace("-", "").Replace("/", "");
            if (cnpj.Length != 14)
                return false;
            tempCnpj = cnpj.Substring(0, 12);
            soma = 0;
            for (int i = 0; i < 12; i++)
                soma += int.Parse(tempCnpj[i].ToString()) * multiplicador1[i];
            resto = (soma % 11);
            if (resto < 2)
                resto = 0;
            else
                resto = 11 - resto;
            digito = resto.ToString();
            tempCnpj = tempCnpj + digito;
            soma = 0;
            for (int i = 0; i < 13; i++)
                soma += int.Parse(tempCnpj[i].ToString()) * multiplicador2[i];
            resto = (soma % 11);
            if (resto < 2)
                resto = 0;
            else
                resto = 11 - resto;
            digito = digito + resto.ToString();
            return cnpj.EndsWith(digito);
        }

        public static string StripTagsRegex(string source)
        {
            return Replace_HTML(Regex.Replace(source, "<.*?>", string.Empty));
        }

        public static string Replace_HTML(string source)
        {
            source = Regex.Replace(source, "&bull;", ".");
            source = Regex.Replace(source, "&ndash;", " - ");
            source = Regex.Replace(source, "&nbsp;", " ");
            source = Regex.Replace(source, "&Aacute;", "Á");
            source = Regex.Replace(source, "&aacute;", "á");
            source = Regex.Replace(source, "&Acirc;", "Â");
            source = Regex.Replace(source, "&acirc;", "â");
            source = Regex.Replace(source, "&Agrave;", "À");
            source = Regex.Replace(source, "&agrave;", "à");
            source = Regex.Replace(source, "&Aring;", "Å");
            source = Regex.Replace(source, "&aring;", "å");
            source = Regex.Replace(source, "&Atilde;", "Ã");
            source = Regex.Replace(source, "&atilde;", "ã");
            source = Regex.Replace(source, "&Auml;", "Ä");
            source = Regex.Replace(source, "&auml;", "ä");
            source = Regex.Replace(source, "&AElig;", "Æ");
            source = Regex.Replace(source, "&aelig;", "æ");
            source = Regex.Replace(source, "&Eacute;", "É");
            source = Regex.Replace(source, "&eacute;", "é");
            source = Regex.Replace(source, "&Ecirc;", "Ê");
            source = Regex.Replace(source, "&ecirc;", "ê");
            source = Regex.Replace(source, "&Egrave;", "È");
            source = Regex.Replace(source, "&egrave;", "è");
            source = Regex.Replace(source, "&Euml;", "Ë");
            source = Regex.Replace(source, "&euml;", "ë");
            source = Regex.Replace(source, "&ETH;", "Ð");
            source = Regex.Replace(source, "&eth;", "ð");
            source = Regex.Replace(source, "&Iacute;", "Í");
            source = Regex.Replace(source, "&iacute;", "í");
            source = Regex.Replace(source, "&Icirc;", "Î");
            source = Regex.Replace(source, "&icirc;", "î");
            source = Regex.Replace(source, "&Igrave;", "Ì");
            source = Regex.Replace(source, "&igrave;", "ì");
            source = Regex.Replace(source, "&Iuml;", "Ï");
            source = Regex.Replace(source, "&iuml;", "ï");
            source = Regex.Replace(source, "&Oacute;", "Ó");
            source = Regex.Replace(source, "&oacute;", "ó");
            source = Regex.Replace(source, "&Ocirc;", "Ô");
            source = Regex.Replace(source, "&ocirc;", "ô");
            source = Regex.Replace(source, "&Ograve;", "Ò");
            source = Regex.Replace(source, "&ograve;", "ò");
            source = Regex.Replace(source, "&Oslash;", "Ø");
            source = Regex.Replace(source, "&oslash;", "ø");
            source = Regex.Replace(source, "&Otilde;", "Õ");
            source = Regex.Replace(source, "&otilde;", "õ");
            source = Regex.Replace(source, "&Ouml;", "Ö");
            source = Regex.Replace(source, "&ouml;", "ö");
            source = Regex.Replace(source, "&Uacute;", "Ú");
            source = Regex.Replace(source, "&uacute;", "ú");
            source = Regex.Replace(source, "&Ucirc;", "Û");
            source = Regex.Replace(source, "&ucirc;", "û");
            source = Regex.Replace(source, "&Ugrave;", "Ù");
            source = Regex.Replace(source, "&ugrave;", "ù");
            source = Regex.Replace(source, "&Uuml;", "Ü");
            source = Regex.Replace(source, "&uuml;", "ü");
            source = Regex.Replace(source, "&Ccedil;", "Ç");
            source = Regex.Replace(source, "&ccedil;", "ç");
            source = Regex.Replace(source, "&Ntilde;", "Ñ");
            source = Regex.Replace(source, "&ntilde;", "ñ");
            source = Regex.Replace(source, "&lt;", "<");
            source = Regex.Replace(source, "&gt;", ">");
            source = Regex.Replace(source, "&amp;", "&");
            source = Regex.Replace(source, "&reg;", "®");
            source = Regex.Replace(source, "&copy;", "©");
            source = Regex.Replace(source, "&Yacute;", "Ý");
            source = Regex.Replace(source, "&yacute;", "ý");
            source = Regex.Replace(source, "&THORN;", "Þ");
            source = Regex.Replace(source, "&thorn;", "þ");
            source = Regex.Replace(source, "&szlig;", "ß");

            return source;
        }

        /// <summary>
        /// Compiled regular expression for performance.
        /// </summary>
        static Regex _htmlRegex = new Regex("<.*?>", RegexOptions.Compiled);

        /// <summary>
        /// Remove HTML from string with compiled Regex.
        /// </summary>
        public static string StripTagsRegexCompiled(string source)
        {
            return _htmlRegex.Replace(source, string.Empty);
        }

        /// <summary>
        /// Remove HTML tags from string using char array.
        /// </summary>
        public static string StripTagsCharArray(string source)
        {
            char[] array = new char[source.Length];
            int arrayIndex = 0;
            bool inside = false;

            for (int i = 0; i < source.Length; i++)
            {
                char let = source[i];
                if (let == '<')
                {
                    inside = true;
                    continue;
                }
                if (let == '>')
                {
                    inside = false;
                    continue;
                }
                if (!inside)
                {
                    array[arrayIndex] = let;
                    arrayIndex++;
                }
            }
            return new string(array, 0, arrayIndex);
        }

        public void Limpa_Campo(Control.ControlCollection controles)
        {
            //Faz um laço para todos os controles passados no parâmetro
            foreach (Control ctrl in controles)
            {
                //Se o contorle for um TextBox...
                if (ctrl is TextBox)
                {
                    ((TextBox)(ctrl)).Text = String.Empty;
                }
                else if (ctrl is MaskedTextBox)
                {
                    ((MaskedTextBox)(ctrl)).Text = String.Empty;
                }

                else if (ctrl is ComboBox)
                {
                    ((ComboBox)(ctrl)).Text = String.Empty;
                }

                else if (ctrl is DataGridView)
                {
                    ((DataGridView)(ctrl)).DataSource = null;
                    ((DataGridView)(ctrl)).DataMember = null;
                }
            }

        }


        public static void GetRastreioWEBService(bool blSedex)
        {
            string strIdServico;


            if (blSedex == true)
            {
                strIdServico = strIdServicoSedex;
            }
            else
            {
                strIdServico = strIdServicoPAC;
            }

            strIdServico = "124849";

            try
            {
                var ws = new WSCorreios.AtendeClienteClient();

                var resposta = ws.solicitaEtiquetas("C", strCNPJ, long.Parse(strIdServico), 1, strUser, strSenha);

                global.GravaRastreio(resposta, blSedex);

                // MessageBox.Show("Busca do rastreio: " + resposta);

            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro ao efetuar busca do rastreio: " + ex.Message, "Informa");
            }
        }


        public static void BuscaServicoWEBService()
        {
            try
            {
                var ws = new WSCorreios.AtendeClienteClient();

                var binding = ws.Endpoint.Binding;

                var newBinding = new CustomBinding(binding);
                for (var i = 0; i < newBinding.Elements.Count; i++)
                {
                    if (newBinding.Elements[i] is TransportBindingElement)
                    {
                        ((TransportBindingElement)newBinding.Elements[i]).MaxReceivedMessageSize = 1000000;
                    }
                }

                ws.Endpoint.Binding = newBinding;

                var resposta = ws.buscaCliente(strIdContrato, strIdCartaoPostagem, strUser, strSenha);

                MessageBox.Show("Busca do rastreio: " + resposta, "Informa");

                for (int t = 0; t < resposta.contratos[0].cartoesPostagem[0].servicos.Length; t++)
                {
                    if (resposta.contratos[0].cartoesPostagem[0].servicos[t].descricao == "PAC CONTRATO AGENCIA          ")
                    {
                        strIdServicoPAC = resposta.contratos[0].cartoesPostagem[0].servicos[t].codigo;
                    }
                    else if (resposta.contratos[0].cartoesPostagem[0].servicos[t].descricao == "SEDEX CONTRATO AGENCIA        ")
                    {
                        strIdServicoSedex = resposta.contratos[0].cartoesPostagem[0].servicos[t].codigo;
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro ao efetuar busca do rastreio: " + ex.Message, "Informa");
            }
        }

        public static void FechaPLP(string strNumeroEnvio)
        {
            MySqlCommand cmd;
            string[] strRastreios;
            int i = 0;
            string strXMLRastreio;


            string query = "SELECT CONCAT(PREFIXO, NUMERO, SUFIXO) RASTREIO   FROM RASTREIO_CORREIOS WHERE SINCRONIZADO IS NOT TRUE;";


            if (global.myDB.connection.State == ConnectionState.Closed)
            {
                global.myDB.OpenConnection();
            }

            cmd = new MySqlCommand(query, global.myDB.connection);

            try
            {
                dataGlobal = cmd.ExecuteReader();
            }
            catch (Exception)
            {
                global.myDB.OpenConnection();
                cmd = new MySqlCommand(query, global.myDB.connection);
                dataGlobal = cmd.ExecuteReader();
            }



            DataTable dt = new DataTable();
            dt.Load(dataGlobal);

            strRastreios = new string[dt.Rows.Count];


            if (global.myDB.connection.State == ConnectionState.Closed)
            {
                global.myDB.OpenConnection();
            }

            cmd = new MySqlCommand(query, global.myDB.connection);

            try
            {
                dataGlobal = cmd.ExecuteReader();
            }
            catch (Exception)
            {
                global.myDB.OpenConnection();
                cmd = new MySqlCommand(query, global.myDB.connection);
                dataGlobal = cmd.ExecuteReader();
            }


            while (dataGlobal.Read())
            {
                strRastreios[i] = dataGlobal["RASTREIO"].ToString();
                i = i + 1;
            }

            dataGlobal.Close();

            strXMLRastreio = GerarXMLCorreios(strRastreios);

            try
            {
                var ws = new WSCorreios.AtendeClienteClient();
                var resposta = ws.fechaPlpVariosServicos(strXMLRastreio, long.Parse(strNumeroEnvio), strIdCartaoPostagem, strRastreios, strUser, strSenha);

                AtualizaRastreioEnviado(strRastreios, resposta.ToString());
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro ao efetuar fechamento rastreio: " + ex.Message);
            }
            System.Console.ReadLine();

            if (global.myDB.connection.State != ConnectionState.Closed)
            {
                global.myDB.CloseConnection();
            }
        }

        private static void AtualizaRastreioEnviado(string[] strRastreio, string strResposta)
        {
            string query;

            for (int t = 0; t < strRastreio.Length; t++)
            {

                query = " SELECT * FROM RASTREIO_CORREIOS ";

                query = query + " WHERE prefixo = substring('" + strRastreio[t] + "', 1, 2) ";
                query = query + " AND NUMERO = substring('" + strRastreio[t] + "', 3, 8);";


                if (global.myDB.connection.State == ConnectionState.Closed)
                {
                    global.myDB.OpenConnection();
                }

                MySqlCommand cmd = new MySqlCommand(query, global.myDB.connection);

                dataGlobal.Close();


                try
                {
                    dataGlobal = cmd.ExecuteReader();
                }
                catch (Exception)
                {
                    global.myDB.OpenConnection();
                    cmd = new MySqlCommand(query, global.myDB.connection);
                    dataGlobal = cmd.ExecuteReader();
                }

                //***************************************************************************************************************************************************************
                if(dataGlobal.Read())
                {

                    query = "UPDATE RASTREIO_CORREIOS SET SINCRONIZADO = 1, PLP = '" + strResposta + "'";

                    query = query + " WHERE ID = " + double.Parse(dataGlobal["ID"].ToString());

                    dataGlobal.Close();

                    if (global.myDB.connection.State == ConnectionState.Closed)
                    {
                        global.myDB.OpenConnection();
                    }

                    MySqlCommand cmd1 = new MySqlCommand(query, global.myDB.connection);

                    try
                    {
                        cmd1.ExecuteNonQuery();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message);
                    }
                }
            }

            if (global.myDB.connection.State != ConnectionState.Closed)
            {
                global.myDB.CloseConnection();
            }

        }

        private static string GerarXMLCorreios(string[] strRastreios)
        {
            string strXML = "";
            string queryPedido;
            MySqlCommand cmd;
            string strNOME, strTEL, strEmail, strLOGRADOURO, strCIDADE, strUF, strCEP;
            string strSEDEX;
            double dblPESO, dblCOMPRIMENTO, dblALTURA, dblLARGURA;
            int i;

            strXML = "<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?>";
            strXML = strXML + "<correioslog>";
            strXML = strXML + "<tipo_arquivo>Postagem</tipo_arquivo>";
            strXML = strXML + "<versao_arquivo>2.3</versao_arquivo>";

            strXML = strXML + "<plp>";
            strXML = strXML + "<id_plp></id_plp>";
            strXML = strXML + "<valor_global></valor_global> ";
            strXML = strXML + "<mcu_unidade_postagem></mcu_unidade_postagem>";
            strXML = strXML + "<nome_unidade_postagem></nome_unidade_postagem>";
            strXML = strXML + "<cartao_postagem>" + strIdCartaoPostagem + "</cartao_postagem>";
            strXML = strXML + "</plp>";

            strXML = strXML + "<remetente>";
            strXML = strXML + "<numero_contrato>" + strIdContrato + "</numero_contrato>";            
            strXML = strXML + "<numero_diretoria>72</numero_diretoria>"; //CONFIRMAR
            strXML = strXML + "<codigo_administrativo>" + strCodigoAdm + "</codigo_administrativo>";
            strXML = strXML + "<nome_remetente>" + strRemetente_Nome + "</nome_remetente>";
            strXML = strXML + "<logradouro_remetente>" + strRemetente_Logradouro + "</logradouro_remetente>";
            strXML = strXML + "<numero_remetente>" + strRemetente_Numero + "</numero_remetente>";
            strXML = strXML + "<complemento_remetente>" + strRemetente_Complemento + "</complemento_remetente>";
            strXML = strXML + "<bairro_remetente>" + strRemetente_Bairro + "</bairro_remetente>";
            strXML = strXML + "<cep_remetente>" + strRemetente_CEP + "</cep_remetente>";
            strXML = strXML + "<cidade_remetente>" + strRemetente_Cidade + "</cidade_remetente>";
            strXML = strXML + "<uf_remetente>" + strRemetente_UF + "</uf_remetente>";
            strXML = strXML + "<telefone_remetente>" + strRemetente_Telefone + "</telefone_remetente>";
            strXML = strXML + "<fax_remetente>" + strRemetente_Fax + "</fax_remetente>";
            strXML = strXML + "<email_remetente>" + strRemetente_Email + "</email_remetente>";
            strXML = strXML + "</remetente>";

            strXML = strXML + "<forma_pagamento/>";

            for (int t = 0; t < strRastreios.Length; t++)
            {
                //**************************************************************************************************************************************************************
                queryPedido = "SELECT SO.weight PESO, COMPRIMENTO.value COMPRIMENTO , ALTURA.value ALTURA, LARGURA.value LARGURA, CONCAT(SO.customer_firstname ,' ', SO.customer_lastname) NOME,  ";
                queryPedido = queryPedido + " SA.telephone TEL, SA.email, SA.street LOGRADOURO, SA.city CIDADE, SA.region UF, SA.postcode CEP, RS.SEDEX ";
                queryPedido = queryPedido + " FROM RASTREIO_CORREIOS RS ";

                queryPedido = queryPedido + " INNER JOIN sales_flat_order SO ";
                queryPedido = queryPedido + " ON SO.increment_id = RS.PEDIDO ";

                queryPedido = queryPedido + " INNER JOIN sales_flat_order_address SA ";
                queryPedido = queryPedido + " ON SO.entity_id = SA.parent_id ";
                queryPedido = queryPedido + " AND SA.address_type = 'shipping' ";


                queryPedido = queryPedido + " INNER JOIN sales_flat_order_item SI ";
                queryPedido = queryPedido + " ON SO.entity_id = SI.order_id ";

                queryPedido = queryPedido + " INNER JOIN catalog_product_entity_int COMPRIMENTO ";
                queryPedido = queryPedido + " ON COMPRIMENTO.entity_id = SI.product_id ";
                queryPedido = queryPedido + " AND COMPRIMENTO.attribute_id = 157 ";

                queryPedido = queryPedido + " INNER JOIN catalog_product_entity_int ALTURA ";
                queryPedido = queryPedido + " ON ALTURA.entity_id = SI.product_id ";
                queryPedido = queryPedido + " AND ALTURA.attribute_id = 158 ";

                queryPedido = queryPedido + " INNER JOIN catalog_product_entity_int LARGURA ";
                queryPedido = queryPedido + " ON LARGURA.entity_id = SI.product_id ";
                queryPedido = queryPedido + " AND LARGURA.attribute_id = 159 ";

                queryPedido = queryPedido + " WHERE RS.prefixo = substring('" + strRastreios[t] + "', 1, 2) ";
                queryPedido = queryPedido + " AND RS.NUMERO = substring('" + strRastreios[t] + "', 3, 8);";


                if (global.myDB.connection.State == ConnectionState.Closed)
                {
                    global.myDB.OpenConnection();
                }

                cmd = new MySqlCommand(queryPedido, global.myDB.connection);

                dataGlobal.Close();


                try
                {
                    dataGlobal = cmd.ExecuteReader();
                }
                catch (Exception)
                {
                    global.myDB.OpenConnection();
                    cmd = new MySqlCommand(queryPedido, global.myDB.connection);
                    dataGlobal = cmd.ExecuteReader();
                }

                dblPESO = 10;
                dblCOMPRIMENTO = 10;
                dblALTURA = 10;
                dblLARGURA = 10;
                strNOME = "GIOVANINI";
                strTEL = "972535480";
                strEmail = "a.giovanini@gmail.com";
                strLOGRADOURO = "Aniz tanus resek 285 conjunto cocuera";
                strCIDADE = "Mogi das cruzes";
                strUF = "SP";
                strCEP = "08793020";
                strSEDEX = "0";

                //***************************************************************************************************************************************************************
                while (dataGlobal.Read())
                {

                    dblPESO = dblPESO + double.Parse(dataGlobal["PESO"].ToString());
                    /*
                    if (double.Parse(dataGlobal["COMPRIMENTO"].ToString()) > dblCOMPRIMENTO)
                    {
                        dblCOMPRIMENTO = double.Parse(dataGlobal["COMPRIMENTO"].ToString());
                    }
                    
                    dblALTURA = dblALTURA + double.Parse(dataGlobal["ALTURA"].ToString());
                    
                    if (double.Parse(dataGlobal["LARGURA"].ToString()) > dblLARGURA)
                    {
                        dblLARGURA = double.Parse(dataGlobal["LARGURA"].ToString());
                    }

                    strNOME = dataGlobal["NOME"].ToString();
                    strTEL = dataGlobal["TEL"].ToString();
                    strEmail = dataGlobal["EMAIL"].ToString();
                    strLOGRADOURO = dataGlobal["LOGRADOURO"].ToString().Replace("\n", " ");
                    strCIDADE = dataGlobal["CIDADE"].ToString();
                    strUF = dataGlobal["UF"].ToString();

                    if(strUF.ToString().Length > 2)
                    {
                        strUF = CorrigeUF(strUF);
                    }
                    */
                    strCEP = dataGlobal["CEP"].ToString();
                    strSEDEX = dataGlobal["SEDEX"].ToString();
                }

                strRemetente_Numero = "";

                
                for (int t2 = 0; t2 < strLOGRADOURO.Length; t2++)
                {
                    if(int.TryParse(strLOGRADOURO.Substring(t2, 1), out i))
                    {
                        strRemetente_Numero = strRemetente_Numero + strLOGRADOURO.Substring(t2, 1);
                    }
                    else if(int.TryParse(strLOGRADOURO.Substring(t2, 1), out i) == false && strRemetente_Numero != "")
                    {
                        break;
                    }

                   //i = i + 1;//

                }

                strXML = strXML + "<objeto_postal>";
                strXML = strXML + "<numero_etiqueta>" + strRastreios[t] + "</numero_etiqueta>";
                strXML = strXML + "<codigo_objeto_cliente></codigo_objeto_cliente>";

                if (strSEDEX == "1")
                {
                    strXML = strXML + "<codigo_servico_postagem>" + strIdServicoSedex.Trim() + "</codigo_servico_postagem>";
                }
                else
                {
                    strXML = strXML + "<codigo_servico_postagem>" + strIdServicoPAC.Trim() + "</codigo_servico_postagem>";
                }


                strXML = strXML + "<cubagem>0.00</cubagem>";
                strXML = strXML + "<peso>" + dblPESO.ToString().Replace(",",".") + "</peso>";
                strXML = strXML + "<rt1></rt1><rt2></rt2>";

                strXML = strXML + "<destinatario> ";
                    strXML = strXML + "<nome_destinatario>" + strNOME + "</nome_destinatario>";
                    strXML = strXML + "<telefone_destinatario>" + strTEL + "</telefone_destinatario>";
                    strXML = strXML + "<celular_destinatario>" + strTEL + "</celular_destinatario>";
                    strXML = strXML + "<email_destinatario>" + strEmail + "</email_destinatario>";
                    strXML = strXML + "<logradouro_destinatario>" + strLOGRADOURO + "</logradouro_destinatario>";
                    strXML = strXML + "<complemento_destinatario></complemento_destinatario>";
                    strXML = strXML + "<numero_end_destinatario>" + strRemetente_Numero + "</numero_end_destinatario>";
                strXML = strXML + "</destinatario>";

                strXML = strXML + "<nacional> ";
                    strXML = strXML + "<bairro_destinatario></bairro_destinatario>";
                    strXML = strXML + "<cidade_destinatario>" + strCIDADE + "</cidade_destinatario>";
                    strXML = strXML + "<uf_destinatario>" + strUF + "</uf_destinatario>";
                    strXML = strXML + "<cep_destinatario>" + strCEP + "</cep_destinatario>";
                    strXML = strXML + "<codigo_usuario_postal></codigo_usuario_postal>";
                    strXML = strXML + "<centro_custo_cliente></centro_custo_cliente>";
                    strXML = strXML + "<numero_nota_fiscal></numero_nota_fiscal>";
                    strXML = strXML + "<serie_nota_fiscal></serie_nota_fiscal>";
                    strXML = strXML + "<valor_nota_fiscal></valor_nota_fiscal>";
                    strXML = strXML + "<natureza_nota_fiscal></natureza_nota_fiscal>";
                    strXML = strXML + "<descricao_objeto></descricao_objeto>";
                    strXML = strXML + "<valor_a_cobrar></valor_a_cobrar>";
                strXML = strXML + "</nacional>";

                strXML = strXML + "<servico_adicional>";
                    strXML = strXML + "<codigo_servico_adicional>025</codigo_servico_adicional>";
                    strXML = strXML + "<valor_declarado></valor_declarado>";
                strXML = strXML + "</servico_adicional>";


                strXML = strXML + "<dimensao_objeto>";
                    strXML = strXML + "<tipo_objeto>002</tipo_objeto>";
                    strXML = strXML + "<dimensao_altura>" + dblALTURA + "</dimensao_altura>";
                    strXML = strXML + "<dimensao_largura>" + dblLARGURA + "</dimensao_largura>";
                    strXML = strXML + "<dimensao_comprimento>" + dblCOMPRIMENTO + "</dimensao_comprimento>";
                    strXML = strXML + "<dimensao_diametro>0</dimensao_diametro>";
                strXML = strXML + "</dimensao_objeto>";

                strXML = strXML + "<data_postagem_sara></data_postagem_sara>";
                strXML = strXML + "<status_processamento>0</status_processamento>";
                strXML = strXML + "<numero_comprovante_postagem></numero_comprovante_postagem>";
                strXML = strXML + "<valor_cobrado></valor_cobrado>";
                strXML = strXML + "</objeto_postal>";
                //**************************************************************************************************************************************************************

            }
            strXML = strXML + "</correioslog>";

            dataGlobal.Close();

            if (global.myDB.connection.State != ConnectionState.Closed)
            {
                global.myDB.CloseConnection();
            }

            return strXML;
        }

        private static void GravaRastreio(string strRastreio, bool blSedex)
        {

            String query;

            query = "INSERT INTO RASTREIO_CORREIOS (numero, prefixo, sufixo, sedex, pac)";
            query = query + " VALUES ( '";
            query = query + strRastreio.Substring(2, 8) + "', '" + strRastreio.Substring(0, 2) + "', '" + strRastreio.Substring(11, 2) + "',  ";

            if (blSedex == true)
            {
                query = query + " true, false);";
            }
            else
            {
                query = query + " false, true);";
            }

            global.myDB.CloseConnection();

            if (global.myDB.connection.State == ConnectionState.Closed)
            {
                global.myDB.OpenConnection();
            }

            MySqlCommand cmd1 = new MySqlCommand(query, global.myDB.connection);

            try
            {
                cmd1.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Informa");
            }

            if (global.myDB.connection.State != ConnectionState.Closed)
            {
                global.myDB.CloseConnection();
            }
        }

        private static string CorrigeUF(string strEstado)
        {
            string strUF = strEstado;

            if (strEstado == "Acre")
            {
                strUF = "AC";
            }
            if (strEstado == "Alagoas")
            {
                strUF = "AL";
            }
            if (strEstado == "Amapá" || strEstado == "Amapa")
            {
                strUF = "AP";
            }
            if (strEstado == "Amazonas")
            {
                strUF = "AM";
            }
            if (strEstado == "Bahia")
            {
                strUF = "BA";
            }
            if (strEstado == "ceará"|| strEstado == "ceara")
            {
                strUF = "CE";
            }
            if (strEstado == "DistritoFederal" || strEstado == "Distrito Federal" || strEstado == "BRASILIA")
            {
                strUF = "DF";
            }
            if (strEstado == "EspíritoSanto" || strEstado == "Espírito Santo" || strEstado == "Espirito Santo")
            {
                strUF = "ES";
            }
            if (strEstado == "Goiás" || strEstado == "Goias")
            {
                strUF = "GO";
            }

            if (strEstado == "Maranhão" || strEstado == "Maranhao")
            {
                strUF = "MA";
            }
            if (strEstado == "MatoGrosso")
            {
                strUF = "MT";
            }
            if (strEstado == "MatoGrossodoSul" || strEstado == "Mato Grosso do Sul")
            {
                strUF = "MS";
            }
            if (strEstado == "MinasGerais" || strEstado == "Minas Gerais")
            {
                strUF = "MG";
            }
            if (strEstado == "Paraná" || strEstado == "Parana")
            {
                strUF = "PR";
            }
            if (strEstado == "Paraíba" || strEstado == "Paraiba")
            {
                strUF = "PB";
            }
            if (strEstado == "Pará" || strEstado == "Pará")
            {
                strUF = "PA";
            }
            if (strEstado == "Pernambuco")
            {
                strUF = "PE";
            }
            if (strEstado == "Piauí"|| strEstado == "Piaui")
            {
                strUF = "PI";
            }
            if (strEstado == "RiodeJaneiro" || strEstado == "Rio de Janeiro")
            {
                strUF = "RJ";
            }
            if (strEstado == "RioGrandedoNorte" || strEstado == "Rio Grandedo Norte")
            {
                strUF = "RN";
            }
            if (strEstado == "RioGrandedoSul" || strEstado == "Rio Grande do Sul")
            {
                strUF = "RS";
            }
            if (strEstado == "Rondônia" || strEstado == "Rondonia")
            {
                strUF = "RO";
            }
            if (strEstado == "Roraima")
            {
                strUF = "RR";
            }
            if (strEstado == "SantaCatarina" || strEstado == "Santa Catarina")
            {
                strUF = "SC";
            }
            if (strEstado == "Sergipe")
            {
                strUF = "SE";
            }
            if (strEstado == "SãoPaulo" || strEstado == "São Paulo" || strEstado == "sao paulo" || strEstado == "Sao Paulo")
            {
                strUF = "SP";
            }
            if (strEstado == "Tocantins")
            {
                strUF = "TO";
            }
            return strUF;
        }
        public static string AcertaSenha(string _login, string _senha)
        {
            System.Text.StringBuilder senha = new System.Text.StringBuilder();

            MD5 md5 = MD5.Create();
            byte[] entrada = System.Text.Encoding.ASCII.GetBytes(_login + "//" + _senha);
            byte[] hash = md5.ComputeHash(entrada);
            System.Text.StringBuilder sb = new System.Text.StringBuilder();
            for (int i = 0; i < hash.Length; i++)
            {
                senha.Append(hash[i].ToString("X2"));
            }
            return senha.ToString();
        }

    }

}