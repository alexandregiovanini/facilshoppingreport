﻿namespace FacilShoppingReports
{
    partial class frmCadastroCombo
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmCadastroCombo));
            this.tabPrincipal = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.label4 = new System.Windows.Forms.Label();
            this.btnSair2 = new System.Windows.Forms.Button();
            this.lstLista = new System.Windows.Forms.ListView();
            this.txtPesquisa = new System.Windows.Forms.TextBox();
            this.btPesquisar = new System.Windows.Forms.Button();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.cmdSaida = new System.Windows.Forms.Button();
            this.cmdEntrada = new System.Windows.Forms.Button();
            this.btnGrava = new System.Windows.Forms.Button();
            this.btnSair = new System.Windows.Forms.Button();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.lstCombos = new System.Windows.Forms.ListView();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.txtSKUMagento = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtSKUCombo = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.tabPrincipal.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox6.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabPrincipal
            // 
            this.tabPrincipal.Controls.Add(this.tabPage1);
            this.tabPrincipal.Controls.Add(this.tabPage2);
            this.tabPrincipal.Location = new System.Drawing.Point(7, 7);
            this.tabPrincipal.Name = "tabPrincipal";
            this.tabPrincipal.SelectedIndex = 0;
            this.tabPrincipal.Size = new System.Drawing.Size(700, 321);
            this.tabPrincipal.TabIndex = 2;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.label4);
            this.tabPage1.Controls.Add(this.btnSair2);
            this.tabPage1.Controls.Add(this.lstLista);
            this.tabPage1.Controls.Add(this.txtPesquisa);
            this.tabPage1.Controls.Add(this.btPesquisar);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(692, 295);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Procurar";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(16, 21);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(49, 13);
            this.label4.TabIndex = 20;
            this.label4.Text = "Localizar";
            // 
            // btnSair2
            // 
            this.btnSair2.Image = global::FacilShoppingReports.Properties.Resources.exit_icon;
            this.btnSair2.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnSair2.Location = new System.Drawing.Point(595, 3);
            this.btnSair2.Name = "btnSair2";
            this.btnSair2.Size = new System.Drawing.Size(87, 40);
            this.btnSair2.TabIndex = 19;
            this.btnSair2.Text = "Sair";
            this.btnSair2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnSair2.UseVisualStyleBackColor = true;
            this.btnSair2.Click += new System.EventHandler(this.btnSair2_Click);
            // 
            // lstLista
            // 
            this.lstLista.FullRowSelect = true;
            this.lstLista.GridLines = true;
            this.lstLista.Location = new System.Drawing.Point(10, 49);
            this.lstLista.Name = "lstLista";
            this.lstLista.Size = new System.Drawing.Size(672, 240);
            this.lstLista.TabIndex = 18;
            this.lstLista.UseCompatibleStateImageBehavior = false;
            this.lstLista.View = System.Windows.Forms.View.Details;
            this.lstLista.SelectedIndexChanged += new System.EventHandler(this.lstLista_SelectedIndexChanged);
            this.lstLista.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.lstLista_MouseDoubleClick);
            // 
            // txtPesquisa
            // 
            this.txtPesquisa.Location = new System.Drawing.Point(71, 16);
            this.txtPesquisa.Name = "txtPesquisa";
            this.txtPesquisa.Size = new System.Drawing.Size(267, 20);
            this.txtPesquisa.TabIndex = 17;
            this.txtPesquisa.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtPesquisa_KeyPress);
            // 
            // btPesquisar
            // 
            this.btPesquisar.Image = global::FacilShoppingReports.Properties.Resources.search_icon1;
            this.btPesquisar.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
            this.btPesquisar.Location = new System.Drawing.Point(504, 3);
            this.btPesquisar.Name = "btPesquisar";
            this.btPesquisar.Size = new System.Drawing.Size(85, 40);
            this.btPesquisar.TabIndex = 16;
            this.btPesquisar.Text = "Pesquisar";
            this.btPesquisar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btPesquisar.UseVisualStyleBackColor = true;
            this.btPesquisar.Click += new System.EventHandler(this.btPesquisar_Click);
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.groupBox2);
            this.tabPage2.Controls.Add(this.groupBox6);
            this.tabPage2.Controls.Add(this.groupBox1);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(692, 295);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Cadastro";
            this.tabPage2.UseVisualStyleBackColor = true;
            this.tabPage2.Click += new System.EventHandler(this.tabPage2_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.cmdSaida);
            this.groupBox2.Controls.Add(this.cmdEntrada);
            this.groupBox2.Controls.Add(this.btnGrava);
            this.groupBox2.Controls.Add(this.btnSair);
            this.groupBox2.Location = new System.Drawing.Point(304, 233);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(376, 50);
            this.groupBox2.TabIndex = 25;
            this.groupBox2.TabStop = false;
            // 
            // cmdSaida
            // 
            this.cmdSaida.Image = global::FacilShoppingReports.Properties.Resources.Oxygen_Icons_org_Oxygen_Actions_bookmarks_organize;
            this.cmdSaida.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.cmdSaida.Location = new System.Drawing.Point(99, 8);
            this.cmdSaida.Name = "cmdSaida";
            this.cmdSaida.Size = new System.Drawing.Size(85, 40);
            this.cmdSaida.TabIndex = 18;
            this.cmdSaida.Text = "Excluir";
            this.cmdSaida.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.cmdSaida.UseVisualStyleBackColor = true;
            this.cmdSaida.Click += new System.EventHandler(this.cmdSaida_Click);
            // 
            // cmdEntrada
            // 
            this.cmdEntrada.Image = global::FacilShoppingReports.Properties.Resources.new_icon;
            this.cmdEntrada.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.cmdEntrada.Location = new System.Drawing.Point(192, 8);
            this.cmdEntrada.Name = "cmdEntrada";
            this.cmdEntrada.Size = new System.Drawing.Size(85, 40);
            this.cmdEntrada.TabIndex = 17;
            this.cmdEntrada.Text = "Novo";
            this.cmdEntrada.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.cmdEntrada.UseVisualStyleBackColor = true;
            this.cmdEntrada.Click += new System.EventHandler(this.cmdEntrada_Click);
            // 
            // btnGrava
            // 
            this.btnGrava.Image = ((System.Drawing.Image)(resources.GetObject("btnGrava.Image")));
            this.btnGrava.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnGrava.Location = new System.Drawing.Point(6, 8);
            this.btnGrava.Name = "btnGrava";
            this.btnGrava.Size = new System.Drawing.Size(85, 40);
            this.btnGrava.TabIndex = 16;
            this.btnGrava.Text = "Grava";
            this.btnGrava.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnGrava.UseVisualStyleBackColor = true;
            this.btnGrava.Click += new System.EventHandler(this.btnGrava_Click);
            // 
            // btnSair
            // 
            this.btnSair.Image = global::FacilShoppingReports.Properties.Resources.exit_icon;
            this.btnSair.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnSair.Location = new System.Drawing.Point(285, 8);
            this.btnSair.Name = "btnSair";
            this.btnSair.Size = new System.Drawing.Size(85, 40);
            this.btnSair.TabIndex = 12;
            this.btnSair.Text = "Sair";
            this.btnSair.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnSair.UseVisualStyleBackColor = true;
            this.btnSair.Click += new System.EventHandler(this.btnSair_Click);
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.lstCombos);
            this.groupBox6.Location = new System.Drawing.Point(8, 67);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(678, 160);
            this.groupBox6.TabIndex = 24;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "Itens do Combo";
            // 
            // lstCombos
            // 
            this.lstCombos.FullRowSelect = true;
            this.lstCombos.GridLines = true;
            this.lstCombos.Location = new System.Drawing.Point(7, 17);
            this.lstCombos.Name = "lstCombos";
            this.lstCombos.Size = new System.Drawing.Size(665, 135);
            this.lstCombos.TabIndex = 19;
            this.lstCombos.UseCompatibleStateImageBehavior = false;
            this.lstCombos.View = System.Windows.Forms.View.Details;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.txtSKUMagento);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.txtSKUCombo);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Location = new System.Drawing.Point(5, 2);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(557, 55);
            this.groupBox1.TabIndex = 20;
            this.groupBox1.TabStop = false;
            // 
            // txtSKUMagento
            // 
            this.txtSKUMagento.Location = new System.Drawing.Point(359, 19);
            this.txtSKUMagento.MaxLength = 75;
            this.txtSKUMagento.Name = "txtSKUMagento";
            this.txtSKUMagento.Size = new System.Drawing.Size(175, 20);
            this.txtSKUMagento.TabIndex = 0;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(279, 22);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(74, 13);
            this.label3.TabIndex = 14;
            this.label3.Text = "SKU Magento";
            // 
            // txtSKUCombo
            // 
            this.txtSKUCombo.AcceptsReturn = true;
            this.txtSKUCombo.AcceptsTab = true;
            this.txtSKUCombo.Location = new System.Drawing.Point(76, 19);
            this.txtSKUCombo.Name = "txtSKUCombo";
            this.txtSKUCombo.ReadOnly = true;
            this.txtSKUCombo.Size = new System.Drawing.Size(176, 20);
            this.txtSKUCombo.TabIndex = 11;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(8, 22);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(65, 13);
            this.label1.TabIndex = 10;
            this.label1.Text = "SKU Combo";
            // 
            // frmCadastroCombo
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.ClientSize = new System.Drawing.Size(712, 336);
            this.ControlBox = false;
            this.Controls.Add(this.tabPrincipal);
            this.Name = "frmCadastroCombo";
            this.Text = "Cadastro de Combos";
            this.tabPrincipal.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox6.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabPrincipal;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button btnSair2;
        private System.Windows.Forms.ListView lstLista;
        private System.Windows.Forms.TextBox txtPesquisa;
        private System.Windows.Forms.Button btPesquisar;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox txtSKUMagento;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtSKUCombo;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button cmdSaida;
        private System.Windows.Forms.Button cmdEntrada;
        private System.Windows.Forms.Button btnGrava;
        private System.Windows.Forms.Button btnSair;
        private System.Windows.Forms.ListView lstCombos;
    }
}