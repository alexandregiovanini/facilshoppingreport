DROP PROCEDURE IF EXISTS spINCLUIR_PEDIDO_LUDOSTORE; 

DELIMITER $$
CREATE PROCEDURE spINCLUIR_PEDIDO_LUDOSTORE()
BEGIN
  
	DECLARE v_finished INTEGER DEFAULT 0; 
	DECLARE v_pedido varchar(100) DEFAULT "";
    DECLARE v_idLudostore varchar(100) DEFAULT "";
    DECLARE v_entity_id  VARCHAR(30) DEFAULT "";
    DECLARE v_VlTotal DOUBLE(6,2) DEFAULT 0;
    DECLARE v_Count integer DEFAULT 0;
    DECLARE v_quote_id  VARCHAR(30) DEFAULT "";
    DECLARE v_cep VARCHAR(10) DEFAULT "";
    DECLARE v_nome VARCHAR(60) DEFAULT "";
    DECLARE v_endereco VARCHAR(60) DEFAULT "";
    DECLARE v_cidade VARCHAR(60) DEFAULT "";
    DECLARE v_increment_id VARCHAR (20) DEFAULT '';
    DECLARE v_temp VARCHAR (20) DEFAULT '';
    
	DEClARE cursorPedidoLudostore CURSOR FOR SELECT PL.id, PL.id_pedido, PL.cep, PL.nome, PL.endereco, PL.cidade 
											 FROM PEDIDO_LUDOSTORE PL
	                                         WHERE PL.integrado = 0 and PL.status in ('AGUARDANDO ENVIO', 'ENTREGUE', 'ENVIADO', 'PAGAMENTO EFETUADO')
                                             AND PL.id_pedido NOT IN (SELECT id_pedido FROM ITENS_PEDIDO_LUDOSTORE IP WHERE SKU = '');

	DECLARE CONTINUE HANDLER FOR NOT FOUND SET v_finished = 1;

	OPEN cursorPedidoLudostore;
 
		get_Pedido: LOOP
	 
			FETCH cursorPedidoLudostore INTO v_idLudostore, v_pedido, v_cep, v_nome, v_endereco, v_cidade;
		 
			IF v_finished = 1 THEN 
				LEAVE get_Pedido;
			END IF;
		 
			select max(entity_id) + 1,  max(quote_id) + 1 INTO v_entity_id, v_quote_id from sales_flat_order;
			select sum(VL_TOTAL), count(*) INTO v_VlTotal, v_Count FROM ITENS_PEDIDO_LUDOSTORE WHERE id_pedido = v_pedido;        
			select max(increment_id) + 1 INTO v_increment_id from sales_flat_order;

			/****************************************************************************************************/
            
			UPDATE eav_entity_store
			INNER JOIN eav_entity_type ON eav_entity_type.entity_type_id = eav_entity_store.entity_type_id
			SET eav_entity_store.increment_last_id = v_increment_id
			WHERE eav_entity_type.entity_type_code='order';
			

			/****************************************************************************************************/			
			INSERT into sales_flat_order (entity_id, state, status, shipping_description, is_virtual, store_id, customer_id, base_discount_amount,base_grand_total, base_shipping_amount, 
										  base_shipping_tax_amount, base_subtotal_invoiced, base_tax_amount, base_tax_invoiced, base_to_global_rate, base_to_order_rate, base_total_invoiced, base_total_invoiced_cost,
										  base_total_paid, discount_amount, discount_invoiced, grand_total, shipping_invoiced, shipping_tax_amount, store_to_base_rate, store_to_order_rate, subtotal, subtotal_invoiced,
										  tax_amount, tax_invoiced, total_invoiced, total_paid, total_qty_ordered, customer_is_guest, customer_note_notify, billing_address_id, customer_group_id, email_sent,
										  quote_id, shipping_address_id, base_shipping_discount_amount, base_subtotal_incl_tax, base_total_due, shipping_discount_amount, subtotal_incl_tax, total_due, weight, customer_dob,
										  increment_id, base_currency_code, customer_email, customer_firstname, customer_lastname, customer_taxvat, global_currency_code, order_currency_code, shipping_method,
										  store_currency_code, store_name, created_at, updated_at, total_item_count, hidden_tax_amount, base_hidden_tax_amount, shipping_hidden_tax_amount, base_shipping_hidden_tax_amnt,
										  hidden_tax_invoiced, base_hidden_tax_invoiced, shipping_incl_tax, base_shipping_incl_tax, paypal_ipn_customer_notified, canal, canal_id, fee_amount, base_fee_amount, fee_amount_invoiced,
										  base_fee_amount_refunded, fee_amount_refunded)
			values
										(v_entity_id, 'complete', 'aprovado', 'Frete Grátis Mogi e Região - Apenas para Mogi, Suzano, Poá, Ferraz e Itaquá.', 0, 1, 813, 
										0, v_VlTotal, 0, 0, v_VlTotal, 0, 0, 1, 1, v_VlTotal, 0, v_VlTotal, 0, 0, v_VlTotal, 0, 0, 1, 1, v_VlTotal, v_VlTotal,
										0, 0, v_VlTotal, v_VlTotal, v_VlTotal, 0, 0, 18077, 1, 1, v_quote_id, 18078, 0, v_VlTotal, 0, 0, v_VlTotal, 0, 0,
										now(), v_increment_id, 'BRL', 'ludostore@facilcard.com.br', 'LudoStore', 'Ludopedia', 0, 'BRL', 'BRL', 'freeshipping_freeshipping', 'BRL', 
										'Fácil Shopping Fácil Shopping Default Store View',  now(), now(), v_Count, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'LUDOSTORE', v_pedido, 0, 0, 0,0,0);
										
			/****************************************************************************************************/
			INSERT INTO sales_flat_order_payment
			(
			entity_id, parent_id, base_amount_paid, amount_paid, base_amount_ordered, amount_ordered, method
			)
			VALUES
			(
				v_entity_id, v_entity_id, v_VlTotal, v_VlTotal, v_VlTotal, v_VlTotal, 'LUDOSTORE'
			);
			
            /****************************************************************************************************/
            
            select max(entity_id) + 1 INTO v_temp from sales_flat_order_address;
            
			INSERT INTO sales_flat_order_address 
			(
				entity_id, parent_id, customer_address_id, customer_id, postcode, lastname, street, city, email, telephone, country_id, firstname, address_type 		
			)
			VALUES
			(
				v_temp, v_entity_id, 705, 813, v_cep, "", v_endereco, v_cidade, 'ludostore@facilcard.com.br', '', 'BR', v_nome, 'shipping' 		
			);
            /****************************************************************************************************/
			INSERT INTO sales_flat_order_grid 
			(
				entity_id, status, store_id, store_name, customer_id, base_grand_total, base_total_paid, grand_total, total_paid, increment_id, base_currency_code, order_currency_code, shipping_name, billing_name, created_at,updated_at 
			)
			VALUES
			(
				v_entity_id, 'aprovado', 1, 'Fácil Shopping Fácil Shopping Default Store View',	813, v_VlTotal, v_VlTotal, v_VlTotal, v_VlTotal, v_increment_id, 'BR', 'BR', v_nome, 'LudoStore  Ludopedia',  now(), now()					            
			);
			/****************************************************************************************************/
            
             select max(entity_id) + 1 INTO v_temp from sales_flat_order_status_history;
             
			INSERT INTO sales_flat_order_status_history 
			(
				entity_id, parent_id, is_customer_notified, is_visible_on_front, comment, status, created_at, entity_name
			)
			VALUES
			(
				v_temp, v_entity_id, 0,	1, 'INTEGRADO LUDOSTORE', 'processing', now(), 'order'
			);
			/****************************************************************************************************/			
            select max(entity_id) + 1 INTO v_temp from sales_flat_quote;
            
			 INSERT INTO sales_flat_quote 
			 (
				entity_id, store_id, created_at, updated_at, is_active, is_virtual, is_multi_shipping, items_count, items_qty, orig_order_id, store_to_base_rate, base_currency_code, store_currency_code, quote_currency_code, grand_total, base_grand_total, customer_id, customer_tax_class_id, customer_group_id, customer_email, customer_firstname, customer_lastname, customer_dob, customer_note_notify, customer_is_guest, reserved_order_id, global_currency_code, base_to_global_rate, customer_taxvat, subtotal, base_subtotal, subtotal_with_discount, is_changed, trigger_recollect, is_persistent 
			 )
			 VALUES
			 (
				v_temp, 1, now(), now(), 0, 0, 0, v_Count, v_Count, 0, 1, 'BRL', 'BRL', 'BRL', v_VlTotal, v_VlTotal, 813, 3, 1, 'ludostore@facilcard.com.br', 'LudoStore', 'LudoStore', '2018-05-01 00:00:00', 0, 0, v_increment_id, 'BR', 1, 0, v_VlTotal, v_VlTotal, v_VlTotal, 1, 0, 0
			 );
			 /******************************************************************************************************************
				INSERT sales_flat_quote_address 
				(
					address_id, quote_id, created_at, updated_at, customer_id, save_in_address_book, address_type, email, firstname, lastname, street, city, postcode, country_id, telephone, same_as_billing, free_shipping, collect_shipping_rates, shipping_method, shipping_description, weight, subtotal, base_subtotal, subtotal_with_discount, base_subtotal_with_discount, tax_amount, base_tax_amount, shipping_amount, base_shipping_amount, shipping_tax_amount, base_shipping_tax_amount, discount_amount, base_discount_amount, grand_total, base_grand_total, subtotal_incl_tax, shipping_incl_tax, base_shipping_incl_tax, fee_amount, base_fee_amount
				) 
				VALUES
				(
					7689, v_quote_id, NOW(), NOW(), 813,	0,	705, 'billing',	'ludostore@facilcard.com.br', 'LudoStore', 'Ludopedia' 'Rua Ipiranga 957 Sala 1 Jardim Santista', 'Mogi das Cruzes', '08730000', 'BS', '1138830100', 0,	0,	0,'','', 0.0000,	0.0000,	0.0000,	0.0000,	0.0000,	0.0000,	0.0000,	0.0000,	0.0000,	0.0000,	0.0000,	0.0000,	0.0000,	0.0000,	0.0000, 0.0000, 0.0000,	0.0000, 0.0000,0.0000
				);

				INSERT sales_flat_quote_address 
				(
					address_id, quote_id, created_at, updated_at, customer_id, save_in_address_book, address_type, email, firstname, lastname, street, city, postcode, country_id, telephone, same_as_billing, free_shipping, collect_shipping_rates, shipping_method, shipping_description, weight, subtotal, base_subtotal, subtotal_with_discount, base_subtotal_with_discount, tax_amount, base_tax_amount, shipping_amount, base_shipping_amount, shipping_tax_amount, base_shipping_tax_amount, discount_amount, base_discount_amount, grand_total, base_grand_total, subtotal_incl_tax, shipping_incl_tax, base_shipping_incl_tax, fee_amount, base_fee_amount
				) 
				VALUES
				(
					(select max(address_id) + 1 from sales_flat_quote_address), v_quote_id, NOW(), NOW(), 813,	0,	705, 'billing',	'ludostore@facilcard.com.br', 'LudoStore', 'Ludopedia' 'Rua Ipiranga 957 Sala 1 Jardim Santista', 'Mogi das Cruzes', '08730000', 'BS', '1138830100', 1,	0,	0,'freeshipping_freeshipping','Frete Grátis Mogi e Região - Apenas para Mogi, Suzano, Poá, Ferraz e Itaquá.', 0.0000,	v_VlTotal,	v_VlTotal,	0.0000,	0.0000,	0.0000,	0.0000,	0.0000,	0.0000,	0.0000,	0.0000,	0.0000,	0.0000,	v_VlTotal,	v_VlTotal, 0.0000, 0.0000,	0.0000, 0.0000,0.0000
				);
             ********************************************************************************************************************************/
				INSERT INTO sales_flat_order_item 
				(
				item_id, order_id, quote_item_id, store_id, created_at, updated_at, product_id, product_type, is_virtual, sku, name, qty_invoiced, qty_ordered, qty_shipped, 
				price, base_price, original_price, row_total, base_row_total, base_row_invoiced, row_weight, price_incl_tax, base_price_incl_tax, row_total_incl_tax,
				base_row_total_incl_tax 
				)
				(select (select max(item_id) + 1 from sales_flat_order_item) +  @contador := @contador + 1 AS item_id, v_entity_id, v_quote_item_id, 1,  NOW(), NOW(), 
				coalesce((select entity_id from catalog_product_entity where sku =  ITENS_PEDIDO_LUDOSTORE.sku),0) product_id, 'simple', 0, 
				CASE ITENS_PEDIDO_LUDOSTORE.sku WHEN '' THEN 'PRODUTO LUDOSTORE' ELSE ITENS_PEDIDO_LUDOSTORE.sku END SKU,
				ITENS_PEDIDO_LUDOSTORE.nm_produto, ITENS_PEDIDO_LUDOSTORE.qtde, ITENS_PEDIDO_LUDOSTORE.qtde, ITENS_PEDIDO_LUDOSTORE.qtde, 
				ITENS_PEDIDO_LUDOSTORE.vl_unitario,ITENS_PEDIDO_LUDOSTORE.vl_unitario,ITENS_PEDIDO_LUDOSTORE.vl_unitario, ITENS_PEDIDO_LUDOSTORE.vl_total, 
				ITENS_PEDIDO_LUDOSTORE.vl_total,ITENS_PEDIDO_LUDOSTORE.vl_total, 0, ITENS_PEDIDO_LUDOSTORE.vl_total,ITENS_PEDIDO_LUDOSTORE.vl_total,ITENS_PEDIDO_LUDOSTORE.vl_total,ITENS_PEDIDO_LUDOSTORE.vl_total  

				FROM ITENS_PEDIDO_LUDOSTORE, (SELECT @contador := 0) AS nada

				WHERE ID_PEDIDO = v_pedido);
                
                
                /**********************************************************************************************
					INSERT INTO sales_flat_quote_payment
					(
						payment_id, quote_id, created_at, updated_at, method, cc_exp_month, cc_exp_year, cc_ss_start_month, cc_ss_start_year
					)
					VALUES
					(
						999999, 9999999, now(),now(), 'banktransfer',0,0,0,0
					);
                
                ************************************************************************************************/
				/**************************************************************************************************
					INSERT INTO sales_flat_quote_item
					(
						item_id, quote_id, created_at, updated_at, product_id, store_id, is_virtual,               sku,        name, free_shipping, is_qty_decimal, no_discount,    weight,    qty,    price, base_price, custom_price, discount_percent, discount_amount, base_discount_amount, tax_percent, tax_amount, base_tax_amount, row_total, base_row_total, row_total_with_discount, row_weight, product_type, original_custom_price, price_incl_tax, base_price_incl_tax, row_total_incl_tax, base_row_total_incl_tax, hidden_tax_amount, base_hidden_tax_amount, weee_tax_disposition, weee_tax_row_disposition, base_weee_tax_disposition, base_weee_tax_row_disposition, weee_tax_applied, weee_tax_applied_amount, weee_tax_applied_row_amount)VALUES(
						  1,      9999999,	    NOW(),      NOW(),       3032,	      1,	      0,	     '5943AUZ',	'Auztralia',	         0,	             0,	          0,	     0,	1.0000,	316.3000,	316.3000,	 316.3000,	         0.0000,	      0.0000,           	0.0000,  	 0.0000, 	 0.0000,   	      0.0000,	64.5000,    	316.3000,                  0.0000,	  0.5000,	  'simple',	              316.3000,	      316.3000,	           316.3000,	       316.3000,	            316.3000,	        0.0000,	                0.0000,               0.0000,	                0.0000,	                   0.0000,	                      0.0000,	      'a:0:{}',	                 0.0000,	0.0000	
					);
				****************************************************************************/
                
                /*************************************************************************************
								
				INSERT INTO sales_flat_quote_item_option
				(
					option_id, item_id, product_id, code, value
				)
				VALUES
				(
					1, 1, 3032, 'info_buyRequest', 'a:1:{s:3:"qty";i:1;}'
				);
               
                
                *************************************************************************************/
				/********************************************************************/		


			 /*
			 
				UPDATE cataloginventory_stock_item set qty = qty - v_quantidade where product_id = (select entity_id from catalog_product_entity where sku = v_sku);
				
				
				UPDATE ITENS_PEDIDO_LUDOSTORE
				SET integrado = 1
				WHERE id_pedido = v_pedido AND SKU = V_SKU;        
				*/
				
		/***********************************************************************/
		END LOOP get_Pedido;
	 
	 CLOSE cursorPedidoLudostore;   
	   
   
   
END $$
DELIMITER ;

CALL spINCLUIR_PEDIDO_LUDOSTORE;


 
SHOW CREATE table sales_flat_order_item;