﻿
namespace FacilShoppingReports
{
    partial class frmExibeMovEstoque
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lstEstoque = new System.Windows.Forms.ListView();
            this.SuspendLayout();
            // 
            // lstEstoque
            // 
            this.lstEstoque.HideSelection = false;
            this.lstEstoque.Location = new System.Drawing.Point(12, 12);
            this.lstEstoque.Name = "lstEstoque";
            this.lstEstoque.Size = new System.Drawing.Size(776, 426);
            this.lstEstoque.Sorting = System.Windows.Forms.SortOrder.Ascending;
            this.lstEstoque.TabIndex = 1;
            this.lstEstoque.UseCompatibleStateImageBehavior = false;
            this.lstEstoque.View = System.Windows.Forms.View.Details;
            // 
            // frmExibeMovEstoque
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.lstEstoque);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmExibeMovEstoque";
            this.Text = "Movimento de Item";
            this.Load += new System.EventHandler(this.frmExibeMovEstoque_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ListView lstEstoque;
    }
}