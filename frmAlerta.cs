﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FacilShoppingReports
{
    public partial class frmAlerta : Form
    {
        public MySqlDataReader dataReader;

        public frmAlerta()
        {
            InitializeComponent();
        }

        private void frmAlerta_Load(object sender, EventArgs e)
        {

        }

        public void Carrega_Alerta()
        {
            lstAlerta.Columns.Add("SKU", 150);
            lstAlerta.Columns.Add("NOME", 310);
            lstAlerta.Columns.Add("ID", 0);

            while (dataReader.Read())
            {

                ListViewItem item;

                item = new ListViewItem(new[] { dataReader["sku"].ToString(), dataReader["nm_produto"].ToString(), dataReader["ID"].ToString() });

                lstAlerta.Items.Add(item);

            }

            dataReader.Close();
        }

        private void lstAlerta_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void lstAlerta_DoubleClick(object sender, EventArgs e)
        {

            frmCorrecaoSKU frmCorr = new frmCorrecaoSKU();

            frmCorr.intID = Convert.ToInt32(lstAlerta.SelectedItems[0].SubItems[2].Text);

            frmCorr.ShowDialog(this);

            lstAlerta.SelectedItems[0].SubItems[0].Text = frmCorr.strSKU;


        }
    }
}
