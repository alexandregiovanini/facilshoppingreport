﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;
using System.Net;


namespace FacilShoppingReports
{
    public partial class frmEstoque : Form
    {        
        MySqlDataReader dataLista;
        

        public frmEstoque()
        {
            InitializeComponent();
        }

        private void btnSair2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btPesquisar_Click(object sender, EventArgs e)
        {
            Pesquisa();
        }

        private void Pesquisa()
        {
            MySqlCommand cmd;
            string strSincronizar;

            lstLista.Columns.Clear();
            lstLista.Items.Clear();

            string query = " SELECT itemEstoque.NAOSINCRONIZAR, catalog_product_entity.sku SKU, catalog_product_entity_varchar.value DESCRICAO, ";

            query = query + "  coalesce(itemEstoque.localidade, '') LOCAL , coalesce(itemEstoque.quantidade, '') QUANTIDADE,";
            query = query + "  (SELECT catalog_product_entity_media_gallery.value FROM catalog_product_entity_media_gallery INNER JOIN catalog_product_entity_media_gallery_value";
            query = query + "  ON catalog_product_entity_media_gallery_value.value_id = catalog_product_entity_media_gallery.value_id";
            query = query + "  WHERE catalog_product_entity_media_gallery.entity_id = catalog_product_entity_varchar.entity_id ORDER BY catalog_product_entity_media_gallery_value.position LIMIT 0,1) IMAGEM  ,";

            query = query + "  `cataloginventory_stock_item`.qty, catalog_product_entity_text.value fabricante, ";
            query = query + " (SELECT value FROM catalog_product_entity_varchar WHERE catalog_product_entity_varchar.attribute_id = 84 ";
            query = query + " AND cataloginventory_stock_item.product_id = catalog_product_entity_varchar.entity_id) FORNECEDOR, EAN.VALUE EAN ";
            query = query + " FROM `catalog_product_entity_varchar`  ";

            query = query + " INNER JOIN catalog_product_entity_varchar EAN ";
            query = query + " ON EAN.attribute_id = 169 ";
            query = query + " AND EAN.entity_id = catalog_product_entity_varchar.entity_id";

            query = query + " INNER JOIN `cataloginventory_stock_item`  ";
            query = query + " ON `cataloginventory_stock_item`.product_id = `catalog_product_entity_varchar`.entity_id ";
            query = query + " INNER JOIN  `cataloginventory_stock_status` ";
            query = query + " ON `cataloginventory_stock_status`.product_id = `catalog_product_entity_varchar`.entity_id ";
            query = query + " LEFT JOIN catalog_product_entity_decimal ";
            query = query + " ON catalog_product_entity_decimal.attribute_id = 75 ";
            query = query + " AND catalog_product_entity_decimal.entity_id = catalog_product_entity_varchar.entity_id ";
            query = query + " INNER JOIN  `catalog_product_entity_text` ";
            query = query + " ON `catalog_product_entity_text`.entity_id = `catalog_product_entity_varchar`.entity_id ";
            query = query + " AND catalog_product_entity_text.attribute_id = 83 ";
            query = query + " INNER JOIN  `catalog_product_entity` ";
            query = query + " ON `catalog_product_entity`.entity_id = `catalog_product_entity_varchar`.entity_id ";

            query = query + " LEFT JOIN itemEstoque ";
            query = query + " ON itemEstoque.SKU = catalog_product_entity.sku ";

            query = query + " where catalog_product_entity_varchar.attribute_id = '71' ";

            if (txtPesquisa.Text.Length != 0)
            {
                query = query + " AND (catalog_product_entity_varchar.value LIKE '%" + txtPesquisa.Text + "%' OR catalog_product_entity.sku LIKE '%" + txtPesquisa.Text + "%' ";
                query = query + " OR  EAN.VALUE LIKE '%" + txtPesquisa.Text + "%') ";
            }

            query = query + " limit 0,10000;";
           
            if (global.myDB.connection.State == ConnectionState.Closed)
            {
                global.myDB.OpenConnection();
            }

            cmd = new MySqlCommand(query, global.myDB.connection);

            try
            {
                dataLista = cmd.ExecuteReader();
            }
            catch (Exception)
            {
                global.myDB.OpenConnection();
                cmd = new MySqlCommand(query, global.myDB.connection);
                //dataLista.Close();

                dataLista = cmd.ExecuteReader();
            }

            lstLista.Columns.Add("SKU", 100);
            lstLista.Columns.Add("DESCRICAO", 350);
            lstLista.Columns.Add("QUANT SIST", 90);
            lstLista.Columns.Add("FABRICANTE", 150);
            lstLista.Columns.Add("FORNECEDOR", 150);
            lstLista.Columns.Add("EAN", 150);
            lstLista.Columns.Add("IMAGEM", 1);
            lstLista.Columns.Add("LOCALIDADE", 1);
            lstLista.Columns.Add("QUANTIDADE", 1);
            lstLista.Columns.Add("NAOSINCRONIZAR", 1);
            while (dataLista.Read())
            {
                if(dataLista["NAOSINCRONIZAR"].ToString() != "1")
                {
                    strSincronizar = "false";
                }
                else
                {
                    strSincronizar = "true";
                }
                

                ListViewItem item = new ListViewItem(new[] { dataLista["SKU"].ToString(), dataLista["DESCRICAO"].ToString(), String.Format("{0:N}", dataLista["qty"]).ToString(), dataLista["fabricante"].ToString(), dataLista["FORNECEDOR"].ToString(), dataLista["EAN"].ToString(), dataLista["IMAGEM"].ToString(), dataLista["LOCAL"].ToString(), dataLista["QUANTIDADE"].ToString(), strSincronizar });

                lstLista.Items.Add(item);

            }

            dataLista.Close();

            if (lstLista.Items.Count == 1)
            {
                lstLista.Items[0].Selected = true;
                Exibe_Registros();
                tabPrincipal.SelectedTab = tabPage2;
            }
           
        }

        private void btnSair_Click(object sender, EventArgs e)
        {
           
        }


        private bool Valida()
        {

           if (txtQuantidade.TextLength <= 0)
            {
                txtQuantidade.Text = "0";                
            }
            
            return true;
        }

        public bool Gravar()
        {
            MySqlCommand cmd;
            MySqlDataReader dataListaGravar;
            string query;

            query = "SELECT * FROM itemEstoque WHERE SKU = '" + txtID.Text + "'";


            if (global.myDB.connection.State == ConnectionState.Closed)
            {
                global.myDB.OpenConnection();
            }

            cmd = new MySqlCommand(query, global.myDB.connection);

            try
            {
                dataListaGravar = cmd.ExecuteReader();
            }
            catch (Exception)
            {
                global.myDB.OpenConnection();
                cmd = new MySqlCommand(query, global.myDB.connection);
                
                dataListaGravar = cmd.ExecuteReader();
            }


            if (dataListaGravar.Read())
            {
                query = "UPDATE itemEstoque " +
                        " SET LOCALIDADE = '" + txtLocalidade.Text + "', " +
                        " IMAGEM = '" + imgImagem.ImageLocation + "', " +
                        " QUANTIDADE = " + txtQuantidade.Text + ", " +
                        " NAOSINCRONIZAR = " + chkNaoSincronizar.Checked +
                        " WHERE SKU = '" + txtID.Text + "'; ";

            }
            else
            {
                query = "INSERT INTO itemEstoque (SKU, LOCALIDADE, IMAGEM, quantidade ) " +
                        " VALUES ( '" +
                        txtID.Text + "', '" + txtLocalidade.Text + "', " + txtQuantidade.Text +
                        ",'" + imgImagem.ImageLocation + "'); ";
            }
            
            dataListaGravar.Close();
            
            if (global.myDB.connection.State == ConnectionState.Closed)
            {
                global.myDB.OpenConnection();
            }

            MySqlCommand cmd1 = new MySqlCommand(query, global.myDB.connection);

            try
            {
                cmd1.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                
                return false;
            }
            
            return true;
        }

        private void lstLista_DoubleClick(object sender, EventArgs e)
        {
            Exibe_Registros();
            tabPrincipal.SelectedTab = tabPage2;
        }

        private Boolean Checa_URL(String strURL)
        {
            HttpWebResponse response = null;
            var request = (HttpWebRequest)WebRequest.Create(strURL);
            request.Method = "HEAD";

            try
            {
                response = (HttpWebResponse)request.GetResponse();
            }
            catch (WebException ex)
            {
                // response.Close();
                return false;
            }
            finally
            {
                // Não esqueça de fechar
                if (response != null)
                {
                    response.Close();
                }
            }

            return true;

        }


        private void Exibe_Registros()
        {
            Boolean blAchou;
            String strImagem;

            
            txtID.Text = lstLista.SelectedItems[0].SubItems[0].Text;
            txtDescricao.Text = lstLista.SelectedItems[0].SubItems[1].Text;
            txtQuant_Sist.Text = lstLista.SelectedItems[0].SubItems[2].Text;
            txtFabricante.Text = lstLista.SelectedItems[0].SubItems[3].Text;
            txtFornecedor.Text = lstLista.SelectedItems[0].SubItems[4].Text;
            txtEan.Text = lstLista.SelectedItems[0].SubItems[5].Text;

            txtLocalidade.Text = lstLista.SelectedItems[0].SubItems[7].Text;
            txtQuantidade.Text = lstLista.SelectedItems[0].SubItems[8].Text;
            chkNaoSincronizar.Checked = Convert.ToBoolean(lstLista.SelectedItems[0].SubItems[9].Text);
            blAchou = false;

            strImagem = "https://facilshopping.com.br/media/catalog/product" + lstLista.SelectedItems[0].SubItems[6].Text;

            blAchou = Checa_URL(strImagem);

            if (blAchou == false)
            {
                strImagem = "https://facilshopping.com.br/media/catalog/product/cache/1/image/9df78eab33525d08d6e5fb8d27136e95" + lstLista.SelectedItems[0].SubItems[6].Text;
                blAchou = Checa_URL(strImagem);
            }

            if (blAchou == false)
            {
                strImagem = "https://facilshopping.com.br/media/catalog/product/cache/1/image/308x472/9df78eab33525d08d6e5fb8d27136e95" + lstLista.SelectedItems[0].SubItems[6].Text;
                blAchou = Checa_URL(strImagem);
            }

            if (blAchou == true)
            {
                imgImagem.ImageLocation = strImagem;
            }
            else
            {
                imgImagem.ImageLocation = "";
            }

            Carrega_Movimentacao();

        }

        private void Carrega_Movimentacao()
        {
            MySqlCommand cmd;

            lstMovimento.Columns.Clear();
            lstMovimento.Items.Clear();

            string query = " SELECT * FROM MOVESTOQUE ";

            query = query + " WHERE SKU = '" + txtID.Text + "' ORDER BY ID DESC";

            if (global.myDB.connection.State == ConnectionState.Closed)
            {
                global.myDB.OpenConnection();
            }

            cmd = new MySqlCommand(query, global.myDB.connection);

            try
            {
                dataLista = cmd.ExecuteReader();
            }
            catch (Exception)
            {
                dataLista.Close();

                if (global.myDB.connection.State == ConnectionState.Closed)
                {
                    global.myDB.OpenConnection();
                }

                cmd = new MySqlCommand(query, global.myDB.connection);

                dataLista = cmd.ExecuteReader();
            }

            lstMovimento.Columns.Add("DATA", 115);
            lstMovimento.Columns.Add("OBSERVAÇÃO", 160);
            lstMovimento.Columns.Add("MOVIMENTO", 78, HorizontalAlignment.Right);
            lstMovimento.Columns.Add("ESTOQUE", 70, HorizontalAlignment.Right);

            while (dataLista.Read())
            {
                ListViewItem item = new ListViewItem(new[] { dataLista["DATA"].ToString(), dataLista["OBSERVACAO"].ToString(), String.Format("{0:N}", dataLista["QUANTIDADE"]).ToString(), String.Format("{0:N}", dataLista["QUANT_ESTOQUE"]).ToString() });

                if (dataLista["ENTRADA"].ToString()=="0")
                {
                    item.BackColor = Color.Coral;
                }

                lstMovimento.Items.Add(item);
            }

            dataLista.Close();

           // Pesquisa();
        }

        private void tabPage1_Click(object sender, EventArgs e)
        {

        }

        private void txtPesquisa_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtPesquisa_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13 )
            {
                

                Pesquisa();
            }
        }

        private void btnGrava_Click(object sender, EventArgs e)
        {
          
        }

        private void cmdEntrada_Click(object sender, EventArgs e)
        {
            

         
            
        }

        private void lstLista_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void cmdSaida_Click(object sender, EventArgs e)
        {
          
        }

        private void cmdSaida_Click_1(object sender, EventArgs e)
        {
            frmMovEstoque frmMov = new frmMovEstoque();
            frmMov.strSKU = txtID.Text;
            frmMov.intQuantidade = 0;


            if (txtQuantidade.Text == "")
            {
                txtQuantidade.Text = "0";
            }
        
            frmMov.intQuantEstoque = Convert.ToInt32(txtQuantidade.Text);
            frmMov.blEntrada = false;
            frmMov.ShowDialog(this);

            if (txtQuantidade.Text == "")
            {
                txtQuantidade.Text = "0";
            }

            txtQuantidade.Text = (Convert.ToInt32(txtQuantidade.Text) - frmMov.intQuantidade).ToString();
            Carrega_Movimentacao();
        }

        private void btnGrava_Click_1(object sender, EventArgs e)
        {
            if (Valida() == true)
            {
                if (Gravar() == true)
                {
                    Pesquisa();
                    MessageBox.Show("Registro gravado com sucesso!");
                }
                else
                {
                    MessageBox.Show("Ocorreu algum erro na gravação!");
                }

            }
        }

        private void cmdEntrada_Click_1(object sender, EventArgs e)
        {
            frmMovEstoque frmMov = new frmMovEstoque();
            frmMov.strSKU = txtID.Text;
            frmMov.intQuantidade = 0;
            if (txtQuantidade.TextLength == 0)
            {
                txtQuantidade.Text = "0";
            }
            frmMov.intQuantEstoque = Convert.ToInt32(txtQuantidade.Text);
            frmMov.blEntrada = true;
            frmMov.ShowDialog(this);

            if (txtQuantidade.Text == "")
            {
                txtQuantidade.Text = "0";
            }
            txtQuantidade.Text = (Convert.ToInt32(txtQuantidade.Text) + frmMov.intQuantidade).ToString();
            Carrega_Movimentacao();
        }

        private void btnSair_Click_1(object sender, EventArgs e)
        {
            this.Close();
        }
    }

}
