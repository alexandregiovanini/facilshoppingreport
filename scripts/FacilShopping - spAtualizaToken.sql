DROP PROCEDURE IF EXISTS spAtualizaToken; 


 DELIMITER $
 CREATE PROCEDURE spAtualizaToken(v_Token VARCHAR(500)) 
 BEGIN
   
   SET SQL_SAFE_UPDATES = 0;
  
   update IDERIS_TOKEN set TOKEN_PROVISORIO = v_Token;
  
   SET SQL_SAFE_UPDATES = 1;
  
 END
 $
  
call spAtualizaToken('teste');
