﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FacilShoppingReports
{
    public partial class frmExibeMovEstoque : Form
    {
        public string strSKU;

        public frmExibeMovEstoque()
        {
            InitializeComponent();
        }

        private void frmExibeMovEstoque_Load(object sender, EventArgs e)
        {
            CarregaMovimento();
        }

        private void CarregaMovimento()
        {
            string query;
            MySqlCommand cmd;
            MySqlDataReader dataReader;

            lstEstoque.Columns.Add("QUANTIDADE", 90, HorizontalAlignment.Right);
            lstEstoque.Columns.Add("VALOR UNITARIO", 110, HorizontalAlignment.Right);
            lstEstoque.Columns.Add("ORIGEM", 150);
            lstEstoque.Columns.Add("DATA1", 0);
            lstEstoque.Columns.Add("DATA", 110);
            lstEstoque.Columns.Add("PEDIDO", 200);
            lstEstoque.Columns.Add("USUARIO", 100);

            query = " SELECT format(QUANTIDADE,0) QTDE, '' VL_UNITARIO, ORIGEM, DATA DATA1, date_format(DATA, '%d/%m/%Y %h:%i') DATA,'' PEDIDO, USUARIO ";
            query = query + " FROM MOVIMENTO_ESTOQUE ";
            query = query + " WHERE SKU = '" + strSKU + "' ";
            query = query + " UNION ";
            query = query + " SELECT format(qty_ordered,0) QTDE, price VL_UNITARIO, 'LOJA' ORIGEM, VENDA.created_at DATA1, date_format(VENDA.created_at, '%d/%m/%Y %h:%i') DATA , VENDA.increment_id PEDIDO, '' USUARIO ";
            query = query + " FROM sales_flat_order_item VENDA_ITEM ";
            query = query + " INNER JOIN sales_flat_order VENDA ";
            query = query + " ON VENDA_ITEM.order_id = VENDA.entity_id ";
            query = query + " WHERE SKU = '" + strSKU + "' ";
            query = query + " UNION ";
            query = query + " SELECT format(QTDE,0) QTDE, VL_UNITARIO, PEDIDOS_INTEGRADOS.MARKETPLACE ORIGEM, PEDIDOS_INTEGRADOS.DT_PEDIDO DATA1, date_format(PEDIDOS_INTEGRADOS.DT_PEDIDO, '%d/%m/%Y %h:%i') DATA , PEDIDOS_INTEGRADOS.COD_PEDIDO PEDIDO, '' USUARIO ";
            query = query + " FROM ITENS_PEDIDOS_INTEGRADOS ";
            query = query + " INNER JOIN PEDIDOS_INTEGRADOS ";
            query = query + " ON PEDIDOS_INTEGRADOS.ID = ITENS_PEDIDOS_INTEGRADOS.ID_PEDIDO ";
            query = query + " WHERE SKU = '" + strSKU + "' ORDER BY 5 DESC; ";

            global.myDB.CloseConnection();

            if (global.myDB.connection.State == ConnectionState.Closed)
            {
                global.myDB.OpenConnection();
            }

            cmd = new MySqlCommand(query, global.myDB.connection);

            try
            {
                dataReader = cmd.ExecuteReader();
            }
            catch (Exception)
{
             global.myDB.OpenConnection();
             cmd = new MySqlCommand(query, global.myDB.connection);
             dataReader = cmd.ExecuteReader();
}

            while (dataReader.Read())
            {
                ListViewItem item;

               item = new ListViewItem(new[] { dataReader["QTDE"].ToString(), dataReader["VL_UNITARIO"].ToString(), dataReader["ORIGEM"].ToString(), dataReader["DATA1"].ToString(), dataReader["DATA"].ToString(), dataReader["PEDIDO"].ToString(), dataReader["USUARIO"].ToString()});


                lstEstoque.Items.Add(item);
            }

            dataReader.Close();

            if (global.myDB.connection.State != ConnectionState.Closed)
            {
                global.myDB.CloseConnection();
            }

            ItemComparer sorter = lstEstoque.ListViewItemSorter as ItemComparer;
            if (sorter == null)
            {
                sorter = new ItemComparer(4);
                sorter.Order = SortOrder.Ascending;
                lstEstoque.ListViewItemSorter = sorter;
            }

            sorter.Column = 4;
            sorter.Order = SortOrder.Ascending;       
            lstEstoque.Sort();

        }

    }
}
  