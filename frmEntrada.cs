﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

namespace FacilShoppingReports
{
    public partial class frmEntrada : Form
    {
        MySqlDataReader dataReader;
        private global global;
        Boolean blAlteracao;
       

        public frmEntrada()
        {
            InitializeComponent();
            global = new global();
            CarregaAgentes();
        }

        private void btnSair2_Click(object sender, EventArgs e)
        {
            this.Close();
        }


        private void CarregaAgentes()
        {
            MySqlCommand cmd;
            DataTable dataTable = new DataTable();

            string query = "select * " +
                           " from agentes " +
                           " where fornecedor = 1" +
                           " order by fantasia; ";


            global.myDB.CloseConnection();

            if (global.myDB.connection.State == ConnectionState.Closed)
            {
                global.myDB.OpenConnection();
            }

            //cmd = new MySqlCommand(query, connection);

            

            cmd = new MySqlCommand(query, global.myDB.connection);

            dataReader = cmd.ExecuteReader();

            dataTable.Load(dataReader);


            // cbCategorias.Items.Add(dataReader["value"].ToString());
            cbFornecedor.DisplayMember = "fantasia";
            cbFornecedor.ValueMember = "id";
            cbFornecedor.DataSource = dataTable;

            dataReader.Close();

        }

        private void txtBaseICMS_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && (e.KeyChar != ','))
            {
                e.Handled = true;
            }
            if ((e.KeyChar == ',') && ((sender as TextBox).Text.IndexOf(',') > -1))
            {
                e.Handled = true;
            }
        }

        private void txtBaseICMS_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtValorICMS_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && (e.KeyChar != ','))
            {
                e.Handled = true;
            }
            if ((e.KeyChar == ',') && ((sender as TextBox).Text.IndexOf(',') > -1))
            {
                e.Handled = true;
            }
        }

        private void txtBaseICMSSubst_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && (e.KeyChar != ','))
            {
                e.Handled = true;
            }
            if ((e.KeyChar == ',') && ((sender as TextBox).Text.IndexOf(',') > -1))
            {
                e.Handled = true;
            }
        }

        private void txtValorICMSSubst_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && (e.KeyChar != ','))
            {
                e.Handled = true;
            }
            if ((e.KeyChar == ',') && ((sender as TextBox).Text.IndexOf(',') > -1))
            {
                e.Handled = true;
            }
        }

        private void txtValorProdutos_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && (e.KeyChar != ','))
            {
                e.Handled = true;
            }
            if ((e.KeyChar == ',') && ((sender as TextBox).Text.IndexOf(',') > -1))
            {
                e.Handled = true;
            }
        }

        private void txtValorFrete_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && (e.KeyChar != ','))
            {
                e.Handled = true;
            }
            if ((e.KeyChar == ',') && ((sender as TextBox).Text.IndexOf(',') > -1))
            {
                e.Handled = true;
            }
        }

        private void txtValorSeguro_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && (e.KeyChar != ','))
            {
                e.Handled = true;
            }
            if ((e.KeyChar == ',') && ((sender as TextBox).Text.IndexOf(',') > -1))
            {
                e.Handled = true;
            }
        }

        private void txtDesconto_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && (e.KeyChar != ','))
            {
                e.Handled = true;
            }
            if ((e.KeyChar == ',') && ((sender as TextBox).Text.IndexOf(',') > -1))
            {
                e.Handled = true;
            }
        }

        private void txtOutrasDespesas_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && (e.KeyChar != ','))
            {
                e.Handled = true;
            }
            if ((e.KeyChar == ',') && ((sender as TextBox).Text.IndexOf(',') > -1))
            {
                e.Handled = true;
            }
        }

        private void txtValorIPI_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && (e.KeyChar != ','))
            {
                e.Handled = true;
            }
            if ((e.KeyChar == ',') && ((sender as TextBox).Text.IndexOf(',') > -1))
            {
                e.Handled = true;
            }
        }

        private void txtValorNF_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && (e.KeyChar != ','))
            {
                e.Handled = true;
            }
            if ((e.KeyChar == ',') && ((sender as TextBox).Text.IndexOf(',') > -1))
            {
                e.Handled = true;
            }
        }

        private void btnNovo_Click(object sender, EventArgs e)
        {

            global.Limpa_Campo(groupBox1.Controls);
            global.Limpa_Campo(groupBox4.Controls);
            global.Limpa_Campo(groupBox5.Controls);
            global.Limpa_Campo(groupBox6.Controls);

            Muda_Status();

           // txtID.Text = global.BuscaID("nfe");
            mskData.Text = DateTime.Now.ToShortDateString();
        }


        private void Muda_Status()
        {
            if (btnSair.Text == "Sair")
            {
                btnSair.Text = "Cancelar";
            }
            else
            {
                btnSair.Text = "Sair";
                blAlteracao = false;
            }

            btnGrava.Enabled = !btnGrava.Enabled;
            btPesquisar.Enabled = !btPesquisar.Enabled;
            txtPesquisa.Enabled = !txtPesquisa.Enabled;
            btnNovo.Enabled = !btnNovo.Enabled;
            
            btnSair2.Enabled = !btnSair2.Enabled;
            btnAlterar.Enabled = !btnAlterar.Enabled;

            groupBox1.Enabled = !groupBox1.Enabled;
            groupBox4.Enabled = !groupBox4.Enabled;
            groupBox5.Enabled = !groupBox5.Enabled;
            groupBox6.Enabled = !groupBox6.Enabled;

            lstLista.Enabled = !lstLista.Enabled;

        }

        private void btnAlterar_Click(object sender, EventArgs e)
        {
            if (txtID.Text.Length != 0)
            {
                Muda_Status();
                blAlteracao = true;
            }
        }

        private void btnSair_Click(object sender, EventArgs e)
        {
            if (btnSair.Text == "Sair")
            {
                this.Close();

            }
            else
            {
                if (blAlteracao == false)
                {
                    global.Limpa_Campo(groupBox1.Controls);
                    global.Limpa_Campo(groupBox4.Controls);
                    global.Limpa_Campo(groupBox5.Controls);
                    global.Limpa_Campo(groupBox6.Controls);
                }

                Muda_Status();

            }
        }

        private void btnGrava_Click(object sender, EventArgs e)
        {
            if (Valida() == true)
            {
                if (Gravar() == true)
                {
                    if (blAlteracao == false)
                    {
                        global.Limpa_Campo(groupBox1.Controls);
                        global.Limpa_Campo(groupBox4.Controls);
                        global.Limpa_Campo(groupBox5.Controls);
                        global.Limpa_Campo(groupBox6.Controls);
                    }

                    Muda_Status();
                }

            }
        }

        private bool Valida()
        {
            return true;
        }

        public bool Gravar()
        {
            string query;

            if (blAlteracao == false)
            {
                
                query = "INSERT INTO nfe (id,data,data_emissao, data_saida,data_vencimento,data_entrega,num_nf,serie,tipo,natureza,agentes_id, " +
                        " base_icms, valor_icms, base_icms_subst, valor_icms_subst, valor_produtos, valor_frete, valor_seguro, deconto, outras_despesas, " +
                        " valor_ipi, valor_nota, observacao)" +
                        " VALUES ( " +
                        txtID.Text + ", '" + System.DateTime.Parse(mskData.Text).ToString("yyyy-MM-dd") + "'," +
                        "'" + System.DateTime.Parse(mskDataEmissao.Text).ToString("yyyy-MM-dd") + "'," +
                        "'" + System.DateTime.Parse(mskDataSaida.Text).ToString("yyyy-MM-dd") + "'," +
                        "'" + System.DateTime.Parse(mskDataVencimento.Text).ToString("yyyy-MM-dd") + "'," +
                        "'" + System.DateTime.Parse(mskDataEntrega.Text).ToString("yyyy-MM-dd") + "'," +
                        "'" + txtNF.Text + "', '" + txtSerie.Text + "', '" + optEntrada.Checked + "'," +
                        "'" + cbNatureza.Text + "'," + cbFornecedor.SelectedValue + "," + txtBaseICMS.Text + "," +
                        txtValorICMS.Text.Replace(",", ".") + ", " + txtBaseICMSSubst.Text.Replace(",", ".") + ", " + txtValorICMSSubst.Text.Replace(",", ".") + ", " +
                        txtValorProdutos.Text.Replace(",", ".") + ", " + txtValorFrete.Text.Replace(",", ".") + ", " + txtValorSeguro.Text.Replace(",", ".") + ", " +
                        txtDesconto.Text.Replace(",", ".") + ", " + txtOutrasDespesas.Text.Replace(",", ".") + ", " + txtValorIPI.Text.Replace(",", ".") + ", " +
                        txtValorNF.Text.Replace(",", ".") + ", '" + txtObservacao.Text + "');";
            }
            else
            {               
                query = "UPDATE nfe " +
                        " SET data_emissao = '" + System.DateTime.Parse(mskDataEmissao.Text).ToString("yyyy-MM-dd") + "', " +
                        " data_saida = '" + System.DateTime.Parse(mskDataSaida.Text).ToString("yyyy-MM-dd") + "', " +
                        " data_vencimento = '" + System.DateTime.Parse(mskDataVencimento.Text).ToString("yyyy-MM-dd") + "', " +
                        " data_entrega = '" + System.DateTime.Parse(mskDataEntrega.Text).ToString("yyyy-MM-dd") + "', " +
                        " num_nf = '" + txtNF.Text + "', " +
                        " serie = '" + txtSerie.Text + "', " +
                        " tipo = '" + optEntrada.Checked + "', " +
                        " natureza = '" + cbNatureza.Text + "', " +
                        " agentes_id = '" + cbFornecedor.SelectedValue + "', " +
                        " base_icms = " + txtBaseICMS.Text.Replace(",",".") + ", " +
                        " valor_icms = " + txtValorICMS.Text.Replace(",", ".") + ", " +
                        " base_icms_subst = " + txtBaseICMSSubst.Text.Replace(",", ".") + ", " +
                        " valor_icms_subst = " + txtValorICMSSubst.Text.Replace(",", ".") + ", " +
                        " valor_produtos = " + txtValorProdutos.Text.Replace(",", ".") + ", " +
                        " valor_frete = " + txtValorFrete.Text.Replace(",", ".") + ", " +
                        " valor_seguro = " + txtValorSeguro.Text.Replace(",", ".") + ", " +
                        " deconto = " + txtDesconto.Text.Replace(",", ".") + ", " +
                        " outras_despesas = " + txtOutrasDespesas.Text.Replace(",", ".") + ", " +
                        " valor_ipi = " + txtValorIPI.Text.Replace(",", ".") + ", " +
                        " valor_nota = " + txtValorNF.Text.Replace(",", ".") + ", " +
                        " observacao = '" + txtObservacao.Text + "' " +
                        " WHERE ID = " + txtID.Text + "; ";
            }

            if (global.myDB.connection.State == ConnectionState.Closed)
            {
                global.myDB.OpenConnection();
            }

            MySqlCommand cmd1 = new MySqlCommand(query, global.myDB.connection);

            try
            {
                cmd1.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return false;
            }


           if( Grava_Itens() == false)
            {
                return false;
            }



            return true;
        }

        private bool Grava_Itens()
        {
           
            int l = 2;
            string query;

            if (lstItens.Columns.Count > 0)
            {
                try
                {
                    
                    

                    foreach (ListViewItem item in lstItens.Items)
                    {
                        l = l + 1;

                        for (int j = 1; j < lstItens.Columns.Count + 1; j++)
                        {
                           query = "INSERT INTO itemnfe (id, nfe_id, item, cod_fabricante, produto_id, ncm, sosn, cfop, unidade, quantidade, valor_unitario, valor_desconto, " +
                                    "valor_liquido, valor_icms, valor_ipi, aliq_icms, aliq_ipi)" +
                                    " VALUES ( " + txtID.Text + j + ", " + txtID.Text + ", " + (j +1) + ", '" + item.SubItems[1].Text + "', '" + item.SubItems[2].Text + "', '" + 
                                    item.SubItems[3].Text + "', '" + item.SubItems[4].Text + "', '" + item.SubItems[5].Text + "', '" + item.SubItems[6].Text + "', " + 
                                    item.SubItems[7].Text.Replace(",", ".") + ", " + item.SubItems[8].Text.Replace(",", ".") + ", " + item.SubItems[9].Text.Replace(",", ".") + ", " + item.SubItems[10].Text.Replace(",", ".") + ", " + 
                                    item.SubItems[11].Text.Replace(",", ".") + ", " + item.SubItems[12].Text.Replace(",", ".") + ", " + item.SubItems[13].Text.Replace(",", ".") + ", " + item.SubItems[14].Text.Replace(",", ".") + ");";
                            

                            if (global.myDB.connection.State == ConnectionState.Closed)
                            {
                                global.myDB.OpenConnection();
                            }

                            MySqlCommand cmd1 = new MySqlCommand(query, global.myDB.connection);

                            try
                            {
                                cmd1.ExecuteNonQuery();
                            }
                            catch (Exception ex)
                            {
                                MessageBox.Show(ex.Message);
                                return false;
                            }

                        }
                    }



                }
                catch (Exception ex)
                {
                    MessageBox.Show("Erro : " + ex.Message);
           
                }
            }
            return true;
        }

        private void btPesquisar_Click(object sender, EventArgs e)
        {
            lstLista.Columns.Clear();
            lstLista.Items.Clear();

            Pesquisa();
        }

        private void Pesquisa()
        {
            MySqlCommand cmd;
            MySqlDataReader dataLista;

            string query = " SELECT * FROM nfe ";

            if (txtPesquisa.Text.Length != 0)
            {
                query = query + " WHERE num_nf LIKE = '" + txtPesquisa.Text + "'; ";
            }


            if (global.myDB.connection.State == ConnectionState.Closed)
            {
                global.myDB.OpenConnection();
            }

            cmd = new MySqlCommand(query, global.myDB.connection);


            try
            {
                dataLista = cmd.ExecuteReader();
            }
            catch (Exception)
            {
                global.myDB.OpenConnection();
                cmd = new MySqlCommand(query, global.myDB.connection);
                //dataLista.Close();

                dataLista = cmd.ExecuteReader();
            }


            lstLista.Columns.Add("ID", 60);
            lstLista.Columns.Add("DATA", 60);
            lstLista.Columns.Add("DATA_EMISSAO", 60);
            lstLista.Columns.Add("DATA_SAIDA", 60);
            lstLista.Columns.Add("DATA_VENCIMENTO", 60);
            lstLista.Columns.Add("DATA_ENTREGA", 60);
            lstLista.Columns.Add("NUM_NF", 60);
            lstLista.Columns.Add("SERIE", 60);
            lstLista.Columns.Add("TIPO", 60);
            lstLista.Columns.Add("NATUREZA", 60);
            lstLista.Columns.Add("AGENTES_ID", 60);
            lstLista.Columns.Add("BASE_ICMS", 60);
            lstLista.Columns.Add("VALOR_ICMS", 60);
            lstLista.Columns.Add("BASE_ICMS_SUBST", 60);
            lstLista.Columns.Add("VALOR_ICMS_SUBST", 60);
            lstLista.Columns.Add("VALOR_PRODUTOS", 60);
            lstLista.Columns.Add("VALOR_FRETE", 60);
            lstLista.Columns.Add("VALOR_SEGURO", 60);
            lstLista.Columns.Add("DECONTO", 60);
            lstLista.Columns.Add("OUTRAS_DESPESAS", 60);
            lstLista.Columns.Add("VALOR_IPI", 60);
            lstLista.Columns.Add("VALOR_NOTA DOUBLE", 60);
            lstLista.Columns.Add("OBSERVACAO", 60);


            while (dataLista.Read())
            {

                ListViewItem item = new ListViewItem(new[] { dataLista["id"].ToString(), dataLista["data"].ToString(),dataLista["data_emissao"].ToString(),dataLista["data_saida"].ToString(),dataLista["data_vencimento"].ToString(),dataLista["data_entrega"].ToString(),dataLista["num_nf"].ToString(), dataLista["serie"].ToString(),dataLista["tipo"].ToString(), dataLista["natureza"].ToString(),dataLista["agentes_id"].ToString(),dataLista["base_icms"].ToString(),dataLista["valor_icms"].ToString(),dataLista["base_icms_subst"].ToString(),dataLista["valor_icms_subst"].ToString(),dataLista["valor_produtos"].ToString(),dataLista["valor_frete"].ToString(),dataLista["valor_seguro"].ToString(),dataLista["deconto"].ToString(),dataLista["outras_despesas"].ToString(),dataLista["valor_ipi"].ToString(),dataLista["valor_nota"].ToString(),dataLista["observacao"].ToString() });

                lstLista.Items.Add(item);

            }

            dataLista.Close();
        }

        private void lstLista_DoubleClick(object sender, EventArgs e)
        {
            Exibe_Registros();
            tabPrincipal.SelectedTab = tabPage2;
        }

        private void Exibe_Registros()
        {
            txtID.Text = lstLista.SelectedItems[0].SubItems[0].Text;
            mskData.Text = lstLista.SelectedItems[0].SubItems[1].Text;
            mskDataEmissao.Text = lstLista.SelectedItems[0].SubItems[2].Text;
            mskDataSaida.Text = lstLista.SelectedItems[0].SubItems[3].Text;
            mskDataVencimento.Text = lstLista.SelectedItems[0].SubItems[4].Text;
            mskDataEntrega.Text = lstLista.SelectedItems[0].SubItems[5].Text;
            txtNF.Text = lstLista.SelectedItems[0].SubItems[6].Text;
            txtSerie.Text = lstLista.SelectedItems[0].SubItems[7].Text;

            if (lstLista.SelectedItems[0].SubItems[8].Text == "1")
            {
                optEntrada.Checked = true;
            }
            else
            {
                optEntrada.Checked = false;
            }

            
            cbNatureza.Text = lstLista.SelectedItems[0].SubItems[9].Text;
            cbFornecedor.Text = lstLista.SelectedItems[0].SubItems[10].Text;

            txtBaseICMS.Text = lstLista.SelectedItems[0].SubItems[11].Text;
            txtValorICMS.Text = lstLista.SelectedItems[0].SubItems[12].Text;
            txtBaseICMSSubst.Text = lstLista.SelectedItems[0].SubItems[13].Text;
            txtValorICMSSubst.Text = lstLista.SelectedItems[0].SubItems[14].Text;
            txtValorProdutos.Text = lstLista.SelectedItems[0].SubItems[15].Text;
            txtValorFrete.Text = lstLista.SelectedItems[0].SubItems[16].Text;
            txtValorSeguro.Text = lstLista.SelectedItems[0].SubItems[17].Text;
            txtDesconto.Text = lstLista.SelectedItems[0].SubItems[18].Text;
            txtOutrasDespesas.Text = lstLista.SelectedItems[0].SubItems[19].Text;
            txtValorIPI.Text = lstLista.SelectedItems[0].SubItems[20].Text;
            txtValorNF.Text = lstLista.SelectedItems[0].SubItems[21].Text;
            txtObservacao.Text = lstLista.SelectedItems[0].SubItems[22].Text;

            Exibe_Itens();


        }

        private void Exibe_Itens()
        {
            MySqlCommand cmd;
            MySqlDataReader dataLista;

            string query = " SELECT * FROM itemnfe ";

            query = query + " WHERE nfe_id  = '" + txtID.Text + "'; ";
            

            if (global.myDB.connection.State == ConnectionState.Closed)
            {
                global.myDB.OpenConnection();
            }

            cmd = new MySqlCommand(query, global.myDB.connection);


            try
            {
                dataLista = cmd.ExecuteReader();
            }
            catch (Exception)
            {
                global.myDB.OpenConnection();
                cmd = new MySqlCommand(query, global.myDB.connection);
                //dataLista.Close();

                dataLista = cmd.ExecuteReader();
            }
            


            while (dataLista.Read())
            {

                ListViewItem item = new ListViewItem(new[] { dataLista["item"].ToString(), dataLista["cod_fabricante"].ToString(), dataLista["produto_id"].ToString(), "", dataLista["ncm"].ToString(), dataLista["sosn"].ToString(), dataLista["cfop"].ToString(), dataLista["unidade"].ToString(), dataLista["quantidade"].ToString(), dataLista["valor_unitario"].ToString(), dataLista["valor_desconto"].ToString(), dataLista["valor_liquido"].ToString(), dataLista["valor_icms"].ToString(), dataLista["valor_ipi"].ToString(), dataLista["aliq_icms"].ToString(), dataLista["aliq_ipi"].ToString() });

                
                lstItens.Items.Add(item);

            }

            dataLista.Close();

        }

        private void txtDesconto_TextChanged(object sender, EventArgs e)
        {

        }

        private void btnAdicionar_Click(object sender, EventArgs e)
        {
            frmIncluirProduto frmIncluir = new frmIncluirProduto();

            frmIncluir.countLista = lstItens.Items.Count + 1;


            frmIncluir.ShowDialog(this);

            lstItens.Items.Add(frmIncluir.itemList); 
        }

        private void frmEntrada_Load(object sender, EventArgs e)
        {

        }

        private void lstLista_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}
