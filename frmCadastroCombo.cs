﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FacilShoppingReports
{
    public partial class frmCadastroCombo : Form
    {
        MySqlDataReader dataLista;
        public string strOperador;

        public frmCadastroCombo()
        {
            InitializeComponent();
        }

        private void tabPage2_Click(object sender, EventArgs e)
        {

        }

        private void btPesquisar_Click(object sender, EventArgs e)
        {
            Pesquisa();
        }

        private void txtPesquisa_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                Pesquisa();
            }
        }

        private void Pesquisa()
        {
            MySqlCommand cmd;

            lstLista.Columns.Clear();
            lstLista.Items.Clear();

            string query = " SELECT COMBO.*  ";

            query = query + " FROM COMBO ";
            query = query + " LEFT JOIN ITEM_COMBO ";
            query = query + " ON ITEM_COMBO.ID_COMBO = COMBO.ID ";
            query = query + " WHERE SKUCOMBO like '%" + txtPesquisa.Text + "%' OR SKUITEM like '%" + txtPesquisa.Text + "%' ";
            
            query = query + " limit 0,10000;";

            if (global.myDB.connection.State == ConnectionState.Closed)
            {
                global.myDB.OpenConnection();
            }

            cmd = new MySqlCommand(query, global.myDB.connection);

            try
            {
                dataLista = cmd.ExecuteReader();
            }
            catch (Exception)
            {
                global.myDB.OpenConnection();
                cmd = new MySqlCommand(query, global.myDB.connection);
                //dataLista.Close();

                dataLista = cmd.ExecuteReader();
            }

            lstLista.Columns.Add("ID", 50);
            lstLista.Columns.Add("SKUCOMBO", 100);
            lstLista.Columns.Add("CRIADOR", 90);
            lstLista.Columns.Add("STATUS", 150);
            lstLista.Columns.Add("CRIACAO", 150);

            while (dataLista.Read())
            {
                ListViewItem item = new ListViewItem(new[] { dataLista["ID"].ToString(), dataLista["SKUCOMBO"].ToString(), dataLista["CRIADOR"].ToString(), dataLista["STATUS"].ToString(), dataLista["DT_CRIACAO"].ToString() });

                lstLista.Items.Add(item);
            }

            dataLista.Close();

            if (lstLista.Items.Count == 1)
            {
                lstLista.Items[0].Selected = true;
                Exibe_Registros();
                tabPrincipal.SelectedTab = tabPage2;
            }

        }

        private void lstLista_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            txtSKUCombo.Text = lstLista.SelectedItems[0].SubItems[1].Text;
            txtSKUCombo.Tag = lstLista.SelectedItems[0].SubItems[0].Text;
            Exibe_Registros();
            tabPrincipal.SelectedTab = tabPage2;
        }

        private void Exibe_Registros()
        {
            txtSKUMagento.Text = "";        
            Carrega_SKUs();
            txtSKUCombo.ReadOnly = true;
        }

        private void Carrega_SKUs()
        {
            MySqlCommand cmd;

            lstCombos.Columns.Clear();
            lstCombos.Items.Clear();

            string query = " SELECT ITEM_COMBO.*, (select value from catalog_product_entity_varchar ";
            query = query + " inner join catalog_product_entity on catalog_product_entity_varchar.entity_id = catalog_product_entity.entity_id  ";
            query = query + " where attribute_id = 71 ";
            query = query + " and sku = ITEM_COMBO.SKUITEM) NOME ";
            query = query + " FROM ITEM_COMBO ";

            query = query + " WHERE ID_COMBO = '" + txtSKUCombo.Tag + "' ORDER BY ID DESC";

            if (global.myDB.connection.State == ConnectionState.Closed)
            {
                global.myDB.OpenConnection();
            }

            cmd = new MySqlCommand(query, global.myDB.connection);

            try
            {
                dataLista = cmd.ExecuteReader();
            }
            catch (Exception)
            {
                dataLista.Close();

                if (global.myDB.connection.State == ConnectionState.Closed)
                {
                    global.myDB.OpenConnection();
                }

                cmd = new MySqlCommand(query, global.myDB.connection);

                dataLista = cmd.ExecuteReader();
            }

            lstCombos.CheckBoxes = true;

            lstCombos.Columns.Add("ID", 60);
            lstCombos.Columns.Add("SKUITEM", 80);
            lstCombos.Columns.Add("NOME", 300);
            lstCombos.Columns.Add("CRIADOR", 90, HorizontalAlignment.Right);
            lstCombos.Columns.Add("DATA", 130, HorizontalAlignment.Right);

            while (dataLista.Read())
            {
                ListViewItem item = new ListViewItem(new[] { dataLista["ID"].ToString(), dataLista["SKUITEM"].ToString(), dataLista["NOME"].ToString(), dataLista["CRIADOR"].ToString(), dataLista["DT_CRIACAO"].ToString() });
                lstCombos.Items.Add(item);
            }

            dataLista.Close();

            // Pesquisa();
        }

        private void btnSair2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnGrava_Click(object sender, EventArgs e)
        {
            if (Valida() == true)
            {
                if (Gravar() == true)
                {
                    Exibe_Registros();
                    MessageBox.Show("Registro gravado com sucesso!");
                    txtSKUMagento.Text = "";
                }
                else
                {
                    MessageBox.Show("Ocorreu algum erro na gravação!");
                }

            }
        }

        private bool Valida()
        {
            bool blRetorno;

            blRetorno = true;

            if (txtSKUCombo.TextLength <= 0)
            {
                MessageBox.Show("Campo SKUCombo obrigatório!", "PontoZero Informa");
                
                blRetorno = false;
            }

            if (txtSKUMagento.TextLength <= 0)
            {
                MessageBox.Show("Campo SKUMagento obrigatório!", "PontoZero Informa");

                blRetorno = false;
            }

            if (txtSKUCombo.ReadOnly == false)
            {
                if (SKUExiste(txtSKUCombo.Text) == true)
                {
                    MessageBox.Show("Campo SKUCombo já existente!", "PontoZero Informa");

                    blRetorno = false;
                }
            }



            return blRetorno;
        }

        public bool Gravar()
        {
            
            string query;


            if(txtSKUCombo.Tag.ToString() == "")
            {

                query = "INSERT INTO COMBO (SKUCOMBO, CRIADOR, DT_CRIACAO) ";
                query = query + " VALUES( ";
                query = query + " '" + txtSKUCombo.Text + "', '" + strOperador + "', NOW()    );";



                if (global.myDB.connection.State == ConnectionState.Closed)
                {
                    global.myDB.OpenConnection();
                }

                MySqlCommand cmd2 = new MySqlCommand(query, global.myDB.connection);

                try
                {
                    cmd2.ExecuteNonQuery();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);

                    return false;
                }

                Carrega_Tag(txtSKUCombo.Text);


            }


            query = "INSERT INTO ITEM_COMBO (ID_COMBO, SKUITEM, CRIADOR, DT_CRIACAO) ";
            query = query + " VALUES( ";
            query = query + " '" + txtSKUCombo.Tag + "', '" + txtSKUMagento.Text + "', " ;
            query = query + " '" + strOperador + "', NOW()    );";
                
            

            if (global.myDB.connection.State == ConnectionState.Closed)
            {
                global.myDB.OpenConnection();
            }

            MySqlCommand cmd1 = new MySqlCommand(query, global.myDB.connection);

            try
            {
                cmd1.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);

                return false;
            }

            return true;
        }

        private void btnSair_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void cmdEntrada_Click(object sender, EventArgs e)
        {
            txtSKUMagento.Text = "";
            txtSKUCombo.Text = "";

            txtSKUCombo.Enabled = true;
            txtSKUMagento.Enabled = true;

            txtSKUCombo.ReadOnly = false;
            txtSKUCombo.Tag = "";
            
            lstCombos.Columns.Clear();
            lstCombos.Items.Clear();
        }

        private void lstLista_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void Carrega_Tag(String strSKUCombo)
        {
            MySqlCommand cmd;

            string query = " SELECT ID FROM COMBO ";

            query = query + " WHERE SKUCOMBO = '" + strSKUCombo + "'";

            if (global.myDB.connection.State == ConnectionState.Closed)
            {
                global.myDB.OpenConnection();
            }

            cmd = new MySqlCommand(query, global.myDB.connection);

            try
            {
                dataLista = cmd.ExecuteReader();
            }
            catch (Exception)
            {
              //  dataLista.Close();

                if (global.myDB.connection.State == ConnectionState.Closed)
                {
                    global.myDB.OpenConnection();
                }

                cmd = new MySqlCommand(query, global.myDB.connection);

                dataLista = cmd.ExecuteReader();
            }


            while (dataLista.Read())
            {
                txtSKUCombo.Tag = dataLista["ID"].ToString();
                
            }

            dataLista.Close();

            // Pesquisa();
        }

        private void cmdSaida_Click(object sender, EventArgs e)
        {
            ExcluiSelecionado();
        }

        private void ExcluiSelecionado()
        {
            foreach (ListViewItem item in lstCombos.Items)
            {
                if (item.Checked == true)
                {
                    ApagarItemCombo(item.SubItems[0].Text);
                }
            }

            Exibe_Registros();
        }

        private void ApagarItemCombo(string strID)
        {
            String query;

            global.myDB.CloseConnection();

            query = "DELETE FROM ITEM_COMBO WHERE ID = " + strID;

            if (global.myDB.connection.State == ConnectionState.Closed)
            {
                global.myDB.OpenConnection();
            }

            MySqlCommand cmd1 = new MySqlCommand(query, global.myDB.connection);

            try
            {
                cmd1.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "PontoZero Informa");
            }
        }

        private bool SKUExiste(string strSKU) 
        {
            MySqlCommand cmd;
            bool blAchou = false;

            string query = " SELECT * ";
            query = query + " FROM COMBO ";
            query = query + " WHERE SKUCOMBO = '" + strSKU + "'";

            if (global.myDB.connection.State == ConnectionState.Closed)
            {
                global.myDB.OpenConnection();
            }

            cmd = new MySqlCommand(query, global.myDB.connection);

            try
            {
                dataLista = cmd.ExecuteReader();
            }
            catch (Exception)
            {
                dataLista.Close();

                if (global.myDB.connection.State == ConnectionState.Closed)
                {
                    global.myDB.OpenConnection();
                }

                cmd = new MySqlCommand(query, global.myDB.connection);

                dataLista = cmd.ExecuteReader();
            }

            while (dataLista.Read())
            {
                blAchou = true;
            }

            dataLista.Close();

            
            return blAchou;
            // Pesquisa();
        }
    }
}
