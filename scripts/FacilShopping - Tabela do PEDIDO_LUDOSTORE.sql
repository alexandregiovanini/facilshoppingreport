CREATE TABLE PEDIDO_LUDOSTORE (id INT NOT NULL PRIMARY KEY AUTO_INCREMENT, 
							  id_pedido INT NOT NULL,
                              dt_pedido datetime NOT NULL,
                              dt_integracao datetime NOT NULL,
                              nome CHAR(30) NULL,
                              cpf CHAR(20) NULL,
                              endereco CHAR(80) NULL,
                              complemento CHAR(30) NULL,
                              cep  CHAR(9) NULL,
                              bairro CHAR(60) NULL,
                              numero CHAR(5) NULL,
                              cidade CHAR(30) NULL,
                              UF CHAR(2) NULL,
                              pagamento CHAR(30) NULL,
                              cd_rastreio  CHAR(30) NULL,
                              integrado bit NULL,
                              status char(30) NULL,
                              operador char(30) NULL);
/*
AGUARDANDO ENVIO
AGUARDANDO PAGAMENTO
CRIADO
ENTREGUE
ENVIADO
PAGAMENTO EFETUADO

SEPARADO, FALSE) SEPARADO, COALESCE(VENDA_ITEM.SEPARAR*/                              
                            
CREATE TABLE ITENS_PEDIDO_LUDOSTORE (id INT NOT NULL PRIMARY KEY AUTO_INCREMENT, 
									 id_pedido INT NOT NULL,
                                     sku CHAR(30) NOT NULL,
                                     id_produto INT NOT NULL,
                                     nm_produto CHAR(60) NULL,
                                     sub_titulo CHAR(30) NULL,
                                     qtde int not null,
									 vl_unitario DOUBLE NOT NULL,
                                     vl_total DOUBLE NOT NULL,
                                     integrado bit NULL,
                                     separado bit default 0,
                                     separar bit default 0,
                                     data_impressao datetime NULL);

SELECT * FROM PEDIDO_LUDOSTORE;
delete FROM PEDIDO_LUDOSTORE where id = 5;
SELECT CASE WHEN coalesce(data_impressao,'')  = '' THEN 1 ELSE 2 END  RETORNO, ITENS_PEDIDO_LUDOSTORE.* FROM ITENS_PEDIDO_LUDOSTORE ;

select * FROM ITENS_PEDIDO_LUDOSTORE;



DROP TABLE PEDIDO_LUDOSTORE;
DROP TABLE ITENS_PEDIDO_LUDOSTORE;


SELECT * FROM sales_flat_order_status_history;


alter table ITENS_PEDIDO_LUDOSTORE add separado bit default 0 ;
alter table PEDIDO_LUDOSTORE add operador char(30) NULL;