﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FacilShoppingReports
{
    public partial class frmCEPEncontrado : Form
    {
        public MySqlDataReader dataReaderPedidos;

        public string strPedido;
        public string strLoja;
        public string strCanal;
        public string strConta;
        public string strNome;
        public string strSobrenome;
        public string strPrimeiroNome;
        public string strNomeMeio;
        public string strUltimoNome;
        public string strPedidoPlataforma;
        public string strChave;
        public string strNF;
        public string strNFSerie;
        public string strDtEmissao;
        public string strEndereco1;
        public string strEndereco2;
        public string strEndereco3;
        public string strSeparado;

        public frmCEPEncontrado()
        {
            InitializeComponent();
        }

        private void frmCEPEncontrado_Load(object sender, EventArgs e)
        {
            strPedido = "";
            strLoja = "";
            strCanal = "";
            strConta = "";
            strNome = "";
            strSobrenome = "";
            strPrimeiroNome = "";
            strNomeMeio = "";
            strUltimoNome = "";
            strPedidoPlataforma = "";
            strChave = "";
            strNF = "";
            strNFSerie = "";
            strDtEmissao = "";
            strEndereco1 = "";
            strEndereco2 = "";
            strEndereco3 = "";
            strSeparado = "";


            lstPedidos.Columns.Clear();
            lstPedidos.Items.Clear();

            lstPedidos.CheckBoxes = false;

            lstPedidos.Columns.Add("Pedido Plataforma", 100);


            lstPedidos.Columns.Add("Pedido", 80);
            lstPedidos.Columns.Add("Loja", 110);
            lstPedidos.Columns.Add("Canal", 100);
            lstPedidos.Columns.Add("Conta", 100);
            lstPedidos.Columns.Add("Nome", 200);
            lstPedidos.Columns.Add("Sobrenome", 130);
            lstPedidos.Columns.Add("PrimeiroNome", 130);
            lstPedidos.Columns.Add("NomeMeio", 130);
            lstPedidos.Columns.Add("UltimoNome", 130);
            lstPedidos.Columns.Add("chave", 130);
            lstPedidos.Columns.Add("NF", 130);
            lstPedidos.Columns.Add("Serie", 130);
            lstPedidos.Columns.Add("DtEmissao", 130);

            lstPedidos.Columns.Add("Endereco1", 130);
            lstPedidos.Columns.Add("Endereco2", 130);
            lstPedidos.Columns.Add("Endereco3", 130);
            lstPedidos.Columns.Add("separado", 130);

            while (dataReaderPedidos.Read())
            {

                ListViewItem item;

                if(dataReaderPedidos["dt_emissao"].ToString() != "")
                {
                    strDtEmissao = System.DateTime.Parse(dataReaderPedidos["dt_emissao"].ToString()).ToString("dd/MM/yyyy");
                }
                else
                {
                    strDtEmissao = DateTime.Now.ToString("dd-MM-yyyy");
                }
                                

                item = new ListViewItem(new[] { dataReaderPedidos["canal_id"].ToString(),  dataReaderPedidos["increment_id"].ToString(), dataReaderPedidos["LOJA"].ToString().ToUpper(), dataReaderPedidos["CANAL"].ToString().ToUpper(), dataReaderPedidos["CONTALOJA"].ToString(), dataReaderPedidos["firstname"].ToString(), dataReaderPedidos["lastname"].ToString(), dataReaderPedidos["customer_firstname"].ToString(), dataReaderPedidos["customer_middlename"].ToString(), dataReaderPedidos["customer_lastname"].ToString(), dataReaderPedidos["chave"].ToString(), dataReaderPedidos["nf"].ToString(), dataReaderPedidos["serie"].ToString(), strDtEmissao, dataReaderPedidos["endereco"].ToString(), dataReaderPedidos["complemento"].ToString() + " " + dataReaderPedidos["bairro"].ToString(), dataReaderPedidos["cep"].ToString() + " " + dataReaderPedidos["cidade"].ToString() + " " + dataReaderPedidos["uf"].ToString(), dataReaderPedidos["separado"].ToString() });
                
                lstPedidos.Items.Add(item);
                
                
            }

            if(lstPedidos.Items.Count == 1)
            {
                foreach (ListViewItem item in lstPedidos.Items)
                {
                    strPedidoPlataforma = item.SubItems[0].Text;
                    strPedido = item.SubItems[1].Text;
                    strLoja = item.SubItems[2].Text;
                    strCanal = item.SubItems[3].Text;
                    strConta = item.SubItems[4].Text;
                    strNome = item.SubItems[5].Text;
                    strSobrenome = item.SubItems[6].Text;
                    strPrimeiroNome = item.SubItems[7].Text;
                    strNomeMeio = item.SubItems[8].Text;
                    strUltimoNome = item.SubItems[9].Text;
                    strChave = item.SubItems[10].Text;
                    strNF = item.SubItems[11].Text;
                    strNFSerie = item.SubItems[12].Text;
                    strDtEmissao = item.SubItems[13].Text;

                    strEndereco1 = item.SubItems[14].Text;
                    strEndereco2 = item.SubItems[15].Text;
                    strEndereco3 = item.SubItems[16].Text;

                    strSeparado = item.SubItems[17].Text;
                }
                        
                
                this.Close();
            }

            if (lstPedidos.Items.Count == 0)
            {
                this.Close();
            }

        }

        private void btnSair2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void lstPedidos_DoubleClick(object sender, EventArgs e)
        {
            strPedidoPlataforma = lstPedidos.SelectedItems[0].SubItems[1].Text;
            strPedido = lstPedidos.SelectedItems[0].SubItems[1].Text;
            strLoja = lstPedidos.SelectedItems[0].SubItems[2].Text;
            strCanal = lstPedidos.SelectedItems[0].SubItems[3].Text;
            strConta = lstPedidos.SelectedItems[0].SubItems[4].Text;
            strNome = lstPedidos.SelectedItems[0].SubItems[5].Text;
            strSobrenome = lstPedidos.SelectedItems[0].SubItems[6].Text;
            strPrimeiroNome = lstPedidos.SelectedItems[0].SubItems[7].Text;
            strNomeMeio = lstPedidos.SelectedItems[0].SubItems[8].Text;
            strUltimoNome = lstPedidos.SelectedItems[0].SubItems[9].Text;
            strChave = lstPedidos.SelectedItems[0].SubItems[10].Text;
            strNF = lstPedidos.SelectedItems[0].SubItems[11].Text;
            strNFSerie = lstPedidos.SelectedItems[0].SubItems[12].Text;
            strDtEmissao = lstPedidos.SelectedItems[0].SubItems[13].Text;

            strEndereco1 = lstPedidos.SelectedItems[0].SubItems[14].Text;
            strEndereco2 = lstPedidos.SelectedItems[0].SubItems[15].Text;
            strEndereco3 = lstPedidos.SelectedItems[0].SubItems[16].Text;

            strSeparado = lstPedidos.SelectedItems[0].SubItems[17].Text;

            this.Close();


        }
    }
}
