SELECT PEDIDOS_INTEGRADOS.NOME, ENDERECO, CEP, CIDADE, UF, CPF, 0 PESO, COALESCE(SEPARADO) SEPARADO, COALESCE(SEPARAR) SEPARAR, ITENS_PEDIDOS_INTEGRADOS.id ITEM_ID,  
PEDIDOS_INTEGRADOS.id_pedido PEDIDO_FS, PEDIDOS_INTEGRADOS.cod_pedido PEDIDO, dt_integracao, dt_pedido, ITENS_PEDIDOS_INTEGRADOS.nm_produto, ITENS_PEDIDOS_INTEGRADOS.sku,  
ITENS_PEDIDOS_INTEGRADOS.qtde,  ITENS_PEDIDOS_INTEGRADOS.vl_total, 0 FRETE, PEDIDOS_INTEGRADOS.marketplace, 'INTEGRADO', PAGAMENTO, PEDIDOS_INTEGRADOS.status,  
(SELECT value FROM catalog_category_entity_varchar  INNER JOIN catalog_category_product_index  ON catalog_category_product_index.category_id = catalog_category_entity_varchar.entity_id  WHERE attribute_id = 41 AND entity_id > 2  AND catalog_category_product_index.product_id = (select entity_id from catalog_product_entity where catalog_product_entity.SKU = ITENS_PEDIDOS_INTEGRADOS.sku)  ORDER BY entity_id DESC LIMIT 0,1) CATEGORIA,  
coalesce(IC.data, 0) DATA_LIDO, coalesce(EC.data_impressao, 0) DATA_IMPRESSAO,  
COALESCE((SELECT catalog_product_entity_decimal.value  FROM catalog_product_entity_decimal  INNER JOIN catalog_product_entity  ON catalog_product_entity.entity_id = catalog_product_entity_decimal.entity_id  WHERE catalog_product_entity_decimal.attribute_id = 173  AND catalog_product_entity.SKU = ITENS_PEDIDOS_INTEGRADOS.sku),0)  * ITENS_PEDIDOS_INTEGRADOS.qtde CUSTO  

from PEDIDOS_INTEGRADOS  

INNER join ITENS_PEDIDOS_INTEGRADOS  
ON ITENS_PEDIDOS_INTEGRADOS.id_pedido = PEDIDOS_INTEGRADOS.id_pedido  

left JOIN ITENS_ENVIO_CORREIOS IC  
ON IC.increment_id = ITENS_PEDIDOS_INTEGRADOS.id_pedido  

left JOIN ENVIO_CORREIOS EC  
ON EC.id = IC.id_ENVIO_CORREIOS  

INNER JOIN PRODUTOS_SKU
ON PRODUTOS_SKU.SKU =  ITENS_PEDIDOS_INTEGRADOS.sku

WHERE (dt_PEDIDO between '2019-12-01' AND '2019-12-30 23:59:59')  

AND (PRODUTOS_SKU.NOME like '%dix101%'OR PRODUTOS_SKU.sku like '%dix101%')

AND  (status <> 'Pagamento cancelado')   ORDER BY 14,12;



SELECT catalog_product_entity.sku SKU, catalog_product_entity_varchar.value
FROM catalog_product_entity_varchar
INNER JOIN  catalog_product_entity  ON catalog_product_entity.entity_id = catalog_product_entity_varchar.entity_id
where catalog_product_entity_varchar.attribute_id = 71
and (catalog_product_entity_varchar.value like '%galapagos%' OR catalog_product_entity.sku like '%galapagos%') limit 5000;


select * from catalog_product_entity where sku = 3743;
