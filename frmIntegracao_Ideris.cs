﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FacilShoppingReports
{
    public partial class frmIntegracao_Ideris : Form
    {
        public string strOperador;

        public frmIntegracao_Ideris()
        {
            InitializeComponent();
        }

        private void btnGrava_Click(object sender, EventArgs e)
        {
            if (Gravar() == true)
            {
                MessageBox.Show("Registro salvo com sucesso!!", "PontoZero Informa");
            }
        }

        private bool Gravar()
        {
            string query;
            
            query = "UPDATE IDERIS_TOKEN " +
                        " SET TOKEN = '" + txtToken.Text + "', USUARIO = '" + strOperador +  "'; ";
            

            if (global.myDB.connection.State == ConnectionState.Closed)
            {
                global.myDB.OpenConnection();
            }

            MySqlCommand cmd1 = new MySqlCommand(query, global.myDB.connection);

            try
            {
                cmd1.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return false;
            }

            return true;
        }

        private void btnSair_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void txtToken_TextChanged(object sender, EventArgs e)
        {

        }

        private void label4_Click(object sender, EventArgs e)
        {

        }
    }
}
