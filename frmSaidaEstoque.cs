﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FacilShoppingReports
{
    public partial class frmSaidaEstoque : Form
    {
        public frmSaidaEstoque()
        {
            InitializeComponent();
        }

        private void btPesquisar_Click(object sender, EventArgs e)
        {
            Pesquisa();
        }
        private void Pesquisa()
        {
            MySqlCommand cmd;
            MySqlDataReader dataListaItem;
            string strSKU;

            lstLista.Columns.Clear();
            lstLista.Items.Clear();


            string query = " SELECT catalog_product_entity.sku SKU, catalog_product_entity_varchar.value DESCRICAO, ";

            query = query + "  coalesce(itemEstoque.localidade, '') LOCAL , coalesce(itemEstoque.quantidade, '') QUANTIDADE,";
            query = query + "  (SELECT catalog_product_entity_media_gallery.value FROM catalog_product_entity_media_gallery INNER JOIN catalog_product_entity_media_gallery_value";
            query = query + "  ON catalog_product_entity_media_gallery_value.value_id = catalog_product_entity_media_gallery.value_id";
            query = query + "  WHERE catalog_product_entity_media_gallery.entity_id = catalog_product_entity_varchar.entity_id ORDER BY catalog_product_entity_media_gallery_value.position LIMIT 0,1) IMAGEM  ,";

            query = query + "  `cataloginventory_stock_item`.qty, catalog_product_entity_text.value fabricante, ";
            query = query + " (SELECT value FROM catalog_product_entity_varchar WHERE catalog_product_entity_varchar.attribute_id = 84 ";
            query = query + " AND cataloginventory_stock_item.product_id = catalog_product_entity_varchar.entity_id) FORNECEDOR, EAN.VALUE EAN ";
            query = query + " FROM `catalog_product_entity_varchar`  ";

            query = query + " INNER JOIN catalog_product_entity_varchar EAN ";
            query = query + " ON EAN.attribute_id = 169 ";
            query = query + " AND EAN.entity_id = catalog_product_entity_varchar.entity_id";

            query = query + " INNER JOIN `cataloginventory_stock_item`  ";
            query = query + " ON `cataloginventory_stock_item`.product_id = `catalog_product_entity_varchar`.entity_id ";
            query = query + " INNER JOIN  `cataloginventory_stock_status` ";
            query = query + " ON `cataloginventory_stock_status`.product_id = `catalog_product_entity_varchar`.entity_id ";
            query = query + " LEFT JOIN catalog_product_entity_decimal ";
            query = query + " ON catalog_product_entity_decimal.attribute_id = 75 ";
            query = query + " AND catalog_product_entity_decimal.entity_id = catalog_product_entity_varchar.entity_id ";
            query = query + " INNER JOIN  `catalog_product_entity_text` ";
            query = query + " ON `catalog_product_entity_text`.entity_id = `catalog_product_entity_varchar`.entity_id ";
            query = query + " AND catalog_product_entity_text.attribute_id = 83 ";
            query = query + " INNER JOIN  `catalog_product_entity` ";
            query = query + " ON `catalog_product_entity`.entity_id = `catalog_product_entity_varchar`.entity_id ";

            query = query + " LEFT JOIN itemEstoque ";
            query = query + " ON itemEstoque.SKU = catalog_product_entity.sku ";

            query = query + " where catalog_product_entity_varchar.attribute_id = '71' ";

            if (txtPesquisa.Text.Length != 0)
            {
                query = query + " AND (catalog_product_entity_varchar.value LIKE '%" + txtPesquisa.Text + "%' OR catalog_product_entity.sku LIKE '%" + txtPesquisa.Text + "%' ";
                query = query + " OR  EAN.VALUE LIKE '%" + txtPesquisa.Text + "%') ";
            }

            query = query + " limit 0,10000;";

            if (global.myDB.connection.State == ConnectionState.Closed)
            {
                global.myDB.OpenConnection();
            }

            cmd = new MySqlCommand(query, global.myDB.connection);

            try
            {
                dataListaItem = cmd.ExecuteReader();
            }
            catch (Exception)
            {
                global.myDB.OpenConnection();
                cmd = new MySqlCommand(query, global.myDB.connection);
                //dataLista.Close();

                dataListaItem = cmd.ExecuteReader();
            }

            lstLista.Columns.Add("SKU", 100);
            lstLista.Columns.Add("DESCRICAO", 350);
            lstLista.Columns.Add("QUANT SIST", 90);
            lstLista.Columns.Add("FABRICANTE", 150);
            lstLista.Columns.Add("FORNECEDOR", 150);
            lstLista.Columns.Add("EAN", 150);

            strSKU = "";

            while (dataListaItem.Read())
            {
                strSKU = dataListaItem["SKU"].ToString();

                ListViewItem item = new ListViewItem(new[] { dataListaItem["SKU"].ToString(), dataListaItem["DESCRICAO"].ToString(), String.Format("{0:N}", dataListaItem["qty"]).ToString(), dataListaItem["fabricante"].ToString(), dataListaItem["FORNECEDOR"].ToString(), dataListaItem["EAN"].ToString()});

                lstLista.Items.Add(item);

            }

            dataListaItem.Close();
            if (lstLista.Items.Count == 0)
            {
                MessageBox.Show("Não existe nenhum produto com esses parametros!", "PontoZero Informa", MessageBoxButtons.OK);
            }
            else if (lstLista.Items.Count == 1)
            {
                MovimentaEstoque(strSKU);                
            }

            
        }

        private void MovimentaEstoque(string strSKU)
        {
            if (ValidaSeparacao(strSKU) == true)
            {
                txtPesquisa.Text = "";
                lstLista.Clear();
                txtPesquisa.Focus();

                // MessageBox.Show("ACHOU e gravou!", "PontoZero Informa");
            }
            else
            {

                if (MessageBox.Show("Esse produto não foi achado em nenhum pedido em aberto para separação!!! Grava mesmo assim? ", "PontoZero Informa", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1) == DialogResult.Yes)
                {
                    //DIGITAR SENHA
                    MovEstoque(strSKU, "SAIDA MESMO SEM PEDIDO");
                    txtPesquisa.Text = "";
                    lstLista.Clear();
                    txtPesquisa.Focus();
                }

            }
        }
        
        public void MovEstoque(string strSKU, string strMotivo)
        {
            MySqlCommand cmd;
            MySqlDataReader dataListaMov;
            string query;
            bool blAtualizou = false;

            query = "SELECT * FROM itemEstoque WHERE SKU = '" + strSKU + "'";


            if (global.myDB.connection.State == ConnectionState.Closed)
            {
                global.myDB.OpenConnection();
            }

            cmd = new MySqlCommand(query, global.myDB.connection);

            try
            {
                dataListaMov = cmd.ExecuteReader();
            }
            catch (Exception)
            {
                global.myDB.OpenConnection();
                cmd = new MySqlCommand(query, global.myDB.connection);

                dataListaMov = cmd.ExecuteReader();
            }


            if (dataListaMov.Read())
            {
                query = "UPDATE itemEstoque " +
                            " SET QUANTIDADE = QUANTIDADE - 1 WHERE SKU = '" + strSKU + "'; ";

            }
            else
            {
                query = "INSERT INTO itemEstoque (SKU, QUANTIDADE ) " +
                        " VALUES ( '" + strSKU + "', -1  )";

            }

            dataListaMov.Close();

            if (global.myDB.connection.State == ConnectionState.Closed)
            {
                global.myDB.OpenConnection();
            }

            MySqlCommand cmd1 = new MySqlCommand(query, global.myDB.connection);

            try
            {
                cmd1.ExecuteNonQuery();
                blAtualizou = true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                dataListaMov.Close();
            }

            if (blAtualizou == true)
            {

                query = "INSERT INTO MOVESTOQUE (SKU, QUANTIDADE, OBSERVACAO, DATA, ENTRADA, QUANT_ESTOQUE) ";
                query = query + " VALUES ( '" + strSKU + "', -1, '" + strMotivo + "', ";
                query = query + " NOW(), ";

                query = query + " FALSE, -1 );";

                if (global.myDB.connection.State == ConnectionState.Closed)
                {
                    global.myDB.OpenConnection();
                }

                cmd = new MySqlCommand(query, global.myDB.connection);

                try
                {
                    cmd.ExecuteNonQuery();
                }
                catch (Exception)
                {
                    global.myDB.OpenConnection();
                    cmd = new MySqlCommand(query, global.myDB.connection);
                    cmd.ExecuteNonQuery();
                }
            }

            dataListaMov.Close();

        }

        private void btnSair2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private bool ValidaSeparacao(string strSKU)
        {
            MySqlCommand cmd;
            string query;
            Boolean blValido;
            MySqlDataReader dataPedido;

            blValido = false;

            DateTime resultado = DateTime.MinValue;
            
            query = " select SKU, 0 BANCO, item_id id";
            query = query + " from sales_flat_order_item ";

            query = query + " where separar = true ";
            query = query + " and separado = false ";
            query = query + " and SKU = '" + strSKU + "'";
            query = query + " union ";

            query = query + " select sku, 1 BANCO, id  from ITENS_PEDIDOS_INTEGRADOS where separar = true and separado = false ";
            query = query + " and SKU = '" + strSKU + "'";

            if (global.myDB.connection.State == ConnectionState.Closed)
            {
                global.myDB.OpenConnection();
            }

            cmd = new MySqlCommand(query, global.myDB.connection);
            
            try
            {
                dataPedido = cmd.ExecuteReader();
            }
            catch (Exception)
            {
                global.myDB.OpenConnection();
                cmd = new MySqlCommand(query, global.myDB.connection);
                //dataLista.Close();

                dataPedido = cmd.ExecuteReader();
            }


            try
            {
                if (dataPedido.Read())
                {
                    if(dataPedido["BANCO"].ToString() == "0")
                    {
                        query = "UPDATE sales_flat_order_item SET separado = 1";
                        query = query + " WHERE item_id = '" + dataPedido["id"].ToString() + "'";

                    }
                    else
                    {
                        query = "UPDATE ITENS_PEDIDOS_INTEGRADOS SET separado = 1";
                        query = query + " WHERE id = '" + dataPedido["id"].ToString() + "'";

                    }


                    global.myDB.CloseConnection();

                    if (global.myDB.connection.State == ConnectionState.Closed)
                    {
                        global.myDB.OpenConnection();
                    }

                    MySqlCommand cmd1 = new MySqlCommand(query, global.myDB.connection);

                    try
                    {
                        cmd1.ExecuteNonQuery();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message, "PontoZero Informa");
                    }
                    

                    blValido = true;

                    MovEstoque(strSKU, "SAIDA POR PEDIDO");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "PontoZero Informa");
                blValido = false;
            }


            dataPedido.Close();

            return blValido;
        }

        private void lstLista_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void lstLista_DoubleClick(object sender, EventArgs e)
        {           
            MovimentaEstoque(lstLista.SelectedItems[0].SubItems[0].Text);
        }

        private void txtPesquisa_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((e.KeyChar.ToString() == "\r" || txtPesquisa.TextLength > 11))
            {
                txtPesquisa.Text = txtPesquisa.Text + e.KeyChar.ToString();
                Pesquisa();

                if (txtPesquisa.TextLength==0)
                {
                    e.KeyChar = '\0';
                }
            }

//            this.Text = this.Text + e.KeyChar.ToString();


        }
    }
}
