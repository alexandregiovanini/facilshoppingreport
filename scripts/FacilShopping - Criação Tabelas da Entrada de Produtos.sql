CREATE TABLE ENTRADA_PRODUTOS (id INT NOT NULL PRIMARY KEY AUTO_INCREMENT, 
						 data date NOT NULL,
                         data_vencimento date NOT NULL,
                         nf char(20) NOT NULL,     
                         fornecedor char(20) NULL,    
                         status char(20)  NOT NULL DEFAULT 'NÃO INTEGRADO',   
                         usuario char(20) NOT NULL,    
                         observacao CHAR(100) NULL) ;
                         
CREATE TABLE ITENS_ENTRADA_PRODUTOS (id INT NOT NULL PRIMARY KEY AUTO_INCREMENT, 
						 idEntrada INT NOT NULL,
						 DATA_ENTRADA date,                         
                         SKU char(30),     
                         DESCRICAO char(30),     
                         QUANTIDADE INT NOT NULL,   
                         CUSTO DOUBLE NOT NULL,  
                         VENDA DOUBLE NOT NULL,  
                         usuario char(20),    
                         INTEGRACAO_MAGENTO CHAR(100) NOT NULL DEFAULT 'NÃO INTEGRADO',
                         INTEGRACAO_IDERIS CHAR(100) NOT NULL DEFAULT 'NÃO INTEGRADO',
                         DATA_INTEGRACAO date NULL,                         
                         INTEGRADO BIT NOT NULL DEFAULT 0);     
                         

select * from ITENS_ENTRADA_PRODUTOS;
select * from ENTRADA_PRODUTOS;
select * from ENTRADA_PRODUTOS where data_vencimento = '2020-04-27';

SELECT *   FROM ENTRADA_PRODUTOS  WHERE nf like '%27/04/2020%' OR fornecedor like '%27/04/2020%'   OR observacao like '%27/04/2020%' OR status like '%27/04/2020%'  OR data_vencimento =  '2020/04/27'  limit 0,10000;


UPDATE ENTRADA_PRODUTOS 
SET data_vencimento = '2020-04-15',  
nf = '9874', 
fornecedor = 'TESTE', 
observacao = ''  WHERE ID = 2; 


