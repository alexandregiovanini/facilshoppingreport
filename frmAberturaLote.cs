﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;
using System.Windows.Forms;

namespace FacilShoppingReports
{
    public partial class frmAberturaLote : Form
    {
        MySqlDataReader dataReader;
        MySqlCommand cmd;

        public frmAberturaLote()
        {
            InitializeComponent();
        }

        private void frmAberturaLote_Load(object sender, EventArgs e)
        {
            CarregaAbertos();
        }

        private void CarregaAbertos()
        {

            btnMercadoEnvios.Enabled = true;
            btnB2W.Enabled = true;
            btnMagalu.Enabled = true;
            btnMercadoPlace.Enabled = true;
            btnTotalExpress.Enabled = true;
            btnSequoia.Enabled = true;
            btnCorreios.Enabled = true;
            btnAmazon.Enabled = true;

            string query = "SELECT * ";
            query = query + " FROM ENVIO_CORREIOS ENVIO ";
            query = query + "  WHERE ENVIO.DATA_IMPRESSAO IS NULL;";

            if (global.myDB.connection.State == ConnectionState.Closed)
            {
                global.myDB.OpenConnection();
            }

            cmd = new MySqlCommand(query, global.myDB.connection);

            try
            {
                dataReader = cmd.ExecuteReader();
            }
            catch (Exception)
            {
                // dataReader.Close();
                global.myDB.OpenConnection();
                cmd = new MySqlCommand(query, global.myDB.connection);
                dataReader = cmd.ExecuteReader();
            }

            while (dataReader.Read())
            {

                if (dataReader["mercadolivre"].ToString() == "1")
                {
                    btnMercadoEnvios.Enabled = false;
                }
                else if (dataReader["b2w"].ToString() == "1")
                {
                    btnB2W.Enabled = false;
                }
                else if (dataReader["MAGALU"].ToString() == "1")
                {
                    btnMagalu.Enabled = false;
                }
                else if (dataReader["mlplace"].ToString() == "1")
                {
                    btnMercadoPlace.Enabled = false;
                }
                else if (dataReader["totalexpress"].ToString() == "1")
                {
                    btnTotalExpress.Enabled = false;
                }
                else if (dataReader["sequoia"].ToString() == "1")
                {
                    btnSequoia.Enabled = false;
                }
                else if (dataReader["amazon"].ToString() == "1")
                {
                    btnAmazon.Enabled = false;
                }
                else
                {
                    btnCorreios.Enabled = false;
                }
            }

            dataReader.Close();

            if (global.myDB.connection.State != ConnectionState.Closed)
            {
                global.myDB.CloseConnection();
            }
        }

        private void GravaNovoEnvio(bool blMercadoLivre, bool blB2W, bool blMagalu, bool blMLPlace, bool blTotalExpress, bool blSequoia, bool blAmazon)
        {
            
            String query;
            string strNomeLote = "Correios";

            if (blMercadoLivre == true)
            {
                strNomeLote = "Mercado Livre";
            }
            else if (blB2W == true)
            {
                strNomeLote = "B2W";
            }
            else if(blMagalu == true)
            {
                strNomeLote = "Magalu";
            }
            else if (blMLPlace == true)
            {
                strNomeLote = "Mercado Place";
            }
            else if (blTotalExpress == true)
            {
                strNomeLote = "Total Express";
            }
            else if (blSequoia == true)
            {
                strNomeLote = "Sequoia";
            }
            else if (blAmazon == true)
            {
                strNomeLote = "Amazon";
            }

            if (MessageBox.Show("Confirma a Abertura do lote para "+ strNomeLote + "?", "PontoZero Informa", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1) == DialogResult.Yes)
            {
                query = "INSERT INTO ENVIO_CORREIOS (data_criacao, mercadolivre, b2w, magalu, mlplace, totalexpress, sequoia, amazon)" +
                   " VALUES ( '" + DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss") + "', " + blMercadoLivre + ", " + blB2W + ", " + blMagalu + ", " + blMLPlace + ", " + blTotalExpress + ", " + blSequoia + ", " + blAmazon + "); ";

                if (global.myDB.connection.State == ConnectionState.Closed)
                {
                    global.myDB.OpenConnection();
                }

                MySqlCommand cmd1 = new MySqlCommand(query, global.myDB.connection);

                try
                {
                    cmd1.ExecuteNonQuery();

                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, "PontoZero Informa");
                }

                if (global.myDB.connection.State != ConnectionState.Closed)
                {
                    global.myDB.CloseConnection();
                }

                CarregaAbertos();
            }
        }

        private void btnCorreios_Click(object sender, EventArgs e)
        {
            GravaNovoEnvio(false, false, false, false, false, false, false);
        }

        private void btnB2W_Click(object sender, EventArgs e)
        {
            GravaNovoEnvio(false, true, false, false, false, false, false );
        }

        private void btnMagalu_Click(object sender, EventArgs e)
        {
            GravaNovoEnvio(false, false, true, false, false, false, false);
        }

        private void btnMercadoEnvios_Click(object sender, EventArgs e)
        {
            GravaNovoEnvio(true, false, false, false, false, false, false);
        }

        private void btnMercadoPlace_Click(object sender, EventArgs e)
        {
            GravaNovoEnvio(false, false, false,true, false, false, false);
        }

        private void btnTotalExpress_Click(object sender, EventArgs e)
        {
            GravaNovoEnvio(false, false, false, false, true, false, false);
        }

        private void btnSequoia_Click(object sender, EventArgs e)
        {
           GravaNovoEnvio(false, false, false, false, false, true, false);
        }

        private void btnAmazon_Click(object sender, EventArgs e)
        {
            GravaNovoEnvio(false, false, false, false, false, false, true);
        }
    }
}
