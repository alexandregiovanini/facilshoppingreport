﻿namespace FacilShoppingReports
{
    partial class ExportacaoPlanilhaIderis
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lstEstoque = new System.Windows.Forms.ListView();
            this.btnExportar = new System.Windows.Forms.Button();
            this.btnSair = new System.Windows.Forms.Button();
            this.btPesquisar = new System.Windows.Forms.Button();
            this.lblAviso = new System.Windows.Forms.Label();
            this.cmdImportar = new System.Windows.Forms.Button();
            this.cmdErros = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // lstEstoque
            // 
            this.lstEstoque.Location = new System.Drawing.Point(12, 23);
            this.lstEstoque.Name = "lstEstoque";
            this.lstEstoque.Size = new System.Drawing.Size(859, 474);
            this.lstEstoque.TabIndex = 1;
            this.lstEstoque.UseCompatibleStateImageBehavior = false;
            this.lstEstoque.View = System.Windows.Forms.View.Details;
            // 
            // btnExportar
            // 
            this.btnExportar.Image = global::FacilShoppingReports.Properties.Resources.new_icon;
            this.btnExportar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnExportar.Location = new System.Drawing.Point(878, 68);
            this.btnExportar.Name = "btnExportar";
            this.btnExportar.Size = new System.Drawing.Size(85, 40);
            this.btnExportar.TabIndex = 17;
            this.btnExportar.Text = "Exportar";
            this.btnExportar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnExportar.UseVisualStyleBackColor = true;
            this.btnExportar.Click += new System.EventHandler(this.btnExportar_Click);
            // 
            // btnSair
            // 
            this.btnSair.Image = global::FacilShoppingReports.Properties.Resources.exit_icon;
            this.btnSair.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnSair.Location = new System.Drawing.Point(878, 160);
            this.btnSair.Name = "btnSair";
            this.btnSair.Size = new System.Drawing.Size(85, 40);
            this.btnSair.TabIndex = 16;
            this.btnSair.Text = "Sair";
            this.btnSair.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnSair.UseVisualStyleBackColor = true;
            this.btnSair.Click += new System.EventHandler(this.btnSair_Click);
            // 
            // btPesquisar
            // 
            this.btPesquisar.Image = global::FacilShoppingReports.Properties.Resources.search_icon1;
            this.btPesquisar.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
            this.btPesquisar.Location = new System.Drawing.Point(878, 22);
            this.btPesquisar.Name = "btPesquisar";
            this.btPesquisar.Size = new System.Drawing.Size(85, 40);
            this.btPesquisar.TabIndex = 15;
            this.btPesquisar.Text = "Pesquisar";
            this.btPesquisar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btPesquisar.UseVisualStyleBackColor = true;
            this.btPesquisar.Click += new System.EventHandler(this.btPesquisar_Click);
            // 
            // lblAviso
            // 
            this.lblAviso.AutoSize = true;
            this.lblAviso.Font = new System.Drawing.Font("Microsoft Sans Serif", 30F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAviso.Location = new System.Drawing.Point(18, 221);
            this.lblAviso.Name = "lblAviso";
            this.lblAviso.Size = new System.Drawing.Size(950, 46);
            this.lblAviso.TabIndex = 18;
            this.lblAviso.Text = "AGUARDE PESQUISANDO ESTOQUE NA BASE...";
            this.lblAviso.Visible = false;
            // 
            // cmdImportar
            // 
            this.cmdImportar.Image = global::FacilShoppingReports.Properties.Resources.Oxygen_Icons_org_Oxygen_Actions_bookmarks_organize;
            this.cmdImportar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.cmdImportar.Location = new System.Drawing.Point(877, 114);
            this.cmdImportar.Name = "cmdImportar";
            this.cmdImportar.Size = new System.Drawing.Size(85, 40);
            this.cmdImportar.TabIndex = 19;
            this.cmdImportar.Text = "Importar";
            this.cmdImportar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.cmdImportar.UseVisualStyleBackColor = true;
            this.cmdImportar.Click += new System.EventHandler(this.cmdImportar_Click);
            // 
            // cmdErros
            // 
            this.cmdErros.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.cmdErros.Location = new System.Drawing.Point(877, 206);
            this.cmdErros.Name = "cmdErros";
            this.cmdErros.Size = new System.Drawing.Size(85, 40);
            this.cmdErros.TabIndex = 20;
            this.cmdErros.Text = "Tabela de Erros";
            this.cmdErros.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.cmdErros.UseVisualStyleBackColor = true;
            this.cmdErros.Click += new System.EventHandler(this.cmdErros_Click);
            // 
            // ExportacaoPlanilhaIderis
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(969, 502);
            this.ControlBox = false;
            this.Controls.Add(this.cmdErros);
            this.Controls.Add(this.cmdImportar);
            this.Controls.Add(this.lblAviso);
            this.Controls.Add(this.btnExportar);
            this.Controls.Add(this.btnSair);
            this.Controls.Add(this.btPesquisar);
            this.Controls.Add(this.lstEstoque);
            this.MinimizeBox = false;
            this.Name = "ExportacaoPlanilhaIderis";
            this.Text = "Exportacao por Planilha - Ideris";
            this.Load += new System.EventHandler(this.ExportacaoPlanilhaIderis_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListView lstEstoque;
        private System.Windows.Forms.Button btnExportar;
        private System.Windows.Forms.Button btnSair;
        private System.Windows.Forms.Button btPesquisar;
        private System.Windows.Forms.Label lblAviso;
        private System.Windows.Forms.Button cmdImportar;
        private System.Windows.Forms.Button cmdErros;
    }
}