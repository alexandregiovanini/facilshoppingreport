﻿using MySql.Data.MySqlClient;
using System;
using System.Data;
using System.Net.Http.Headers;
using System.Windows.Forms;



namespace FacilShoppingReports
{
    public partial class frmEntradaProdutos : Form
    {
        MySqlDataReader dataLista;
        public string strOperador;

        public frmEntradaProdutos()
        {
            InitializeComponent();
            Limpa_Campos();
            Limpa_Campos_Itens();
        }

        private void btnSair_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnSair2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btPesquisar_Click(object sender, EventArgs e)
        {
            Pesquisa();
        }

        private void Pesquisa()
        {
            MySqlCommand cmd;

            lstLista.Columns.Clear();
            lstLista.Items.Clear();

            string query = " SELECT *  ";

            query = query + " FROM ENTRADA_PRODUTOS ";
            query = query + " WHERE nf like '%" + txtPesquisa.Text + "%' OR fornecedor like '%" + txtPesquisa.Text + "%'  ";
            query = query + " OR observacao like '%" + txtPesquisa.Text + "%' OR status like '%" + txtPesquisa.Text + "%' ";

            if (global.validarData(txtPesquisa.Text + " 00:00:00") == true)
            {
                query = query + " OR data_vencimento =  '" + System.DateTime.Parse(txtPesquisa.Text.ToString()).ToString("yyyy/MM/dd") + "' ";
            }

            query = query + " limit 0,10000;";

            if (global.myDB.connection.State == ConnectionState.Closed)
            {
                global.myDB.OpenConnection();
            }

            cmd = new MySqlCommand(query, global.myDB.connection);

            try
            {
                dataLista = cmd.ExecuteReader();
            }
            catch (Exception)
            {
                global.myDB.OpenConnection();
                cmd = new MySqlCommand(query, global.myDB.connection);
                //dataLista.Close();

                dataLista = cmd.ExecuteReader();
            }

            lstLista.Columns.Add("ID", 50);
            lstLista.Columns.Add("DT ENTRADA", 100);
            lstLista.Columns.Add("DT VENCIMENTO", 110);
            lstLista.Columns.Add("NF", 100);
            lstLista.Columns.Add("STATUS", 150);
            lstLista.Columns.Add("USUARIO", 80);
            lstLista.Columns.Add("FORNECEDOR", 150);
            lstLista.Columns.Add("OBSERVAÇÃO", 150);
            lstLista.Columns.Add("", 0);

            while (dataLista.Read())
            {
                ListViewItem item = new ListViewItem(new[] { dataLista["ID"].ToString(), System.DateTime.Parse(dataLista["data"].ToString()).ToString("dd/MM/yyyy"), System.DateTime.Parse(dataLista["data_vencimento"].ToString()).ToString("dd/MM/yyyy"), dataLista["nf"].ToString(), dataLista["status"].ToString(),  dataLista["usuario"].ToString(), dataLista["fornecedor"].ToString(), dataLista["observacao"].ToString(), " " });

                lstLista.Items.Add(item);
            }

            dataLista.Close();

            if (lstLista.Items.Count == 1)
            {
                lstLista.Items[0].Selected = true;
                Exibe_Registros();
                tabPrincipal.SelectedTab = tabPage2;
            }

        }

        private void Exibe_Registros()
        {
            Limpa_Campos();
            Limpa_Campos_Itens();

            CarregaNF();

        }

        private void Limpa_Campos()
        {
            txtFornecedor.Text = "";
            txtNF.Text = "";
            txtNF.Tag = "";
            dtVencimento.Text = "";
            lblDtEntrada.Text = "";
            lblStatus.Text = "";
            lblUsuario.Text = "";
            txtObs.Text = "";
            txtPesquisa.Text = "";

            lstItensNF.Clear();
            lstItensNF.Items.Clear();

            grupoNF.Enabled = true;

            //lstLista.Clear();
            //lstLista.Items.Clear();
        }

        private void Limpa_Campos_Itens()
        {
            txtSku.Text = "";
            lblDescricao.Text = "";
            txtQuantidade.Text = "";
            txtCusto.Text = "";
            txtVenda.Text = "";
        }

        private void CarregaNF()
        {
            txtNF.Text = lstLista.SelectedItems[0].SubItems[3].Text;
            txtNF.Tag = lstLista.SelectedItems[0].SubItems[0].Text;            
            
            lblDtEntrada.Text = lstLista.SelectedItems[0].SubItems[1].Text;
            dtVencimento.Value = System.DateTime.Parse(lstLista.SelectedItems[0].SubItems[2].Text);
            
            lblStatus.Text = lstLista.SelectedItems[0].SubItems[4].Text;
            lblUsuario.Text = lstLista.SelectedItems[0].SubItems[5].Text;
            txtFornecedor.Text = lstLista.SelectedItems[0].SubItems[6].Text;
            txtObs.Text = lstLista.SelectedItems[0].SubItems[7].Text;

            CarregaNFItens();

            if (lblStatus.Text == "INTEGRADO")
            {
                grupoNF.Enabled = false;
            }
            else
            {
                grupoNF.Enabled = true;
            }
            // Pesquisa();
        }

        private void CarregaNFItens()
        {
            MySqlCommand cmd;
            string strDtIntegracao;
            string strIntegrado;

            lstItensNF.Clear();
            lstItensNF.Items.Clear();

            string query = " SELECT * ";
            query = query + " FROM ITENS_ENTRADA_PRODUTOS ";

            query = query + " WHERE idEntrada = '" + txtNF.Tag + "' ORDER BY ID DESC";

            if (global.myDB.connection.State == ConnectionState.Closed)
            {
                global.myDB.OpenConnection();
            }

            cmd = new MySqlCommand(query, global.myDB.connection);

            try
            {
                dataLista = cmd.ExecuteReader();
            }
            catch (Exception)
            {
                dataLista.Close();

                if (global.myDB.connection.State == ConnectionState.Closed)
                {
                    global.myDB.OpenConnection();
                }

                cmd = new MySqlCommand(query, global.myDB.connection);

                dataLista = cmd.ExecuteReader();
            }

            lstItensNF.CheckBoxes = true;

            lstItensNF.Columns.Add("ID", 60);
            lstItensNF.Columns.Add("ENTRADA", 70);
            lstItensNF.Columns.Add("SKU", 80);
            lstItensNF.Columns.Add("DESCRICAO", 200);
            lstItensNF.Columns.Add("QTDE", 50);
            lstItensNF.Columns.Add("CUSTO", 50);
            lstItensNF.Columns.Add("VENDA", 50);
            lstItensNF.Columns.Add("USUARIO", 70);
            lstItensNF.Columns.Add("STATUS MAGENTO", 120);
            lstItensNF.Columns.Add("STATUS IDERIS", 120);
            lstItensNF.Columns.Add("INTEGRACAO", 100);
            lstItensNF.Columns.Add("INTEGRADO", 80);

            while (dataLista.Read())
            {
                if(dataLista["DATA_INTEGRACAO"].ToString() == "")
                {
                    strDtIntegracao = " ";
                }
                else
                {
                    strDtIntegracao = System.DateTime.Parse(dataLista["DATA_INTEGRACAO"].ToString()).ToString("dd/MM/yyyy");
                }

                if(dataLista["INTEGRADO"].ToString() == "0")
                {
                    strIntegrado = "NÃO";
                }
                else
                {
                    strIntegrado = "SIM";
                }
                

                ListViewItem item = new ListViewItem(new[] { dataLista["ID"].ToString(), System.DateTime.Parse(dataLista["DATA_ENTRADA"].ToString()).ToString("dd/MM/yyyy"), dataLista["SKU"].ToString(), dataLista["DESCRICAO"].ToString(), dataLista["QUANTIDADE"].ToString(), String.Format("{0:N}", dataLista["CUSTO"]).ToString(), String.Format("{0:N}", dataLista["VENDA"]).ToString(), dataLista["usuario"].ToString(), dataLista["INTEGRACAO_MAGENTO"].ToString(), dataLista["INTEGRACAO_IDERIS"].ToString(), strDtIntegracao, strIntegrado });
                lstItensNF.Items.Add(item);
            }

            dataLista.Close();
            
            
        }

        private void lstLista_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            Limpa_Campos();
            Limpa_Campos_Itens();

            CarregaNF();
                        
            tabPrincipal.SelectedTab = tabPage2;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Limpa_Campos();
            Limpa_Campos_Itens();

            tabPrincipal.SelectedTab = tabPage2;
        }

        private void btnGrava_Click(object sender, EventArgs e)
        {
            if (Valida() == true)
            {
                if (Gravar() == true)
                {
                    
                    MessageBox.Show("Registro gravado com sucesso!", "PontoZero Informa");

                    lstLista.Clear();
                    lstLista.Items.Clear();

                }
                else
                {
                    MessageBox.Show("Ocorreu algum erro na gravação!", "PontoZero Informa");
                }

            }
        }

        public bool Gravar()
        {

            string query;


            if (txtNF.Tag.ToString() == "")
            {

                query = "INSERT INTO ENTRADA_PRODUTOS (data, data_vencimento, nf, fornecedor, usuario, observacao ) ";
                query = query + " VALUES( ";
                query = query + " '" + DateTime.Now.ToString("yyyy-MM-dd") + "', '" + dtVencimento.Value.ToString("yyyy-MM-dd") + "', '" + txtNF.Text + "', ";
                query = query + "'" + txtFornecedor.Text + "', '" + strOperador + "', '" + txtObs.Text + "');";
                
                if (global.myDB.connection.State == ConnectionState.Closed)
                {
                    global.myDB.OpenConnection();
                }

                MySqlCommand cmd2 = new MySqlCommand(query, global.myDB.connection);

                try
                {
                    cmd2.ExecuteNonQuery();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);

                    return false;
                }

                Carrega_Tag();

            }
            else
            {
                query = "UPDATE ENTRADA_PRODUTOS SET data_vencimento = '" + dtVencimento.Value.ToString("yyyy-MM-dd") + "', ";
                query = query + " nf = '" + txtNF.Text + "',";
                query = query + " fornecedor = '" + txtFornecedor.Text + "',";
                query = query + " observacao = '" + txtObs.Text + "' ";
                query = query + " WHERE ID = " + txtNF.Tag.ToString() + "; ";

                global.myDB.CloseConnection();

                if (global.myDB.connection.State == ConnectionState.Closed)
                {
                    global.myDB.OpenConnection();
                }

                MySqlCommand cmd1 = new MySqlCommand(query, global.myDB.connection);

                try
                {
                    cmd1.ExecuteNonQuery();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, "PontoZero Informa");
                }

                if (global.myDB.connection.State != ConnectionState.Closed)
                {
                    global.myDB.CloseConnection();
                }
            }

            return true;
        }


        public bool GravarItem()
        {

            string query;


            if (txtNF.Tag.ToString() != "")
            {

                query = "INSERT INTO ITENS_ENTRADA_PRODUTOS (idEntrada, DATA_ENTRADA, SKU, DESCRICAO, QUANTIDADE, USUARIO";

                if(txtCusto.TextLength > 0 )
                {
                    query = query + ", CUSTO ";
                }

                if (txtVenda.TextLength > 0)
                {
                    query = query + ", VENDA ";
                }
                
                query = query + " )VALUES( ";

                query = query + " '" + txtNF.Tag.ToString() + "', '" + DateTime.Now.ToString("yyyy-MM-dd") + "', '" + txtSku.Text + "', ";
                query = query + "'" + lblDescricao.Text + "', '" + txtQuantidade.Text + "', '" + strOperador + "'";

                if (txtCusto.TextLength > 0)
                {
                    query = query + ", '" + txtCusto.Text + "'";
                }

                if (txtVenda.TextLength > 0)
                {
                    query = query + ", '" + txtVenda.Text + "'";
                }

                query = query + " ); ";


  

                if (global.myDB.connection.State == ConnectionState.Closed)
                {
                    global.myDB.OpenConnection();
                }

                MySqlCommand cmd2 = new MySqlCommand(query, global.myDB.connection);

                try
                {
                    cmd2.ExecuteNonQuery();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);

                    return false;
                }

                //Carrega_Tag();

            }

            return true;
        }

        private void Carrega_Tag()
        {
            MySqlCommand cmd;

            string query = " SELECT ID FROM ENTRADA_PRODUTOS ";

            query = query + " WHERE nf = '" + txtNF.Text + "' AND FORNECEDOR = '" + txtFornecedor.Text + "'";

            if (global.myDB.connection.State == ConnectionState.Closed)
            {
                global.myDB.OpenConnection();
            }

            cmd = new MySqlCommand(query, global.myDB.connection);

            try
            {
                dataLista = cmd.ExecuteReader();
            }
            catch (Exception)
            {
                //  dataLista.Close();

                if (global.myDB.connection.State == ConnectionState.Closed)
                {
                    global.myDB.OpenConnection();
                }

                cmd = new MySqlCommand(query, global.myDB.connection);

                dataLista = cmd.ExecuteReader();
            }


            while (dataLista.Read())
            {
                txtNF.Tag = dataLista["ID"].ToString();
            }

            dataLista.Close();

            // Pesquisa();
        }


        private bool Valida()
        {
            bool blRetorno;

            blRetorno = true;

            if (txtFornecedor.TextLength <= 0)
            {
                MessageBox.Show("Campo Fornecedor obrigatório!", "PontoZero Informa");

                blRetorno = false;
            }

            if (txtNF.TextLength <= 0)
            {
                MessageBox.Show("Campo NF obrigatório!", "PontoZero Informa");

                blRetorno = false;
            }

            if (global.validarData(dtVencimento.Value.ToString()) != true)
            {
                MessageBox.Show("Campo Data Vencimento obrigatório!", "PontoZero Informa");

                blRetorno = false;
            }
            
            return blRetorno;
        }

        private bool ValidaItens()
        {
            bool blRetorno;

            blRetorno = true;

            if (lblDescricao.Text == "")
            {
                MessageBox.Show("Digite um SKU válido!", "PontoZero Informa");

                blRetorno = false;
            }

            if (txtQuantidade.TextLength <= 0)
            {
                MessageBox.Show("Digite uma quantidade válida!", "PontoZero Informa");

                blRetorno = false;
            }

            return blRetorno;
        }

        private void label9_Click(object sender, EventArgs e)
        {

        }

        private void txtQuantidade_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtQuantidade_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar < 48 || e.KeyChar > 57)
            {
                e.KeyChar = '\0';
            }
        }

        private void txtCusto_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((e.KeyChar < 48 || e.KeyChar > 57) && e.KeyChar != 46 && e.KeyChar != 8 && e.KeyChar != 44)
            {
                e.KeyChar = '\0';
            }
        }

        private void txtVenda_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((e.KeyChar < 48 || e.KeyChar > 57) && e.KeyChar != 46 && e.KeyChar != 8 && e.KeyChar != 44)
            {
                e.KeyChar = '\0';
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (txtNF.Tag.ToString().Length > 0)
            {
                if(ValidaItens()==true)
                {
                    if (GravarItem() == true)
                    {
                        MessageBox.Show("Item gravado com sucesso!", "PontoZero Informa");
                        Limpa_Campos_Itens();
                        CarregaNFItens();
                    }
                    else
                    {
                        MessageBox.Show("Ocorreu algum erro na gravação do Item!", "PontoZero Informa");
                    }
                }

            }
        }

        private void btnExcluirItem_Click(object sender, EventArgs e)
        {
            if (txtNF.Tag.ToString().Length > 0)
            {
                if (MessageBox.Show("Confirma a Exclusão dos itens selecionados?", "PontoZero Informa", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1) == DialogResult.Yes)
                {
                    ExcluirItem();
                }                    
            }
        }

        private void ExcluirItem()
        {
            int l;

            l = 0;

            foreach (ListViewItem item in lstItensNF.Items)
            {
                l = l + 1;

                if (item.Checked == true && item.SubItems[9].Text == "NÃO INTEGRADO")
                {
                    global.ApagarItemEntrada(item.SubItems[0].Text);
                }
            }

            CarregaNFItens();
        }

        private void cmdSaida_Click(object sender, EventArgs e)
        {
            if (txtNF.Tag.ToString().Length > 0)
            {

            }
        }

        private void txtSku_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtSku_Leave(object sender, EventArgs e)
        {
            if (txtNF.Tag.ToString().Length > 0)
            {
                if (txtSku.Text != "")
                {
                    lblDescricao.Text = global.CarregaDescricao(txtSku.Text);
                }
            }
        }

        private void cmdIntegrar_Click(object sender, EventArgs e)
        {
            IntegrarIderis();
        }

        private async void IntegrarIderis()
        {
            int l;

            l = 0;

            foreach (ListViewItem item in lstItensNF.Items)
            {
                l = l + 1;

                if (item.SubItems[9].Text == "NÃO INTEGRADO")
                {
                    try
                    {
                        System.Net.Http.HttpClient cliente = new System.Net.Http.HttpClient();
                        
                        cliente.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJDdXN0b21lcklkIjoiNDE0OCIsIlVzZXJJZCI6IjcxMDgiLCJBdXRoZW50aWNhdGlvblRva2VuSWQiOiIxOCIsIlVzZXJJZEJhbmNvIjoiOCIsIm5iZiI6MTU5MDQzMTYxMCwiZXhwIjoxNTkwNTE4MDEwLCJpYXQiOjE1OTA0MzE2MTAsImlzcyI6Imh0dHBzOi8vYXBpLmlkZXJpcy5jb20uYnIiLCJhdWQiOiJodHRwczovL2FwaS5pZGVyaXMuY29tLmJyIn0.Yo9FLTp0WEygGxTFMBjbmI-ESid1kcKw7oQvJWtKtc8");

                        Object resultado = await cliente.GetStringAsync("http://api.ideris.com.br/Produto/?sku=" + item.SubItems[2].Text);


                    }
                    catch
                    {
                        throw;
                    }
                }
            }


        }
    }
}
