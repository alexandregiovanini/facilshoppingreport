﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FacilShoppingReports
{
    public partial class frmCorrecaoSKU : Form
    {
        public int intID;
        public string strSKU;

        public frmCorrecaoSKU()
        {
            InitializeComponent();
        }

        private void btnSair_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnGrava_Click(object sender, EventArgs e)
        {
            if (Gravar() == true)
            {
                MessageBox.Show("Registro salvo com sucesso!!", "PontoZero Informa");

                strSKU = txtSKU.Text;

                this.Close();
            }
        }

        private bool Gravar()
        {
            string query;

            query = "UPDATE ITENS_PEDIDOS_INTEGRADOS " +
                        " SET sku = '" + txtSKU.Text + "', ERRO_SKU = 0 WHERE ID = " + intID;


            if (global.myDB.connection.State == ConnectionState.Open )
            {
                global.myDB.CloseConnection();
            }

            global.myDB.OpenConnection();

            MySqlCommand cmd1 = new MySqlCommand(query, global.myDB.connection);

            try
            {
                cmd1.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return false;
            }

            return true;
        }
    }
}
