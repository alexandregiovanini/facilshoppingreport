DROP FUNCTION IF EXISTS fncPROCURA_PEDIDO_LUDOSTORE; 

#Criando FUNCTION
 DELIMITER $
 CREATE FUNCTION fncPROCURA_PEDIDO_LUDOSTORE(v_Pedido CHAR(20)) RETURNS bit
 BEGIN
  
  DECLARE v_Count INTEGER DEFAULT 0; 
  DECLARE v_Return bit DEFAULT false;
  
  SELECT * INTO v_Count FROM PEDIDO_LUDOSTORE WHERE ID_PEDIDO = v_Pedido;
  
  if v_Count > 0 then  
	set v_Return = true;  
  else
	set v_Return = false;
  END IF;
  
  return v_Return;
 END
 $
  
select fncPROCURA_PEDIDO_LUDOSTORE(645);