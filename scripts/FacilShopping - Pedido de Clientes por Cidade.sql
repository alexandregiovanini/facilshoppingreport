select VENDA.customer_firstname, VENDA.customer_lastname, VENDA.customer_email, ENDERECO.city CIDADE, 
VENDA.increment_id PEDIDOS, shipping_description
from sales_flat_order VENDA
inner JOIN sales_flat_order_address ENDERECO 
ON VENDA.entity_id = ENDERECO.parent_id
AND ENDERECO.address_type = 'shipping' 
AND ENDERECO.city LIKE '%FERRAZ%'
WHERE VENDA.canal = 'Loja'
AND shipping_description not in ('Correios - SEDEX', 'Correios - PAC','Correios - PAC - Em média 7 dia(s)')
and customer_email not like ('%facil%')
