﻿namespace FacilShoppingReports
{
    partial class frmEntradaProdutos
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmEntradaProdutos));
            this.tabPrincipal = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.label4 = new System.Windows.Forms.Label();
            this.lstLista = new System.Windows.Forms.ListView();
            this.txtPesquisa = new System.Windows.Forms.TextBox();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.txtObs = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.txtVenda = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.txtCusto = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.txtQuantidade = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.lblDescricao = new System.Windows.Forms.Label();
            this.txtSku = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.lstItensNF = new System.Windows.Forms.ListView();
            this.grupoNF = new System.Windows.Forms.GroupBox();
            this.txtNF = new System.Windows.Forms.TextBox();
            this.lblStatus = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.lblUsuario = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.lblDtEntrada = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.dtVencimento = new System.Windows.Forms.DateTimePicker();
            this.txtFornecedor = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.btnSair2 = new System.Windows.Forms.Button();
            this.btPesquisar = new System.Windows.Forms.Button();
            this.btnExcluirItem = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.cmdIntegrar = new System.Windows.Forms.Button();
            this.cmdSaida = new System.Windows.Forms.Button();
            this.btnGrava = new System.Windows.Forms.Button();
            this.btnSair = new System.Windows.Forms.Button();
            this.tabPrincipal.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.groupBox6.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.grupoNF.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabPrincipal
            // 
            this.tabPrincipal.Controls.Add(this.tabPage1);
            this.tabPrincipal.Controls.Add(this.tabPage2);
            this.tabPrincipal.Location = new System.Drawing.Point(12, 12);
            this.tabPrincipal.Name = "tabPrincipal";
            this.tabPrincipal.SelectedIndex = 0;
            this.tabPrincipal.Size = new System.Drawing.Size(829, 496);
            this.tabPrincipal.TabIndex = 3;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.button1);
            this.tabPage1.Controls.Add(this.label4);
            this.tabPage1.Controls.Add(this.btnSair2);
            this.tabPage1.Controls.Add(this.lstLista);
            this.tabPage1.Controls.Add(this.txtPesquisa);
            this.tabPage1.Controls.Add(this.btPesquisar);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(821, 470);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Procurar";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(16, 21);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(49, 13);
            this.label4.TabIndex = 20;
            this.label4.Text = "Localizar";
            // 
            // lstLista
            // 
            this.lstLista.FullRowSelect = true;
            this.lstLista.GridLines = true;
            this.lstLista.Location = new System.Drawing.Point(10, 49);
            this.lstLista.Name = "lstLista";
            this.lstLista.Size = new System.Drawing.Size(805, 415);
            this.lstLista.TabIndex = 18;
            this.lstLista.UseCompatibleStateImageBehavior = false;
            this.lstLista.View = System.Windows.Forms.View.Details;
            this.lstLista.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.lstLista_MouseDoubleClick);
            // 
            // txtPesquisa
            // 
            this.txtPesquisa.Location = new System.Drawing.Point(71, 16);
            this.txtPesquisa.Name = "txtPesquisa";
            this.txtPesquisa.Size = new System.Drawing.Size(267, 20);
            this.txtPesquisa.TabIndex = 17;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.txtObs);
            this.tabPage2.Controls.Add(this.label6);
            this.tabPage2.Controls.Add(this.groupBox6);
            this.tabPage2.Controls.Add(this.grupoNF);
            this.tabPage2.Controls.Add(this.btnSair);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(821, 470);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Cadastro";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // txtObs
            // 
            this.txtObs.Location = new System.Drawing.Point(82, 422);
            this.txtObs.MaxLength = 75;
            this.txtObs.Multiline = true;
            this.txtObs.Name = "txtObs";
            this.txtObs.Size = new System.Drawing.Size(642, 40);
            this.txtObs.TabIndex = 25;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(18, 436);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(65, 13);
            this.label6.TabIndex = 26;
            this.label6.Text = "Observação";
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.groupBox1);
            this.groupBox6.Controls.Add(this.lstItensNF);
            this.groupBox6.Location = new System.Drawing.Point(14, 111);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(801, 305);
            this.groupBox6.TabIndex = 24;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "Itens da Nota";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.btnExcluirItem);
            this.groupBox1.Controls.Add(this.button2);
            this.groupBox1.Controls.Add(this.txtVenda);
            this.groupBox1.Controls.Add(this.label12);
            this.groupBox1.Controls.Add(this.txtCusto);
            this.groupBox1.Controls.Add(this.label11);
            this.groupBox1.Controls.Add(this.txtQuantidade);
            this.groupBox1.Controls.Add(this.label10);
            this.groupBox1.Controls.Add(this.lblDescricao);
            this.groupBox1.Controls.Add(this.txtSku);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Location = new System.Drawing.Point(7, 9);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(788, 64);
            this.groupBox1.TabIndex = 20;
            this.groupBox1.TabStop = false;
            // 
            // txtVenda
            // 
            this.txtVenda.Location = new System.Drawing.Point(374, 39);
            this.txtVenda.MaxLength = 75;
            this.txtVenda.Name = "txtVenda";
            this.txtVenda.Size = new System.Drawing.Size(77, 20);
            this.txtVenda.TabIndex = 9;
            this.txtVenda.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtVenda_KeyPress);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(306, 42);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(65, 13);
            this.label12.TabIndex = 46;
            this.label12.Text = "Valor Venda";
            // 
            // txtCusto
            // 
            this.txtCusto.Location = new System.Drawing.Point(206, 39);
            this.txtCusto.MaxLength = 75;
            this.txtCusto.Name = "txtCusto";
            this.txtCusto.Size = new System.Drawing.Size(77, 20);
            this.txtCusto.TabIndex = 8;
            this.txtCusto.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtCusto_KeyPress);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(171, 42);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(34, 13);
            this.label11.TabIndex = 44;
            this.label11.Text = "Custo";
            // 
            // txtQuantidade
            // 
            this.txtQuantidade.Location = new System.Drawing.Point(69, 39);
            this.txtQuantidade.MaxLength = 75;
            this.txtQuantidade.Name = "txtQuantidade";
            this.txtQuantidade.Size = new System.Drawing.Size(77, 20);
            this.txtQuantidade.TabIndex = 7;
            this.txtQuantidade.TextChanged += new System.EventHandler(this.txtQuantidade_TextChanged);
            this.txtQuantidade.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtQuantidade_KeyPress);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(4, 42);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(62, 13);
            this.label10.TabIndex = 42;
            this.label10.Text = "Quantidade";
            // 
            // lblDescricao
            // 
            this.lblDescricao.AutoSize = true;
            this.lblDescricao.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDescricao.Location = new System.Drawing.Point(122, 13);
            this.lblDescricao.Name = "lblDescricao";
            this.lblDescricao.Size = new System.Drawing.Size(114, 20);
            this.lblDescricao.TabIndex = 41;
            this.lblDescricao.Text = "DESCRIÇÃO";
            // 
            // txtSku
            // 
            this.txtSku.Location = new System.Drawing.Point(39, 13);
            this.txtSku.MaxLength = 75;
            this.txtSku.Name = "txtSku";
            this.txtSku.Size = new System.Drawing.Size(77, 20);
            this.txtSku.TabIndex = 6;
            this.txtSku.TextChanged += new System.EventHandler(this.txtSku_TextChanged);
            this.txtSku.Leave += new System.EventHandler(this.txtSku_Leave);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(4, 16);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(29, 13);
            this.label9.TabIndex = 39;
            this.label9.Text = "SKU";
            // 
            // lstItensNF
            // 
            this.lstItensNF.FullRowSelect = true;
            this.lstItensNF.GridLines = true;
            this.lstItensNF.Location = new System.Drawing.Point(7, 79);
            this.lstItensNF.Name = "lstItensNF";
            this.lstItensNF.Size = new System.Drawing.Size(788, 218);
            this.lstItensNF.TabIndex = 19;
            this.lstItensNF.UseCompatibleStateImageBehavior = false;
            this.lstItensNF.View = System.Windows.Forms.View.Details;
            // 
            // grupoNF
            // 
            this.grupoNF.Controls.Add(this.txtNF);
            this.grupoNF.Controls.Add(this.lblStatus);
            this.grupoNF.Controls.Add(this.label8);
            this.grupoNF.Controls.Add(this.groupBox2);
            this.grupoNF.Controls.Add(this.lblUsuario);
            this.grupoNF.Controls.Add(this.label7);
            this.grupoNF.Controls.Add(this.lblDtEntrada);
            this.grupoNF.Controls.Add(this.label5);
            this.grupoNF.Controls.Add(this.label2);
            this.grupoNF.Controls.Add(this.dtVencimento);
            this.grupoNF.Controls.Add(this.txtFornecedor);
            this.grupoNF.Controls.Add(this.label3);
            this.grupoNF.Controls.Add(this.label1);
            this.grupoNF.Location = new System.Drawing.Point(14, 2);
            this.grupoNF.Name = "grupoNF";
            this.grupoNF.Size = new System.Drawing.Size(798, 103);
            this.grupoNF.TabIndex = 20;
            this.grupoNF.TabStop = false;
            // 
            // txtNF
            // 
            this.txtNF.Location = new System.Drawing.Point(35, 19);
            this.txtNF.MaxLength = 75;
            this.txtNF.Name = "txtNF";
            this.txtNF.Size = new System.Drawing.Size(175, 20);
            this.txtNF.TabIndex = 28;
            // 
            // lblStatus
            // 
            this.lblStatus.AutoSize = true;
            this.lblStatus.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblStatus.Location = new System.Drawing.Point(244, 58);
            this.lblStatus.Name = "lblStatus";
            this.lblStatus.Size = new System.Drawing.Size(101, 20);
            this.lblStatus.TabIndex = 27;
            this.lblStatus.Text = "Fornecedor";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(203, 61);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(40, 13);
            this.label8.TabIndex = 26;
            this.label8.Text = "Status:";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.cmdIntegrar);
            this.groupBox2.Controls.Add(this.cmdSaida);
            this.groupBox2.Controls.Add(this.btnGrava);
            this.groupBox2.Location = new System.Drawing.Point(525, 42);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(267, 59);
            this.groupBox2.TabIndex = 25;
            this.groupBox2.TabStop = false;
            // 
            // lblUsuario
            // 
            this.lblUsuario.AutoSize = true;
            this.lblUsuario.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblUsuario.Location = new System.Drawing.Point(691, 19);
            this.lblUsuario.Name = "lblUsuario";
            this.lblUsuario.Size = new System.Drawing.Size(101, 20);
            this.lblUsuario.TabIndex = 22;
            this.lblUsuario.Text = "Fornecedor";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(639, 24);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(46, 13);
            this.label7.TabIndex = 21;
            this.label7.Text = "Usuário:";
            // 
            // lblDtEntrada
            // 
            this.lblDtEntrada.AutoSize = true;
            this.lblDtEntrada.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDtEntrada.Location = new System.Drawing.Point(535, 18);
            this.lblDtEntrada.Name = "lblDtEntrada";
            this.lblDtEntrada.Size = new System.Drawing.Size(101, 20);
            this.lblDtEntrada.TabIndex = 20;
            this.lblDtEntrada.Text = "Fornecedor";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(468, 22);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(61, 13);
            this.label5.TabIndex = 18;
            this.label5.Text = "Dt Entrada:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(8, 61);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(77, 13);
            this.label2.TabIndex = 16;
            this.label2.Text = "Dt Vencimento";
            // 
            // dtVencimento
            // 
            this.dtVencimento.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtVencimento.Location = new System.Drawing.Point(91, 58);
            this.dtVencimento.Name = "dtVencimento";
            this.dtVencimento.Size = new System.Drawing.Size(91, 20);
            this.dtVencimento.TabIndex = 15;
            // 
            // txtFornecedor
            // 
            this.txtFornecedor.Location = new System.Drawing.Point(284, 19);
            this.txtFornecedor.MaxLength = 75;
            this.txtFornecedor.Name = "txtFornecedor";
            this.txtFornecedor.Size = new System.Drawing.Size(175, 20);
            this.txtFornecedor.TabIndex = 0;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(217, 22);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(61, 13);
            this.label3.TabIndex = 14;
            this.label3.Text = "Fornecedor";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(8, 22);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(21, 13);
            this.label1.TabIndex = 10;
            this.label1.Text = "NF";
            // 
            // button1
            // 
            this.button1.Image = global::FacilShoppingReports.Properties.Resources.new_icon;
            this.button1.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button1.Location = new System.Drawing.Point(637, 3);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(85, 40);
            this.button1.TabIndex = 21;
            this.button1.Text = "Novo";
            this.button1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // btnSair2
            // 
            this.btnSair2.Image = global::FacilShoppingReports.Properties.Resources.exit_icon;
            this.btnSair2.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnSair2.Location = new System.Drawing.Point(728, 3);
            this.btnSair2.Name = "btnSair2";
            this.btnSair2.Size = new System.Drawing.Size(85, 40);
            this.btnSair2.TabIndex = 19;
            this.btnSair2.Text = "Sair";
            this.btnSair2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnSair2.UseVisualStyleBackColor = true;
            this.btnSair2.Click += new System.EventHandler(this.btnSair2_Click);
            // 
            // btPesquisar
            // 
            this.btPesquisar.Image = global::FacilShoppingReports.Properties.Resources.search_icon1;
            this.btPesquisar.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
            this.btPesquisar.Location = new System.Drawing.Point(546, 3);
            this.btPesquisar.Name = "btPesquisar";
            this.btPesquisar.Size = new System.Drawing.Size(85, 40);
            this.btPesquisar.TabIndex = 16;
            this.btPesquisar.Text = "Pesquisar";
            this.btPesquisar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btPesquisar.UseVisualStyleBackColor = true;
            this.btPesquisar.Click += new System.EventHandler(this.btPesquisar_Click);
            // 
            // btnExcluirItem
            // 
            this.btnExcluirItem.Image = global::FacilShoppingReports.Properties.Resources.application_delete_icon;
            this.btnExcluirItem.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnExcluirItem.Location = new System.Drawing.Point(616, 13);
            this.btnExcluirItem.Name = "btnExcluirItem";
            this.btnExcluirItem.Size = new System.Drawing.Size(85, 40);
            this.btnExcluirItem.TabIndex = 11;
            this.btnExcluirItem.Text = "Excluir";
            this.btnExcluirItem.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnExcluirItem.UseVisualStyleBackColor = true;
            this.btnExcluirItem.Click += new System.EventHandler(this.btnExcluirItem_Click);
            // 
            // button2
            // 
            this.button2.Image = global::FacilShoppingReports.Properties.Resources.Add_Folder_icon;
            this.button2.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button2.Location = new System.Drawing.Point(525, 13);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(85, 40);
            this.button2.TabIndex = 10;
            this.button2.Text = "Incluir";
            this.button2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // cmdIntegrar
            // 
            this.cmdIntegrar.Image = global::FacilShoppingReports.Properties.Resources.Network_Upload_icon;
            this.cmdIntegrar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.cmdIntegrar.Location = new System.Drawing.Point(176, 11);
            this.cmdIntegrar.Name = "cmdIntegrar";
            this.cmdIntegrar.Size = new System.Drawing.Size(85, 40);
            this.cmdIntegrar.TabIndex = 19;
            this.cmdIntegrar.Text = "Integrar";
            this.cmdIntegrar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.cmdIntegrar.UseVisualStyleBackColor = true;
            this.cmdIntegrar.Click += new System.EventHandler(this.cmdIntegrar_Click);
            // 
            // cmdSaida
            // 
            this.cmdSaida.Image = global::FacilShoppingReports.Properties.Resources.Oxygen_Icons_org_Oxygen_Actions_bookmarks_organize;
            this.cmdSaida.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.cmdSaida.Location = new System.Drawing.Point(90, 11);
            this.cmdSaida.Name = "cmdSaida";
            this.cmdSaida.Size = new System.Drawing.Size(85, 40);
            this.cmdSaida.TabIndex = 18;
            this.cmdSaida.Text = "Excluir";
            this.cmdSaida.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.cmdSaida.UseVisualStyleBackColor = true;
            this.cmdSaida.Click += new System.EventHandler(this.cmdSaida_Click);
            // 
            // btnGrava
            // 
            this.btnGrava.Image = ((System.Drawing.Image)(resources.GetObject("btnGrava.Image")));
            this.btnGrava.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnGrava.Location = new System.Drawing.Point(4, 11);
            this.btnGrava.Name = "btnGrava";
            this.btnGrava.Size = new System.Drawing.Size(85, 40);
            this.btnGrava.TabIndex = 16;
            this.btnGrava.Text = "Grava";
            this.btnGrava.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnGrava.UseVisualStyleBackColor = true;
            this.btnGrava.Click += new System.EventHandler(this.btnGrava_Click);
            // 
            // btnSair
            // 
            this.btnSair.Image = global::FacilShoppingReports.Properties.Resources.exit_icon;
            this.btnSair.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnSair.Location = new System.Drawing.Point(730, 422);
            this.btnSair.Name = "btnSair";
            this.btnSair.Size = new System.Drawing.Size(85, 40);
            this.btnSair.TabIndex = 12;
            this.btnSair.Text = "Sair";
            this.btnSair.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnSair.UseVisualStyleBackColor = true;
            this.btnSair.Click += new System.EventHandler(this.btnSair_Click);
            // 
            // frmEntradaProdutos
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(845, 520);
            this.ControlBox = false;
            this.Controls.Add(this.tabPrincipal);
            this.Name = "frmEntradaProdutos";
            this.Text = "Entrada de Produtos";
            this.tabPrincipal.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            this.groupBox6.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.grupoNF.ResumeLayout(false);
            this.grupoNF.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabPrincipal;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button btnSair2;
        private System.Windows.Forms.ListView lstLista;
        private System.Windows.Forms.TextBox txtPesquisa;
        private System.Windows.Forms.Button btPesquisar;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button cmdSaida;
        private System.Windows.Forms.Button btnGrava;
        private System.Windows.Forms.Button btnSair;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.ListView lstItensNF;
        private System.Windows.Forms.GroupBox grupoNF;
        private System.Windows.Forms.TextBox txtFornecedor;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label lblDtEntrada;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.DateTimePicker dtVencimento;
        private System.Windows.Forms.Label lblStatus;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label lblUsuario;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtObs;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtNF;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.TextBox txtVenda;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox txtCusto;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox txtQuantidade;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label lblDescricao;
        private System.Windows.Forms.TextBox txtSku;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Button btnExcluirItem;
        private System.Windows.Forms.Button cmdIntegrar;
    }
}