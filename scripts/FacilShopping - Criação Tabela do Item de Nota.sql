CREATE TABLE itemnfe (id INT NOT NULL PRIMARY KEY AUTO_INCREMENT, 
					  nfe_id INT NOT NULL,
                      item INT NOT NULL,
                      cod_fabricante INT NOT NULL,
					  produto_id INT NOT NULL,
                      ncm CHAR(10),
                      sosn CHAR(10),
                      cfop CHAR(10),
                      unidade CHAR(10),
                      quantidade DOUBLE,
                      valor_unitario DOUBLE,
                      valor_desconto DOUBLE,
                      valor_liquido DOUBLE,
                      valor_icms DOUBLE,
                      valor_ipi DOUBLE,
                      aliq_icms DOUBLE,
                      aliq_ipi DOUBLE);
                      
                      
                      select * from itemnfe;
                      