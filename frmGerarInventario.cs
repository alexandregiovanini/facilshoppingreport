﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FacilShoppingReports
{
    public partial class frmGerarInventario : Form
    {
        public frmGerarInventario()
        {
            InitializeComponent();
        }

        private void txtQuantidade_TextChanged(object sender, EventArgs e)
        {

        }

        private void cmdGerar_Click(object sender, EventArgs e)
        {
            PesquisarInventario();
        }

        private void txtQuantidade_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar < 48 || e.KeyChar > 57)
            {
                if (e.KeyChar != 8)
                {
                    e.KeyChar = '\0';
                }
            }
        }

        private void PesquisarInventario()
        {
            string strSQL;
            MySqlCommand cmd;
            MySqlDataReader dataReader2;
            MySqlDataReader dataReader1;
            string strUPdate = "";

            //dataReader.Close();

            strSQL = "SELECT CATEGORIA FROM ESTOQUE_FECHAMENTO where fechado = 0 order by floor(rand() * 11)  LIMIT 1;";


            if (global.myDB.connection.State == ConnectionState.Closed)
            {
                global.myDB.OpenConnection();
            }

            cmd = new MySqlCommand(strSQL, global.myDB.connection);


            try
            {
                dataReader1 = cmd.ExecuteReader();
            }
            catch (Exception)
            {
                global.myDB.OpenConnection();
                cmd = new MySqlCommand(strSQL, global.myDB.connection);                
                dataReader1 = cmd.ExecuteReader();
            }

            
            if (dataReader1.Read())
            {


                strSQL = "SELECT DESCRICAO, SKU, QUANTIDADE FROM ESTOQUE_FECHAMENTO where fechado = 0 and categoria = '" + dataReader1["CATEGORIA"].ToString() + "'order by floor(rand() * 11)  LIMIT " + txtQuantidade.Text;

                if (global.myDB.connection.State == ConnectionState.Closed)
                {
                    global.myDB.OpenConnection();
                }

                cmd = new MySqlCommand(strSQL, global.myDB.connection);

                dataReader1.Close();

                try
                {
                    dataReader2 = cmd.ExecuteReader();
                }
                catch (Exception)
                {
                    global.myDB.OpenConnection();
                    cmd = new MySqlCommand(strSQL, global.myDB.connection);
                    dataReader2 = cmd.ExecuteReader();
                }
                

                try
                {
                    File.Delete("C:\\temp\\teste.csv");
                }
                catch (Exception ex)
                {

                }

                string strLinha = "SKU; DESCRICAO; QUANTIDADE";

                try
                {
                    using (StreamWriter writer = new StreamWriter("C:\\temp\\teste.csv", true))
                    {
                        writer.WriteLine(strLinha.Replace("\n", "").Replace("\"", ""));
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Erro Gerando XLS - " + ex.Message, "PontoZero Informa");
                    return;
                }

                while (dataReader2.Read())
                {
                    strLinha = dataReader2["SKU"].ToString() + ";" + dataReader2["DESCRICAO"].ToString() + ";" + dataReader2["QUANTIDADE"].ToString();

                    strUPdate = strUPdate + "'" + dataReader2["SKU"].ToString() + "',";
                    try
                    {
                        using (StreamWriter writer = new StreamWriter("C:\\temp\\teste.csv", true))
                        {
                            writer.WriteLine(strLinha.Replace("\n", "").Replace("\"", ""));
                        }
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show("Erro Gerando XLS - " + ex.Message, "PontoZero Informa");
                        return;
                    }
                }

                try
                {
                    using (StreamWriter writer = new StreamWriter("C:\\temp\\teste.csv", true))
                    System.Diagnostics.Process.Start(@"C:\\temp\\teste.csv");
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Erro Gerando XLS - " + ex.Message, "PontoZero Informa"); ;
                }

                dataReader2.Close();

                AtualizaInventario(strUPdate.Substring(0, strUPdate.Length - 1));
            }           

        }

        private void AtualizaInventario(string strSKUs)
        {
            String query = "UPDATE ESTOQUE_FECHAMENTO SET fechado = 1 , dt_fechamento = '" + DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss") + "' WHERE sku in (" + strSKUs + "); ";


                global.myDB.CloseConnection();

                if (global.myDB.connection.State == ConnectionState.Closed)
                {
                    global.myDB.OpenConnection();
                }

                MySqlCommand cmd1 = new MySqlCommand(query, global.myDB.connection);

                try
                {
                    cmd1.ExecuteNonQuery();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, "PontoZero Informa");
                }

                if (global.myDB.connection.State != ConnectionState.Closed)
                {
                    global.myDB.CloseConnection();
                }


        }


    }
}
