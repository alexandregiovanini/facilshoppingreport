CREATE TABLE CAPA_PEDIDO (id INT NOT NULL PRIMARY KEY AUTO_INCREMENT, 
							  id_pedido INT NOT NULL,
                              codigo CHAR(30) NULL,
                              marketplace CHAR(30) NULL,
                              status CHAR(20) NULL,
                              integrado bit default 0,
                              dt_pedido datetime NOT NULL);
                              

drop table CAPA_PEDIDO ;
drop table PEDIDOS_INTEGRADOS ;
drop table ITENS_PEDIDOS_INTEGRADOS ;
                              
CREATE TABLE PEDIDOS_INTEGRADOS (id INT NOT NULL PRIMARY KEY AUTO_INCREMENT, 
							  id_pedido INT NOT NULL,
                              cod_pedido CHAR(30) NULL,
                              dt_pedido datetime NOT NULL,
                              dt_integracao datetime NOT NULL,
                              nome CHAR(30) NULL,
                              cpf CHAR(20) NULL,
                              endereco CHAR(80) NULL,
                              complemento CHAR(30) NULL,
                              cep  CHAR(9) NULL,
                              bairro CHAR(60) NULL,
                              numero CHAR(5) NULL,
                              cidade CHAR(30) NULL,
                              UF CHAR(2) NULL,
                              pagamento CHAR(30) NULL,
                              cd_rastreio  CHAR(30) NULL,
                              LOJA  CHAR(50) NULL,
                              CONTALOJA  CHAR(50) NULL,
                              marketplace  CHAR(30) NULL,
                              vl_total DOUBLE NOT NULL,
                              nome_entrega CHAR(30) NULL,
                              nf CHAR(30) not NULL default 0,
                              separado bit default 0,
                              serie CHAR(30) NULL,
                              chave CHAR(130) NULL,
                              statusnf CHAR(30) NULL,
                              dt_emissao datetime NULL,
                              INTEGRADOCANCELAMENT0 bit NULL DEFAULT 0,                              
                              status char(30) NULL);

                            
CREATE TABLE ITENS_PEDIDOS_INTEGRADOS (id INT NOT NULL PRIMARY KEY AUTO_INCREMENT, 
									 id_pedido INT NOT NULL,
                                     sku CHAR(30) NOT NULL,
                                     id_produto CHAR(60) NULL,
                                     nm_produto CHAR(60) NULL,
                                     sub_titulo CHAR(30) NULL,
                                     qtde int not null,
									 vl_unitario DOUBLE NOT NULL,
                                     vl_total DOUBLE NOT NULL,
                                     integrado bit NULL,
                                     separado bit default 0,
                                     separar bit default 1,
                                     ERRO_SKU bit default 0,
                                     marketplace  CHAR(30) NULL,
                                     fechado bit default 0,
                                     data_impressao datetime NULL);

select * from CAPA_PEDIDO  limit 5000;
select * from PEDIDOS_INTEGRADOS where id_pedido = 7050 limit 5000;
select * from ITENS_PEDIDOS_INTEGRADOS limit 5000;

alter table ITENS_PEDIDOS_INTEGRADOS add COLUMN fechado bit default 0;
SET SQL_SAFE_UPDATES = 0;


select * from PEDIDOS_INTEGRADOS where CONTALOJA is not null order by id desc;