﻿
namespace FacilShoppingReports
{
    partial class frmStatusPedido
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtPedido = new System.Windows.Forms.TextBox();
            this.optIderis = new System.Windows.Forms.RadioButton();
            this.optMarketPlace = new System.Windows.Forms.RadioButton();
            this.cmdPesquisar = new System.Windows.Forms.Button();
            this.txtResultado = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // txtPedido
            // 
            this.txtPedido.Location = new System.Drawing.Point(257, 12);
            this.txtPedido.Name = "txtPedido";
            this.txtPedido.Size = new System.Drawing.Size(307, 20);
            this.txtPedido.TabIndex = 0;
            // 
            // optIderis
            // 
            this.optIderis.AutoSize = true;
            this.optIderis.Location = new System.Drawing.Point(26, 12);
            this.optIderis.Name = "optIderis";
            this.optIderis.Size = new System.Drawing.Size(86, 17);
            this.optIderis.TabIndex = 1;
            this.optIderis.TabStop = true;
            this.optIderis.Text = "Pedido Ideris";
            this.optIderis.UseVisualStyleBackColor = true;
            // 
            // optMarketPlace
            // 
            this.optMarketPlace.AutoSize = true;
            this.optMarketPlace.Location = new System.Drawing.Point(117, 12);
            this.optMarketPlace.Name = "optMarketPlace";
            this.optMarketPlace.Size = new System.Drawing.Size(121, 17);
            this.optMarketPlace.TabIndex = 2;
            this.optMarketPlace.TabStop = true;
            this.optMarketPlace.Text = "Pedido MarketPlace";
            this.optMarketPlace.UseVisualStyleBackColor = true;
            // 
            // cmdPesquisar
            // 
            this.cmdPesquisar.Location = new System.Drawing.Point(586, 14);
            this.cmdPesquisar.Name = "cmdPesquisar";
            this.cmdPesquisar.Size = new System.Drawing.Size(145, 26);
            this.cmdPesquisar.TabIndex = 3;
            this.cmdPesquisar.Text = "Pesquisar";
            this.cmdPesquisar.UseVisualStyleBackColor = true;
            this.cmdPesquisar.Click += new System.EventHandler(this.cmdPesquisar_Click);
            // 
            // txtResultado
            // 
            this.txtResultado.Location = new System.Drawing.Point(26, 47);
            this.txtResultado.Multiline = true;
            this.txtResultado.Name = "txtResultado";
            this.txtResultado.ReadOnly = true;
            this.txtResultado.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtResultado.Size = new System.Drawing.Size(705, 469);
            this.txtResultado.TabIndex = 4;
            this.txtResultado.TextChanged += new System.EventHandler(this.txtResultado_TextChanged);
            // 
            // frmStatusPedido
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(747, 542);
            this.Controls.Add(this.txtResultado);
            this.Controls.Add(this.cmdPesquisar);
            this.Controls.Add(this.optMarketPlace);
            this.Controls.Add(this.optIderis);
            this.Controls.Add(this.txtPedido);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmStatusPedido";
            this.Text = "Pesquisa Pedido";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txtPedido;
        private System.Windows.Forms.RadioButton optIderis;
        private System.Windows.Forms.RadioButton optMarketPlace;
        private System.Windows.Forms.Button cmdPesquisar;
        private System.Windows.Forms.TextBox txtResultado;
    }
}