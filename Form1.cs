﻿using System;
//using System.Collections.Generic;
//using System.ComponentModel;
using System.Data;
using System.Drawing;
//using System.Linq;
//using System.Text;
//using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;
using MySql.Data.MySqlClient;
using System.Net;
//using System.Text.RegularExpressions;
using System.IO;
using System.Net.Mail;
//using System.Data.OleDb;



namespace FacilShoppingReports
{
    public partial class frmRelatorio : Form
    {


        MySqlDataReader dataReader;
        private DSBarCode.BarCodeCtrl userControl11;
        bool blReimpressãoRastreio;


        public frmRelatorio()
        {
            InitializeComponent();
        }

        private void frmRelatorio_Load(object sender, EventArgs e)
        {
            CarregaCategoria();
            CarregaCanal();
            CarregaStatus();
            CarregaLoja();
            CarregaFabricante();
            optEstoque.Checked = true;

        }

        private void listView1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void CarregaCategoria()
        {
            MySqlCommand cmd;
            DataTable dataTable = new DataTable();

            string query = "select * " +
                           " from catalog_category_entity_varchar " +
                           " where attribute_id = 41 " +
                           " and entity_id > 2 " +
                           " order by value; ";

            global.myDB.CloseConnection();

            if (global.myDB.connection.State == ConnectionState.Closed)
            {
                global.myDB.OpenConnection();
            }

            cmd = new MySqlCommand(query, global.myDB.connection);

            dataReader = cmd.ExecuteReader();

            dataTable.Load(dataReader);

            cbCategorias.DisplayMember = "value";
            cbCategorias.ValueMember = "entity_id";
            cbCategorias.DataSource = dataTable;

            cbCategoriasVenda.DisplayMember = "value";
            cbCategoriasVenda.ValueMember = "entity_id";
            cbCategoriasVenda.DataSource = dataTable;

            dataReader.Close();

        }

        private void CarregaCanal()
        {
            MySqlCommand cmd;
            DataTable dataTable = new DataTable();

            string query = "SELECT 'Amazon' canal UNION SELECT 'B2W' canal UNION  SELECT 'LUDOSTORE' canal UNION SELECT DISTINCT CANAL FROM sales_flat_order where canal is not null and canal <> 'Amazon.com.br' UNION SELECT DISTINCT marketplace CANAL FROM PEDIDOS_INTEGRADOS where marketplace is not null order by canal;";

            if (global.myDB.connection.State == ConnectionState.Closed)
            {
                global.myDB.OpenConnection();
            }

            cmd = new MySqlCommand(query, global.myDB.connection);

            dataReader = cmd.ExecuteReader();

            dataTable.Load(dataReader);

            cbCanal.DisplayMember = "canal";
            cbCanal.ValueMember = "canal";
            cbCanal.DataSource = dataTable;

            dataReader.Close();
        }

        private void CarregaLoja()
        {
            MySqlCommand cmd;
            DataTable dataTable = new DataTable();

            string query = "SELECT DISTINCT LOJA FROM PEDIDOS_INTEGRADOS where loja is not null order by 1;";

            if (global.myDB.connection.State == ConnectionState.Closed)
            {
                global.myDB.OpenConnection();
            }

            cmd = new MySqlCommand(query, global.myDB.connection);

            dataReader = cmd.ExecuteReader();

            dataTable.Load(dataReader);

            dcLoja.DisplayMember = "Loja";
            dcLoja.ValueMember = "Loja";
            dcLoja.DataSource = dataTable;

            dataReader.Close();
        }



        private void CarregaStatus()
        {
            MySqlCommand cmd;
            DataTable dataTable = new DataTable();

            string query = "SELECT DISTINCT status FROM sales_flat_order where canal is not null union SELECT DISTINCT status FROM PEDIDO_LUDOSTORE UNION SELECT DISTINCT STATUS FROM PEDIDOS_INTEGRADOS order by status; ";

            if (global.myDB.connection.State == ConnectionState.Closed)
            {
                global.myDB.OpenConnection();
            }

            cmd = new MySqlCommand(query, global.myDB.connection);

            dataReader = cmd.ExecuteReader();

            dataTable.Load(dataReader);

            cbStatus.DisplayMember = "status";
            cbStatus.ValueMember = "status";
            cbStatus.DataSource = dataTable;

            dataReader.Close();
        }

        private void CarregaFabricante()
        {
            MySqlCommand cmd;
            DataTable dataTable = new DataTable();

            string query = "select distinct replace(replace(replace(replace(value,'FABRICANTE: ',''),'FABRICANTE:',''),'Fabricante:',''),'FORNECEDOR:','') fab";
            query = query + " from catalog_product_entity_text ";
            query = query + " where catalog_product_entity_text.attribute_id = 83 ";
            query = query + " and value is not null order by 1; ";

            //string query = "SELECT DISTINCT status FROM sales_flat_order where canal is not null union SELECT DISTINCT status FROM PEDIDO_LUDOSTORE UNION SELECT DISTINCT STATUS FROM PEDIDOS_INTEGRADOS order by status; ";

            if (global.myDB.connection.State == ConnectionState.Closed)
            {
                global.myDB.OpenConnection();
            }

            cmd = new MySqlCommand(query, global.myDB.connection);

            dataReader = cmd.ExecuteReader();

            dataTable.Load(dataReader);

            cbFabricante.DisplayMember = "fab";
            cbFabricante.ValueMember = "fab";

            //cbFabricante.DisplayMember = "status";
            //cbFabricante.ValueMember = "status";
            cbFabricante.DataSource = dataTable;

            dataReader.Close();
        }
        private void Pesquisa()
        {
            if (optVenda.Checked == true)
            {
                PesquisaPedido();
            }

            else if (optDeclaração.Checked == true)
            {
                PesquisaDeclaracao();
            }
        }


        private void PesquisaDeclaracao()
        {
            MySqlCommand cmd;
            string query;
            string queryUnionIDERIS;

            Double dblCusto;
            int intQuantPedido;
            string strPedido;

            DateTime resultado = DateTime.MinValue;

            int int0, int1, int2, int3, int4;

            int0 = 0;
            int1 = 0;
            int2 = 0;
            int3 = 0;
            int4 = 0;

            dblCusto = 0;

            DateTime dateInicial = DateTime.Today;
            DateTime dateFinal = DateTime.Today;

            lblValorTotal.Text = "VALOR TOTAL: 0";
            lblFreteTotal.Text = "FRETE TOTAL: 0";
            lblCusto.Text = "CUSTO TOTAL: 0";

            lblQuantidadeTotal.Text = "QUANTIDADE: 0";

            lblRegistros.Text = "TOTAL DE PEDIDOS: 0";

            if (DateTime.TryParse(mskDtFinal.Text, out resultado) & DateTime.TryParse(mskDtInicial.Text, out resultado))
            { }
            else
            {
                MessageBox.Show("Favor digitar uma data válida", "PontoZero Informa");
                return;
            }


            query = "SELECT DISTINCT UCASE(CONCAT(VENDA.customer_firstname,' ', VENDA.customer_lastname)) CLIENTE, ";
            query = query + "ENDERECO.STREET ENDERECO, ENDERECO.POSTCODE CEP, ENDERECO.city MUNICIPIO, ENDERECO.REGION UF, CCPF.value CPF, COALESCE(VENDA.weight,0) PESO, COALESCE(VENDA.SEPARADO, FALSE) SEPARADO, COALESCE(VENDA_ITEM.SEPARAR, FALSE) SEPARAR,";
            query = query + " VENDA_ITEM.item_id, VENDA.increment_id PEDIDO_FS, VENDA.canal_id PEDIDO, VENDA.updated_at DATA,  VENDA.created_at DATA_PEDIDO, VENDA_ITEM.NAME DESCRICAO, VENDA_ITEM.SKU CODIGO, VENDA_ITEM.qty_ordered QTDE,  VENDA_ITEM.qty_ordered * VENDA_ITEM.price VALOR_TOTAL,  ";
            query = query + " COALESCE(VENDA.base_shipping_amount,0) FRETE, ";
            query = query + " CASE WHEN  VENDA.customer_firstname like '%Ludostore%' THEN 'LUDOSTORE' ELSE CASE WHEN  VENDA.customer_firstname like '%Mercado Livre%' THEN 'MercadoLivre' ELSE CASE WHEN  VENDA.customer_firstname like '%B2W%' THEN 'B2W' ELSE CASE WHEN  VENDA.customer_firstname like '%Amazon%' THEN 'Amazon' ELSE VENDA.canal END END END  END LOJA, ";
            query = query + " VENDA.shipping_description ENTREGA, ";
            query = query + " CASE WHEN VENDA_PAGAMENTO.additional_data IS NULL THEN VENDA_PAGAMENTO.method ELSE VENDA_PAGAMENTO.additional_data END PAGAMENTO, ";
            query = query + " CASE WHEN VENDA.STATUS IS NULL THEN VENDA.STATE ELSE VENDA.STATUS END STATUS, ";

            query = query + " (SELECT value FROM catalog_category_entity_varchar ";
            query = query + " INNER JOIN catalog_category_product_index ";
            query = query + " ON catalog_category_product_index.category_id = catalog_category_entity_varchar.entity_id";
            query = query + " WHERE attribute_id = 41 AND entity_id > 2 AND catalog_category_product_index.product_id = VENDA_ITEM.product_id";
            query = query + " ORDER BY entity_id DESC LIMIT 0,1) CATEGORIA,";

            query = query + " coalesce(IC.data, 0) DATA_LIDO, coalesce(EC.data_impressao, 0) DATA_IMPRESSAO, ";

            query = query + " (SELECT value FROM catalog_product_entity_decimal WHERE catalog_product_entity_decimal.attribute_id = 173 AND catalog_product_entity_decimal.entity_id = VENDA_ITEM.product_id)  * VENDA_ITEM.qty_ordered CUSTO  ";

            query = query + " FROM sales_flat_order VENDA ";
            query = query + " INNER JOIN sales_flat_order_item VENDA_ITEM ";
            query = query + " ON VENDA_ITEM.order_id = VENDA.entity_id ";
            query = query + " LEFT JOIN sales_flat_order_payment VENDA_PAGAMENTO ";
            query = query + " ON VENDA_PAGAMENTO.entity_id = VENDA.entity_id ";

            query = query + " LEFT JOIN customer_address_entity CE ";
            query = query + " ON CE.parent_id = VENDA.customer_id ";

            query = query + " LEFT JOIN sales_flat_order_address ENDERECO ";
            query = query + " ON VENDA.entity_id = ENDERECO.parent_id ";
            query = query + " AND ENDERECO.address_type = 'shipping' ";

            query = query + " LEFT JOIN ITENS_ENVIO_CORREIOS IC ";
            query = query + " ON IC.increment_id = VENDA.increment_id ";

            query = query + " LEFT JOIN ENVIO_CORREIOS EC ";
            query = query + " ON EC.id = IC.id_ENVIO_CORREIOS ";
            query = query + " AND VENDA.increment_id = VENDA.increment_id ";

            query = query + " LEFT JOIN customer_entity_varchar CCPF ";
            query = query + " ON VENDA.customer_id = CCPF.entity_id ";
            query = query + " AND CCPF.attribute_id = 15 ";

            if (optStatus.Checked == true)
            {
                query = query + " WHERE VENDA_ITEM.price > 0 and (VENDA.updated_at between '";
            }
            else
            {
                query = query + " WHERE VENDA_ITEM.price > 0 and  (VENDA.created_at between '";
            }

            query = query + System.DateTime.Parse(mskDtInicial.Text).ToString("yyyy-MM-dd") + "' AND '" + System.DateTime.Parse(mskDtFinal.Text).ToString("yyyy-MM-dd") + " 23:59:59') ";

            if (chkCanal.Checked == true)
            {
                if (cbCanal.Text == "MercadoLivre")
                {
                    query = query + " AND  (VENDA.canal = '" + cbCanal.Text + "' OR VENDA.customer_firstname like '%Mercado Livre%')";
                }
                else if (cbCanal.Text == "B2W")
                {
                    query = query + " AND  (VENDA.canal IN ('SUBMARINO', 'SHOPTIME', 'LOJASAMERICANAS') OR VENDA.customer_firstname like '%B2W%')";
                }
                else if (cbCanal.Text == "Loja")
                {
                    query = query + " AND  (VENDA.canal = '" + cbCanal.Text + "' and VENDA.customer_firstname not like '%Ludostore%')";
                }
                else
                {
                    query = query + " AND  (VENDA.canal like '%" + cbCanal.Text + "%' OR VENDA.customer_firstname like '%" + cbCanal.Text + "%')";
                }
            }

            if (chkLoja.Checked == true)
            {
                query = query + " AND  (VENDA.canal  = '" + dcLoja.Text + "')";
            }


            if (txtProduto.Text.Length != 0)
            {
                if (optDeclaração.Checked == true)
                {
                    query = query + " AND ( VENDA_ITEM.SKU like '%" + txtProduto.Text + "%' OR VENDA.customer_lastname like '%" + txtProduto.Text + "%' or VENDA_ITEM.NAME like '%" + txtProduto.Text + "%' or VENDA.customer_firstname like '%" + txtProduto.Text + "%' or VENDA.increment_id like '%" + txtProduto.Text + "%' or VENDA.canal_id like '%" + txtProduto.Text + "%' ) ";
                }
                else
                {
                    query = query + " AND (VENDA_ITEM.SKU like '%" + txtProduto.Text + "%' OR VENDA_ITEM.NAME like '%" + txtProduto.Text + "%')";
                }

            }

            if (chkStatus.Checked == true)
            {
                query = query + "AND  (VENDA.status = '" + cbStatus.Text + "'  or (VENDA.STATUS IS NULL and VENDA.STATE  = '" + cbStatus.Text + "' )) ";
            }
            else
            {
                if (chkCancelados.Checked == false)
                {
                    query = query + "AND  (VENDA.STATE <> 'canceled') ";
                }
            }

            if (chkPesquisarPedido.Checked == false)
            {
                query = query + " AND VENDA_ITEM.NAME not like '%pluggto%'";
            }

            if (chkSKUPluggto.Checked == true)
            {
                query = query + " AND VENDA_ITEM.SKU like '%pluggto%'";
            }

            query = query + " AND (CE.entity_id = (SELECT CE2.entity_id FROM customer_address_entity CE2 WHERE CE2.parent_id = VENDA.customer_id  ORDER BY CE2.entity_id DESC LIMIT 1)";
            query = query + " OR (SELECT COUNT(*) FROM customer_address_entity CE2 WHERE CE2.parent_id = VENDA.customer_id  ORDER BY CE2.entity_id DESC LIMIT 1) = 0) ";


            //*********************************** UNION INTEGRADOS ****************************************************************

            queryUnionIDERIS = " UNION ";

            queryUnionIDERIS = queryUnionIDERIS + " SELECT distinct NOME, ENDERECO, CEP, CIDADE, UF, CPF, 0 PESO, COALESCE(PEDIDOS_INTEGRADOS.SEPARADO) SEPARADO, COALESCE(SEPARAR) SEPARAR, ITENS_PEDIDOS_INTEGRADOS.id ITEM_ID, ";
            queryUnionIDERIS = queryUnionIDERIS + " PEDIDOS_INTEGRADOS.id_pedido PEDIDO_FS, PEDIDOS_INTEGRADOS.cod_pedido PEDIDO, dt_integracao, PEDIDOS_INTEGRADOS.dt_pedido, ITENS_PEDIDOS_INTEGRADOS.nm_produto, ITENS_PEDIDOS_INTEGRADOS.sku, ";
            queryUnionIDERIS = queryUnionIDERIS + " ITENS_PEDIDOS_INTEGRADOS.qtde,  ITENS_PEDIDOS_INTEGRADOS.vl_total, 0 FRETE, PEDIDOS_INTEGRADOS.marketplace, 'INTEGRADO', PAGAMENTO, PEDIDOS_INTEGRADOS.status, ";

            queryUnionIDERIS = queryUnionIDERIS + " (SELECT value FROM catalog_category_entity_varchar ";
            queryUnionIDERIS = queryUnionIDERIS + " INNER JOIN catalog_category_product_index ";
            queryUnionIDERIS = queryUnionIDERIS + " ON catalog_category_product_index.category_id = catalog_category_entity_varchar.entity_id ";
            queryUnionIDERIS = queryUnionIDERIS + " WHERE attribute_id = 41 AND entity_id > 2 ";
            queryUnionIDERIS = queryUnionIDERIS + " AND catalog_category_product_index.product_id = (select entity_id from catalog_product_entity where catalog_product_entity.SKU = ITENS_PEDIDOS_INTEGRADOS.sku) ";
            queryUnionIDERIS = queryUnionIDERIS + " ORDER BY entity_id DESC LIMIT 0,1) CATEGORIA, ";

            queryUnionIDERIS = queryUnionIDERIS + " coalesce(IC.data, 0) DATA_LIDO, coalesce(EC.data_impressao, 0) DATA_IMPRESSAO, ";

            queryUnionIDERIS = queryUnionIDERIS + " COALESCE((SELECT catalog_product_entity_decimal.value ";

            queryUnionIDERIS = queryUnionIDERIS + " FROM catalog_product_entity_decimal ";
            queryUnionIDERIS = queryUnionIDERIS + " INNER JOIN catalog_product_entity ";
            queryUnionIDERIS = queryUnionIDERIS + " ON catalog_product_entity.entity_id = catalog_product_entity_decimal.entity_id ";
            queryUnionIDERIS = queryUnionIDERIS + " WHERE catalog_product_entity_decimal.attribute_id = 173 ";
            queryUnionIDERIS = queryUnionIDERIS + " AND catalog_product_entity.SKU = ITENS_PEDIDOS_INTEGRADOS.sku),0)  *ITENS_PEDIDOS_INTEGRADOS.qtde CUSTO ";

            queryUnionIDERIS = queryUnionIDERIS + " from PEDIDOS_INTEGRADOS ";

            queryUnionIDERIS = queryUnionIDERIS + " INNER join ITENS_PEDIDOS_INTEGRADOS ";
            queryUnionIDERIS = queryUnionIDERIS + " ON ITENS_PEDIDOS_INTEGRADOS.id_pedido = PEDIDOS_INTEGRADOS.id_pedido ";

            queryUnionIDERIS = queryUnionIDERIS + " and  ITENS_PEDIDOS_INTEGRADOS.marketplace = PEDIDOS_INTEGRADOS.marketplace ";

            queryUnionIDERIS = queryUnionIDERIS + " left JOIN ITENS_ENVIO_CORREIOS IC ";
            queryUnionIDERIS = queryUnionIDERIS + " ON IC.increment_id = ITENS_PEDIDOS_INTEGRADOS.id_pedido ";

            queryUnionIDERIS = queryUnionIDERIS + " left JOIN ENVIO_CORREIOS EC ";
            queryUnionIDERIS = queryUnionIDERIS + " ON EC.id = IC.id_ENVIO_CORREIOS ";


            queryUnionIDERIS = queryUnionIDERIS + " WHERE (PEDIDOS_INTEGRADOS.dt_PEDIDO between '";

            queryUnionIDERIS = queryUnionIDERIS + System.DateTime.Parse(mskDtInicial.Text).ToString("yyyy-MM-dd") + "' AND '" + System.DateTime.Parse(mskDtFinal.Text).ToString("yyyy-MM-dd") + " 23:59:59') ";

            if (txtProduto.Text.Length != 0)
            {
                if (chkPesquisarPedido.Checked == false)
                {
                    queryUnionIDERIS = queryUnionIDERIS + " AND(ITENS_PEDIDOS_INTEGRADOS.sku in (SELECT catalog_product_entity.sku SKU ";
                    queryUnionIDERIS = queryUnionIDERIS + " FROM catalog_product_entity_varchar ";
                    queryUnionIDERIS = queryUnionIDERIS + " INNER JOIN  catalog_product_entity  ON catalog_product_entity.entity_id = catalog_product_entity_varchar.entity_id ";
                    queryUnionIDERIS = queryUnionIDERIS + " where catalog_product_entity_varchar.attribute_id = 71 ";
                    queryUnionIDERIS = queryUnionIDERIS + " and(catalog_product_entity_varchar.value like '%" + txtProduto.Text + "%'OR catalog_product_entity.sku like '%" + txtProduto.Text + "%')))";
                }
                else
                {
                    queryUnionIDERIS = queryUnionIDERIS + " and (PEDIDOS_INTEGRADOS.COD_PEDIDO like '%" + txtProduto.Text + "%' OR PEDIDOS_INTEGRADOS.ID_PEDIDO like '%" + txtProduto.Text + "%')";
                }

                


            }


            if (chkCanal.Checked == true)
            {
                if (cbCanal.Text == "MercadoLivre")
                {
                    queryUnionIDERIS = queryUnionIDERIS + " AND  (PEDIDOS_INTEGRADOS.marketplace LIKE '%Mercado Livre%')";
                }
                else
                {
                    queryUnionIDERIS = queryUnionIDERIS + " AND  (PEDIDOS_INTEGRADOS.marketplace LIKE '%" + cbCanal.Text + "%')";
                }
            }

            if (chkLoja.Checked == true)
            {
                queryUnionIDERIS = queryUnionIDERIS + " AND  (PEDIDOS_INTEGRADOS.loja = '" + dcLoja.Text + "')";
            }

            if (chkStatus.Checked == true)
            {
                if (cbStatus.Text == "aprovado")
                {
                    queryUnionIDERIS = queryUnionIDERIS + "AND  (status = 'Aberto') ";
                }
                else
                {
                    queryUnionIDERIS = queryUnionIDERIS + "AND  (status = '" + cbStatus.Text + "') ";
                }
            }
            else
            {
                if (chkCancelados.Checked == false)
                {
                    queryUnionIDERIS = queryUnionIDERIS + "AND  (status <> 'Pagamento cancelado') ";
                }
            }

            query = query + queryUnionIDERIS;

            query = query + "  ORDER BY 14,12; ";


            if (global.myDB.connection.State == ConnectionState.Closed)
            {
                global.myDB.OpenConnection();
            }

            cmd = new MySqlCommand(query, global.myDB.connection);

            try
            {
                dataReader = cmd.ExecuteReader();
            }
            catch (Exception)
            {
                global.myDB.OpenConnection();
                cmd = new MySqlCommand(query, global.myDB.connection);
                dataReader = cmd.ExecuteReader();
            }

            lstEstoque.CheckBoxes = true;

            lstEstoque.Columns.Add("PEDIDO", 100);

            lstEstoque.Columns.Add("PEDIDO MKP", 100);
            lstEstoque.Columns.Add("STATUS", 70);
            lstEstoque.Columns.Add("CLIENTE", 100);
            lstEstoque.Columns.Add("DESCRICAO", 200);
            lstEstoque.Columns.Add("QTDE", 50);
            lstEstoque.Columns.Add("LOJA", 120);

            lstEstoque.Columns.Add("DATA PEDIDO", 90);
            lstEstoque.Columns.Add("DATA STATUS", 90);

            lstEstoque.Columns.Add("SKU", 100);

            lstEstoque.Columns.Add("VALOR", 60, HorizontalAlignment.Right);
            lstEstoque.Columns.Add("FRETE", 60, HorizontalAlignment.Right);

            lstEstoque.Columns.Add("ENTREGA", 70);
            lstEstoque.Columns.Add("PAGAMENTO", 100);

            lstEstoque.Columns.Add("ENDERECO", 100);
            lstEstoque.Columns.Add("CEP", 100);
            lstEstoque.Columns.Add("MUNICIPIO", 100);
            lstEstoque.Columns.Add("UF", 50);
            lstEstoque.Columns.Add("CPF", 100);
            lstEstoque.Columns.Add("PESO", 100);
            lstEstoque.Columns.Add("CATEGORIA", 100);
            lstEstoque.Columns.Add("PEDIDO DECLARACAO", 0);
            lstEstoque.Columns.Add("ITEMID", 0);

            int i = 0;
            intQuantPedido = 0;
            strPedido = "";

            while (dataReader.Read())
            {
                i = i + 1;

                ListViewItem item;



                if (strPedido != dataReader["PEDIDO_FS"].ToString())
                {
                    strPedido = dataReader["PEDIDO_FS"].ToString();

                    intQuantPedido = intQuantPedido + 1;

                    lblRegistros.Text = "TOTAL DE PEDIDOS: " + Convert.ToString(intQuantPedido);

                    item = new ListViewItem(new[] { dataReader["PEDIDO_FS"].ToString(), dataReader["PEDIDO"].ToString(), dataReader["STATUS"].ToString(), dataReader["CLIENTE"].ToString(), dataReader["DESCRICAO"].ToString(), String.Format("{0:N}", dataReader["QTDE"]).ToString(), dataReader["LOJA"].ToString(), System.DateTime.Parse(dataReader["DATA_PEDIDO"].ToString()).ToString("dd/MM/yyyy"), System.DateTime.Parse(dataReader["DATA"].ToString()).ToString("dd/MM/yyyy"), dataReader["CODIGO"].ToString(), String.Format("{0:N}", dataReader["VALOR_TOTAL"]).ToString(), "", dataReader["ENTREGA"].ToString(), dataReader["PAGAMENTO"].ToString(), dataReader["ENDERECO"].ToString(), dataReader["CEP"].ToString(), dataReader["MUNICIPIO"].ToString(), dataReader["UF"].ToString(), dataReader["CPF"].ToString(), dataReader["PESO"].ToString(), dataReader["CATEGORIA"].ToString(), dataReader["PEDIDO"].ToString(), dataReader["ITEM_ID"].ToString() });
                }
                else
                {
                    item = new ListViewItem(new[] { "", "", "", "", dataReader["DESCRICAO"].ToString(), String.Format("{0:N}", dataReader["QTDE"]).ToString(), dataReader["LOJA"].ToString(), System.DateTime.Parse(dataReader["DATA_PEDIDO"].ToString()).ToString("dd/MM/yyyy"), System.DateTime.Parse(dataReader["DATA"].ToString()).ToString("dd/MM/yyyy"), dataReader["CODIGO"].ToString(), String.Format("{0:N}", dataReader["VALOR_TOTAL"]).ToString(), "", dataReader["ENTREGA"].ToString(), dataReader["PAGAMENTO"].ToString(), dataReader["ENDERECO"].ToString(), dataReader["CEP"].ToString(), dataReader["MUNICIPIO"].ToString(), dataReader["UF"].ToString(), dataReader["CPF"].ToString(), dataReader["PESO"].ToString(), dataReader["CATEGORIA"].ToString(), dataReader["PEDIDO"].ToString(), dataReader["ITEM_ID"].ToString() });
                }

                if (item.SubItems[10].Text.Length > 0 && optVenda.Checked == true)
                {
                    dblCusto = dblCusto + (Double.Parse(item.SubItems[7].Text) * Double.Parse(item.SubItems[10].Text)); ;
                }

                if (dataReader["DATA_IMPRESSAO"].ToString() != "0")
                {
                    item.BackColor = Color.PaleGreen;
                    cmdEnviado.BackColor = Color.PaleGreen;
                    int3 = int3 + 1;
                }
                else if (dataReader["DATA_LIDO"].ToString() != "0")
                {
                    item.BackColor = Color.Khaki;
                    cmdLidos.BackColor = Color.Khaki;
                    int2 = int2 + 1;
                }
                else if (dataReader["SEPARADO"].ToString() == "1")
                {
                    item.BackColor = Color.CadetBlue;
                    cmdSeparado.BackColor = Color.CadetBlue;
                    int1 = int1 + 1;
                }
                else if (dataReader["SEPARAR"].ToString() == "1")
                {
                    item.BackColor = Color.SandyBrown;
                    cmdSeparar.BackColor = Color.SandyBrown;
                    int0 = int0 + 1;
                }
                else
                {
                    item.BackColor = Color.White;
                    cmdParado.BackColor = Color.White;
                    int4 = int4 + 1;
                    break;
                }

                cmdEnviado.Visible = true;
                cmdParado.Visible = true;
                cmdLidos.Visible = true;
                cmdSeparado.Visible = true;
                cmdSeparar.Visible = true;

                cmdSeparar.Text = "SEPARAR:" + int0;
                cmdSeparado.Text = "SEPARADO:" + int1;
                cmdLidos.Text = "LIDO:" + int2;
                cmdEnviado.Text = "ENVIADO:" + int3;

                cmdParado.Text = "PARADO:" + int4;

                lstEstoque.Items.Add(item);

            }

            dataReader.Close();
        }


        private void PesquisaPedido()
        {
            MySqlCommand cmd;
            string query;
            string queryUnionIDERIS;
            Double dblValor;
            Double dblValorFrete;
            Double dblCusto;
            Double dblQuant;
            int intQuantPedido;
            string strPedido;
            string strDataInicial;
            string strDataFinal;
            int intDias;

            DateTime resultado = DateTime.MinValue;

            dblValor = 0;
            dblValorFrete = 0;
            dblQuant = 0;
            dblCusto = 0;

            DateTime dateInicial;
            DateTime dateFinal;

            lblValorTotal.Text = "VALOR TOTAL: 0";
            lblFreteTotal.Text = "FRETE TOTAL: 0";
            lblCusto.Text = "CUSTO TOTAL: 0";

            lblQuantidadeTotal.Text = "QUANTIDADE: 0";

            lblRegistros.Text = "TOTAL DE PEDIDOS: 0";

            if (DateTime.TryParse(mskDtFinal.Text, out resultado) & DateTime.TryParse(mskDtInicial.Text, out resultado))
            { }
            else
            {
                MessageBox.Show("Favor digitar uma data válida", "PontoZero Informa");
                return;
            }


            dateFinal = DateTime.Parse(mskDtFinal.Text);
            dateInicial = DateTime.Parse(mskDtInicial.Text);


            intDias = (int)dateFinal.Subtract(dateInicial).TotalDays;


            int i = 0;
            intQuantPedido = 0;
            strPedido = "";

            strDataInicial = "";
            strDataFinal = "";

            lstEstoque.CheckBoxes = true;

            lstEstoque.Columns.Add("PEDIDO", 70);
            lstEstoque.Columns.Add("PEDIDO MKP", 100);
            lstEstoque.Columns.Add("DATA PEDIDO", 90);
            lstEstoque.Columns.Add("DATA STATUS", 90);
            lstEstoque.Columns.Add("DESCRICAO", 200);
            lstEstoque.Columns.Add("SKU", 100);
            lstEstoque.Columns.Add("QTDE", 50);
            lstEstoque.Columns.Add("VALOR", 60, HorizontalAlignment.Right);
            lstEstoque.Columns.Add("FRETE", 60, HorizontalAlignment.Right);
            lstEstoque.Columns.Add("CUSTO", 60, HorizontalAlignment.Right);
            lstEstoque.Columns.Add("LOJA", 120);



            for (i = 0; i <= intDias; i++)
            {

                if (intDias > 5)
                {
                    if (strDataInicial == dateInicial.ToString())
                    {


                        dateFinal = dateFinal.AddDays(-6);

                        if (dateFinal <= DateTime.Parse(mskDtInicial.Text))
                        {
                            dateFinal = DateTime.Parse(mskDtInicial.Text);
                            i = intDias + 1;
                        }

                        dateInicial = dateFinal.AddDays(-5);

                        if (dateInicial <= DateTime.Parse(mskDtInicial.Text))
                        {
                            dateInicial = DateTime.Parse(mskDtInicial.Text);
                            i = intDias + 1;
                        }
                    }
                    else
                    {
                        dateInicial = dateFinal.AddDays(-5);
                    }

                }
                else
                {
                    i = intDias + 1;
                }

                strDataFinal = dateFinal.ToString();
                strDataInicial = dateInicial.ToString();

                query = "SELECT DISTINCT VENDA_ITEM.item_id, VENDA.increment_id PEDIDO_FS, VENDA.canal_id PEDIDO, VENDA.updated_at DATA,  VENDA.created_at DATA_PEDIDO, VENDA_ITEM.NAME DESCRICAO, VENDA_ITEM.SKU CODIGO, VENDA_ITEM.qty_ordered QTDE,  VENDA_ITEM.qty_ordered * VENDA_ITEM.price VALOR_TOTAL,  ";
                query = query + " COALESCE(VENDA.base_shipping_amount,0) FRETE, ";
                query = query + " CASE WHEN  VENDA.customer_firstname like '%Ludostore%' THEN 'LUDOSTORE' ELSE CASE WHEN  VENDA.customer_firstname like '%Mercado Livre%' THEN 'MercadoLivre' ELSE CASE WHEN  VENDA.customer_firstname like '%B2W%' THEN 'B2W' ELSE CASE WHEN  VENDA.customer_firstname like '%Amazon%' THEN 'Amazon' ELSE VENDA.canal END END END  END LOJA, ";

                query = query + " (SELECT value FROM catalog_product_entity_decimal WHERE catalog_product_entity_decimal.attribute_id = 173 AND catalog_product_entity_decimal.entity_id = VENDA_ITEM.product_id)  * VENDA_ITEM.qty_ordered CUSTO  ";

                query = query + " FROM sales_flat_order VENDA ";
                query = query + " INNER JOIN sales_flat_order_item VENDA_ITEM ";
                query = query + " ON VENDA_ITEM.order_id = VENDA.entity_id ";
                query = query + " LEFT JOIN sales_flat_order_payment VENDA_PAGAMENTO ";
                query = query + " ON VENDA_PAGAMENTO.entity_id = VENDA.entity_id ";

                query = query + " LEFT JOIN sales_flat_order_address ENDERECO ";
                query = query + " ON VENDA.entity_id = ENDERECO.parent_id ";
                query = query + " AND ENDERECO.address_type = 'shipping' ";

                query = query + " LEFT JOIN customer_entity_varchar CCPF ";
                query = query + " ON VENDA.customer_id = CCPF.entity_id ";
                query = query + " AND CCPF.attribute_id = 15 ";

                if (chkCategoriaVenda.Checked == true)
                {
                    query = query + " INNER JOIN  catalog_category_product_index" +
                                    " ON catalog_category_product_index.product_id = VENDA_ITEM.product_id " +
                                    " AND catalog_category_product_index.category_id = " + cbCategoriasVenda.SelectedValue;
                }

                if (chkFabricante.Checked == true)
                {
                    query = query + " INNER JOIN  `catalog_product_entity_text`   ";
                    query = query + " ON `catalog_product_entity_text`.entity_id = VENDA_ITEM.product_id ";
                    query = query + " AND catalog_product_entity_text.attribute_id = 83 ";
                    query = query + " AND catalog_product_entity_text.value like '%" + cbFabricante.Text.Trim() + "%'";
                }

                if (optStatus.Checked == true)
                {
                    query = query + " WHERE VENDA_ITEM.price > 0 and (VENDA.updated_at between '";
                }
                else
                {
                    query = query + " WHERE VENDA_ITEM.price > 0 and  (VENDA.created_at between '";
                }

                query = query + System.DateTime.Parse(strDataInicial).ToString("yyyy-MM-dd") + "' AND '" + System.DateTime.Parse(strDataFinal).ToString("yyyy-MM-dd") + " 23:59:59') ";

                if (chkCanal.Checked == true)
                {
                    if (cbCanal.Text == "MercadoLivre")
                    {
                        query = query + " AND  (VENDA.canal = '" + cbCanal.Text + "' OR VENDA.customer_firstname like '%Mercado Livre%')";
                    }
                    else if (cbCanal.Text == "B2W")
                    {
                        query = query + " AND  (VENDA.canal IN ('SUBMARINO', 'SHOPTIME', 'LOJASAMERICANAS') OR VENDA.customer_firstname like '%B2W%')";
                    }
                    else if (cbCanal.Text == "Loja")
                    {
                        query = query + " AND  (VENDA.canal = '" + cbCanal.Text + "' and VENDA.customer_firstname not like '%Ludostore%')";
                    }
                    else
                    {
                        query = query + " AND  (VENDA.canal like '%" + cbCanal.Text + "%' OR VENDA.customer_firstname like '%" + cbCanal.Text + "%')";
                    }
                }

                if (txtProduto.Text.Length != 0)
                {
                    query = query + " AND (VENDA_ITEM.SKU like '%" + txtProduto.Text + "%' OR VENDA_ITEM.NAME like '%" + txtProduto.Text + "%')";
                }

                if (chkStatus.Checked == true)
                {
                    query = query + "AND  (VENDA.status = '" + cbStatus.Text + "'  or (VENDA.STATUS IS NULL and VENDA.STATE  = '" + cbStatus.Text + "' )) ";
                }
                else
                {
                    if (chkCancelados.Checked == false)
                    {
                        query = query + "AND  (VENDA.STATE <> 'canceled') ";
                    }
                }

                if (chkPesquisarPedido.Checked == false)
                {
                    query = query + " AND VENDA_ITEM.NAME not like '%pluggto%'";
                }

                if (chkSKUPluggto.Checked == true)
                {
                    query = query + " AND VENDA_ITEM.SKU like '%pluggto%'";
                }

                //query = query + "  ORDER BY 3 DESC ";

                //*********************************** UNION INTEGRADOS ****************************************************************

                queryUnionIDERIS = " UNION ";

                queryUnionIDERIS = queryUnionIDERIS + " SELECT distinct ITENS_PEDIDOS_INTEGRADOS.id ITEM_ID, ";
                queryUnionIDERIS = queryUnionIDERIS + " PEDIDOS_INTEGRADOS.id_pedido PEDIDO_FS, PEDIDOS_INTEGRADOS.cod_pedido PEDIDO, dt_integracao, PEDIDOS_INTEGRADOS.dt_pedido, ITENS_PEDIDOS_INTEGRADOS.nm_produto, ITENS_PEDIDOS_INTEGRADOS.sku, ";
                queryUnionIDERIS = queryUnionIDERIS + " ITENS_PEDIDOS_INTEGRADOS.qtde,  ITENS_PEDIDOS_INTEGRADOS.vl_total, 0 FRETE, PEDIDOS_INTEGRADOS.marketplace, ";

                queryUnionIDERIS = queryUnionIDERIS + " COALESCE((SELECT catalog_product_entity_decimal.value ";

                queryUnionIDERIS = queryUnionIDERIS + " FROM catalog_product_entity_decimal ";
                queryUnionIDERIS = queryUnionIDERIS + " INNER JOIN catalog_product_entity ";
                queryUnionIDERIS = queryUnionIDERIS + " ON catalog_product_entity.entity_id = catalog_product_entity_decimal.entity_id ";
                queryUnionIDERIS = queryUnionIDERIS + " WHERE catalog_product_entity_decimal.attribute_id = 173 ";
                queryUnionIDERIS = queryUnionIDERIS + " AND catalog_product_entity.SKU = ITENS_PEDIDOS_INTEGRADOS.sku),0)  *ITENS_PEDIDOS_INTEGRADOS.qtde CUSTO ";

                queryUnionIDERIS = queryUnionIDERIS + " from PEDIDOS_INTEGRADOS ";

                queryUnionIDERIS = queryUnionIDERIS + " INNER join ITENS_PEDIDOS_INTEGRADOS ";
                queryUnionIDERIS = queryUnionIDERIS + " ON ITENS_PEDIDOS_INTEGRADOS.id_pedido = PEDIDOS_INTEGRADOS.id_pedido ";

                if (chkCategoriaVenda.Checked == true)
                {
                    queryUnionIDERIS = queryUnionIDERIS + " INNER JOIN catalog_product_entity ";
                    queryUnionIDERIS = queryUnionIDERIS + " ON catalog_product_entity.SKU = ITENS_PEDIDOS_INTEGRADOS.sku  ";


                    queryUnionIDERIS = queryUnionIDERIS + " INNER JOIN  catalog_category_product_index" +
                                    " ON catalog_category_product_index.product_id = catalog_product_entity.entity_id " +
                                    " AND catalog_category_product_index.category_id = " + cbCategoriasVenda.SelectedValue;
                }

                if (chkFabricante.Checked == true)
                {
                    if (chkCategoriaVenda.Checked != true)
                    {
                        queryUnionIDERIS = queryUnionIDERIS + " INNER JOIN catalog_product_entity ";
                        queryUnionIDERIS = queryUnionIDERIS + " ON catalog_product_entity.SKU = ITENS_PEDIDOS_INTEGRADOS.sku  ";
                    }

                    queryUnionIDERIS = queryUnionIDERIS + " INNER JOIN  `catalog_product_entity_text`   ";
                    queryUnionIDERIS = queryUnionIDERIS + " ON `catalog_product_entity_text`.entity_id = catalog_product_entity.entity_id ";
                    queryUnionIDERIS = queryUnionIDERIS + " AND catalog_product_entity_text.attribute_id = 83 ";
                    queryUnionIDERIS = queryUnionIDERIS + " AND catalog_product_entity_text.value like '%" + cbFabricante.Text.Trim() + "%' ";
                }

                queryUnionIDERIS = queryUnionIDERIS + " WHERE (PEDIDOS_INTEGRADOS.dt_PEDIDO between '";

                queryUnionIDERIS = queryUnionIDERIS + System.DateTime.Parse(strDataInicial).ToString("yyyy-MM-dd") + "' AND '" + System.DateTime.Parse(strDataFinal).ToString("yyyy-MM-dd") + " 23:59:59') ";

                queryUnionIDERIS = queryUnionIDERIS + " AND(PEDIDOS_INTEGRADOS.marketplace = ITENS_PEDIDOS_INTEGRADOS.marketplace) ";

                if (txtProduto.Text.Length != 0)
                {
                    queryUnionIDERIS = queryUnionIDERIS + " AND(ITENS_PEDIDOS_INTEGRADOS.sku in (SELECT catalog_product_entity.sku SKU ";
                    queryUnionIDERIS = queryUnionIDERIS + " FROM catalog_product_entity_varchar ";
                    queryUnionIDERIS = queryUnionIDERIS + " INNER JOIN  catalog_product_entity  ON catalog_product_entity.entity_id = catalog_product_entity_varchar.entity_id ";
                    queryUnionIDERIS = queryUnionIDERIS + " where catalog_product_entity_varchar.attribute_id = 71 ";
                    queryUnionIDERIS = queryUnionIDERIS + " and(catalog_product_entity_varchar.value like '%" + txtProduto.Text + "%'OR catalog_product_entity.sku like '%" + txtProduto.Text + "%')))";
                }

                if (chkCanal.Checked == true)
                {
                    if (cbCanal.Text == "MercadoLivre")
                    {
                        queryUnionIDERIS = queryUnionIDERIS + " AND  (PEDIDOS_INTEGRADOS.marketplace LIKE '%Mercado Livre%')";
                    }
                    else
                    {
                        queryUnionIDERIS = queryUnionIDERIS + " AND  (PEDIDOS_INTEGRADOS.marketplace LIKE '%" + cbCanal.Text + "%')";
                    }
                }

                if (chkStatus.Checked == true)
                {
                    if (cbStatus.Text == "aprovado")
                    {
                        queryUnionIDERIS = queryUnionIDERIS + "AND  (status = 'Aberto') ";
                    }
                    else
                    {
                        queryUnionIDERIS = queryUnionIDERIS + "AND  (status = '" + cbStatus.Text + "') ";
                    }
                }
                else
                {
                    if (chkCancelados.Checked == false)
                    {

                        queryUnionIDERIS = queryUnionIDERIS + "AND  (status <> 'Pagamento cancelado') ";
                    }
                }

                query = query + queryUnionIDERIS;

                query = query + "  ORDER BY 1; ";

                if (global.myDB.connection.State == ConnectionState.Closed)
                {
                    global.myDB.OpenConnection();
                }

                cmd = new MySqlCommand(query, global.myDB.connection);

                try
                {
                    dataReader = cmd.ExecuteReader();
                }
                catch (Exception)
                {
                    global.myDB.OpenConnection();
                    cmd = new MySqlCommand(query, global.myDB.connection);
                    dataReader = cmd.ExecuteReader();
                }


                while (dataReader.Read())
                {

                    ListViewItem item;

                    if (strPedido != dataReader["PEDIDO_FS"].ToString())
                    {
                        strPedido = dataReader["PEDIDO_FS"].ToString();

                        intQuantPedido = intQuantPedido + 1;

                        lblRegistros.Text = "TOTAL DE PEDIDOS: " + Convert.ToString(intQuantPedido);

                        item = new ListViewItem(new[] { dataReader["PEDIDO_FS"].ToString(), dataReader["PEDIDO"].ToString(), System.DateTime.Parse(dataReader["DATA_PEDIDO"].ToString()).ToString("dd/MM/yyyy"), System.DateTime.Parse(dataReader["DATA"].ToString()).ToString("dd/MM/yyyy"), dataReader["DESCRICAO"].ToString(), dataReader["CODIGO"].ToString(), String.Format("{0:N}", dataReader["QTDE"]).ToString(), String.Format("{0:N}", dataReader["VALOR_TOTAL"]).ToString(), String.Format("{0:N}", dataReader["FRETE"]).ToString(), String.Format("{0:N}", dataReader["CUSTO"]).ToString(), dataReader["LOJA"].ToString() });
                        dblValorFrete = dblValorFrete + Double.Parse(item.SubItems[8].Text);
                    }
                    else
                    {
                        item = new ListViewItem(new[] { dataReader["PEDIDO_FS"].ToString(), dataReader["PEDIDO"].ToString(), System.DateTime.Parse(dataReader["DATA_PEDIDO"].ToString()).ToString("dd/MM/yyyy"), System.DateTime.Parse(dataReader["DATA"].ToString()).ToString("dd/MM/yyyy"), dataReader["DESCRICAO"].ToString(), dataReader["CODIGO"].ToString(), String.Format("{0:N}", dataReader["QTDE"]).ToString(), String.Format("{0:N}", dataReader["VALOR_TOTAL"]).ToString(), dataReader["FRETE"].ToString(), String.Format("{0:N}", dataReader["CUSTO"]).ToString(), dataReader["LOJA"].ToString() });
                    }

                    if (item.SubItems[9].Text.Length > 0)
                    {
                        dblCusto = dblCusto +  Double.Parse(item.SubItems[9].Text); ;
                        //dblCusto = dblCusto + (Double.Parse(item.SubItems[6].Text) * Double.Parse(item.SubItems[9].Text)); ;
                    }

                    lstEstoque.Items.Add(item);

                    //dblValor = dblValor + (Double.Parse(item.SubItems[6].Text) * Double.Parse(item.SubItems[7].Text)); ;
                    dblValor = dblValor + (Double.Parse(item.SubItems[7].Text)); ;

                    dblQuant = dblQuant + Double.Parse(item.SubItems[6].Text);

                    lblFreteTotal.Text = "FRETE TOTAL: " + Math.Round(dblValorFrete, 2);
                    lblValorTotal.Text = "VALOR TOTAL: " + Math.Round(dblValor, 2);
                    lblQuantidadeTotal.Text = "QUANTIDADE TOTAL: " + Math.Round(dblQuant, 2);
                    lblCusto.Text = "CUSTO TOTAL: " + Math.Round(dblCusto, 2);
                }

                dataReader.Close();

            }

        }

        private void ExibeLinha(string strCor)
        {

            if (lstEstoque.Items.Count > 0)
            {
                try
                {
                    foreach (ListViewItem item in lstEstoque.Items)
                    {
                        if (item.BackColor.ToString() != strCor)
                        {
                            item.Remove();
                        }
                    }

                }
                catch (Exception ex)
                {
                    MessageBox.Show("Erro : " + ex.Message, "PontoZero Informa");
                }
            }
        }


        private void TesteExcel()
        {

        }

        private void PesquisaEstoqueADM()
        {
            MySqlCommand cmd;
            string query;
            string queryWhere;


            DateTime resultado = DateTime.MinValue;

            DateTime dateInicial = DateTime.Today;
            DateTime dateFinal = DateTime.Today;

            lblValorTotal.Text = "VALOR TOTAL: 0";
            lblFreteTotal.Text = "FRETE TOTAL: 0";
            lblCusto.Text = "CUSTO TOTAL: 0";

            lblQuantidadeTotal.Text = "QUANTIDADE: 0";

            lblRegistros.Text = "TOTAL DE PEDIDOS: 0";

            if (optEstoque.Checked == false)
            {
                if (DateTime.TryParse(mskDtFinal.Text, out resultado) & DateTime.TryParse(mskDtInicial.Text, out resultado))
                { }
                else
                {
                    MessageBox.Show("Favor digitar uma data válida", "PontoZero Informa");
                    return;
                }
            }

            query = " SELECT * ";

            query = query + " FROM ESTOQUE_FECHAMENTO ";

            queryWhere = "";

            if (txtFornecedor.Text.Length != 0)
            {
                queryWhere = " WHERE FORNECEDOR LIKE '%" + txtFornecedor.Text + "%'";
            }

            if (txtDescricao.Text.Length != 0)
            {
                if (queryWhere == "")
                {
                    queryWhere = " WHERE ";
                }
                else
                {
                    queryWhere = queryWhere + " AND ";
                }

                queryWhere = queryWhere + " DESCRICAO like '%" + txtDescricao.Text + "%'";
            }


            if (txtFabricante.Text.Length != 0)
            {
                if (queryWhere == "")
                {
                    queryWhere = " WHERE ";
                }
                else
                {
                    queryWhere = queryWhere + " AND ";
                }

                queryWhere = queryWhere + " FABRICANTE like '%" + txtFabricante.Text + "%'";
            }

            if (txtEAN.Text.Length != 0)
            {
                if (queryWhere == "")
                {
                    queryWhere = " WHERE ";
                }
                else
                {
                    queryWhere = queryWhere + " AND ";
                }

                queryWhere = queryWhere + " EAN like '%" + txtEAN.Text + "%' ";
            }

            query = query + queryWhere + " ORDER BY sku;";

            if (global.myDB.connection.State == ConnectionState.Closed)
            {
                global.myDB.OpenConnection();
            }

            cmd = new MySqlCommand(query, global.myDB.connection);


            try
            {
                dataReader = cmd.ExecuteReader();
            }
            catch (Exception)
            {
                global.myDB.OpenConnection();
                cmd = new MySqlCommand(query, global.myDB.connection);
                dataReader = cmd.ExecuteReader();
            }

            //CABECALHO ESTOQUE
            lstEstoque.CheckBoxes = false;

            lstEstoque.Columns.Add("SKU", 100);
            lstEstoque.Columns.Add("DESCRICAO", 350);
            lstEstoque.Columns.Add("QUANTIDADE", 90);
            lstEstoque.Columns.Add("VENDIDO", 90);
            lstEstoque.Columns.Add("ULT 30 DIAS", 90);
            lstEstoque.Columns.Add("CUSTO", 90, HorizontalAlignment.Right);
            lstEstoque.Columns.Add("VALOR", 90, HorizontalAlignment.Right);
            lstEstoque.Columns.Add("VALOR PROMO", 100, HorizontalAlignment.Right);

            int i = 0;

            try
            {
                while (dataReader.Read())
                {
                    i = i + 1;


                    ListViewItem item;

                    lblRegistros.Text = "TOTAL DE REGISTROS: " + Convert.ToString(i);
                    item = new ListViewItem(new[] { dataReader["SKU"].ToString(), dataReader["DESCRICAO"].ToString(), String.Format("{0:N}", dataReader["QUANTIDADE"]).ToString(), String.Format("{0:N}", dataReader["VENDIDO"]).ToString(), String.Format("{0:N}", dataReader["30_DIAS"]).ToString(), String.Format("{0:N}", dataReader["CUSTO"]).ToString(), String.Format("{0:N}", dataReader["VALOR"]).ToString(), String.Format("{0:N}", dataReader["VALORPROMO"]).ToString() });

                    lstEstoque.Items.Add(item);

                }
            }
            catch (Exception)
            {
            }
             

            dataReader.Close();
        }


        private void Pesquisa_Continental()
        {
            MySqlCommand cmd;
            string query;
            string strImagem1;
            string strImagem2;
            string strDescricao;
            bool blAchou;

            lblValorTotal.Text = "VALOR TOTAL: 0";
            lblFreteTotal.Text = "FRETE TOTAL: 0";
            lblRegistros.Text = "TOTAL DE REGISTROS: 0";
            lblCusto.Text = "CUSTO TOTAL = 0";

            //PESQUISA ESTOQUE CONTINENTAL
            query = " SELECT CURDATE() DATE, catalog_product_entity_varchar.value NOME_DO_PRODUTO," +
                    " (SELECT value FROM catalog_product_entity_decimal WHERE catalog_product_entity_decimal.attribute_id = 169 AND catalog_product_entity_decimal.entity_id = catalog_product_entity_varchar.entity_id) EAN," +
                    " '' NCM, 1 STOCKCONTROL, 'true' ENABLEFOREMPTYPRICELIST, catalog_product_entity_varchar.value SLUG, catalog_product_entity.sku COD_DO_PRODUTO," +
                    " 'product.type.simple' PRODUCTTYPE, '' DELETEDAT, '' CATEGORY, 1 MASTER, '' PRESENTATION, catalog_product_entity.sku COD_DO_PRODUTO, catalog_product_entity_decimal.value PRECO," +
                    " cataloginventory_stock_item.qty ESTOQUE, 1 AVISO_QUANDO_O_ESTOQUE_ESTIVER_BAIXO," +

                    " (SELECT value from catalog_product_entity_int where catalog_product_entity_int.attribute_id = 158 and catalog_product_entity_int.entity_id = catalog_product_entity_varchar.entity_id) ALTURA," +
                    " (SELECT value from catalog_product_entity_decimal where catalog_product_entity_decimal.attribute_id = 80 and catalog_product_entity_decimal.entity_id = catalog_product_entity_varchar.entity_id) PESO," +
                    " (SELECT value from catalog_product_entity_int where catalog_product_entity_int.attribute_id = 159 and catalog_product_entity_int.entity_id = catalog_product_entity_varchar.entity_id) LARGURA," +
                    " (SELECT value from catalog_product_entity_int where catalog_product_entity_int.attribute_id = 157 and catalog_product_entity_int.entity_id = catalog_product_entity_varchar.entity_id) COMPRIMENTO," +

                    " (SELECT catalog_product_entity_media_gallery.value FROM catalog_product_entity_media_gallery" +
                    " INNER JOIN catalog_product_entity_media_gallery_value ON catalog_product_entity_media_gallery_value.value_id = catalog_product_entity_media_gallery.value_id" +
                    " WHERE catalog_product_entity_media_gallery.entity_id = catalog_product_entity_varchar.entity_id ORDER BY catalog_product_entity_media_gallery_value.position LIMIT 0,1) IMAGEM1 ," +

                    " (SELECT catalog_product_entity_media_gallery.value FROM catalog_product_entity_media_gallery" +
                    " INNER JOIN catalog_product_entity_media_gallery_value ON catalog_product_entity_media_gallery_value.value_id = catalog_product_entity_media_gallery.value_id" +
                    " WHERE catalog_product_entity_media_gallery.entity_id = catalog_product_entity_varchar.entity_id ORDER BY catalog_product_entity_media_gallery_value.position DESC LIMIT 0,1) IMAGEM2," +
                    " '' IMAGEM3, '' KEYWORD, '' DESCRIPTION, '' TRACKINLD, catalog_product_entity_varchar.value DESCRICAO_RESUMIDA," +
                    " (select value from catalog_product_entity_text where attribute_id = 72 and entity_id = catalog_product_entity_varchar.entity_id) DESCRICAO_COMPLETA," +

                    " CONCAT('DIMENSOES: ', " +
                    " coalesce((SELECT value  from catalog_product_entity_int where catalog_product_entity_int.attribute_id = 158 and catalog_product_entity_int.entity_id = catalog_product_entity_varchar.entity_id), 0)" +
                    " ,'X'," +
                    " coalesce((SELECT value  from catalog_product_entity_int where catalog_product_entity_int.attribute_id = 159 and catalog_product_entity_int.entity_id = catalog_product_entity_varchar.entity_id),0)" +
                    " ,'X'," +
                    " coalesce((SELECT value  from catalog_product_entity_int where catalog_product_entity_int.attribute_id = 157 and catalog_product_entity_int.entity_id = catalog_product_entity_varchar.entity_id),0) " +
                    " , ' PESO: '," +
                    " coalesce((SELECT value  from catalog_product_entity_decimal where catalog_product_entity_decimal.attribute_id = 80 and catalog_product_entity_decimal.entity_id = catalog_product_entity_varchar.entity_id),0)" +
                    " , ' '," +
                    " coalesce((SELECT value FROM catalog_product_entity_varchar WHERE catalog_product_entity_varchar.attribute_id = 84 AND cataloginventory_stock_item.product_id = catalog_product_entity_varchar.entity_id),0)" +
                    " , ' '," +
                    " catalog_product_entity_text.value" +
                    " ) INFORMACOES_TECNICAS," +
                    " '' INFORMACOES_COMPLEMENTARES" +

                    " FROM `catalog_product_entity_varchar` " +

                    " INNER JOIN `cataloginventory_stock_item` " +
                    " ON `cataloginventory_stock_item`.product_id = `catalog_product_entity_varchar`.entity_id " +

                    " INNER JOIN  `cataloginventory_stock_status` " +
                    " ON `cataloginventory_stock_status`.product_id = `catalog_product_entity_varchar`.entity_id " +

                    " LEFT JOIN catalog_product_entity_decimal " +
                    " ON catalog_product_entity_decimal.attribute_id = 75 " +
                    " AND catalog_product_entity_decimal.entity_id = catalog_product_entity_varchar.entity_id" +

                    " INNER JOIN  `catalog_product_entity_text` " +
                    " ON `catalog_product_entity_text`.entity_id = `catalog_product_entity_varchar`.entity_id " +
                    " AND catalog_product_entity_text.attribute_id = 83 " +

                    " INNER JOIN  `catalog_product_entity`  " +
                    " ON `catalog_product_entity`.entity_id = `catalog_product_entity_varchar`.entity_id ";

            if (chkCategoria.Checked == true)
            {
                query = query + " INNER JOIN  catalog_category_product_index" +
                                " ON catalog_category_product_index.product_id = catalog_product_entity_varchar.entity_id " +
                                " AND catalog_category_product_index.category_id = " + cbCategorias.SelectedValue;
            }

            query = query + " where catalog_product_entity_varchar.attribute_id = '71'";

            if (txtFornecedor.Text.Length != 0)
            {
                query = query + " AND (SELECT count(catalog_product_entity_varchar.entity_id) FROM catalog_product_entity_varchar " +
                                " WHERE catalog_product_entity_varchar.attribute_id = 84 " +
                                " AND cataloginventory_stock_item.product_id = catalog_product_entity_varchar.entity_id " +
                                " AND catalog_product_entity_varchar.value LIKE '%" + txtFornecedor.Text + "%') > 0";
            }

            if (txtDescricao.Text.Length != 0)
            {
                query = query + " AND catalog_product_entity_varchar.value like '%" + txtDescricao.Text + "%'";
            }

            if (chkQtdeMinima.Checked == true)
            {
                query = query + " AND cataloginventory_stock_item.qty <= cataloginventory_stock_item.min_qty";
            }

            if (txtFabricante.Text.Length != 0)
            {
                query = query + " AND catalog_product_entity_text.value like '%" + txtFabricante.Text + "%'";
            }

            if (chkAtivos.Checked == true)
            {
                query = query + " AND cataloginventory_stock_status.stock_status = 1";
            }

            if (global.myDB.connection.State == ConnectionState.Closed)
            {
                global.myDB.OpenConnection();
            }

            cmd = new MySqlCommand(query, global.myDB.connection);

            try
            {
                dataReader = cmd.ExecuteReader();
            }
            catch (Exception)
            {
                global.myDB.OpenConnection();
                cmd = new MySqlCommand(query, global.myDB.connection);
                dataReader = cmd.ExecuteReader();
            }

            //CABECALHO ESTOQUE

            lstEstoque.Columns.Add("DATE", 150);
            lstEstoque.Columns.Add("NOME DO PRODUTO", 150);
            lstEstoque.Columns.Add("EAN", 150);
            lstEstoque.Columns.Add("NCM", 150);
            lstEstoque.Columns.Add("STOCKCONTROL", 150);
            lstEstoque.Columns.Add("ENABLEFOREMPTYPRICELIST", 150);
            lstEstoque.Columns.Add("SLUG", 150);
            lstEstoque.Columns.Add("COD DO PRODUTO", 150);
            lstEstoque.Columns.Add("PRODUCTTYPE", 150);
            lstEstoque.Columns.Add("DELETEDAT", 150);
            lstEstoque.Columns.Add("CATEGORY", 150);
            lstEstoque.Columns.Add("MASTER", 150);
            lstEstoque.Columns.Add("PRESENTATION", 150);
            lstEstoque.Columns.Add("COD DO PRODUTO", 150);
            lstEstoque.Columns.Add("PRECO", 150);
            lstEstoque.Columns.Add("ESTOQUE", 150);
            lstEstoque.Columns.Add("AVISO QUANDO O ESTOQUE ESTIVER BAIXO", 150);
            lstEstoque.Columns.Add("ALTURA", 150);
            lstEstoque.Columns.Add("PESO", 150);
            lstEstoque.Columns.Add("LARGURA", 150);
            lstEstoque.Columns.Add("COMPRIMENTO", 150);
            lstEstoque.Columns.Add("IMAGEM1", 150);
            lstEstoque.Columns.Add("IMAGEM2", 150);
            lstEstoque.Columns.Add("IMAGEM3", 150);
            lstEstoque.Columns.Add("KEYWORD", 150);
            lstEstoque.Columns.Add("DESCRIPTION", 150);
            lstEstoque.Columns.Add("TRACKINLD", 150);
            lstEstoque.Columns.Add("DESCRICAO RESUMIDA", 150);
            lstEstoque.Columns.Add("DESCRICAO COMPLETA", 150);
            lstEstoque.Columns.Add("INFORMACOES TECNICAS", 150);
            lstEstoque.Columns.Add("INFORMACOESCOMPLEMENTARES", 150);

            while (dataReader.Read())
            {
                ListViewItem item;

                strImagem1 = " ";
                strImagem2 = " ";

                //IMAGENS *************************************************************************************************

                blAchou = false;

                strImagem1 = "https://facilshopping.com.br/media/catalog/product" + dataReader["IMAGEM1"].ToString();

                blAchou = Checa_URL(strImagem1);

                if (blAchou == false)
                {
                    strImagem1 = "https://facilshopping.com.br/media/catalog/product/cache/1/image/9df78eab33525d08d6e5fb8d27136e95" + dataReader["IMAGEM1"].ToString();
                    blAchou = Checa_URL(strImagem1);
                }

                if (blAchou == false)
                {
                    strImagem1 = "https://facilshopping.com.br/media/catalog/product/cache/1/image/308x472/9df78eab33525d08d6e5fb8d27136e95" + dataReader["IMAGEM1"].ToString();
                }

                blAchou = false;

                strImagem2 = "https://facilshopping.com.br/media/catalog/product" + dataReader["IMAGEM2"].ToString();

                blAchou = Checa_URL(strImagem2);

                if (blAchou == false)
                {
                    strImagem2 = "https://facilshopping.com.br/media/catalog/product/cache/1/image/9df78eab33525d08d6e5fb8d27136e95" + dataReader["IMAGEM2"].ToString();

                    blAchou = Checa_URL(strImagem2);

                }

                if (blAchou == false)
                {
                    strImagem2 = "https://facilshopping.com.br/media/catalog/product/cache/1/image/308x472/9df78eab33525d08d6e5fb8d27136e95" + dataReader["IMAGEM2"].ToString();
                }

                //*********************************************************************************************************

                // global.StripTagsRegex(global.StripTagsRegexCompiled(global.StripTagsCharArray(dataReader["DESCRICAO_COMPLETA"].ToString())));

                strDescricao = global.StripTagsRegex(global.StripTagsRegexCompiled(global.StripTagsCharArray(dataReader["DESCRICAO_COMPLETA"].ToString()))); ;

                //item = new ListViewItem(new[] { dataReader["PEDIDO_FS"].ToString(), dataReader["PEDIDO"].ToString(), dataReader["STATUS"].ToString(), System.DateTime.Parse(dataReader["DATA_PEDIDO"].ToString()).ToString("dd/MM/yyyy"), System.DateTime.Parse(dataReader["DATA"].ToString()).ToString("dd/MM/yyyy"), dataReader["DESCRICAO"].ToString(), dataReader["CODIGO"].ToString(), String.Format("{0:N}", dataReader["QTDE"]).ToString(), String.Format("{0:N}", dataReader["VALOR_TOTAL"]).ToString(), dataReader["FRETE"].ToString(), dataReader["LOJA"].ToString(), dataReader["ENTREGA"].ToString(), dataReader["PAGAMENTO"].ToString() });
                item = new ListViewItem(new[] { System.DateTime.Parse(dataReader["DATE"].ToString()).ToString("yyyy-MM-ddThh:mm:ss"), dataReader["NOME_DO_PRODUTO"].ToString(), dataReader["EAN"].ToString(), dataReader["NCM"].ToString(), dataReader["STOCKCONTROL"].ToString(), dataReader["ENABLEFOREMPTYPRICELIST"].ToString(), dataReader["SLUG"].ToString(), dataReader["COD_DO_PRODUTO"].ToString(), dataReader["PRODUCTTYPE"].ToString(), dataReader["DELETEDAT"].ToString(), dataReader["CATEGORY"].ToString(), dataReader["MASTER"].ToString(), dataReader["PRESENTATION"].ToString(), dataReader["COD_DO_PRODUTO"].ToString(), dataReader["PRECO"].ToString(), dataReader["ESTOQUE"].ToString(), dataReader["AVISO_QUANDO_O_ESTOQUE_ESTIVER_BAIXO"].ToString(), dataReader["ALTURA"].ToString(), dataReader["PESO"].ToString(), dataReader["LARGURA"].ToString(), dataReader["COMPRIMENTO"].ToString(), strImagem1, strImagem2, dataReader["IMAGEM3"].ToString(), dataReader["KEYWORD"].ToString(), dataReader["DESCRIPTION"].ToString(), dataReader["TRACKINLD"].ToString(), dataReader["DESCRICAO_RESUMIDA"].ToString(), strDescricao, dataReader["INFORMACOES_TECNICAS"].ToString(), dataReader["INFORMACOES_COMPLEMENTARES"].ToString() });

                lstEstoque.Items.Add(item);
            }

            dataReader.Close();
        }

        private void XML_PriceQuest()
        {
            MySqlCommand cmd;
            string query;
            string strImagem;

            lblRegistros.Text = "TOTAL DE REGISTROS: 0";

            //PESQUISA ESTOQUE
            query = " SELECT CUSTO.value CUSTO, catalog_category_product_index.category_id, catalog_product_entity.sku SKU, catalog_product_entity_varchar.value_id, catalog_product_entity_varchar.entity_id, replace( catalog_product_entity_varchar.value,\"'\",\"\") value, ";

            query = query + " CASE WHEN FIMDAPROMOCAO.value > now() THEN PROMOCAO.VALUE ELSE 0 END PROMOCAO,";

            query = query + " (PROMOCAO.value - (PROMOCAO.value / 100 * 5)) PROMOCAO_DEPOSITO,";

            query = query + " (catalog_product_entity_decimal.value - (catalog_product_entity_decimal.value / 100 * 5)) VALOR_DEPOSITO, `cataloginventory_stock_item`.qty, `cataloginventory_stock_item`.min_qty,";
            query = query + " catalog_product_entity_text.value fabricante, catalog_product_entity_decimal.value VALOR, ";
            query = query + " CASE WHEN cataloginventory_stock_status.stock_status = 1 THEN 'ATIVO' ELSE 'DESATIVADO' END STATUS, ";
            query = query + " (SELECT value FROM catalog_product_entity_varchar WHERE catalog_product_entity_varchar.attribute_id = 84 AND cataloginventory_stock_item.product_id = catalog_product_entity_varchar.entity_id) FORNECEDOR,  ";
            query = query + " (SELECT value FROM catalog_product_entity_varchar WHERE catalog_product_entity_varchar.attribute_id = 98 AND catalog_product_entity_varchar.store_id = 0 AND cataloginventory_stock_item.product_id = catalog_product_entity_varchar.entity_id) LINK  ,  ";
            query = query + " (SELECT value FROM catalog_category_entity_varchar INNER JOIN catalog_category_product_index ON catalog_category_product_index.category_id = catalog_category_entity_varchar.entity_id WHERE attribute_id = 41 AND entity_id > 2 AND catalog_category_product_index.product_id = catalog_product_entity_varchar.entity_id  ORDER BY entity_id DESC LIMIT 0,1) CATEGORIA,   ";
            //query = query + "(SELECT value FROM catalog_product_entity_media_gallery WHERE entity_id = catalog_product_entity_varchar.entity_id limit 0,1) IMAGEM ";

            query = query + " (SELECT catalog_product_entity_media_gallery.value FROM catalog_product_entity_media_gallery ";
            query = query + " INNER JOIN catalog_product_entity_media_gallery_value ON catalog_product_entity_media_gallery_value.value_id = catalog_product_entity_media_gallery.value_id ";
            query = query + " WHERE catalog_product_entity_media_gallery.entity_id = catalog_product_entity_varchar.entity_id ORDER BY catalog_product_entity_media_gallery_value.position LIMIT 0,1) IMAGEM ";

            query = query + " FROM `catalog_product_entity_varchar`   ";
            query = query + " INNER JOIN `cataloginventory_stock_item`   ";
            query = query + " ON `cataloginventory_stock_item`.product_id = `catalog_product_entity_varchar`.entity_id ";
            query = query + " INNER JOIN  `cataloginventory_stock_status` ";
            query = query + " ON `cataloginventory_stock_status`.product_id = `catalog_product_entity_varchar`.entity_id ";
            query = query + " LEFT JOIN catalog_product_entity_decimal ";
            query = query + " ON catalog_product_entity_decimal.attribute_id = 75 ";
            query = query + " AND catalog_product_entity_decimal.entity_id = catalog_product_entity_varchar.entity_id ";

            query = query + " LEFT JOIN catalog_product_entity_decimal CUSTO ";
            query = query + " ON catalog_product_entity_decimal.attribute_id = 173 ";
            query = query + " AND catalog_product_entity_decimal.entity_id = catalog_product_entity_varchar.entity_id ";

            query = query + " INNER JOIN  `catalog_product_entity_text`   ";
            query = query + " ON `catalog_product_entity_text`.entity_id = `catalog_product_entity_varchar`.entity_id ";
            query = query + " AND catalog_product_entity_text.attribute_id = 83 ";
            query = query + " INNER JOIN  `catalog_product_entity`    ";
            query = query + " ON `catalog_product_entity`.entity_id = `catalog_product_entity_varchar`.entity_id ";
            query = query + " INNER JOIN catalog_category_product_index ";
            query = query + " ON catalog_category_product_index.product_id = catalog_product_entity_varchar.entity_id ";
            query = query + " AND catalog_category_product_index.category_id = 32 ";

            query = query + " LEFT JOIN catalog_product_entity_decimal PROMOCAO";
            query = query + " ON PROMOCAO.attribute_id = 76";
            query = query + " AND PROMOCAO.entity_id = catalog_product_entity_varchar.entity_id";

            query = query + " LEFT JOIN catalog_product_entity_datetime FIMDAPROMOCAO";
            query = query + " ON FIMDAPROMOCAO.entity_id = PROMOCAO.entity_id";
            query = query + " AND FIMDAPROMOCAO.attribute_id = 78";

            query = query + " where catalog_product_entity_varchar.attribute_id = '71' limit 0,5000; ";

            if (global.myDB.connection.State == ConnectionState.Closed)
            {
                global.myDB.OpenConnection();
            }

            cmd = new MySqlCommand(query, global.myDB.connection);

            try
            {
                dataReader = cmd.ExecuteReader();
            }
            catch (Exception)
            {
                global.myDB.OpenConnection();
                cmd = new MySqlCommand(query, global.myDB.connection);
                dataReader = cmd.ExecuteReader();
            }

            lstEstoque.CheckBoxes = false;
            lstEstoque.Columns.Add("CODIGO", 60);
            lstEstoque.Columns.Add("CATEGORIA", 160);
            lstEstoque.Columns.Add("DESCRICAO", 350);
            lstEstoque.Columns.Add("QUANTIDADE", 90);
            lstEstoque.Columns.Add("QUANTIDADE MINIMA", 150);
            lstEstoque.Columns.Add("FABRICANTE", 150);
            lstEstoque.Columns.Add("FORNECEDOR", 150);
            lstEstoque.Columns.Add("VALOR", 90, HorizontalAlignment.Right);
            lstEstoque.Columns.Add("STATUS", 60);

            int i = 0;
            XmlTextWriter writer = new XmlTextWriter(@"c:\Temp\facilshopping.xml", null);

            try
            {
                //inicia o documento xml
                writer.WriteStartDocument();
                //escreve o elmento raiz
                writer.WriteStartElement("feed");
                //escrever o atributo do feed
                writer.WriteAttributeString("xmlns", "http://www.w3.org/2005/Atom");
                writer.WriteAttributeString("xmlns:g", "http://base.google.com/ns/1.0");
                writer.WriteAttributeString("xmlns:pq", "https://pricequest.com.br/ns/1.0");
                writer.WriteElementString("title", "FacilShopping");
                writer.WriteElementString("updated", DateTime.Now.ToString("yyyy-MM-ddThh:mm:ssZ"));
                writer.WriteElementString("title", "FacilShopping");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "PontoZero Informa");
            }

            while (dataReader.Read())
            {
                i = i + 1;
                lblRegistros.Text = "TOTAL DE REGISTROS: " + Convert.ToString(i);

                ListViewItem item;
                bool blAchou;

                item = new ListViewItem(new[] { dataReader["entity_id"].ToString(), dataReader["CATEGORIA"].ToString(), dataReader["value"].ToString(), dataReader["qty"].ToString(), dataReader["min_qty"].ToString(), dataReader["FABRICANTE"].ToString(), dataReader["FORNECEDOR"].ToString(), String.Format("{0:N}", dataReader["VALOR"]).ToString(), dataReader["STATUS"].ToString() });

                lstEstoque.Items.Add(item);

                blAchou = false;

                strImagem = "https://facilshopping.com.br/media/catalog/product" + dataReader["IMAGEM"].ToString();

                blAchou = Checa_URL(strImagem);

                if (blAchou == false)
                {
                    strImagem = "https://facilshopping.com.br/media/catalog/product/cache/1/image/9df78eab33525d08d6e5fb8d27136e95" + dataReader["IMAGEM"].ToString();
                    blAchou = Checa_URL(strImagem);
                }

                if (blAchou == false)
                {
                    strImagem = "https://facilshopping.com.br/media/catalog/product/cache/1/image/308x472/9df78eab33525d08d6e5fb8d27136e95" + dataReader["IMAGEM"].ToString();
                    blAchou = Checa_URL(strImagem);
                }

                if (blAchou == true)
                {
                    try
                    {
                        //escreve o elmento entry
                        writer.WriteStartElement("entry");

                        /* if (dataReader["SKU"].ToString() == "CIV001")
                         {
                             blAchou = blAchou;
                         }*/

                        //Escreve os sub-elementos
                        writer.WriteElementString("id", dataReader["SKU"].ToString());
                        writer.WriteElementString("title", dataReader["value"].ToString());
                        writer.WriteElementString("link", "https://facilshopping.com.br/" + dataReader["LINK"].ToString());
                        writer.WriteElementString("updated", DateTime.Now.ToString("yyyy-MM-ddThh:mm:ssZ"));
                        writer.WriteElementString("g:image_link", strImagem);

                        if (dataReader["PROMOCAO"].ToString() == "0")
                        {
                            writer.WriteElementString("g:price", String.Format("{0:N}", dataReader["VALOR"]).ToString() + " BRL");
                            writer.WriteElementString("g:sale_price", String.Format("{0:N}", dataReader["VALOR"]).ToString());
                        }
                        else
                        {
                            writer.WriteElementString("g:price", String.Format("{0:N}", dataReader["PROMOCAO"]).ToString() + " BRL");
                            writer.WriteElementString("g:sale_price", String.Format("{0:N}", dataReader["PROMOCAO"]).ToString());
                        }

                        if (dataReader["qty"].ToString() == "0,0000")
                        {
                            writer.WriteElementString("g:availability", "out of stock");
                        }
                        else
                        {
                            writer.WriteElementString("g:availability", "in stock");
                        }

                        //escreve o elmento payment
                        writer.WriteStartElement("pq:payment");


                        if (dataReader["SKU"].ToString() == "OSDA001")
                        {
                            writer.WriteElementString("pq:price", String.Format("{0:N}", Convert.ToDouble(dataReader["VALOR"]) * 0.89) + " BRL");
                        }
                        else
                        {
                            if (dataReader["PROMOCAO"].ToString() == "0")
                            {
                                writer.WriteElementString("pq:price", String.Format("{0:N}", dataReader["VALOR_DEPOSITO"]).ToString() + " BRL");
                            }
                            else
                            {
                                writer.WriteElementString("pq:price", String.Format("{0:N}", dataReader["PROMOCAO_DEPOSITO"]).ToString() + " BRL");
                            }
                        }
                        writer.WriteElementString("pq:method", "deposito");

                        // encerra o elemento payment
                        writer.WriteEndElement();

                        // encerra o elemento entry
                        writer.WriteEndElement();

                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message, "PontoZero Informa");
                    }
                }
                else
                {
                    // MessageBox.Show("não achou " + strImagem);
                }
            }

            try
            {
                // encerra o elemento raiz
                writer.WriteEndElement();
                //Escreve o XML para o arquivo e fecha o objeto escritor
                writer.Close();
                MessageBox.Show("Arquivo XML gerado com sucesso.", "PontoZero Informa");

                // Envia_FTP();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "PontoZero Informa");
            }

            dataReader.Close();
        }

        /* private void DBConecta()
         {
             server = "magento_fs.mysql.dbaas.com.br";
             database = "magento_fs";
             uid = "magento_fs";
             password = "Facil32245";
             string connectionString;
             connectionString = "SERVER=" + server + ";" + "DATABASE=" +
             database + ";" + "UID=" + uid + ";" + "PASSWORD=" + password + ";";

             connection = new MySqlConnection(connectionString);

             connection.Open();
         }*/


        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }

        private void groupBox2_Enter_1(object sender, EventArgs e)
        {

        }

        private void btnLimpar_Click(object sender, EventArgs e)
        {
            Limpar();
            // ConectaWEBService();
            //GetRastreioWEBService();
        }

        private void Limpar()
        {
            lstEstoque.Columns.Clear();
            lstEstoque.Items.Clear();
            txtDescricao.Text = "";
            chkQtdeMinima.Checked = false;
            txtFabricante.Text = "";
            chkAtivos.Checked = true;
            lblRegistros.Text = "TOTAL DE REGISTROS: 0";
            chkXML.Checked = false;

            mskDtFinal.Text = DateTime.Now.ToString("dd-MM-yyyy");
            mskDtInicial.Text = DateTime.Now.ToString("dd-MM-yyyy");
        }

        private void btnSair_Click(object sender, EventArgs e)
        {
            dataReader.Close();
            //connection.Close();
            this.Close();
        }

        private void btPesquisar_Click(object sender, EventArgs e)
        {
            lstEstoque.Columns.Clear();
            lstEstoque.Items.Clear();
            lstEstoque.Visible = false;
            lblAviso.Visible = true;
            lblAviso.Refresh();

            if (chkXML.Checked == true)
            {
                XML_PriceQuest();
            }
            else
            {
                if (optContinental.Checked == true) //SEPARAÇAÕ
                {
                    //Pesquisa_Continental(); 
                    Pesquisa_Separacao();
                }
                else if (optEstoque.Checked == true) //SEPARAÇAÕ
                {
                    PesquisaEstoqueADM();
                }
                else
                {

                    Pesquisa();
                }
            }

            lblAviso.Visible = false;
            lstEstoque.Visible = true;
        }

        private void Pesquisa_Separacao()
        {
            MySqlCommand cmd;
            string query;
            Double dblQuant;


            DateTime resultado = DateTime.MinValue;

            dblQuant = 0;

            lblQuantidadeTotal.Text = "QUANTIDADE: 0";

            /*query = " select name, SUM(COALESCE(qty_ordered, 0) )  QUANT, SKU, catalog_product_entity_text.value FABRICANTE ";
            query = query + " from sales_flat_order_item ";
            query = query + " LEFT join catalog_product_entity_text ";
            query = query + " on catalog_product_entity_text.attribute_id = 83 ";
            query = query + " and catalog_product_entity_text.entity_id = sales_flat_order_item.product_id ";
            query = query + " where separar = true and separado = false ";
            query = query + " group by sku ";
            query = query + " order by catalog_product_entity_text.value, name ";
            query = query + " LIMIT 1,5000; "; */

            query = " select name, SUM(COALESCE(qty_ordered, 0))  QUANT, SKU ";
            query = query + " from sales_flat_order_item ";

            query = query + " where separar = true ";

            query = query + " and separado = false ";
            query = query + " union ";

            query = query + " select nm_produto, SUM(COALESCE(qtde, 0))  QUANT, sku from ITENS_PEDIDOS_INTEGRADOS ";

            query = query + " inner join PEDIDOS_INTEGRADOS ";
            query = query + " on PEDIDOS_INTEGRADOS.id_pedido = ITENS_PEDIDOS_INTEGRADOS.id_pedido ";

            query = query + " where separar = true and separado = false ";

            query = query + " group by sku order by 1 LIMIT 0, 5000; ";


            if (global.myDB.connection.State == ConnectionState.Closed)
            {
                global.myDB.OpenConnection();
            }

            cmd = new MySqlCommand(query, global.myDB.connection);



            try
            {
                dataReader = cmd.ExecuteReader();
            }
            catch (Exception)
            {
                global.myDB.OpenConnection();
                cmd = new MySqlCommand(query, global.myDB.connection);
                dataReader = cmd.ExecuteReader();
            }

            lstEstoque.CheckBoxes = true;
            lstEstoque.Columns.Add("NOME", 500);
            lstEstoque.Columns.Add("QUANTIDADE", 100);
            lstEstoque.Columns.Add("SKU", 150);
            // lstEstoque.Columns.Add("FABRICANTE", 200);


            int i = 0;


            while (dataReader.Read())
            {
                i = i + 1;

                ListViewItem item;

                item = new ListViewItem(new[] { dataReader["name"].ToString(), dataReader["QUANT"].ToString(), dataReader["SKU"].ToString() });

                lstEstoque.Items.Add(item);

                dblQuant = dblQuant + Double.Parse(item.SubItems[1].Text);

                lblQuantidadeTotal.Text = "QUANTIDADE TOTAL: " + Math.Round(dblQuant, 2);

            }

            dataReader.Close();
        }

        private void lblRegistros_Click(object sender, EventArgs e)
        {

        }

        private void cbCategorias_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (optDeclaração.Checked == true)
            {
                if (ApagarImagens() == true)
                {

                    //if (MessageBox.Show("Deseja imprimir as etiquetas desses Pedidos?", "PontoZero Informa", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1) == DialogResult.Yes)
                    //{
                    //    if (global.strIdServicoPAC == "")
                    //    {
                    //        global.BuscaServicoWEBService();
                    //    }
                    //    Gera_Rastreio();
                    //}

                    if (MessageBox.Show("Deseja gerar as declarações desses Pedidos?", "PontoZero Informa", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1) == DialogResult.Yes)
                    {
                        Gera_Declaracao();
                    }
                }
            }
        }

        private bool ApagarImagens()
        {
            string[] imgList = Directory.GetFiles("C:\\temp\\", "*.bmp");

            try
            {
                foreach (string f in imgList)
                {
                    File.Delete(f);
                }
            }
            catch (System.IO.IOException e)
            {
                Console.WriteLine(e.Message);
                // return false;
            }

            return true;
        }
        private void Gera_Declaracao()
        {
            int l;

            l = 0;

            foreach (ListViewItem item in lstEstoque.Items)
            {
                l = l + 1;

                if (item.Checked == true)
                {
                    if (ValidaCadastroProduto(item.SubItems[9].Text) == true)
                    {
                        global.PreencherPorReplace(Application.StartupPath + "\\" + "Formulario Declaracao de Conteudo - A4.docx", item.SubItems[1].Text, item.SubItems[3].Text, item.SubItems[14].Text.Replace("\n", ""), item.SubItems[15].Text, item.SubItems[16].Text, item.SubItems[17].Text, item.SubItems[18].Text, item.SubItems[19].Text, lstEstoque);
                    }
                    else
                    {
                        MessageBox.Show("O SKU " + item.SubItems[9].Text + " NÃO ESTÁ CADASTRADO OU SEUS DADOS ESTÃO INCOMPLETOS!!", "PontoZero Informa");
                    }
                }
            }
        }

        private bool Consulta_Sedex(string strCEP)
        {
            MySqlCommand cmd1;
            MySqlDataReader dataReaderRastreio;
            bool blSedex;

            string query = "SELECT * FROM SEDEX_CIDADES WHERE '" + strCEP.Replace("-", "") + "' between CEPINICIAL AND CEPFINAL;";

            global.myDB.CloseConnection();
            if (global.myDB.connection.State == ConnectionState.Closed)
            {
                global.myDB.OpenConnection();
            }

            cmd1 = new MySqlCommand(query, global.myDB.connection);

            dataReaderRastreio = cmd1.ExecuteReader();

            if (dataReaderRastreio.Read())
            {
                blSedex = true;
            }
            else
            {
                blSedex = false;
            }

            dataReaderRastreio.Close();

            return blSedex;
        }

        private void Gera_Rastreio()
        {
            int j;
            bool blSedex;
            string strRastreio;

            j = 0;

            foreach (ListViewItem item in lstEstoque.Items)
            {
                j = j + 1;

                if (item.Checked == true)
                {
                    if (ValidaCadastroProduto(item.SubItems[9].Text) == true)
                    {

                        if (Consulta_Sedex(item.SubItems[15].Text) == true)//colocar regra de sedex x pac
                        {
                            blSedex = true;
                        }
                        else
                        {
                            blSedex = false;
                        }

                        strRastreio = Imprime(System.DateTime.Parse(DateTime.Now.ToString()).ToString("yyyymmddsss"), blSedex, item.SubItems[0].Text);

                        if (strRastreio != "")
                        {
                            ImprimeCEP("CEP" + System.DateTime.Parse(DateTime.Now.ToString()).ToString("yyyymmddsss"), item.SubItems[15].Text);

                            global.PreencherRastreio(Application.StartupPath + "\\Rastreio.docx", "", item.SubItems[0].Text, strRastreio, item.SubItems[3].Text, item.SubItems[14].Text.Replace("\n51", "").Replace("\n", " "), item.SubItems[16].Text + " - " + item.SubItems[17].Text, item.SubItems[15].Text, "", imgCodigoBarra.Image, imgCodigoBarraCEP.Image);

                            if (blReimpressãoRastreio == false)
                            {
                                AtualizaRastreio(strRastreio, item.SubItems[0].Text, blSedex);
                            }
                        }
                        blReimpressãoRastreio = false;
                    }
                    else
                    {
                        MessageBox.Show("O SKU " + item.SubItems[9].Text + " NÃO ESTÁ CADASTRADO OU SEUS DADOS ESTÃO INCOMPLETOS!!", "PontoZero Informa");
                    }

                }
            }
        }

        private bool ValidaCadastroProduto(string strSKU)
        {
            bool blValido = false;
            MySqlCommand cmd1;
            MySqlDataReader dataPedido;

            string query = "select COMPRIMENTO.value COMPRIMENTO, ALTURA.value ALTURA, LARGURA.value LARGURA ";

            query = query + " from catalog_product_entity CP ";

            query = query + " INNER JOIN catalog_product_entity_int COMPRIMENTO ";
            query = query + " ON COMPRIMENTO.entity_id = CP.entity_id ";
            query = query + " AND COMPRIMENTO.attribute_id = 157 ";

            query = query + " INNER JOIN catalog_product_entity_int ALTURA ";
            query = query + " ON ALTURA.entity_id = CP.entity_id ";
            query = query + " AND ALTURA.attribute_id = 158 ";

            query = query + " INNER JOIN catalog_product_entity_int LARGURA ";
            query = query + " ON LARGURA.entity_id = CP.entity_id ";
            query = query + " AND LARGURA.attribute_id = 159 ";

            query = query + " where CP.sku = '" + strSKU + "' or CP.sku = '0" + strSKU + "'; ";

            global.myDB.CloseConnection();

            if (global.myDB.connection.State == ConnectionState.Closed)
            {
                global.myDB.OpenConnection();
            }

            cmd1 = new MySqlCommand(query, global.myDB.connection);

            dataPedido = cmd1.ExecuteReader();



            try
            {
                if (dataPedido.Read())
                {
                    blValido = true;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "PontoZero Informa");
                blValido = false;
            }


            dataPedido.Close();

            return blValido;
        }


        private void AtualizaRastreio(string strRastreio, string strPedido, bool blSedex)
        {

            String query;

            query = "UPDATE RASTREIO_CORREIOS SET PEDIDO = '" + strPedido + "'";
            query = query + " WHERE NUMERO =  " + strRastreio.Substring(2, 8) + " AND PREFIXO =  '" + strRastreio.Substring(0, 2) + "'";

            global.myDB.CloseConnection();

            if (global.myDB.connection.State == ConnectionState.Closed)
            {
                global.myDB.OpenConnection();
            }

            MySqlCommand cmd1 = new MySqlCommand(query, global.myDB.connection);

            try
            {
                cmd1.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "PontoZero Informa");
            }
        }



        public static String geraEtiquetaComDigitoVerificador(String numeroEtiqueta)
        {
            String prefixo = numeroEtiqueta.Substring(0, 2);
            String numero = numeroEtiqueta.Substring(2, 8);
            String sufixo = numeroEtiqueta.Substring(10);
            String retorno = numero;
            String dv;
            int[] multiplicadores = { 8, 6, 4, 2, 3, 5, 9, 7 };
            int soma = 0;
            // Preenche número com 0 à esquerda  
            if (numeroEtiqueta.Length < 12)
            {
                retorno = "Error…";
            }
            else if (numero.Length < 8 && numeroEtiqueta.Length == 12)
            {
                String zeros = "";
                int diferenca = 8 - numero.Length;
                for (int i = 0; i < diferenca; i++)
                {
                    zeros += "0";
                }
                retorno = zeros + numero;
            }
            else
            {
                retorno = numero.Substring(0, 8);
            }

            for (int i = 0; i < 8; i++)
            {
                soma += Convert.ToInt32(retorno.Substring(i, 1)) * multiplicadores[i];
            }

            int resto = soma % 11;

            if (resto == 0)
            {
                dv = "5";
            }
            else if (resto == 1)
            {
                dv = "0";
            }
            else
            {
                dv = Convert.ToString(11 - resto);
            }

            retorno += dv;
            retorno = prefixo + retorno + sufixo;
            return retorno;
        }

        private void chkAtivos_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void btnExportar_Click(object sender, EventArgs e)
        {
            if (optDeclaração.Checked == true)
            {
                if (MessageBox.Show("Deseja exportar itens marcados para Separação?", "PontoZero Informa", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1) == DialogResult.Yes)
                {
                    GerarXLS();
                }

                Marcar_Separacao();

            }
            else
            {
                GerarXLS();
            }
        }

        private void Marcar_Separacao()
        {
            int l = 2;

            if (lstEstoque.Items.Count > 0)
            {
                try
                {
                    foreach (ListViewItem item in lstEstoque.Items)
                    {
                        if (item.Checked == true)
                        {
                            Grava_Separacao(item.SubItems[22].Text, item.SubItems[12].Text);

                            l = l + 1;
                        }
                    }

                }
                catch (Exception ex)
                {
                    MessageBox.Show("Erro : " + ex.Message, "PontoZero Informa");
                }
            }
        }

        private void Grava_Separacao(string strItemID, string strPlataforma)
        {

            String query;

            if (strPlataforma == "INTEGRADO" || strPlataforma == "LUDOSTORE")
            {
                query = "UPDATE ITENS_PEDIDOS_INTEGRADOS set separar = true  ";
                query = query + " WHERE ID  =  " + strItemID;
            }
            //else if(strPlataforma == "LUDOSTORE")
            //{
            //    query = "UPDATE ITENS_PEDIDO_LUDOSTORE set separar = true ";
            //    query = query + " WHERE ID  =  " + strItemID;
            //}
            else
            {
                query = "UPDATE sales_flat_order_item set separar = true ";
                query = query + " WHERE ITEM_ID  =  " + strItemID;
            }
            global.myDB.CloseConnection();

            if (global.myDB.connection.State == ConnectionState.Closed)
            {
                global.myDB.OpenConnection();
            }

            MySqlCommand cmd1 = new MySqlCommand(query, global.myDB.connection);

            try
            {
                cmd1.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "PontoZero Informa");
            }
        }

        private void AtualisaEstoqueLudostore(string strOperador)
        {
            int l;
            String query;
            l = 0;

            foreach (ListViewItem item in lstEstoque.Items)
            {
                l = l + 1;

                if (item.Checked == true)
                {
                    if (item.SubItems[6].Text == "LUDOSTORE")
                    {
                        query = "update cataloginventory_stock_item set qty = qty - " + item.SubItems[5].Text.Replace(",", ".") + " where product_id = (select entity_id from catalog_product_entity where sku = '" + item.SubItems[9].Text + "')";

                        global.myDB.CloseConnection();

                        if (global.myDB.connection.State == ConnectionState.Closed)
                        {
                            global.myDB.OpenConnection();
                        }

                        MySqlCommand cmd1 = new MySqlCommand(query, global.myDB.connection);

                        try
                        {
                            cmd1.ExecuteNonQuery();
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show("Erro : " + ex.Message, "PontoZero Informa");
                            return;
                        }
                        //**************************************************************************
                        query = "update cataloginventory_stock_status set qty = qty - " + item.SubItems[5].Text.Replace(",", ".") + " where product_id = (select entity_id from catalog_product_entity where sku = '" + item.SubItems[9].Text + "')";

                        global.myDB.CloseConnection();

                        if (global.myDB.connection.State == ConnectionState.Closed)
                        {
                            global.myDB.OpenConnection();
                        }

                        cmd1 = new MySqlCommand(query, global.myDB.connection);

                        try
                        {
                            cmd1.ExecuteNonQuery();
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show("Erro : " + ex.Message, "PontoZero Informa");
                            return;
                        }

                        //**************************************************************************

                        query = "update PEDIDO_LUDOSTORE set status = 'LIBERADO', operador = '" + strOperador + "' where id_pedido = '" + item.SubItems[1].Text + "'";

                        global.myDB.CloseConnection();

                        if (global.myDB.connection.State == ConnectionState.Closed)
                        {
                            global.myDB.OpenConnection();
                        }

                        cmd1 = new MySqlCommand(query, global.myDB.connection);

                        try
                        {
                            cmd1.ExecuteNonQuery();
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show("Erro : " + ex.Message, "PontoZero Informa");
                            return;
                        }
                    }
                }
            }

            MessageBox.Show("Registros gravados com sucesso!", "PontoZero Informa");
        }

        private void GerarXLS()
        {
            string strLinha;

            try
            {
                File.Delete("C:\\temp\\teste.csv");
            }
            catch (Exception ex)
            {

            }

            int l = 2;

            if (lstEstoque.Items.Count > 0)
            {
                try
                {
                    strLinha = "";

                    for (int i = 1; i < lstEstoque.Columns.Count + 1; i++)
                    {
                        if (strLinha != "")
                        {
                            strLinha = strLinha + ";";
                        }

                        strLinha = strLinha + lstEstoque.Columns[i - 1].Text;
                    }

                    try
                    {
                        using (StreamWriter writer = new StreamWriter("C:\\temp\\teste.csv", true))
                        {
                            writer.WriteLine(strLinha);
                        }
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show("Erro Gerando XLS - " + ex.Message, "PontoZero Informa");
                        return;
                    }




                    foreach (ListViewItem item in lstEstoque.Items)
                    {


                        if (item.Checked == true || lstEstoque.CheckBoxes == false)
                        {
                            l = l + 1;
                            strLinha = "";

                            if (optContinental.Checked == false && optVenda.Checked == false && lstEstoque.CheckBoxes == true)
                            {
                                Grava_Separacao(item.SubItems[22].Text, item.SubItems[12].Text);
                            }


                            for (int j = 1; j < lstEstoque.Columns.Count + 1; j++)
                            {
                                if (strLinha != "")
                                {
                                    strLinha = strLinha + ";";
                                }

                                if ((j == 8 || j == 9 || j == 10) && optVenda.Checked == true)
                                {


                                    if (item.SubItems[j - 1].Text == "")
                                    {
                                        strLinha = strLinha + "0";
                                    }
                                    else
                                    {
                                        strLinha = strLinha + Double.Parse(item.SubItems[j - 1].Text);
                                    }
                                }
                                else
                                {
                                    if (item.SubItems[j - 1].Text == "")
                                    {
                                        strLinha = strLinha + "0";
                                    }
                                    else
                                    {
                                        strLinha = strLinha + item.SubItems[j - 1].Text;
                                    }
                                }
                            }
                            try
                            {
                                using (StreamWriter writer = new StreamWriter("C:\\temp\\teste.csv", true))
                                {
                                    writer.WriteLine(strLinha.Replace("\n", "").Replace("\"", ""));
                                }
                            }
                            catch (Exception ex)
                            {
                                MessageBox.Show("Erro Gerando XLS - " + ex.Message, "PontoZero Informa");
                                return;
                            }
                        }
                    }

                    System.Diagnostics.Process.Start(@"C:\\temp\\teste.csv");
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Erro Gerando XLS - " + ex.Message, "PontoZero Informa"); ;
                }
            }
        }

        private void GerarXLSOffice()
        {

        //    Microsoft.Office.Interop.Excel.Application XcelApp = new Microsoft.Office.Interop.Excel.Application();
        //    int l = 2;

        //    if (lstEstoque.Items.Count > 0)
        //    {
        //        try
        //        {
        //            XcelApp.Application.Workbooks.Add(Type.Missing);

        //            //i = i++;

        //            for (int i = 1; i < lstEstoque.Columns.Count + 1; i++)
        //            {
        //                XcelApp.Cells[1, i] = lstEstoque.Columns[i - 1].Text;
        //            }
        //            //
        //            // for (int i = 0; i < dgvDados.Rows.Count - 1; i++)
        //            //{
        //            //   for (int j = 0; j < dgvDados.Columns.Count; j++)
        //            //  {
        //            //     XcelApp.Cells[i + 2, j + 1] = dgvDados.Rows[i].Cells[j].Value.ToString();
        //            //}
        //            //}

        //            foreach (ListViewItem item in lstEstoque.Items)
        //            {


        //                if (item.Checked == true)
        //                {
        //                    l = l + 1;

        //                    if (optContinental.Checked == false && optVenda.Checked == false)
        //                    {
        //                        Grava_Separacao(item.SubItems[22].Text, item.SubItems[12].Text);
        //                    }


        //                    for (int j = 1; j < lstEstoque.Columns.Count + 1; j++)
        //                    {
        //                        if ((j == 8 || j == 9 || j == 10) && optVenda.Checked == true)
        //                        {
        //                            if (item.SubItems[j - 1].Text == "")
        //                            {
        //                                XcelApp.Cells[l, j] = 0;
        //                            }
        //                            else
        //                            {
        //                                XcelApp.Cells[l, j] = Double.Parse(item.SubItems[j - 1].Text);
        //                            }
        //                        }
        //                        else
        //                        {
        //                            XcelApp.Cells[l, j] = item.SubItems[j - 1].Text;
        //                        }
        //                    }
        //                }
        //            }

        //            XcelApp.Columns.AutoFit();
        //            //
        //            XcelApp.Visible = true;
        //        }
        //        catch (Exception ex)
        //        {
        //            MessageBox.Show("Erro : " + ex.Message, "PontoZero Informa");
        //            XcelApp.Quit();
        //        }
        //    }
        }


        private void chkCategoria_CheckedChanged(object sender, EventArgs e)
        {
            cbCategorias.Enabled = chkCategoria.Checked;
        }

        private void optEstoque_CheckedChanged(object sender, EventArgs e)
        {
            Troca_Pesquisa();
        }

        private void Troca_Pesquisa()
        {
            if (optContinental.Checked == true)
            {

                frameEstoque.Visible = false;
                frameVendas.Visible = false;

                chkAtivos.Visible = true;
                chkXML.Visible = true;
                chkCategoria.Visible = true;
                cbCategorias.Visible = true;
            }
            else if (optEstoque.Checked == false)
            {
                frameEstoque.Visible = optEstoque.Checked;
                frameVendas.Visible = optVenda.Checked;

                chkAtivos.Visible = false;
                chkAtivos.Checked = false;

                chkXML.Visible = false;
                chkXML.Checked = false;

                chkCategoria.Visible = false;
                chkCategoria.Checked = false;

                cbCategorias.Visible = false;

            }
            else
            {
                frameEstoque.Visible = optEstoque.Checked;
                frameVendas.Visible = optVenda.Checked;


            }
            //frameCorreios.Visible = false;
            Limpar();
        }

        private void optVenda_CheckedChanged(object sender, EventArgs e)
        {
            Troca_Pesquisa();
            lblBusca.Text = "Produto";
        }

        private Boolean Checa_URL(String strURL)
        {
            HttpWebResponse response = null;
            var request = (HttpWebRequest)WebRequest.Create(strURL);
            request.Method = "HEAD";

            try
            {
                response = (HttpWebResponse)request.GetResponse();
            }
            catch (WebException ex)
            {
                // response.Close();
                return false;
            }
            finally
            {
                // Não esqueça de fechar
                if (response != null)
                {
                    response.Close();
                }
            }

            return true;

        }

        private void frameVendas_Enter(object sender, EventArgs e)
        {

        }

        private void chkCanal_CheckedChanged(object sender, EventArgs e)
        {
            cbCanal.Enabled = chkCanal.Checked;
        }

        private void chkStatus_CheckedChanged(object sender, EventArgs e)
        {
            cbStatus.Enabled = chkStatus.Checked;
        }

        private void Envia_FTP()
        {
            FtpWebRequest ftpRequest;
            FtpWebResponse ftpResponse;
            try
            {
                //define os requesitos para se conectar com o servidor
                ftpRequest = (FtpWebRequest)FtpWebRequest.Create(new Uri("ftp://facilshopping.com.br"));
                ftpRequest.Method = WebRequestMethods.Ftp.UploadFile;
                ftpRequest.Proxy = null;
                ftpRequest.UseBinary = true;
                ftpRequest.Credentials = new NetworkCredential("facilshopping", "facil32245");

                //Seleção do arquivo a ser enviado
                FileInfo arquivo = new FileInfo("C:/Temp/facilshopping.xml");
                byte[] fileContents = new byte[arquivo.Length];

                using (FileStream fr = arquivo.OpenRead())
                {
                    fr.Read(fileContents, 0, Convert.ToInt32(arquivo.Length));
                }

                using (Stream writer = ftpRequest.GetRequestStream())
                {
                    writer.Write(fileContents, 0, fileContents.Length);
                }

                //obtem o FtpWebResponse da operação de upload
                ftpResponse = (FtpWebResponse)ftpRequest.GetResponse();
                MessageBox.Show(ftpResponse.StatusDescription, "PontoZero Informa");
            }
            catch (WebException webex)
            {
                MessageBox.Show(webex.ToString(), "PontoZero Informa");
            }
        }

        private void optContinental_CheckedChanged(object sender, EventArgs e)
        {
            Troca_Pesquisa();
        }

        private void radioButton1_CheckedChanged(object sender, EventArgs e)
        {
            frameEstoque.Visible = optEstoque.Checked;
            frameVendas.Visible = optDeclaração.Checked;
            // frameCorreios.Visible = false;
            lblBusca.Text = "Consulta";
            Limpar();
        }

        private void reportViewer1_Load(object sender, EventArgs e)
        {

        }

        private void lblQuantidadeTotal_Click(object sender, EventArgs e)
        {

        }

        private void lblValorTotal_Click(object sender, EventArgs e)
        {

        }

        private void lblFreteTotal_Click(object sender, EventArgs e)
        {

        }

        private void optCorreio_CheckedChanged(object sender, EventArgs e)
        {
            frameEstoque.Visible = false;
            frameVendas.Visible = false;
            // frameCorreios.Visible = true;
            Limpar();
        }

        private string Novo_Rastreio(bool blSedex, string strPedido)
        {
            MySqlCommand cmd1;
            MySqlDataReader dataReaderRastreio;
            string strRastreio;

            strRastreio = "";

            string query = "SELECT numero, prefixo, sufixo FROM RASTREIO_CORREIOS WHERE PEDIDO = '" + strPedido + "'";

            global.myDB.CloseConnection();
            if (global.myDB.connection.State == ConnectionState.Closed)
            {
                global.myDB.OpenConnection();
            }

            cmd1 = new MySqlCommand(query, global.myDB.connection);

            //dataReader.Close();            

            dataReaderRastreio = cmd1.ExecuteReader();

            if (dataReaderRastreio.Read())
            {
                blReimpressãoRastreio = true;
                strRastreio = dataReaderRastreio["prefixo"].ToString() + dataReaderRastreio["numero"].ToString() + dataReaderRastreio["sufixo"].ToString();
            }
            else
            {
                blReimpressãoRastreio = false;
                dataReaderRastreio.Close();

                query = "SELECT * FROM RASTREIO_CORREIOS";

                if (blSedex == true)
                {
                    query = query + " WHERE SEDEX = 1";
                }
                else
                {
                    query = query + " WHERE PAC = 1";
                }

                query = query + " AND  PEDIDO IS NULL ";

                query = query + "  ORDER BY NUMERO DESC  limit 0,1;";

                global.myDB.CloseConnection();
                if (global.myDB.connection.State == ConnectionState.Closed)
                {
                    global.myDB.OpenConnection();
                }

                cmd1 = new MySqlCommand(query, global.myDB.connection);

                dataReaderRastreio = cmd1.ExecuteReader();

                if (dataReaderRastreio.Read())
                {
                    strRastreio = dataReaderRastreio["prefixo"].ToString();

                    if (dataReaderRastreio["numero"].ToString().Length < 8)
                    {
                        strRastreio = strRastreio + "0";
                    }

                    strRastreio = strRastreio + dataReaderRastreio["numero"].ToString();
                    strRastreio = strRastreio + dataReaderRastreio["sufixo"].ToString();
                }

            }

            dataReaderRastreio.Close();

            return strRastreio;
        }

        private string Imprime(String strNome, bool blSedex, string strPedido)
        {
            string strRastreio;
            this.userControl11 = new DSBarCode.BarCodeCtrl();

            global.GetRastreioWEBService(blSedex);

            strRastreio = Novo_Rastreio(blSedex, strPedido);
            if (strRastreio != "")
            {


                strRastreio = geraEtiquetaComDigitoVerificador(strRastreio);
                // 
                // userControl11
                // 
                this.userControl11.BarCode = strRastreio;

                this.userControl11.BarCodeHeight = 80;
                //this.userControl11.Font = new System.Drawing.Font("Comic Sans MS", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
                //this.userControl11.FooterFont = new System.Drawing.Font("Microsoft Sans Serif", 8F);
                // this.userControl11.HeaderFont = new System.Drawing.Font("Comic Sans MS", 26.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
                // this.userControl11.HeaderText = "GIOVANINI";
                this.userControl11.LeftMargin = 10;
                this.userControl11.Location = new System.Drawing.Point(8, 8);
                this.userControl11.Name = "userControl11";
                // this.userControl11.ShowFooter = true;
                // this.userControl11.ShowHeader = true;
                this.userControl11.Size = new System.Drawing.Size(336, 100);
                this.userControl11.TabIndex = 0;
                this.userControl11.TopMargin = 5;
                this.userControl11.VertAlign = DSBarCode.BarCodeCtrl.AlignType.Center;
                this.userControl11.Weight = DSBarCode.BarCodeCtrl.BarCodeWeight.Small;


                userControl11.SaveImage("c:\\temp\\" + strNome + ".bmp");

                imgCodigoBarra.Image = Image.FromFile(@"C:\temp\" + strNome + ".bmp");
            }
            return strRastreio;
        }

        private void ImprimeCEP(String strNome, String strCEP)
        {
            this.userControl11.BarCode = strCEP;

            this.userControl11.BarCodeHeight = 50;
            // this.userControl11.Font = new System.Drawing.Font("Comic Sans MS", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));

            this.userControl11.LeftMargin = 0;
            this.userControl11.Location = new System.Drawing.Point(0, 0);
            this.userControl11.Name = "userControl11";
            this.userControl11.Size = new System.Drawing.Size(224, 60);
            this.userControl11.TabIndex = 0;
            this.userControl11.TopMargin = 0;
            this.userControl11.VertAlign = DSBarCode.BarCodeCtrl.AlignType.Center;
            this.userControl11.Weight = DSBarCode.BarCodeCtrl.BarCodeWeight.Small;

            userControl11.SaveImage("c:\\temp\\" + strNome + ".bmp");

            imgCodigoBarraCEP.Image = Image.FromFile(@"C:\temp\" + strNome + ".bmp");

        }
        public void ConectaWEBService()
        {
            //System.Console.Write("Digite o CEP: ");
            var valor = "08793-020";
            try
            {
                var ws = new WSCorreios.AtendeClienteClient();
                var resposta = ws.consultaCEP(valor);
                System.Console.WriteLine();
                System.Console.WriteLine("Endereço: {0}", resposta.end);
                System.Console.WriteLine("Complemento: {0}", resposta.complemento2);
                System.Console.WriteLine("Complemento 2: {0}", resposta.complemento2);
                System.Console.WriteLine("Bairro: {0}", resposta.bairro);
                System.Console.WriteLine("Cidade: {0}", resposta.cidade);
                System.Console.WriteLine("Estado: {0}", resposta.uf);
                System.Console.WriteLine("Unidades de Postagem: {0}", resposta.unidadesPostagem);
            }
            catch (Exception ex)
            {
                System.Console.WriteLine("Erro ao efetuar busca do CEP: {0}", ex.Message);
            }
            System.Console.ReadLine();
        }

        private void cmdMarcar_Click(object sender, EventArgs e)
        {

            foreach (ListViewItem item in lstEstoque.Items)
            {
                item.Checked = !Convert.ToBoolean(cmdMarcar.Tag.ToString());

            }

            cmdMarcar.Tag = !Convert.ToBoolean(cmdMarcar.Tag.ToString());
        }

        private void cmdLiberar_Click(object sender, EventArgs e)
        {
            string strOperador;

            if (optDeclaração.Checked == true && lstEstoque.Items.Count > 0)
            {
                LOGIN frmLogin = new LOGIN();
                frmLogin.intPermissao = 0;
                frmLogin.ShowDialog(this);


                if (frmLogin.blAprovado == true)
                {
                    strOperador = frmLogin.strOperador;
                    AtualisaEstoqueLudostore(frmLogin.strOperador);
                }
            }
        }

        private void chkProcesso_CheckedChanged(object sender, EventArgs e)
        {
            cbProcesso.Enabled = chkProcesso.Checked;
        }

        private void label5_Click(object sender, EventArgs e)
        {

        }

        private void cmdSeparar_Click(object sender, EventArgs e)
        {
            ExibeLinha(cmdSeparar.BackColor.ToString());
        }

        private void cmdSeparado_Click(object sender, EventArgs e)
        {
            ExibeLinha(cmdSeparado.BackColor.ToString());
        }

        private void cmdEnviado_Click(object sender, EventArgs e)
        {
            ExibeLinha(cmdEnviado.BackColor.ToString());
        }

        private void cmdLidos_Click(object sender, EventArgs e)
        {
            ExibeLinha(cmdLidos.BackColor.ToString());
        }

        private void cmdParado_Click(object sender, EventArgs e)
        {
            ExibeLinha(cmdParado.BackColor.ToString());
        }

        private void label5_Click_1(object sender, EventArgs e)
        {

        }


        private void Email(String strEmail, String strPedido)
        {
            //GeraCodBarra("12345678");
            Enviar(strEmail, strPedido + ".jpg", strPedido);

        }

        private void GeraCodBarra(String strBarra)
        {
            this.userControl11 = new DSBarCode.BarCodeCtrl();

            this.userControl11.BarCode = strBarra;

            this.userControl11.BarCodeHeight = 50;
            // this.userControl11.Font = new System.Drawing.Font("Comic Sans MS", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));

            this.userControl11.LeftMargin = 0;
            this.userControl11.Location = new System.Drawing.Point(0, 0);
            this.userControl11.Name = "userControl11";
            this.userControl11.Size = new System.Drawing.Size(224, 60);
            this.userControl11.TabIndex = 0;
            this.userControl11.TopMargin = 0;
            this.userControl11.VertAlign = DSBarCode.BarCodeCtrl.AlignType.Center;
            this.userControl11.Weight = DSBarCode.BarCodeCtrl.BarCodeWeight.Small;

            userControl11.SaveImage(strBarra + ".bmp");

        }

        private static void Enviar(String strEmail, String strImagem, String strPedido)
        {
            // throw new NotImplementedException
            String msgEmail;



            msgEmail = "	<div>";

            msgEmail = msgEmail + "	<div>";
            msgEmail = msgEmail + "		<p>";
            msgEmail = msgEmail + "		<br />";
            msgEmail = msgEmail + "		</p>";
            msgEmail = msgEmail + "		<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\">";
            msgEmail = msgEmail + "		<tbody>";
            msgEmail = msgEmail + "			<tr>";
            msgEmail = msgEmail + "				<td class=\"full\">";
            msgEmail = msgEmail + "				</td>";
            msgEmail = msgEmail + "			</tr>";
            msgEmail = msgEmail + "			<tr>";
            msgEmail = msgEmail + "				<td class=\"full\">";
            msgEmail = msgEmail + "					<img src=\"https://facilshopping.com.br/media/email/logo/default/MogiGeek.jpg\" border=\"0\" width=\"804\" height=\"274\" />  ";
            msgEmail = msgEmail + "					<table class=\"columns\" style=\"width: 811px; height: 55px;\">";
            msgEmail = msgEmail + "						<tbody>";
            msgEmail = msgEmail + "							<tr>";
            msgEmail = msgEmail + "								<td class=\"panel\">";
            msgEmail = msgEmail + "									<p style=\"text-align: center;\">";
            msgEmail = msgEmail + "										<span style=\"font-size: xx-large;\">";
            msgEmail = msgEmail + "											<strong>Guarde o codigo do ingresso: " + strPedido + " </strong>";

            msgEmail = msgEmail + "										</span>";
            msgEmail = msgEmail + "									</p>";
            msgEmail = msgEmail + "								</td>";
            msgEmail = msgEmail + "								<td class=\"expander\">";
            msgEmail = msgEmail + "								</td>";
            msgEmail = msgEmail + "							</tr>";
            msgEmail = msgEmail + "						</tbody>";
            msgEmail = msgEmail + "					</table>";
            msgEmail = msgEmail + "					<table border=\"0\" style=\"width: 809px; height: 38px;\">";
            msgEmail = msgEmail + "						<tbody>";
            msgEmail = msgEmail + "							<tr>";
            msgEmail = msgEmail + "								<td style=\"text-align: right;\">";
            msgEmail = msgEmail + "									<table border=\"0\">";
            msgEmail = msgEmail + "										<tbody>";
            msgEmail = msgEmail + "											<tr>";
            msgEmail = msgEmail + "												<td>";
            msgEmail = msgEmail + "													<p style=\"text-align: justify;\">";

            msgEmail = msgEmail + "															<font size=\"4\" style=\"font-size: 16pt;\">";
            msgEmail = msgEmail + "																<strong style=\"\"> Em anexo está o QRCode que deverá ser mostrado na entrada, com ele você receberá as credenciais para o evento.</ strong > ";

            msgEmail = msgEmail + "													</font>";
            msgEmail = msgEmail + "														</p>";
            msgEmail = msgEmail + "													<p style=\"text-align: justify;\">";

            msgEmail = msgEmail + "															<font size=\"4\" style=\"font-size: 16pt;\">";
            msgEmail = msgEmail + "																<strong>";
            msgEmail = msgEmail + "															</strong>";

            msgEmail = msgEmail + "															<strong style=\"\"> Cada ingresso está vinculado a um QRCode, ou seja se você comprou mais de um ingresso precisa mostrar os QRCode de cada um deles </ strong > ";
            msgEmail = msgEmail + "															</font>";

            msgEmail = msgEmail + "													</p>";

            msgEmail = msgEmail + "													<p style=\"text-align: justify;\">";

            msgEmail = msgEmail + "															<font size=\"4\" style=\"font-size: 16pt;\">";
            msgEmail = msgEmail + "																<strong>";
            msgEmail = msgEmail + "															</strong>";

            msgEmail = msgEmail + "															<strong style=\"\"> Esse QRcode deverá ser acessado apenas no dia do Evento e apenas por pessoal autorizado, caso ele seja acessado de forma indevida e por pessoas não autorizadas correrá o risco do acesso ser bloqueado </ strong > ";
            msgEmail = msgEmail + "															</font>";

            msgEmail = msgEmail + "													</p>";



            msgEmail = msgEmail + "														<p style=\"text-align: justify;\">";
            msgEmail = msgEmail + "															<font size=\"4\" style=\"font-size: 16pt;\">";
            msgEmail = msgEmail + "																<strong style=\"\">";
            msgEmail = msgEmail + "																	<br />";
            msgEmail = msgEmail + "																</strong>";
            msgEmail = msgEmail + "															</font>";
            msgEmail = msgEmail + "														</p>";
            //msgEmail = msgEmail + "														<p style=\"text-align: justify;\">";
            msgEmail = msgEmail + "																		<p style=\"text-align: center; \">";
            msgEmail = msgEmail + "																			<img src=\"cid:" + strPedido + "\">";
            msgEmail = msgEmail + "																		</p>";
            //msgEmail = msgEmail + "														</p>";
            msgEmail = msgEmail + "														<p style=\"text-align: center;\">";
            msgEmail = msgEmail + "															<br />";
            msgEmail = msgEmail + "														</p>";
            msgEmail = msgEmail + "														<p style=\"text-align: justify;\">";
            msgEmail = msgEmail + "															<span style=\"font-size: large;\">";
            msgEmail = msgEmail + "																<strong>";
            msgEmail = msgEmail + "																	<br />";
            msgEmail = msgEmail + "																</strong>";
            msgEmail = msgEmail + "															</span>";
            msgEmail = msgEmail + "														</p>";
            msgEmail = msgEmail + "												</td>";
            msgEmail = msgEmail + "											</tr>";
            msgEmail = msgEmail + "										</tbody>";
            msgEmail = msgEmail + "									</table>";
            msgEmail = msgEmail + "	*Esta e uma mensagem gerada automaticamente. Caso tenha alguma duvida, entre em contato no link abaixo</td>";
            msgEmail = msgEmail + "							</tr>";
            msgEmail = msgEmail + "						</tbody>";
            msgEmail = msgEmail + "					</table>";
            msgEmail = msgEmail + "					<br />";
            msgEmail = msgEmail + "					<table class=\"button\" style=\"width: 810px; height: 22px;\">";
            msgEmail = msgEmail + "						<tbody>";
            msgEmail = msgEmail + "							<tr>";
            msgEmail = msgEmail + "								<td style=\"text-align: center;\">";
            msgEmail = msgEmail + "									<a href=\"http://facilshopping.com.br/contacts/\"> ENTRE EM CONTATO!</ a > ";
            msgEmail = msgEmail + "								</td>";
            msgEmail = msgEmail + "							</tr>";
            msgEmail = msgEmail + "						</tbody>";
            msgEmail = msgEmail + "					</table>";
            msgEmail = msgEmail + "				</td>";
            msgEmail = msgEmail + "			</tr>";
            msgEmail = msgEmail + "			<tr>";
            msgEmail = msgEmail + "				<td>";
            msgEmail = msgEmail + "					<table class=\"row\">";
            msgEmail = msgEmail + "						<tbody>";
            msgEmail = msgEmail + "							<tr>";
            msgEmail = msgEmail + "								<td class=\"full wrapper last\">";
            msgEmail = msgEmail + "									<table class=\"columns\">";
            msgEmail = msgEmail + "										<tbody>";

            msgEmail = msgEmail + "										</tbody>";
            msgEmail = msgEmail + "									</table>";
            msgEmail = msgEmail + "								</td>";
            msgEmail = msgEmail + "							</tr>";
            msgEmail = msgEmail + "						</tbody>";
            msgEmail = msgEmail + "					</table>";
            msgEmail = msgEmail + "				</td>";
            msgEmail = msgEmail + "			</tr>";
            msgEmail = msgEmail + "		</tbody>";
            msgEmail = msgEmail + "		</table>";
            msgEmail = msgEmail + "		<p>";
            msgEmail = msgEmail + "		<br />";
            msgEmail = msgEmail + "		</p>";
            msgEmail = msgEmail + "	</div>";

            msgEmail = msgEmail + "	</div>";

            System.Net.Mail.SmtpClient client = new System.Net.Mail.SmtpClient();
            client.Host = "smtp.facilshopping.com.br";
            client.Port = 587;
            client.EnableSsl = false;
            client.Credentials = new System.Net.NetworkCredential("sac@facilshopping.com.br", "Facil32245-Card");
            MailMessage mail = new MailMessage();
            mail.Sender = new System.Net.Mail.MailAddress("sac@facilshopping.com.br", "FacilShopping");
            mail.From = new MailAddress("sac@facilshopping.com.br", "FacilShopping");
            mail.To.Add(new MailAddress(strEmail, "Cliente MogiGeek!"));
            mail.To.Add(new MailAddress("pedidos@facilshopping.com.br", "SAC"));
            mail.Subject = "Ingresso MogiGeek!! " + strPedido;
            mail.Body = msgEmail;
            mail.IsBodyHtml = true;
            mail.Priority = MailPriority.High;



            // Cria o anexo
            Attachment att = new Attachment(strImagem);
            //Adiciona a mensagem
            mail.Attachments.Add(att);
            //Envia.

            string contentID = strPedido;
            att.ContentId = contentID;



            Console.WriteLine(strEmail);

            System.Threading.Thread.Sleep(2000);

            try
            {
                client.Send(mail);
            }
            catch (Exception e)
            {
                Console.WriteLine("{0} Exception caught.", e);

                Console.Read();

                client.Send(mail);

            }
            finally
            {
                mail = null;
                // Console.Read();
            }
        }


        public Bitmap GerarQRCode(int width, int height, string text)
        {
            try
            {
                var bw = new ZXing.BarcodeWriter();
                var encOptions = new ZXing.Common.EncodingOptions() { Width = width, Height = height, Margin = 0 };
                bw.Options = encOptions;
                bw.Format = ZXing.BarcodeFormat.QR_CODE;
                var resultado = new Bitmap(bw.Write(text));
                return resultado;
            }
            catch
            {
                throw;
            }
        }

        private void cmdEmail_Click(object sender, EventArgs e)
        {
            string strEmail, strSKU = "";

            if (MessageBox.Show("Confirma o envio do email???", "PontoZero Informa", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1) == DialogResult.Yes)
            {
                foreach (ListViewItem item in lstEstoque.Items)
                {
                    strSKU = item.SubItems[9].Text;
                    if (item.Checked == true && (strSKU == "INGRESSO2DIAS" || strSKU == "INGRESSO1DIA" || strSKU == "INGRESSOS-Dia: 02/02" || strSKU == "INGRESSOS-Dia: 01/02"))
                    {

                        picQRCode.Image = GerarQRCode(300, 300, "https://www.facilshopping.com.br/mogigeek1.php?cod=" + item.SubItems[0].Text);
                        picQRCode.Image.Save(item.SubItems[0].Text + ".jpg");

                        strEmail = BuscaEmail(item.SubItems[0].Text);

                        // Email("a.giovanini@gmail.com", item.SubItems[0].Text);
                        Email(strEmail, item.SubItems[0].Text);

                        if (System.IO.File.Exists(@item.SubItems[0].Text))
                        {
                            try
                            {
                                System.IO.File.Delete(@item.SubItems[0].Text);
                            }
                            catch (Exception ex)
                            {
                                //MessageBox.Show(ex.Message, "PontoZero Informa");
                            }
                        }

                    }


                }


                MessageBox.Show("Ingressos enviados com sucesso!!", "PontoZero Informa");
            }
        }

        private string BuscaEmail(string strPedido)
        {
            MySqlCommand cmd;
            MySqlDataReader dataReader;
            string strRetorno = "";

            string query = "select customer_email from sales_flat_order where increment_id = '" + strPedido + "';";

            if (global.myDB.connection.State == ConnectionState.Closed)
            {
                global.myDB.OpenConnection();
            }

            cmd = new MySqlCommand(query, global.myDB.connection);



            try
            {
                dataReader = cmd.ExecuteReader();
            }
            catch (Exception)
            {
                // dataReader.Close();
                global.myDB.OpenConnection();
                cmd = new MySqlCommand(query, global.myDB.connection);
                dataReader = cmd.ExecuteReader();
            }

            while (dataReader.Read())
            {
                strRetorno = dataReader["customer_email"].ToString();
            }



            dataReader.Close();

            return strRetorno;

        }

        private void lstEstoque_ColumnClick(object sender, ColumnClickEventArgs e)
        {
            ItemComparer sorter = lstEstoque.ListViewItemSorter as ItemComparer;
            if (sorter == null)
            {
                sorter = new ItemComparer(e.Column);
                sorter.Order = SortOrder.Ascending;
                lstEstoque.ListViewItemSorter = sorter;
            }
            // if clicked column is already the column that is being sorted
            if (e.Column == sorter.Column)
            {
                // Reverse the current sort direction
                if (sorter.Order == SortOrder.Ascending)
                    sorter.Order = SortOrder.Descending;
                else
                    sorter.Order = SortOrder.Ascending;
            }
            else
            {
                // Set the column number that is to be sorted; default to ascending.
                sorter.Column = e.Column;
                sorter.Order = SortOrder.Ascending;
            }
            lstEstoque.Sort();
        }

        private void chkLoja_CheckedChanged(object sender, EventArgs e)
        {
            dcLoja.Enabled = chkLoja.Checked;
        }

        private void chkCategoriaVenda_CheckedChanged(object sender, EventArgs e)
        {
            cbCategoriasVenda.Enabled = chkCategoriaVenda.Checked;
        }

        private void chkFabricante_CheckedChanged(object sender, EventArgs e)
        {
            cbFabricante.Enabled = chkFabricante.Checked;
        }

        private void lstEstoque_DoubleClick(object sender, EventArgs e)
        {
            if(optEstoque.Checked)
            {
                frmExibeMovEstoque frmExibe = new frmExibeMovEstoque();
                frmExibe.strSKU = lstEstoque.SelectedItems[0].SubItems[0].Text;
                frmExibe.ShowDialog(this);
            }
        }
    }
}

