
insert into catalog_category_entity (entity_id, entity_type_id, attribute_set_id, parent_id, created_at, updated_at, path, position, level, children_count) value
(50, 3,3,2,'2018-02-27 09:46:18','2018-02-27 09:46:18','1/2/50',11, 2,1);

insert into catalog_category_entity_varchar (value_id, entity_type_id, attribute_id, store_id, entity_id, value) value 
( 541,	3,	41,	0,	50,	'Plus Size');

insert into catalog_category_entity_varchar (value_id, entity_type_id, attribute_id, store_id, entity_id, value) value 
( 542,	3,	43,	0,	50,	'plus size');

insert into catalog_category_entity_varchar (value_id, entity_type_id, attribute_id, store_id, entity_id) value 
( 543,	3,	46,	0,	50);	

insert into catalog_category_entity_varchar (value_id, entity_type_id, attribute_id, store_id, entity_id, value) value 
( 544,	3,	49,	0,	50,	'PRODUCTS' );

insert into catalog_category_entity_varchar (value_id, entity_type_id, attribute_id, store_id, entity_id) value 
( 545,	3,	58,	0,	50);	

insert into catalog_category_entity_varchar (value_id, entity_type_id, attribute_id, store_id, entity_id) value 
( 546,	3,	61,	0,	50);	

insert into catalog_category_entity_varchar (value_id, entity_type_id, attribute_id, store_id, entity_id, value) value 
( 547,	3,	57,	1,	50,	'plus-size.html');

insert into catalog_category_entity_varchar (value_id, entity_type_id, attribute_id, store_id, entity_id, value) value 
( 548,	3,	57,	0,	50,	'plus-size.html');



insert into catalog_category_entity_datetime(value_id, entity_type_id, attribute_id, store_id, entity_id) value
(219,3,59,0,50);

insert into catalog_category_entity_datetime(value_id, entity_type_id, attribute_id, store_id, entity_id) value
(220,3,60,0,50);

insert into catalog_category_entity_decimal (value_id, entity_type_id, attribute_id, store_id, entity_id) value
(49,3,70,0,50);

insert into catalog_category_entity_int (value_id, entity_type_id, attribute_id, store_id, entity_id, value) value
(378,	3,	42,	0,	50,	1);

insert into catalog_category_entity_int (value_id, entity_type_id, attribute_id, store_id, entity_id, value) value
(379,	3,	67,	0,	50,	1);

insert into catalog_category_entity_int (value_id, entity_type_id, attribute_id, store_id, entity_id) value
(380,	3,	50,	0,	50);	

insert into catalog_category_entity_int (value_id, entity_type_id, attribute_id, store_id, entity_id, value) value
(381,	3,	51,	0,	50,	0);

insert into catalog_category_entity_int (value_id, entity_type_id, attribute_id, store_id, entity_id, value) value
(382,	3,	68,	0,	50,	0);

insert into catalog_category_entity_int (value_id, entity_type_id, attribute_id, store_id, entity_id, value) value
(383,	3,	69,	0,	50,	0);
					
insert into catalog_category_entity_int (value_id, entity_type_id, attribute_id, store_id, entity_id) value
(532, 3, 44, 0, 50);

insert into catalog_category_entity_int (value_id, entity_type_id, attribute_id, store_id, entity_id) value
(533, 3, 47, 0, 50);

insert into catalog_category_entity_int (value_id, entity_type_id, attribute_id, store_id, entity_id) value
(534, 3, 48, 0, 50);

insert into catalog_category_entity_int (value_id, entity_type_id, attribute_id, store_id, entity_id) value
(535, 3, 62, 0, 50);

insert into catalog_category_entity_int (value_id, entity_type_id, attribute_id, store_id, entity_id) value
(536, 3, 65, 0, 50);


