SELECT  catalog_product_entity_varchar.value_id, catalog_product_entity_varchar.entity_id, catalog_product_entity_varchar.value, `cataloginventory_stock_item`.qty, 
`cataloginventory_stock_item`.min_qty, catalog_product_entity_text.value fabricante, catalog_product_entity_decimal.value VALOR,

CASE WHEN cataloginventory_stock_status.stock_status = 1 THEN 'ATIVO' ELSE 'DESATIVADO' END STATUS, 
(SELECT value FROM catalog_product_entity_varchar WHERE catalog_product_entity_varchar.attribute_id = 84 AND cataloginventory_stock_item.product_id = catalog_product_entity_varchar.entity_id  ) FORNECEDOR


FROM `catalog_product_entity_varchar` 

INNER JOIN `cataloginventory_stock_item`
ON `cataloginventory_stock_item`.product_id = `catalog_product_entity_varchar`.entity_id



INNER JOIN  `cataloginventory_stock_status`
ON `cataloginventory_stock_status`.product_id = `catalog_product_entity_varchar`.entity_id

INNER JOIN  `catalog_product_entity_text`
ON `catalog_product_entity_text`.entity_id = `catalog_product_entity_varchar`.entity_id
AND catalog_product_entity_text.attribute_id = 83


LEFT JOIN catalog_product_entity_decimal
ON catalog_product_entity_decimal.attribute_id = 75
AND catalog_product_entity_decimal.entity_id = catalog_product_entity_varchar.entity_id

where catalog_product_entity_varchar.attribute_id = '71'

limit 500000
