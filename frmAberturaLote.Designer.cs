﻿
namespace FacilShoppingReports
{
    partial class frmAberturaLote
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);  
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmAberturaLote));
            this.btnAmazon = new System.Windows.Forms.Button();
            this.btnSequoia = new System.Windows.Forms.Button();
            this.btnTotalExpress = new System.Windows.Forms.Button();
            this.btnMercadoPlace = new System.Windows.Forms.Button();
            this.btnMercadoEnvios = new System.Windows.Forms.Button();
            this.btnMagalu = new System.Windows.Forms.Button();
            this.btnB2W = new System.Windows.Forms.Button();
            this.btnCorreios = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btnAmazon
            // 
            this.btnAmazon.Image = ((System.Drawing.Image)(resources.GetObject("btnAmazon.Image")));
            this.btnAmazon.Location = new System.Drawing.Point(230, 200);
            this.btnAmazon.Name = "btnAmazon";
            this.btnAmazon.Size = new System.Drawing.Size(202, 88);
            this.btnAmazon.TabIndex = 7;
            this.btnAmazon.UseVisualStyleBackColor = true;
            this.btnAmazon.Click += new System.EventHandler(this.btnAmazon_Click);
            // 
            // btnSequoia
            // 
            this.btnSequoia.Image = ((System.Drawing.Image)(resources.GetObject("btnSequoia.Image")));
            this.btnSequoia.Location = new System.Drawing.Point(22, 200);
            this.btnSequoia.Name = "btnSequoia";
            this.btnSequoia.Size = new System.Drawing.Size(202, 88);
            this.btnSequoia.TabIndex = 6;
            this.btnSequoia.UseVisualStyleBackColor = true;
            this.btnSequoia.Click += new System.EventHandler(this.btnSequoia_Click);
            // 
            // btnTotalExpress
            // 
            this.btnTotalExpress.BackColor = System.Drawing.Color.White;
            this.btnTotalExpress.Image = ((System.Drawing.Image)(resources.GetObject("btnTotalExpress.Image")));
            this.btnTotalExpress.Location = new System.Drawing.Point(438, 106);
            this.btnTotalExpress.Name = "btnTotalExpress";
            this.btnTotalExpress.Size = new System.Drawing.Size(202, 88);
            this.btnTotalExpress.TabIndex = 5;
            this.btnTotalExpress.UseVisualStyleBackColor = false;
            this.btnTotalExpress.Click += new System.EventHandler(this.btnTotalExpress_Click);
            // 
            // btnMercadoPlace
            // 
            this.btnMercadoPlace.BackColor = System.Drawing.Color.White;
            this.btnMercadoPlace.Image = ((System.Drawing.Image)(resources.GetObject("btnMercadoPlace.Image")));
            this.btnMercadoPlace.Location = new System.Drawing.Point(230, 106);
            this.btnMercadoPlace.Name = "btnMercadoPlace";
            this.btnMercadoPlace.Size = new System.Drawing.Size(202, 88);
            this.btnMercadoPlace.TabIndex = 4;
            this.btnMercadoPlace.UseVisualStyleBackColor = false;
            this.btnMercadoPlace.Click += new System.EventHandler(this.btnMercadoPlace_Click);
            // 
            // btnMercadoEnvios
            // 
            this.btnMercadoEnvios.Image = ((System.Drawing.Image)(resources.GetObject("btnMercadoEnvios.Image")));
            this.btnMercadoEnvios.Location = new System.Drawing.Point(22, 106);
            this.btnMercadoEnvios.Name = "btnMercadoEnvios";
            this.btnMercadoEnvios.Size = new System.Drawing.Size(202, 88);
            this.btnMercadoEnvios.TabIndex = 3;
            this.btnMercadoEnvios.UseVisualStyleBackColor = true;
            this.btnMercadoEnvios.Click += new System.EventHandler(this.btnMercadoEnvios_Click);
            // 
            // btnMagalu
            // 
            this.btnMagalu.BackColor = System.Drawing.Color.White;
            this.btnMagalu.Image = ((System.Drawing.Image)(resources.GetObject("btnMagalu.Image")));
            this.btnMagalu.Location = new System.Drawing.Point(438, 12);
            this.btnMagalu.Name = "btnMagalu";
            this.btnMagalu.Size = new System.Drawing.Size(202, 88);
            this.btnMagalu.TabIndex = 2;
            this.btnMagalu.UseVisualStyleBackColor = false;
            this.btnMagalu.Click += new System.EventHandler(this.btnMagalu_Click);
            // 
            // btnB2W
            // 
            this.btnB2W.BackColor = System.Drawing.Color.White;
            this.btnB2W.Image = ((System.Drawing.Image)(resources.GetObject("btnB2W.Image")));
            this.btnB2W.Location = new System.Drawing.Point(230, 12);
            this.btnB2W.Name = "btnB2W";
            this.btnB2W.Size = new System.Drawing.Size(202, 88);
            this.btnB2W.TabIndex = 1;
            this.btnB2W.UseVisualStyleBackColor = false;
            this.btnB2W.Click += new System.EventHandler(this.btnB2W_Click);
            // 
            // btnCorreios
            // 
            this.btnCorreios.Image = ((System.Drawing.Image)(resources.GetObject("btnCorreios.Image")));
            this.btnCorreios.Location = new System.Drawing.Point(22, 12);
            this.btnCorreios.Name = "btnCorreios";
            this.btnCorreios.Size = new System.Drawing.Size(202, 88);
            this.btnCorreios.TabIndex = 0;
            this.btnCorreios.UseVisualStyleBackColor = true;
            this.btnCorreios.Click += new System.EventHandler(this.btnCorreios_Click);
            // 
            // frmAberturaLote
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(659, 305);
            this.Controls.Add(this.btnAmazon);
            this.Controls.Add(this.btnSequoia);
            this.Controls.Add(this.btnTotalExpress);
            this.Controls.Add(this.btnMercadoPlace);
            this.Controls.Add(this.btnMercadoEnvios);
            this.Controls.Add(this.btnMagalu);
            this.Controls.Add(this.btnB2W);
            this.Controls.Add(this.btnCorreios);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmAberturaLote";
            this.Text = "Abertura de Lote";
            this.Load += new System.EventHandler(this.frmAberturaLote_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnCorreios;
        private System.Windows.Forms.Button btnB2W;
        private System.Windows.Forms.Button btnMagalu;
        private System.Windows.Forms.Button btnMercadoEnvios;
        private System.Windows.Forms.Button btnMercadoPlace;
        private System.Windows.Forms.Button btnTotalExpress;
        private System.Windows.Forms.Button btnSequoia;
        private System.Windows.Forms.Button btnAmazon;
    }
}