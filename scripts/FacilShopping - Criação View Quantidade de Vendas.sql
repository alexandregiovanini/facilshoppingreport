select * from VENDIDOS_LOJA where sku = '01690';
select * from VENDIDOS_MARKETPLACES where sku = '01690';
SELECT * FROM ESTOQUE_FECHAMENTO  where sku = '01690';
SELECT * FROM ESTOQUE_FECHAMENTO limit 10000;

alter VIEW VENDIDOS_LOJA
AS SELECT sum(qty_ordered) qtde, sku 
FROM sales_flat_order_item  
GROUP BY 2 ORDER BY SKU LIMIT 10000;

CREATE VIEW VENDIDOS_MARKETPLACES
AS SELECT sum(QTDE) qtde, sku 
FROM ITENS_PEDIDOS_INTEGRADOS 
GROUP BY 2 ORDER BY SKU LIMIT 10000;

alter VIEW VENDIDOS_MARKETPLACES_A_SEPARAR
AS select sum(QTDE) qtde, sku 
from ITENS_PEDIDOS_INTEGRADOS 
where id_pedido in (select id_pedido 
					from PEDIDOS_INTEGRADOS 
                    where dt_pedido >  now() - interval + 5 day                 
                    and separado = 0)                    
GROUP BY 2 ORDER BY SKU LIMIT 10000;

alter VIEW VENDIDOS_LOJA_A_SEPARAR
AS SELECT sum(qty_ordered) qtde, sku 
FROM sales_flat_order_item  
WHERE separado = 0 and created_at > now() - interval + 5 day
GROUP BY 2 ORDER BY SKU LIMIT 10000;

CREATE VIEW VENDIDOS_LOJA_MES
AS 
SELECT sum(qty_ordered) qtde, sku 
FROM sales_flat_order_item  
WHERE sales_flat_order_item.created_at BETWEEN DATE_ADD(CURRENT_DATE(), INTERVAL - 30 DAY) AND CURRENT_DATE()
GROUP BY 2 ORDER BY SKU LIMIT 10000;

CREATE VIEW VENDIDOS_MARKETPLACES_MES
AS SELECT sum(QTDE) qtde, sku 
FROM ITENS_PEDIDOS_INTEGRADOS 
WHERE id_pedido IN (SELECT id_pedido FROM PEDIDOS_INTEGRADOS WHERE PEDIDOS_INTEGRADOS.dt_pedido BETWEEN DATE_ADD(CURRENT_DATE(), INTERVAL - 30 DAY) AND CURRENT_DATE())  
GROUP BY 2 ORDER BY SKU LIMIT 10000;

ALTER VIEW PRODUTOS_SKU
AS SELECT catalog_product_entity.sku SKU, catalog_product_entity_varchar.value NOME
FROM catalog_product_entity_varchar
INNER JOIN  catalog_product_entity  ON catalog_product_entity.entity_id = catalog_product_entity_varchar.entity_id
where catalog_product_entity_varchar.attribute_id = 71 ORDER BY catalog_product_entity.sku;

SELECT * FROM ESTOQUE_FECHAMENTO LIMIT 50000;

SELECT * FROM ESTOQUE_FECHAMENTO where fabricante like '%galapago%';

CREATE TABLE ESTOQUE_FECHAMENTO ( SKU CHAR(60) NULL PRIMARY KEY,
                              DESCRICAO CHAR(130) NULL,
                              FABRICANTE CHAR(130) NULL,
                              EAN CHAR(30) NULL,
                              FORNECEDOR CHAR(130) NULL,
                              QUANTIDADE INT NULL,
                              30_DIAS DOUBLE NOT NULL  default 0,
                              VENDIDO INT NULL default 0,
                              CUSTO DOUBLE NOT NULL,                      
                              VALOR DOUBLE NOT NULL,
                              VALORPROMO DOUBLE NULL DEFAULT 0,
                              CATEGORIA CHAR(130) NULL,
                              fechado bit default 0,
							  dt_fechamento datetime NULL);
                              
ALTER TABLE ESTOQUE_FECHAMENTO ADD COLUMN VALORPROMO DOUBLE NULL DEFAULT 0; 

SELECT DESCRICAO, SKU, QUANTIDADE FROM ESTOQUE_FECHAMENTO where fechado = 0 order by floor(rand() * 11)  LIMIT 100;     
SELECT * FROM ESTOQUE_FECHAMENTO where fechado = 1 ;
update ESTOQUE_FECHAMENTO set fechado = 0 ;

delete from ESTOQUE_FECHAMENTO;


select * from VENDIDOS_LOJA_MES where sku LIKE 'PPGJ%' ;
select * from VENDIDOS_MARKETPLACES where sku = 'WOD002';
select * from VENDIDOS_LOJA where sku = 'WOD002';
SELECT * FROM ESTOQUE_FECHAMENTO where sku = 'WOD002';
SELECT * FROM ESTOQUE_FECHAMENTO where descricao like '%trito%';

SELECT SUM(qtde) QTDE, sku, 'MARKETPLACES' FROM VENDIDOS_MARKETPLACES_A_SEPARAR group by sku
UNION
SELECT  SUM(qtde) QTDE, sku, 'LOJA' FROM VENDIDOS_LOJA_A_SEPARAR group by sku;


