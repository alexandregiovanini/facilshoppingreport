﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FacilShoppingReports
{
    public partial class mdiPrincipal : Form
    {
        //private DBConnect myDB;
       // private int childFormNumber = 0;

        public mdiPrincipal()
        {            
            InitializeComponent();
            global.myDB = new DBConnect();
            global.myDB.OpenConnection();
            DateTime date1 = new DateTime(2022, 04, 17, 0, 0, 0); //giovanini
            DateTime date2 = DateTime.Now;           


            if (DateTime.Compare(date2, date1) > 0)
            {
                MessageBox.Show("","PontoZero Informa", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation);
                this.Close();
            }

            LOGIN frmLogin = new LOGIN();
            frmLogin.intPermissao = 1;
            frmLogin.ShowDialog(this);


            if (frmLogin.blAprovado == true)
            {
                stripUsuario.Text = frmLogin.strOperador;

                if(Recebe_Alerta(stripUsuario.Text) == true)
                {
                    timer1.Enabled = true;
                }
                else
                {
                    timer1.Enabled = false;
                }
            }
            else
            {
                this.Close();
            }
        }

        private bool Recebe_Alerta(string strLogin)
        {
            MySqlCommand cmd1;
            MySqlDataReader dataReaderRastreio;
            bool blAchou;

            string query = "SELECT * FROM USUARIOS WHERE USUARIO = '" + strLogin + "' AND ALERTA = 1";

            global.myDB.CloseConnection();
            if (global.myDB.connection.State == ConnectionState.Closed)
            {
                global.myDB.OpenConnection();
            }

            cmd1 = new MySqlCommand(query, global.myDB.connection);

            dataReaderRastreio = cmd1.ExecuteReader();

            if (dataReaderRastreio.Read())
            {
                blAchou = true;
            }
            else
            {
                blAchou = false;
            }

            dataReaderRastreio.Close();

            return blAchou;
        }

        private bool PermiteAcesso(string strTela)
        {
            MySqlCommand cmd1;
            MySqlDataReader dataReaderRastreio;
            bool blAchou;

            string query = "SELECT * FROM USUARIOS WHERE USUARIO = '" + stripUsuario.Text + "'";

            global.myDB.CloseConnection();
            if (global.myDB.connection.State == ConnectionState.Closed)
            {
                global.myDB.OpenConnection();
            }

            cmd1 = new MySqlCommand(query, global.myDB.connection);

            dataReaderRastreio = cmd1.ExecuteReader();

            if (dataReaderRastreio.Read())
            {
                blAchou = false;
                if (strTela == "ESTOQUE") 
                {
                    if (dataReaderRastreio["ESTOQUE"].ToString() == "1")
                    {
                        blAchou = true;
                    }
                }
                else if (strTela == "ENTRADA")
                {
                    if (dataReaderRastreio["ENTRADA"].ToString() == "1")
                    {
                        blAchou = true;
                    }
                }
            }
            else
            {
                blAchou = false;
            }

            dataReaderRastreio.Close();

            return blAchou;
        }

        private void toolStripButton2_Click(object sender, EventArgs e)
        {
            
            this.Close();
        }

        private void sairToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            
            this.Close();
        }

        private void toolStripButton1_Click(object sender, EventArgs e)
        {
            frmRelatorio frmRel = new frmRelatorio();
            frmRel.ShowDialog(this);
        }

        private void copyToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmRelatorio frmRel = new frmRelatorio();
            frmRel.ShowDialog(this);
        }

        private void editMenu_Click(object sender, EventArgs e)
        {

        }

        private void mdiPrincipal_Load(object sender, EventArgs e)
        {

        }

        private void newToolStripButton_Click(object sender, EventArgs e)
        {
            frmProduto frmProd = new frmProduto();
            frmProd.ShowDialog(this);
        }

        private void fornecedoresToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmFornecedor frmForn = new frmFornecedor();
            frmForn.ShowDialog(this);
        }

        private void entradaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (PermiteAcesso("ENTRADA") == true)
            {
                frmEntradaProdutos frmEntraProd = new frmEntradaProdutos();
                frmEntraProd.strOperador = stripUsuario.Text;
                frmEntraProd.ShowDialog(this);
            }
            else
            {
                MessageBox.Show("Você não tem permissão de acesso a essa tela!", "PontoZero Informa");
            }
        }

        private void envioCorreiosToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void coreiosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmRelatorioCorreio frmRelCorreios = new frmRelatorioCorreio();
            frmRelCorreios.ShowDialog(this);
        }

        private void estoqueToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (PermiteAcesso("ESTOQUE") == true)
            {
                frmEstoque frmEst = new frmEstoque();
                frmEst.ShowDialog(this);
            }
            else
            {
                MessageBox.Show("Você não tem permissão de acesso a essa tela!", "PontoZero Informa");
            }

            
        }

        private void categoriasToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void iderisToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void toolStripStatusLabel1_Click(object sender, EventArgs e)
        {

        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            //Verifica_Alertas();
        }

        private void Verifica_Alertas()
        {
            MySqlCommand cmd1;
            MySqlDataReader dataReaderRastreio;
            
            string query = "SELECT * FROM ITENS_PEDIDOS_INTEGRADOS WHERE ERRO_SKU = 1 and id > 92024";

            global.myDB.CloseConnection();
            if (global.myDB.connection.State == ConnectionState.Closed)
            {
                global.myDB.OpenConnection();
            }

            cmd1 = new MySqlCommand(query, global.myDB.connection);

            dataReaderRastreio = cmd1.ExecuteReader();

            if (dataReaderRastreio.Read())
            {
                frmAlerta frmAlert = new frmAlerta();
                frmAlert.dataReader = dataReaderRastreio;
                frmAlert.Carrega_Alerta();
                frmAlert.ShowDialog(this);
            }

            dataReaderRastreio.Close();

            
        }

        private void saídaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (PermiteAcesso("ESTOQUE") == true)
            {
                frmSaidaEstoque frmSaida = new frmSaidaEstoque();
                frmSaida.ShowDialog(this);
            }
            else
            {
                MessageBox.Show("Você não tem permissão de acesso a essa tela!", "PontoZero Informa");
            }

        }

        private void combosToolStripMenuItem_Click_1(object sender, EventArgs e)
        {
            frmCadastroCombo frmCombo = new frmCadastroCombo();
            frmCombo.strOperador = stripUsuario.Text;
            frmCombo.ShowDialog(this);

        }

        private void configuraçãoToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            LOGIN frmLogin = new LOGIN();
            frmLogin.intPermissao = 1;
            frmLogin.ShowDialog(this);


            if (frmLogin.blAprovado == true)
            {
                frmIntegracao_Ideris frmIderis = new frmIntegracao_Ideris();
                frmIderis.strOperador = frmLogin.strOperador;
                frmIderis.ShowDialog(this);
            }
        }

        private void exportaçãoViaPlanilhaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ExportacaoPlanilhaIderis frmExport = new ExportacaoPlanilhaIderis();


            frmExport.ShowDialog(this);
        }

        private void enviosDeCorreioToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmEnvio_Correios frmEnvio = new frmEnvio_Correios();

            frmEnvio.ShowDialog(this);
        }

        private void conferênciaDeSeparaçãoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmConfSeparacao frmConfSep = new frmConfSeparacao();

            frmConfSep.ShowDialog(this);
        }

        private void etiquetasNFToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmEtiquetaNF frmEtNF = new frmEtiquetaNF();

            frmEtNF.ShowDialog(this);
        }

        private void aberturaDeLoteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmAberturaLote frmAbreLote = new frmAberturaLote();

            frmAbreLote.ShowDialog(this);
        }

        private void fechamentoDeLoteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmFechamentoLote frmFechaLote = new frmFechamentoLote();

            frmFechaLote.ShowDialog(this);
        }

        private void consultaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmGerarInventario frmGerarInv = new frmGerarInventario();

            frmGerarInv.ShowDialog(this);
        }

        private void statusPedidoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmStatusPedido frmStatusPed = new frmStatusPedido();

            frmStatusPed.ShowDialog(this);
        }
    }
}
