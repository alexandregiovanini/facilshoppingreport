﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FacilShoppingReports
{
    public partial class frmStatusPedido : Form
    {
        MySqlDataReader dataReader;
        string strCertificado;

        public frmStatusPedido()
        {
            InitializeComponent();
        }

        private void cmdPesquisar_Click(object sender, EventArgs e)
        {
            if (txtPedido.TextLength > 0)
            {
                txtResultado.Text = "";
                Processa();
            }
        }

        private void Processa()
        {
            MySqlCommand cmd;
            String strPedido;

            string query = "SELECT * FROM PEDIDOS_INTEGRADOS ";

            if (optIderis.Checked)
            {
                query = query + " where id_pedido = " + txtPedido.Text + "; ";
            }
            else
            {
                query = query + " where cod_pedido = '" + txtPedido.Text + "'; ";
            }



            if (global.myDB.connection.State == ConnectionState.Closed)
            {
                global.myDB.OpenConnection();
            }

            cmd = new MySqlCommand(query, global.myDB.connection);                       

            try
            {
                dataReader = cmd.ExecuteReader();
            }
            catch (Exception)
            {
                global.myDB.OpenConnection();
                cmd = new MySqlCommand(query, global.myDB.connection);
                dataReader = cmd.ExecuteReader();
            }

            if (dataReader.Read())
            {
                strPedido = dataReader["id_pedido"].ToString();

                txtResultado.Text = txtResultado.Text + "ID  :  " + dataReader["ID"].ToString() + Environment.NewLine;
                txtResultado.Text = txtResultado.Text + "DATA DO PEDIDO  :  " + dataReader["dt_pedido"].ToString() + Environment.NewLine;
                txtResultado.Text = txtResultado.Text + "DATA INTEGRAÇÃO :  " + dataReader["dt_integracao"].ToString() + Environment.NewLine;
                txtResultado.Text = txtResultado.Text + "DATA SEPARAÇÃO:  " + dataReader["data_separado"].ToString() + Environment.NewLine;
                

                txtResultado.Text = txtResultado.Text + "PED IDERIS :  " + dataReader["id_pedido"].ToString().ToUpper() + " PED MARKETPLACE :  " + dataReader["cod_pedido"].ToString().ToUpper() + Environment.NewLine;
                
                
                txtResultado.Text = txtResultado.Text + "TOTAL           :  " + dataReader["vl_total"].ToString().ToUpper() + Environment.NewLine;
                txtResultado.Text = txtResultado.Text + "STATUS          :  " + dataReader["status"].ToString().ToUpper() + Environment.NewLine;
                
                txtResultado.Text = txtResultado.Text + "--------------------------------------------------------------------" + Environment.NewLine;
                txtResultado.Text = txtResultado.Text + "NOME :        " + dataReader["nome"].ToString().ToUpper() + Environment.NewLine;
                txtResultado.Text = txtResultado.Text + "CPF :         " + dataReader["cpf"].ToString().ToUpper() + Environment.NewLine;
                txtResultado.Text = txtResultado.Text + "ENDERECO :    " + dataReader["endereco"].ToString().ToUpper() + " - " + dataReader["numero"].ToString().ToUpper();
                txtResultado.Text = txtResultado.Text + "COMPLEMENTO : " + dataReader["complemento"].ToString().ToUpper() + Environment.NewLine;
                txtResultado.Text = txtResultado.Text + "BAIRRO :  " + dataReader["bairro"].ToString().ToUpper() + " CEP: " + dataReader["cep"].ToString().ToUpper();
                txtResultado.Text = txtResultado.Text + "CIDADE :  " + dataReader["cidade"].ToString().ToUpper() + " UF: " + dataReader["UF"].ToString().ToUpper() + Environment.NewLine;
                txtResultado.Text = txtResultado.Text + "--------------------------------------------------------------------" + Environment.NewLine;
                txtResultado.Text = txtResultado.Text + "RASTREIO    :  " + dataReader["cd_rastreio"].ToString().ToUpper() + " PAGAMENTO: " + dataReader["pagamento"].ToString().ToUpper();
                txtResultado.Text = txtResultado.Text + " MARKETPLACE :  " + dataReader["marketplace"].ToString().ToUpper() + Environment.NewLine;
                txtResultado.Text = txtResultado.Text + "--------------------------------------------------------------------" + Environment.NewLine;
                txtResultado.Text = txtResultado.Text + "NF         :  " + dataReader["nf"].ToString().ToUpper() + " SERIE: " + dataReader["serie"].ToString();
                txtResultado.Text = txtResultado.Text + "STATUS NFe :  " + dataReader["statusnf"].ToString().ToUpper() + " DT EMISSÃO: " + dataReader["dt_emissao"].ToString() + Environment.NewLine;
                txtResultado.Text = txtResultado.Text + "CHAVE NFe  :  " + dataReader["chave"].ToString().ToUpper() + Environment.NewLine;

                dataReader.Close();

                query = "SELECT * FROM ITENS_PEDIDOS_INTEGRADOS WHERE id_pedido =  " + strPedido;


                if (global.myDB.connection.State == ConnectionState.Closed)
                {
                    global.myDB.OpenConnection();
                }

                cmd = new MySqlCommand(query, global.myDB.connection);

                try
                {
                    dataReader = cmd.ExecuteReader();
                }
                catch (Exception)
                {
                    global.myDB.OpenConnection();
                    cmd = new MySqlCommand(query, global.myDB.connection);
                    dataReader = cmd.ExecuteReader();
                }

                txtResultado.Text = txtResultado.Text + "----------------------- ITENS -----------------------------" + Environment.NewLine;
                while (dataReader.Read())
                {
                    txtResultado.Text = txtResultado.Text + " SKU             :  " + dataReader["sku"].ToString().ToUpper() + " DESCR : " + dataReader["nm_produto"].ToString().ToUpper() + Environment.NewLine;
                    txtResultado.Text = txtResultado.Text + " QTDE            :  " + dataReader["qtde"].ToString() + Environment.NewLine;
                    txtResultado.Text = txtResultado.Text + " VALOR UNITARIO  :  " + dataReader["vl_unitario"].ToString() + " TOTAL DO ITEM : " + dataReader["vl_total"].ToString() + Environment.NewLine;
                    txtResultado.Text = txtResultado.Text + "--------------------------------------------------------------------" + Environment.NewLine;
                }

                dataReader.Close();



                query = "select ITENS_ENVIO_CORREIOS.*, ENVIO_CORREIOS.DATA_IMPRESSAO  from ITENS_ENVIO_CORREIOS ";
                query = query + " INNER JOIN ENVIO_CORREIOS ";
                query = query + " ON ENVIO_CORREIOS.ID = ITENS_ENVIO_CORREIOS.id_ENVIO_CORREIOS ";
                query = query + " where increment_id = " + strPedido;


                if (global.myDB.connection.State == ConnectionState.Closed)
                {
                    global.myDB.OpenConnection();
                }

                cmd = new MySqlCommand(query, global.myDB.connection);

                try
                {
                    dataReader = cmd.ExecuteReader();
                }
                catch (Exception)
                {
                    global.myDB.OpenConnection();
                    cmd = new MySqlCommand(query, global.myDB.connection);
                    dataReader = cmd.ExecuteReader();
                }

                txtResultado.Text = txtResultado.Text + "----------------------- ENVIO -----------------------------" + Environment.NewLine;
                while (dataReader.Read()) 
                {
                    txtResultado.Text = txtResultado.Text + " DATA LEITURA :  " + dataReader["data"].ToString().ToUpper() + Environment.NewLine;
                    txtResultado.Text = txtResultado.Text + " DATA IMPRESSAO :  " + dataReader["DATA_IMPRESSAO"].ToString().ToUpper() + Environment.NewLine;                    
                    txtResultado.Text = txtResultado.Text + " LOTE :  " + dataReader["id_ENVIO_CORREIOS"].ToString().ToUpper() + Environment.NewLine;
                    txtResultado.Text = txtResultado.Text + " RASTREIO :  " + dataReader["rastreio"].ToString().ToUpper() + Environment.NewLine;
                    txtResultado.Text = txtResultado.Text + " SEDEX :  " + dataReader["sedex"].ToString().ToUpper() + Environment.NewLine;
                    txtResultado.Text = txtResultado.Text + " PLATAFORMA :  " + dataReader["plataforma"].ToString().ToUpper() + Environment.NewLine;
                }

                dataReader.Close();
            }
            else
            {
                txtResultado.Text = txtResultado.Text + " PEDIDO NÃO SE ENCONTRA NO SISTEMA... " + Environment.NewLine;

                

                CarregaCertificado();

                txtResultado.Text = txtResultado.Text + " AGUARDE PESQUISANDO NO IDERIS... " + Environment.NewLine;

                global.ConsultarPedidoIderis(txtPedido.Text, optIderis.Checked, strCertificado);

                txtResultado.Text = txtResultado.Text + " FIM DO PROCESSO, PODE CONSULTAR NOVAMENTE ... " + Environment.NewLine;
            }

            

        }

        private void CarregaCertificado()
        {
            MySqlCommand cmd1;
            MySqlDataReader dataPedido;

            string query = "select * from IDERIS_TOKEN;";

            global.myDB.CloseConnection();

            if (global.myDB.connection.State == ConnectionState.Closed)
            {
                global.myDB.OpenConnection();
            }

            cmd1 = new MySqlCommand(query, global.myDB.connection);

            dataPedido = cmd1.ExecuteReader();

            if (dataPedido.Read())
            {
                strCertificado = dataPedido["TOKEN_PROVISORIO"].ToString();
            }

            dataPedido.Close();

            if (global.myDB.connection.State != ConnectionState.Closed)
            {
                global.myDB.CloseConnection();
            }
        }

        private void txtResultado_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
