﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

namespace FacilShoppingReports
{
    public partial class frmFechamentoLote : Form
    {
        MySqlDataReader dataReader;
        MySqlCommand cmd;

        public frmFechamentoLote()
        {
            InitializeComponent();
        }

        private void frmFechamentoLote_Load(object sender, EventArgs e)
        {
            CarregaFechados();
        }

        private void CarregaFechados()
        {

            btnMercadoEnvios.Enabled = false;
            btnB2W.Enabled = false;
            btnMagalu.Enabled = false;
            btnMercadoPlace.Enabled = false;
            btnTotalExpress.Enabled = false;
            btnSequoia.Enabled = false;
            btnCorreios.Enabled = false;
            btnAmazon.Enabled = false;


            string query = "SELECT * ";
            query = query + " FROM ENVIO_CORREIOS ENVIO ";
            query = query + "  WHERE ENVIO.DATA_IMPRESSAO IS NULL;";

            if (global.myDB.connection.State == ConnectionState.Closed)
            {
                global.myDB.OpenConnection();
            }

            cmd = new MySqlCommand(query, global.myDB.connection);

            try
            {
                dataReader = cmd.ExecuteReader();
            }
            catch (Exception)
            {
                // dataReader.Close();
                global.myDB.OpenConnection();
                cmd = new MySqlCommand(query, global.myDB.connection);
                dataReader = cmd.ExecuteReader();
            }

            while (dataReader.Read())
            {

                if (dataReader["mercadolivre"].ToString() == "1")
                {
                    btnMercadoEnvios.Enabled = true;
                    btnMercadoEnvios.Tag = dataReader["ID"].ToString();
                }
                else if (dataReader["b2w"].ToString() == "1")
                {
                    btnB2W.Enabled = true;
                    btnB2W.Tag = dataReader["ID"].ToString();
                }
                else if (dataReader["MAGALU"].ToString() == "1")
                {
                    btnMagalu.Enabled = true;
                    btnMagalu.Tag = dataReader["ID"].ToString();
                }
                else if (dataReader["mlplace"].ToString() == "1")
                {
                    btnMercadoPlace.Enabled = true;
                    btnMercadoPlace.Tag = dataReader["ID"].ToString();
                }
                else if (dataReader["totalexpress"].ToString() == "1")
                {
                    btnTotalExpress.Enabled = true;
                    btnTotalExpress.Tag = dataReader["ID"].ToString();
                }
                else if (dataReader["sequoia"].ToString() == "1")
                {
                    btnSequoia.Enabled = true;
                    btnSequoia.Tag = dataReader["ID"].ToString();
                }
                else if (dataReader["amazon"].ToString() == "1")
                { 
                    btnAmazon.Enabled = true;
                    btnAmazon.Tag = dataReader["ID"].ToString();
                }
                else
                {
                    btnCorreios.Enabled = true;
                    btnCorreios.Tag = dataReader["ID"].ToString();
                }
            }

            dataReader.Close();

            if (global.myDB.connection.State != ConnectionState.Closed)
            {
                global.myDB.CloseConnection();
            }
        }

        private void AtualizaEnvio(string strEnvio, string strNomeLote)
        {
            String query = "UPDATE ENVIO_CORREIOS SET data_impressao = '" + DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss") + "' WHERE ID = " + strEnvio + "; ";
            
            if (MessageBox.Show("Confirma o Fechamento do lote para " + strNomeLote + "?", "PontoZero Informa", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1) == DialogResult.Yes)
            {

                global.myDB.CloseConnection();

                if (global.myDB.connection.State == ConnectionState.Closed)
                {
                    global.myDB.OpenConnection();
                }

                MySqlCommand cmd1 = new MySqlCommand(query, global.myDB.connection);

                try
                {
                    cmd1.ExecuteNonQuery();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, "PontoZero Informa");
                }

                if (global.myDB.connection.State != ConnectionState.Closed)
                {
                    global.myDB.CloseConnection();
                }

                CarregaFechados();

                FechaStatus(strEnvio);


            }
        }

        private void FechaStatus(string strEnvio)
        {
            MySqlDataReader dataReaderFecha;
            MySqlCommand cmdFecha;
            int i = 0;
            string[] strIncrement_id;

            string query = "select * from ITENS_ENVIO_CORREIOS where id_ENVIO_CORREIOS = " + strEnvio +  " order by data;";

            if (global.myDB.connection.State == ConnectionState.Closed)
            {
                global.myDB.OpenConnection();
            }

            cmdFecha = new MySqlCommand(query, global.myDB.connection);

            try
            {
                dataReaderFecha = cmdFecha.ExecuteReader();
            }
            catch (Exception)
            {
                // dataReader.Close();
                global.myDB.OpenConnection();
                cmdFecha = new MySqlCommand(query, global.myDB.connection);
                dataReaderFecha = cmdFecha.ExecuteReader();
            }

            strIncrement_id = new string[999];

            while (dataReaderFecha.Read())
            {
                strIncrement_id[i] = dataReaderFecha["increment_id"].ToString();
                i = i + 1;                              
            }

            dataReaderFecha.Close();

            for (int t = 0; t < i; t++)
            {
                global.AtualizaStatus("Enviado", strIncrement_id[t]);
            }
                

        }
        private void btnCorreios_Click(object sender, EventArgs e)
        {
            AtualizaEnvio(btnCorreios.Tag.ToString(), "Correios");
        }

        private void btnB2W_Click(object sender, EventArgs e)
        {
            AtualizaEnvio(btnB2W.Tag.ToString(), "B2W");
        }

        private void btnMagalu_Click(object sender, EventArgs e)
        {
            AtualizaEnvio(btnMagalu.Tag.ToString(), "MAGALU");
        }

        private void btnMercadoEnvios_Click(object sender, EventArgs e)
        {
            AtualizaEnvio(btnMercadoEnvios.Tag.ToString(), "MERCADO ENVIOS");
        }

        private void btnMercadoPlace_Click(object sender, EventArgs e)
        {
            AtualizaEnvio(btnMercadoPlace.Tag.ToString(), "MERCADO PLACE");
        }

        private void btnTotalExpress_Click(object sender, EventArgs e)
        {
            AtualizaEnvio(btnTotalExpress.Tag.ToString(), "TOTAL EXPRESS");
        }

        private void btnSequoia_Click(object sender, EventArgs e)
        {
            AtualizaEnvio(btnSequoia.Tag.ToString(), "SEQUOIA");
        }

        private void btnAmazon_Click(object sender, EventArgs e)
        {
            AtualizaEnvio(btnAmazon.Tag.ToString(), "AMAZON");
        }

        private void btnAmazon_Click_1(object sender, EventArgs e)
        {
            AtualizaEnvio(btnAmazon.Tag.ToString(), "AMAZON");
        }
    }
}
