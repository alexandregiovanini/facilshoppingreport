
insert into catalog_category_entity (entity_id, entity_type_id, attribute_set_id, parent_id, created_at, updated_at, path, position, level, children_count) value
(64, 3,3,63,now(),now(),'1/2/63/64',3, 2,0);

select * from catalog_category_entity;
/**********************************************/

select * from catalog_category_entity_varchar limit 5000;

insert into catalog_category_entity_varchar (value_id, entity_type_id, attribute_id, store_id, entity_id, value) value 
( 654,	3,	41,	0,	64,	'Escolar');

insert into catalog_category_entity_varchar (value_id, entity_type_id, attribute_id, store_id, entity_id, value) value 
( 655,	3,	43,	0,	64,	'Escolar');

insert into catalog_category_entity_varchar (value_id, entity_type_id, attribute_id, store_id, entity_id) value 
( 656,	3,	46,	0,	64);	

insert into catalog_category_entity_varchar (value_id, entity_type_id, attribute_id, store_id, entity_id, value) value 
( 657,	3,	49,	0,	64,	'PRODUCTS' );

insert into catalog_category_entity_varchar (value_id, entity_type_id, attribute_id, store_id, entity_id) value 
( 658,	3,	58,	0,	64);	

insert into catalog_category_entity_varchar (value_id, entity_type_id, attribute_id, store_id, entity_id) value 
( 659,	3,	61,	0,	64);	

insert into catalog_category_entity_varchar (value_id, entity_type_id, attribute_id, store_id, entity_id, value) value 
( 660,	3,	57,	1,	64,	'Escolar.html');

insert into catalog_category_entity_varchar (value_id, entity_type_id, attribute_id, store_id, entity_id, value) value 
( 661,	3,	57,	0,	64,	'Escolar.html');

/**********************************************************************************/

select * from catalog_category_entity_datetime limit 5000;
insert into catalog_category_entity_datetime(value_id, entity_type_id, attribute_id, store_id, entity_id) value
(244,3,59,0,64);

insert into catalog_category_entity_datetime(value_id, entity_type_id, attribute_id, store_id, entity_id) value
(245,3,60,0,64);

/**********************************************************************************/

select * from catalog_category_entity_decimal limit 5000;
insert into catalog_category_entity_decimal (value_id, entity_type_id, attribute_id, store_id, entity_id) value
(62,3,70,0,64);

/**********************************************************************************/

select * from catalog_category_entity_int limit 5000;

insert into catalog_category_entity_int (value_id, entity_type_id, attribute_id, store_id, entity_id, value) value
(656,	3,	42,	0,	64,	1);

insert into catalog_category_entity_int (value_id, entity_type_id, attribute_id, store_id, entity_id, value) value
(657,	3,	67,	0,	64,	1);

insert into catalog_category_entity_int (value_id, entity_type_id, attribute_id, store_id, entity_id) value
(658,	3,	50,	0,	64);	

insert into catalog_category_entity_int (value_id, entity_type_id, attribute_id, store_id, entity_id, value) value
(659,	3,	51,	0,	64,	0);

insert into catalog_category_entity_int (value_id, entity_type_id, attribute_id, store_id, entity_id, value) value
(660,	3,	68,	0,	64,	0);

insert into catalog_category_entity_int (value_id, entity_type_id, attribute_id, store_id, entity_id, value) value
(661,	3,	69,	0,	64,	0);
					
insert into catalog_category_entity_int (value_id, entity_type_id, attribute_id, store_id, entity_id) value
(662, 3, 44, 0, 64);

insert into catalog_category_entity_int (value_id, entity_type_id, attribute_id, store_id, entity_id) value
(663, 3, 47, 0, 64);

insert into catalog_category_entity_int (value_id, entity_type_id, attribute_id, store_id, entity_id) value
(664, 3, 48, 0, 64);

insert into catalog_category_entity_int (value_id, entity_type_id, attribute_id, store_id, entity_id) value
(665, 3, 62, 0, 64);

insert into catalog_category_entity_int (value_id, entity_type_id, attribute_id, store_id, entity_id) value
(666, 3, 65, 0, 64);


