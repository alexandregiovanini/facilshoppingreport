﻿namespace FacilShoppingReports
{
    partial class frmEntrada
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tabPrincipal = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.lstLista = new System.Windows.Forms.ListView();
            this.txtPesquisa = new System.Windows.Forms.TextBox();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.btnExcluir = new System.Windows.Forms.Button();
            this.btnAdicionar = new System.Windows.Forms.Button();
            this.lstItens = new System.Windows.Forms.ListView();
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader17 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader3 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader4 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader5 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader6 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader7 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader8 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader9 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader10 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader11 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader13 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader14 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader15 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader16 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.listView1 = new System.Windows.Forms.ListView();
            this.txtObservacao = new System.Windows.Forms.TextBox();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.txtValorNF = new System.Windows.Forms.TextBox();
            this.label21 = new System.Windows.Forms.Label();
            this.txtValorIPI = new System.Windows.Forms.TextBox();
            this.label20 = new System.Windows.Forms.Label();
            this.txtOutrasDespesas = new System.Windows.Forms.TextBox();
            this.label19 = new System.Windows.Forms.Label();
            this.txtDesconto = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.txtValorSeguro = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.txtValorFrete = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.txtValorProdutos = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.txtValorICMSSubst = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.txtBaseICMSSubst = new System.Windows.Forms.TextBox();
            this.txtValorICMS = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.txtBaseICMS = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.mskDataEntrega = new System.Windows.Forms.MaskedTextBox();
            this.label666 = new System.Windows.Forms.Label();
            this.mskDataVencimento = new System.Windows.Forms.MaskedTextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.mskDataSaida = new System.Windows.Forms.MaskedTextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.mskDataEmissao = new System.Windows.Forms.MaskedTextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.mskData = new System.Windows.Forms.MaskedTextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.optSaida = new System.Windows.Forms.RadioButton();
            this.optEntrada = new System.Windows.Forms.RadioButton();
            this.cbNatureza = new System.Windows.Forms.ComboBox();
            this.label11 = new System.Windows.Forms.Label();
            this.txtSerie = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtNF = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.cbFornecedor = new System.Windows.Forms.ComboBox();
            this.txtID = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.btnAlterar = new System.Windows.Forms.Button();
            this.btnGrava = new System.Windows.Forms.Button();
            this.btnSair2 = new System.Windows.Forms.Button();
            this.btPesquisar = new System.Windows.Forms.Button();
            this.btnNovo = new System.Windows.Forms.Button();
            this.btnSair = new System.Windows.Forms.Button();
            this.tabPrincipal.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.groupBox6.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabPrincipal
            // 
            this.tabPrincipal.Controls.Add(this.tabPage1);
            this.tabPrincipal.Controls.Add(this.tabPage2);
            this.tabPrincipal.Location = new System.Drawing.Point(12, 12);
            this.tabPrincipal.Name = "tabPrincipal";
            this.tabPrincipal.SelectedIndex = 0;
            this.tabPrincipal.Size = new System.Drawing.Size(916, 490);
            this.tabPrincipal.TabIndex = 1;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.btnSair2);
            this.tabPage1.Controls.Add(this.lstLista);
            this.tabPage1.Controls.Add(this.txtPesquisa);
            this.tabPage1.Controls.Add(this.btPesquisar);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(908, 464);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Procurar";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // lstLista
            // 
            this.lstLista.FullRowSelect = true;
            this.lstLista.GridLines = true;
            this.lstLista.Location = new System.Drawing.Point(10, 49);
            this.lstLista.Name = "lstLista";
            this.lstLista.Size = new System.Drawing.Size(890, 339);
            this.lstLista.TabIndex = 18;
            this.lstLista.UseCompatibleStateImageBehavior = false;
            this.lstLista.View = System.Windows.Forms.View.Details;
            this.lstLista.SelectedIndexChanged += new System.EventHandler(this.lstLista_SelectedIndexChanged);
            this.lstLista.DoubleClick += new System.EventHandler(this.lstLista_DoubleClick);
            // 
            // txtPesquisa
            // 
            this.txtPesquisa.Location = new System.Drawing.Point(444, 16);
            this.txtPesquisa.Name = "txtPesquisa";
            this.txtPesquisa.Size = new System.Drawing.Size(267, 20);
            this.txtPesquisa.TabIndex = 17;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.groupBox6);
            this.tabPage2.Controls.Add(this.groupBox5);
            this.tabPage2.Controls.Add(this.groupBox4);
            this.tabPage2.Controls.Add(this.groupBox1);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(908, 464);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Nota";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.btnExcluir);
            this.groupBox6.Controls.Add(this.btnAdicionar);
            this.groupBox6.Controls.Add(this.lstItens);
            this.groupBox6.Enabled = false;
            this.groupBox6.Location = new System.Drawing.Point(7, 277);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(892, 181);
            this.groupBox6.TabIndex = 23;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "Itens";
            // 
            // btnExcluir
            // 
            this.btnExcluir.Image = global::FacilShoppingReports.Properties.Resources.delete_icon;
            this.btnExcluir.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnExcluir.Location = new System.Drawing.Point(813, 59);
            this.btnExcluir.Name = "btnExcluir";
            this.btnExcluir.Size = new System.Drawing.Size(75, 44);
            this.btnExcluir.TabIndex = 21;
            this.btnExcluir.Text = "Excluir";
            this.btnExcluir.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnExcluir.UseVisualStyleBackColor = true;
            // 
            // btnAdicionar
            // 
            this.btnAdicionar.Image = global::FacilShoppingReports.Properties.Resources.new_icon;
            this.btnAdicionar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnAdicionar.Location = new System.Drawing.Point(813, 14);
            this.btnAdicionar.Name = "btnAdicionar";
            this.btnAdicionar.Size = new System.Drawing.Size(75, 44);
            this.btnAdicionar.TabIndex = 20;
            this.btnAdicionar.Text = "Incluir";
            this.btnAdicionar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnAdicionar.UseVisualStyleBackColor = true;
            this.btnAdicionar.Click += new System.EventHandler(this.btnAdicionar_Click);
            // 
            // lstItens
            // 
            this.lstItens.CheckBoxes = true;
            this.lstItens.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1,
            this.columnHeader17,
            this.columnHeader2,
            this.columnHeader3,
            this.columnHeader4,
            this.columnHeader5,
            this.columnHeader6,
            this.columnHeader7,
            this.columnHeader8,
            this.columnHeader9,
            this.columnHeader10,
            this.columnHeader11,
            this.columnHeader13,
            this.columnHeader14,
            this.columnHeader15,
            this.columnHeader16});
            this.lstItens.FullRowSelect = true;
            this.lstItens.GridLines = true;
            this.lstItens.Location = new System.Drawing.Point(6, 15);
            this.lstItens.Name = "lstItens";
            this.lstItens.Size = new System.Drawing.Size(805, 160);
            this.lstItens.TabIndex = 19;
            this.lstItens.UseCompatibleStateImageBehavior = false;
            this.lstItens.View = System.Windows.Forms.View.Details;
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "Item";
            // 
            // columnHeader17
            // 
            this.columnHeader17.Text = "Cod Fabr.";
            // 
            // columnHeader2
            // 
            this.columnHeader2.Text = "Código";
            // 
            // columnHeader3
            // 
            this.columnHeader3.Text = "Produto";
            // 
            // columnHeader4
            // 
            this.columnHeader4.Text = "NCM";
            // 
            // columnHeader5
            // 
            this.columnHeader5.Text = "SOSN";
            // 
            // columnHeader6
            // 
            this.columnHeader6.Text = "CFOP";
            // 
            // columnHeader7
            // 
            this.columnHeader7.Text = "Unidade";
            // 
            // columnHeader8
            // 
            this.columnHeader8.Text = "Quantidade";
            // 
            // columnHeader9
            // 
            this.columnHeader9.Text = "Valor Unitário";
            // 
            // columnHeader10
            // 
            this.columnHeader10.Text = "Valor Desconto";
            // 
            // columnHeader11
            // 
            this.columnHeader11.Text = "Valor Líquido";
            // 
            // columnHeader13
            // 
            this.columnHeader13.Text = "Valor ICMS";
            // 
            // columnHeader14
            // 
            this.columnHeader14.Text = "Valor IPI";
            // 
            // columnHeader15
            // 
            this.columnHeader15.Text = "Aliquota ICMS";
            // 
            // columnHeader16
            // 
            this.columnHeader16.Text = "Aliquota IPI";
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.listView1);
            this.groupBox5.Controls.Add(this.txtObservacao);
            this.groupBox5.Location = new System.Drawing.Point(6, 215);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(893, 52);
            this.groupBox5.TabIndex = 22;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Observação";
            // 
            // listView1
            // 
            this.listView1.FullRowSelect = true;
            this.listView1.GridLines = true;
            this.listView1.Location = new System.Drawing.Point(5, -105);
            this.listView1.Name = "listView1";
            this.listView1.Size = new System.Drawing.Size(800, 96);
            this.listView1.TabIndex = 19;
            this.listView1.UseCompatibleStateImageBehavior = false;
            this.listView1.View = System.Windows.Forms.View.Details;
            // 
            // txtObservacao
            // 
            this.txtObservacao.Location = new System.Drawing.Point(6, 13);
            this.txtObservacao.Multiline = true;
            this.txtObservacao.Name = "txtObservacao";
            this.txtObservacao.Size = new System.Drawing.Size(881, 32);
            this.txtObservacao.TabIndex = 0;
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.txtValorNF);
            this.groupBox4.Controls.Add(this.label21);
            this.groupBox4.Controls.Add(this.txtValorIPI);
            this.groupBox4.Controls.Add(this.label20);
            this.groupBox4.Controls.Add(this.txtOutrasDespesas);
            this.groupBox4.Controls.Add(this.label19);
            this.groupBox4.Controls.Add(this.txtDesconto);
            this.groupBox4.Controls.Add(this.label18);
            this.groupBox4.Controls.Add(this.txtValorSeguro);
            this.groupBox4.Controls.Add(this.label17);
            this.groupBox4.Controls.Add(this.txtValorFrete);
            this.groupBox4.Controls.Add(this.label16);
            this.groupBox4.Controls.Add(this.txtValorProdutos);
            this.groupBox4.Controls.Add(this.label15);
            this.groupBox4.Controls.Add(this.txtValorICMSSubst);
            this.groupBox4.Controls.Add(this.label14);
            this.groupBox4.Controls.Add(this.txtBaseICMSSubst);
            this.groupBox4.Controls.Add(this.txtValorICMS);
            this.groupBox4.Controls.Add(this.label13);
            this.groupBox4.Controls.Add(this.label12);
            this.groupBox4.Controls.Add(this.txtBaseICMS);
            this.groupBox4.Controls.Add(this.label10);
            this.groupBox4.Enabled = false;
            this.groupBox4.Location = new System.Drawing.Point(6, 115);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(894, 98);
            this.groupBox4.TabIndex = 21;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Tributação";
            // 
            // txtValorNF
            // 
            this.txtValorNF.Location = new System.Drawing.Point(779, 58);
            this.txtValorNF.Name = "txtValorNF";
            this.txtValorNF.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtValorNF.Size = new System.Drawing.Size(94, 20);
            this.txtValorNF.TabIndex = 56;
            this.txtValorNF.Text = "999,99";
            this.txtValorNF.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtValorNF_KeyPress);
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(727, 61);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(48, 13);
            this.label21.TabIndex = 55;
            this.label21.Text = "Total NF";
            // 
            // txtValorIPI
            // 
            this.txtValorIPI.Location = new System.Drawing.Point(651, 58);
            this.txtValorIPI.Name = "txtValorIPI";
            this.txtValorIPI.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtValorIPI.Size = new System.Drawing.Size(72, 20);
            this.txtValorIPI.TabIndex = 54;
            this.txtValorIPI.Text = "999,99";
            this.txtValorIPI.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtValorIPI_KeyPress);
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(603, 61);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(47, 13);
            this.label20.TabIndex = 53;
            this.label20.Text = "Valor IPI";
            // 
            // txtOutrasDespesas
            // 
            this.txtOutrasDespesas.Location = new System.Drawing.Point(525, 58);
            this.txtOutrasDespesas.Name = "txtOutrasDespesas";
            this.txtOutrasDespesas.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtOutrasDespesas.Size = new System.Drawing.Size(72, 20);
            this.txtOutrasDespesas.TabIndex = 52;
            this.txtOutrasDespesas.Text = "999,99";
            this.txtOutrasDespesas.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtOutrasDespesas_KeyPress);
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(437, 61);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(88, 13);
            this.label19.TabIndex = 51;
            this.label19.Text = "Outras Despesas";
            // 
            // txtDesconto
            // 
            this.txtDesconto.Location = new System.Drawing.Point(357, 58);
            this.txtDesconto.Name = "txtDesconto";
            this.txtDesconto.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtDesconto.Size = new System.Drawing.Size(72, 20);
            this.txtDesconto.TabIndex = 50;
            this.txtDesconto.Text = "999,99";
            this.txtDesconto.TextChanged += new System.EventHandler(this.txtDesconto_TextChanged);
            this.txtDesconto.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtDesconto_KeyPress);
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(306, 61);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(53, 13);
            this.label18.TabIndex = 49;
            this.label18.Text = "Desconto";
            // 
            // txtValorSeguro
            // 
            this.txtValorSeguro.Location = new System.Drawing.Point(231, 58);
            this.txtValorSeguro.Name = "txtValorSeguro";
            this.txtValorSeguro.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtValorSeguro.Size = new System.Drawing.Size(65, 20);
            this.txtValorSeguro.TabIndex = 48;
            this.txtValorSeguro.Text = "999,99";
            this.txtValorSeguro.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtValorSeguro_KeyPress);
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(166, 61);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(68, 13);
            this.label17.TabIndex = 47;
            this.label17.Text = "Valor Seguro";
            // 
            // txtValorFrete
            // 
            this.txtValorFrete.Location = new System.Drawing.Point(82, 58);
            this.txtValorFrete.Name = "txtValorFrete";
            this.txtValorFrete.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtValorFrete.Size = new System.Drawing.Size(72, 20);
            this.txtValorFrete.TabIndex = 46;
            this.txtValorFrete.Text = "999,99";
            this.txtValorFrete.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtValorFrete_KeyPress);
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(21, 61);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(58, 13);
            this.label16.TabIndex = 45;
            this.label16.Text = "Valor Frete";
            // 
            // txtValorProdutos
            // 
            this.txtValorProdutos.Location = new System.Drawing.Point(785, 22);
            this.txtValorProdutos.Name = "txtValorProdutos";
            this.txtValorProdutos.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtValorProdutos.Size = new System.Drawing.Size(88, 20);
            this.txtValorProdutos.TabIndex = 44;
            this.txtValorProdutos.Text = "999,99";
            this.txtValorProdutos.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtValorProdutos_KeyPress);
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(690, 25);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(96, 13);
            this.label15.TabIndex = 43;
            this.label15.Text = "Valor dos Produtos";
            // 
            // txtValorICMSSubst
            // 
            this.txtValorICMSSubst.Location = new System.Drawing.Point(606, 22);
            this.txtValorICMSSubst.Name = "txtValorICMSSubst";
            this.txtValorICMSSubst.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtValorICMSSubst.Size = new System.Drawing.Size(66, 20);
            this.txtValorICMSSubst.TabIndex = 42;
            this.txtValorICMSSubst.Text = "999,99";
            this.txtValorICMSSubst.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtValorICMSSubst_KeyPress);
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(519, 25);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(90, 13);
            this.label14.TabIndex = 41;
            this.label14.Text = "Valor ICMS Subst";
            // 
            // txtBaseICMSSubst
            // 
            this.txtBaseICMSSubst.Location = new System.Drawing.Point(419, 22);
            this.txtBaseICMSSubst.Name = "txtBaseICMSSubst";
            this.txtBaseICMSSubst.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtBaseICMSSubst.Size = new System.Drawing.Size(74, 20);
            this.txtBaseICMSSubst.TabIndex = 40;
            this.txtBaseICMSSubst.Text = "999,99";
            this.txtBaseICMSSubst.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtBaseICMSSubst_KeyPress);
            // 
            // txtValorICMS
            // 
            this.txtValorICMS.Location = new System.Drawing.Point(239, 22);
            this.txtValorICMS.Name = "txtValorICMS";
            this.txtValorICMS.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtValorICMS.Size = new System.Drawing.Size(65, 20);
            this.txtValorICMS.TabIndex = 39;
            this.txtValorICMS.Text = "999,99";
            this.txtValorICMS.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtValorICMS_KeyPress);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(315, 25);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(105, 13);
            this.label13.TabIndex = 38;
            this.label13.Text = "Base de ICMS Subst";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(166, 25);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(75, 13);
            this.label12.TabIndex = 36;
            this.label12.Text = "Valor de ICMS";
            // 
            // txtBaseICMS
            // 
            this.txtBaseICMS.Location = new System.Drawing.Point(82, 22);
            this.txtBaseICMS.Name = "txtBaseICMS";
            this.txtBaseICMS.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtBaseICMS.Size = new System.Drawing.Size(72, 20);
            this.txtBaseICMS.TabIndex = 35;
            this.txtBaseICMS.Text = "999,99";
            this.txtBaseICMS.TextChanged += new System.EventHandler(this.txtBaseICMS_TextChanged);
            this.txtBaseICMS.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtBaseICMS_KeyPress);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(4, 25);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(75, 13);
            this.label10.TabIndex = 33;
            this.label10.Text = "Base de ICMS";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.mskDataEntrega);
            this.groupBox1.Controls.Add(this.label666);
            this.groupBox1.Controls.Add(this.mskDataVencimento);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.mskDataSaida);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.mskDataEmissao);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.mskData);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.groupBox3);
            this.groupBox1.Controls.Add(this.cbNatureza);
            this.groupBox1.Controls.Add(this.label11);
            this.groupBox1.Controls.Add(this.txtSerie);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.txtNF);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.cbFornecedor);
            this.groupBox1.Controls.Add(this.txtID);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Enabled = false;
            this.groupBox1.Location = new System.Drawing.Point(6, 6);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(895, 110);
            this.groupBox1.TabIndex = 20;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Capa";
            // 
            // mskDataEntrega
            // 
            this.mskDataEntrega.Location = new System.Drawing.Point(681, 77);
            this.mskDataEntrega.Mask = "00/00/0000";
            this.mskDataEntrega.Name = "mskDataEntrega";
            this.mskDataEntrega.Size = new System.Drawing.Size(91, 20);
            this.mskDataEntrega.TabIndex = 39;
            this.mskDataEntrega.ValidatingType = typeof(System.DateTime);
            // 
            // label666
            // 
            this.label666.AutoSize = true;
            this.label666.Location = new System.Drawing.Point(612, 80);
            this.label666.Name = "label666";
            this.label666.Size = new System.Drawing.Size(70, 13);
            this.label666.TabIndex = 38;
            this.label666.Text = "Data Entrega";
            // 
            // mskDataVencimento
            // 
            this.mskDataVencimento.Location = new System.Drawing.Point(504, 77);
            this.mskDataVencimento.Mask = "00/00/0000";
            this.mskDataVencimento.Name = "mskDataVencimento";
            this.mskDataVencimento.Size = new System.Drawing.Size(91, 20);
            this.mskDataVencimento.TabIndex = 37;
            this.mskDataVencimento.ValidatingType = typeof(System.DateTime);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(414, 80);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(89, 13);
            this.label8.TabIndex = 36;
            this.label8.Text = "Data Vencimento";
            // 
            // mskDataSaida
            // 
            this.mskDataSaida.Location = new System.Drawing.Point(304, 77);
            this.mskDataSaida.Mask = "00/00/0000";
            this.mskDataSaida.Name = "mskDataSaida";
            this.mskDataSaida.Size = new System.Drawing.Size(91, 20);
            this.mskDataSaida.TabIndex = 35;
            this.mskDataSaida.ValidatingType = typeof(System.DateTime);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(240, 80);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(62, 13);
            this.label7.TabIndex = 34;
            this.label7.Text = "Data Saída";
            // 
            // mskDataEmissao
            // 
            this.mskDataEmissao.Location = new System.Drawing.Point(119, 77);
            this.mskDataEmissao.Mask = "00/00/0000";
            this.mskDataEmissao.Name = "mskDataEmissao";
            this.mskDataEmissao.Size = new System.Drawing.Size(91, 20);
            this.mskDataEmissao.TabIndex = 33;
            this.mskDataEmissao.ValidatingType = typeof(System.DateTime);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(45, 80);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(72, 13);
            this.label3.TabIndex = 32;
            this.label3.Text = "Data Emissão";
            // 
            // mskData
            // 
            this.mskData.Location = new System.Drawing.Point(401, 48);
            this.mskData.Mask = "00/00/0000";
            this.mskData.Name = "mskData";
            this.mskData.ReadOnly = true;
            this.mskData.Size = new System.Drawing.Size(91, 20);
            this.mskData.TabIndex = 31;
            this.mskData.ValidatingType = typeof(System.DateTime);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(317, 51);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(77, 13);
            this.label5.TabIndex = 30;
            this.label5.Text = "Data Cadastral";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.optSaida);
            this.groupBox3.Controls.Add(this.optEntrada);
            this.groupBox3.Location = new System.Drawing.Point(695, 16);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(158, 32);
            this.groupBox3.TabIndex = 29;
            this.groupBox3.TabStop = false;
            // 
            // optSaida
            // 
            this.optSaida.AutoSize = true;
            this.optSaida.Location = new System.Drawing.Point(95, 10);
            this.optSaida.Name = "optSaida";
            this.optSaida.Size = new System.Drawing.Size(54, 17);
            this.optSaida.TabIndex = 1;
            this.optSaida.TabStop = true;
            this.optSaida.Text = "Saída";
            this.optSaida.UseVisualStyleBackColor = true;
            // 
            // optEntrada
            // 
            this.optEntrada.AutoSize = true;
            this.optEntrada.Location = new System.Drawing.Point(15, 10);
            this.optEntrada.Name = "optEntrada";
            this.optEntrada.Size = new System.Drawing.Size(62, 17);
            this.optEntrada.TabIndex = 0;
            this.optEntrada.TabStop = true;
            this.optEntrada.Text = "Entrada";
            this.optEntrada.UseVisualStyleBackColor = true;
            // 
            // cbNatureza
            // 
            this.cbNatureza.FormattingEnabled = true;
            this.cbNatureza.Items.AddRange(new object[] {
            "VENDA DE MERCADORIA",
            "CONSIGNAÇÃO",
            "DEVOLUÇÃO",
            "REMESSA PARA INDUSTRIALIZAÇÃO, DEMONSTRAÇÃO OU OUTRA",
            "TRANSFERÊNCIA",
            "COMPRA DE MERCADORIA"});
            this.cbNatureza.Location = new System.Drawing.Point(119, 45);
            this.cbNatureza.Name = "cbNatureza";
            this.cbNatureza.Size = new System.Drawing.Size(178, 21);
            this.cbNatureza.TabIndex = 27;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(4, 48);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(113, 13);
            this.label11.TabIndex = 28;
            this.label11.Text = "Natureza da operação";
            // 
            // txtSerie
            // 
            this.txtSerie.Location = new System.Drawing.Point(622, 21);
            this.txtSerie.MaxLength = 6;
            this.txtSerie.Name = "txtSerie";
            this.txtSerie.Size = new System.Drawing.Size(57, 20);
            this.txtSerie.TabIndex = 21;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(585, 24);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(31, 13);
            this.label2.TabIndex = 22;
            this.label2.Text = "Série";
            // 
            // txtNF
            // 
            this.txtNF.Location = new System.Drawing.Point(505, 21);
            this.txtNF.MaxLength = 6;
            this.txtNF.Name = "txtNF";
            this.txtNF.Size = new System.Drawing.Size(72, 20);
            this.txtNF.TabIndex = 19;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(472, 24);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(30, 13);
            this.label6.TabIndex = 20;
            this.label6.Text = "NF-e";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(105, 21);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(61, 13);
            this.label4.TabIndex = 18;
            this.label4.Text = "Fornecedor";
            // 
            // cbFornecedor
            // 
            this.cbFornecedor.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbFornecedor.FormattingEnabled = true;
            this.cbFornecedor.Location = new System.Drawing.Point(169, 18);
            this.cbFornecedor.Name = "cbFornecedor";
            this.cbFornecedor.Size = new System.Drawing.Size(286, 21);
            this.cbFornecedor.TabIndex = 17;
            // 
            // txtID
            // 
            this.txtID.Location = new System.Drawing.Point(31, 19);
            this.txtID.Name = "txtID";
            this.txtID.ReadOnly = true;
            this.txtID.Size = new System.Drawing.Size(57, 20);
            this.txtID.TabIndex = 13;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(10, 22);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(18, 13);
            this.label1.TabIndex = 12;
            this.label1.Text = "ID";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.btnAlterar);
            this.groupBox2.Controls.Add(this.btnGrava);
            this.groupBox2.Controls.Add(this.btnNovo);
            this.groupBox2.Controls.Add(this.btnSair);
            this.groupBox2.Location = new System.Drawing.Point(934, 25);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(96, 194);
            this.groupBox2.TabIndex = 19;
            this.groupBox2.TabStop = false;
            // 
            // btnAlterar
            // 
            this.btnAlterar.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnAlterar.Location = new System.Drawing.Point(6, 54);
            this.btnAlterar.Name = "btnAlterar";
            this.btnAlterar.Size = new System.Drawing.Size(85, 40);
            this.btnAlterar.TabIndex = 17;
            this.btnAlterar.Text = "Alterar";
            this.btnAlterar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnAlterar.UseVisualStyleBackColor = true;
            this.btnAlterar.Click += new System.EventHandler(this.btnAlterar_Click);
            // 
            // btnGrava
            // 
            this.btnGrava.Enabled = false;
            this.btnGrava.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnGrava.Location = new System.Drawing.Point(6, 100);
            this.btnGrava.Name = "btnGrava";
            this.btnGrava.Size = new System.Drawing.Size(85, 40);
            this.btnGrava.TabIndex = 16;
            this.btnGrava.Text = "Grava";
            this.btnGrava.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnGrava.UseVisualStyleBackColor = true;
            this.btnGrava.Click += new System.EventHandler(this.btnGrava_Click);
            // 
            // btnSair2
            // 
            this.btnSair2.Image = global::FacilShoppingReports.Properties.Resources.exit_icon;
            this.btnSair2.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnSair2.Location = new System.Drawing.Point(813, 5);
            this.btnSair2.Name = "btnSair2";
            this.btnSair2.Size = new System.Drawing.Size(87, 40);
            this.btnSair2.TabIndex = 19;
            this.btnSair2.Text = "Sair";
            this.btnSair2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnSair2.UseVisualStyleBackColor = true;
            this.btnSair2.Click += new System.EventHandler(this.btnSair2_Click);
            // 
            // btPesquisar
            // 
            this.btPesquisar.Image = global::FacilShoppingReports.Properties.Resources.search_icon1;
            this.btPesquisar.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
            this.btPesquisar.Location = new System.Drawing.Point(723, 5);
            this.btPesquisar.Name = "btPesquisar";
            this.btPesquisar.Size = new System.Drawing.Size(85, 40);
            this.btPesquisar.TabIndex = 16;
            this.btPesquisar.Text = "Pesquisar";
            this.btPesquisar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btPesquisar.UseVisualStyleBackColor = true;
            this.btPesquisar.Click += new System.EventHandler(this.btPesquisar_Click);
            // 
            // btnNovo
            // 
            this.btnNovo.Image = global::FacilShoppingReports.Properties.Resources.new_icon;
            this.btnNovo.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnNovo.Location = new System.Drawing.Point(6, 10);
            this.btnNovo.Name = "btnNovo";
            this.btnNovo.Size = new System.Drawing.Size(85, 40);
            this.btnNovo.TabIndex = 14;
            this.btnNovo.Text = "Novo";
            this.btnNovo.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnNovo.UseVisualStyleBackColor = true;
            this.btnNovo.Click += new System.EventHandler(this.btnNovo_Click);
            // 
            // btnSair
            // 
            this.btnSair.Image = global::FacilShoppingReports.Properties.Resources.exit_icon;
            this.btnSair.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnSair.Location = new System.Drawing.Point(6, 146);
            this.btnSair.Name = "btnSair";
            this.btnSair.Size = new System.Drawing.Size(87, 40);
            this.btnSair.TabIndex = 12;
            this.btnSair.Text = "Sair";
            this.btnSair.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnSair.UseVisualStyleBackColor = true;
            this.btnSair.Click += new System.EventHandler(this.btnSair_Click);
            // 
            // frmEntrada
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1047, 514);
            this.ControlBox = false;
            this.Controls.Add(this.tabPrincipal);
            this.Controls.Add(this.groupBox2);
            this.Name = "frmEntrada";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Notas Fiscais";
            this.Load += new System.EventHandler(this.frmEntrada_Load);
            this.tabPrincipal.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.groupBox6.ResumeLayout(false);
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabPrincipal;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.Button btnSair2;
        private System.Windows.Forms.ListView lstLista;
        private System.Windows.Forms.TextBox txtPesquisa;
        private System.Windows.Forms.Button btPesquisar;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button btnAlterar;
        private System.Windows.Forms.Button btnGrava;
        private System.Windows.Forms.Button btnNovo;
        private System.Windows.Forms.Button btnSair;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox txtID;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox cbFornecedor;
        private System.Windows.Forms.TextBox txtSerie;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtNF;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ComboBox cbNatureza;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.RadioButton optSaida;
        private System.Windows.Forms.RadioButton optEntrada;
        private System.Windows.Forms.Label label666;
        private System.Windows.Forms.MaskedTextBox mskDataVencimento;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.MaskedTextBox mskDataSaida;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.MaskedTextBox mskDataEmissao;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.MaskedTextBox mskData;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox txtBaseICMS;
        private System.Windows.Forms.TextBox txtOutrasDespesas;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.TextBox txtDesconto;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.TextBox txtValorSeguro;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox txtValorFrete;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.TextBox txtValorProdutos;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox txtValorICMSSubst;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox txtBaseICMSSubst;
        private System.Windows.Forms.TextBox txtValorICMS;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox txtValorNF;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.TextBox txtValorIPI;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.TextBox txtObservacao;
        private System.Windows.Forms.MaskedTextBox mskDataEntrega;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.ListView lstItens;
        private System.Windows.Forms.ListView listView1;
        private System.Windows.Forms.Button btnExcluir;
        private System.Windows.Forms.Button btnAdicionar;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.ColumnHeader columnHeader2;
        private System.Windows.Forms.ColumnHeader columnHeader3;
        private System.Windows.Forms.ColumnHeader columnHeader4;
        private System.Windows.Forms.ColumnHeader columnHeader5;
        private System.Windows.Forms.ColumnHeader columnHeader6;
        private System.Windows.Forms.ColumnHeader columnHeader7;
        private System.Windows.Forms.ColumnHeader columnHeader8;
        private System.Windows.Forms.ColumnHeader columnHeader9;
        private System.Windows.Forms.ColumnHeader columnHeader10;
        private System.Windows.Forms.ColumnHeader columnHeader11;
        private System.Windows.Forms.ColumnHeader columnHeader13;
        private System.Windows.Forms.ColumnHeader columnHeader14;
        private System.Windows.Forms.ColumnHeader columnHeader15;
        private System.Windows.Forms.ColumnHeader columnHeader16;
        private System.Windows.Forms.ColumnHeader columnHeader17;
    }
}