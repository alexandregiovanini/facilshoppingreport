
CREATE TABLE SEDEX_CIDADES (id INT NOT NULL PRIMARY KEY AUTO_INCREMENT, 
   							    CIDADE     CHAR(50) NULL,
                                CEPINICIAL CHAR(8) NULL,
                                CEPFINAL   CHAR(8) NULL);
                         

SELECT * FROM SEDEX_CIDADES;

INSERT INTO SEDEX_CIDADES(CIDADE, CEPINICIAL, CEPFINAL) VALUES ('EXTREMA'
,'37640000'
,'37649999');
INSERT INTO SEDEX_CIDADES(CIDADE, CEPINICIAL, CEPFINAL) VALUES ('AMERICANA'
,'13465001'
,'13479999');
INSERT INTO SEDEX_CIDADES(CIDADE, CEPINICIAL, CEPFINAL) VALUES ('ARARAS'
,'13600001'
,'13609999');
INSERT INTO SEDEX_CIDADES(CIDADE, CEPINICIAL, CEPFINAL) VALUES ('ARTUR NOGUEIRA'
,'13160000'
,'13164999');
INSERT INTO SEDEX_CIDADES(CIDADE, CEPINICIAL, CEPFINAL) VALUES ('ARUJA'
,'07400001'
,'07499999');
INSERT INTO SEDEX_CIDADES(CIDADE, CEPINICIAL, CEPFINAL) VALUES ('ATIBAIA'
,'12940001'
,'12954999');
INSERT INTO SEDEX_CIDADES(CIDADE, CEPINICIAL, CEPFINAL) VALUES ('BARUERI'
,'06400001'
,'06499999');
INSERT INTO SEDEX_CIDADES(CIDADE, CEPINICIAL, CEPFINAL) VALUES ('BRAGANCA PAULISTA'
,'12900001'
,'12929999');
INSERT INTO SEDEX_CIDADES(CIDADE, CEPINICIAL, CEPFINAL) VALUES ('CAIEIRAS'
,'07700001'
,'07749999');
INSERT INTO SEDEX_CIDADES(CIDADE, CEPINICIAL, CEPFINAL) VALUES ('CAJAMAR'
,'07750001'
,'07799999');
INSERT INTO SEDEX_CIDADES(CIDADE, CEPINICIAL, CEPFINAL) VALUES ('CAMPINAS'
,'13000001'
,'13139999');
INSERT INTO SEDEX_CIDADES(CIDADE, CEPINICIAL, CEPFINAL) VALUES ('CARAPICUIBA'
,'06300001'
,'06399999');
INSERT INTO SEDEX_CIDADES(CIDADE, CEPINICIAL, CEPFINAL) VALUES ('COSMOPOLIS'
,'13150000'
,'13159999');
INSERT INTO SEDEX_CIDADES(CIDADE, CEPINICIAL, CEPFINAL) VALUES ('COTIA'
,'06700001'
,'06729999');
INSERT INTO SEDEX_CIDADES(CIDADE, CEPINICIAL, CEPFINAL) VALUES ('DIADEMA'
,'09900001'
,'09999999');
INSERT INTO SEDEX_CIDADES(CIDADE, CEPINICIAL, CEPFINAL) VALUES ('EMBU DAS ARTES'
,'06800001'
,'06849999');
INSERT INTO SEDEX_CIDADES(CIDADE, CEPINICIAL, CEPFINAL) VALUES ('EMBU GUACU'
,'06900000'
,'06949999');
INSERT INTO SEDEX_CIDADES(CIDADE, CEPINICIAL, CEPFINAL) VALUES ('ENGENHEIRO COELHO'
,'13165000'
,'13169999');
INSERT INTO SEDEX_CIDADES(CIDADE, CEPINICIAL, CEPFINAL) VALUES ('FERRAZ DE VASCONCELOS'
,'08500001'
,'08549999');
INSERT INTO SEDEX_CIDADES(CIDADE, CEPINICIAL, CEPFINAL) VALUES ('FRANCISCO MORATO'
,'07900001'
,'07999999');
INSERT INTO SEDEX_CIDADES(CIDADE, CEPINICIAL, CEPFINAL) VALUES ('FRANCO DA ROCHA'
,'07800001'
,'07899999');
INSERT INTO SEDEX_CIDADES(CIDADE, CEPINICIAL, CEPFINAL) VALUES ('GUARUJA'
,'11400001'
,'11499999');
INSERT INTO SEDEX_CIDADES(CIDADE, CEPINICIAL, CEPFINAL) VALUES ('GUARULHOS'
,'07000001'
,'07399999');
INSERT INTO SEDEX_CIDADES(CIDADE, CEPINICIAL, CEPFINAL) VALUES ('HOLAMBRA'
,'13825000'
,'13829999');
INSERT INTO SEDEX_CIDADES(CIDADE, CEPINICIAL, CEPFINAL) VALUES ('HORTOLANDIA'
,'13183001'
,'13189999');
INSERT INTO SEDEX_CIDADES(CIDADE, CEPINICIAL, CEPFINAL) VALUES ('INDAIATUBA'
,'13330001'
,'13349999');
INSERT INTO SEDEX_CIDADES(CIDADE, CEPINICIAL, CEPFINAL) VALUES ('ITAPECERICA DA SERRA'
,'06850001'
,'06889999');
INSERT INTO SEDEX_CIDADES(CIDADE, CEPINICIAL, CEPFINAL) VALUES ('ITAPEVI'
,'06650001'
,'06699999');
INSERT INTO SEDEX_CIDADES(CIDADE, CEPINICIAL, CEPFINAL) VALUES ('ITAQUAQUECETUBA'
,'08570001'
,'08599999');
INSERT INTO SEDEX_CIDADES(CIDADE, CEPINICIAL, CEPFINAL) VALUES ('ITATIBA'
,'13250001'
,'13259999');
INSERT INTO SEDEX_CIDADES(CIDADE, CEPINICIAL, CEPFINAL) VALUES ('ITU'
,'13300001'
,'13314999');
INSERT INTO SEDEX_CIDADES(CIDADE, CEPINICIAL, CEPFINAL) VALUES ('JACAREI'
,'12300001'
,'12349999');
INSERT INTO SEDEX_CIDADES(CIDADE, CEPINICIAL, CEPFINAL) VALUES ('JAGUARIUNA'
,'13820000'
,'13824999');
INSERT INTO SEDEX_CIDADES(CIDADE, CEPINICIAL, CEPFINAL) VALUES ('JANDIRA'
,'06600001'
,'06649999');
INSERT INTO SEDEX_CIDADES(CIDADE, CEPINICIAL, CEPFINAL) VALUES ('JUNDIAI'
,'13200001'
,'13219999');
INSERT INTO SEDEX_CIDADES(CIDADE, CEPINICIAL, CEPFINAL) VALUES ('LIMEIRA'
,'13480001'
,'13489999');
INSERT INTO SEDEX_CIDADES(CIDADE, CEPINICIAL, CEPFINAL) VALUES ('MAIRIPORA'
,'07600000'
,'07699999');
INSERT INTO SEDEX_CIDADES(CIDADE, CEPINICIAL, CEPFINAL) VALUES ('MAUA'
,'09300001'
,'09399999');
INSERT INTO SEDEX_CIDADES(CIDADE, CEPINICIAL, CEPFINAL) VALUES ('MOGI DAS CRUZES'
,'08700001'
,'08899999');
INSERT INTO SEDEX_CIDADES(CIDADE, CEPINICIAL, CEPFINAL) VALUES ('MOGI GUACU'
,'13840001'
,'13856999');
INSERT INTO SEDEX_CIDADES(CIDADE, CEPINICIAL, CEPFINAL) VALUES ('MOGI MIRIM'
,'13800001'
,'13819999');
INSERT INTO SEDEX_CIDADES(CIDADE, CEPINICIAL, CEPFINAL) VALUES ('MONTE MOR'
,'13190000'
,'13199999');
INSERT INTO SEDEX_CIDADES(CIDADE, CEPINICIAL, CEPFINAL) VALUES ('NOVA ODESSA'
,'13380001'
,'13389999');
INSERT INTO SEDEX_CIDADES(CIDADE, CEPINICIAL, CEPFINAL) VALUES ('OSASCO'
,'06000001'
,'06299999');
INSERT INTO SEDEX_CIDADES(CIDADE, CEPINICIAL, CEPFINAL) VALUES ('PAULINIA'
,'13140001'
,'13149999');
INSERT INTO SEDEX_CIDADES(CIDADE, CEPINICIAL, CEPFINAL) VALUES ('PEDREIRA'
,'13920000'
,'13929999');
INSERT INTO SEDEX_CIDADES(CIDADE, CEPINICIAL, CEPFINAL) VALUES ('PIRACICABA'
,'13400001'
,'13439999');
INSERT INTO SEDEX_CIDADES(CIDADE, CEPINICIAL, CEPFINAL) VALUES ('PIRAPORA DO BOM JESUS'
,'06550000'
,'06599999');
INSERT INTO SEDEX_CIDADES(CIDADE, CEPINICIAL, CEPFINAL) VALUES ('POA'
,'08550001'
,'08569999');
INSERT INTO SEDEX_CIDADES(CIDADE, CEPINICIAL, CEPFINAL) VALUES ('RIBEIRAO PIRES'
,'09400001'
,'09449999');
INSERT INTO SEDEX_CIDADES(CIDADE, CEPINICIAL, CEPFINAL) VALUES ('RIO CLARO'
,'13500001'
,'13509999');
INSERT INTO SEDEX_CIDADES(CIDADE, CEPINICIAL, CEPFINAL) VALUES ('RIO GRANDE DA SERRA'
,'09450000'
,'09499999');
INSERT INTO SEDEX_CIDADES(CIDADE, CEPINICIAL, CEPFINAL) VALUES ('SALTO'
,'13320001'
,'13329999');
INSERT INTO SEDEX_CIDADES(CIDADE, CEPINICIAL, CEPFINAL) VALUES ('SANTA BARBARA D OESTE'
,'13450001'
,'13459999');
INSERT INTO SEDEX_CIDADES(CIDADE, CEPINICIAL, CEPFINAL) VALUES ('SANTA ISABEL'
,'07500000'
,'07599999');
INSERT INTO SEDEX_CIDADES(CIDADE, CEPINICIAL, CEPFINAL) VALUES ('SANTANA DE PARNAIBA'
,'06500001'
,'06549999');
INSERT INTO SEDEX_CIDADES(CIDADE, CEPINICIAL, CEPFINAL) VALUES ('SANTO ANDRE'
,'09000001'
,'09299999');
INSERT INTO SEDEX_CIDADES(CIDADE, CEPINICIAL, CEPFINAL) VALUES ('SANTO ANTONIO DE POSSE'
,'13830000'
,'13834999');
INSERT INTO SEDEX_CIDADES(CIDADE, CEPINICIAL, CEPFINAL) VALUES ('SANTOS'
,'11000001'
,'11249999');
INSERT INTO SEDEX_CIDADES(CIDADE, CEPINICIAL, CEPFINAL) VALUES ('SAO BERNARDO DO CAMPO'
,'09600001'
,'09899999');
INSERT INTO SEDEX_CIDADES(CIDADE, CEPINICIAL, CEPFINAL) VALUES ('SAO CAETANO DO SUL'
,'09500001'
,'09599999');
INSERT INTO SEDEX_CIDADES(CIDADE, CEPINICIAL, CEPFINAL) VALUES ('SAO JOSE DOS CAMPOS'
,'12200001'
,'12249999');
INSERT INTO SEDEX_CIDADES(CIDADE, CEPINICIAL, CEPFINAL) VALUES ('SAO LOURENCO DA SERRA'
,'06890000'
,'06899999');
INSERT INTO SEDEX_CIDADES(CIDADE, CEPINICIAL, CEPFINAL) VALUES ('SAO PAULO'
,'08000000'
,'08499999');
INSERT INTO SEDEX_CIDADES(CIDADE, CEPINICIAL, CEPFINAL) VALUES ('SAO PAULO'
,'01000001'
,'05999999');
INSERT INTO SEDEX_CIDADES(CIDADE, CEPINICIAL, CEPFINAL) VALUES ('SOROCABA'
,'18000001'
,'18109999');
INSERT INTO SEDEX_CIDADES(CIDADE, CEPINICIAL, CEPFINAL) VALUES ('SUMARE'
,'13170001'
,'13182999');
INSERT INTO SEDEX_CIDADES(CIDADE, CEPINICIAL, CEPFINAL) VALUES ('SUZANO'
,'08600001'
,'08699999');
INSERT INTO SEDEX_CIDADES(CIDADE, CEPINICIAL, CEPFINAL) VALUES ('TABOAO DA SERRA'
,'06750001'
,'06799999');
INSERT INTO SEDEX_CIDADES(CIDADE, CEPINICIAL, CEPFINAL) VALUES ('VALINHOS'
,'13270001'
,'13279999');
INSERT INTO SEDEX_CIDADES(CIDADE, CEPINICIAL, CEPFINAL) VALUES ('VARGEM GRANDE PAULISTA'
,'06730000'
,'06749999');
INSERT INTO SEDEX_CIDADES(CIDADE, CEPINICIAL, CEPFINAL) VALUES ('VINHEDO'
,'13280000'
,'13289999');
INSERT INTO SEDEX_CIDADES(CIDADE, CEPINICIAL, CEPFINAL) VALUES ('VOTORANTIM'
,'18110001'
,'18119999');
